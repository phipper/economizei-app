import { Component, Input } from '@angular/core';
 
@Component({
  selector: 'progress-bar-dual',
  templateUrl: 'progress-bar-dual.html'
})
export class ProgressBarDualComponent {
  
  @Input('good-progress') goodProgress;
  @Input('bad-progress') badProgress;
 
  constructor() {
  }
 
}