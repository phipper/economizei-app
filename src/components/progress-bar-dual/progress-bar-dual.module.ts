import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgressBarDualComponent } from './progress-bar-dual';

@NgModule({
    declarations: [
        ProgressBarDualComponent,
    ],
    imports: [
        IonicPageModule.forChild(ProgressBarDualComponent),
    ],
    exports: [
        ProgressBarDualComponent
    ]
})
export class ProgressBarDualComponentModule {
}
    