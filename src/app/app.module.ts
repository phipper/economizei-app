import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

//Angular
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, Injectable, Injector } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LottieAnimationViewModule } from 'ng-lottie';


//Plugins
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links';
import { FCM } from '@ionic-native/fcm';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Firebase } from '@ionic-native/firebase';
import { Vibration } from '@ionic-native/vibration';
import { File } from '@ionic-native/file';
import { Crop } from '@ionic-native/crop';
import { Calendar } from '@ionic-native/calendar';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';
import { Toast } from '@ionic-native/toast';
import { CameraPreview } from '@ionic-native/camera-preview';
import { NativeStorage } from '@ionic-native/native-storage';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook } from '@ionic-native/facebook';

//Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';

//Local Providers
import { CoreProvider } from '../providers/CoreProvider';
import { EcoFirestore } from '../providers/EcoFirestore/EcoFirestore';
import { TimeProvider } from '../providers/Time';
import { UtilProvider } from '../providers/Util';
import { UserDataProvider } from '../providers/UserData';
import { EmojiProvider } from '../providers/Emoji';
import { FcmProvider } from '../providers/Fcm';
import { HapticProvider } from '../providers/Haptic';
import { UiFeedBackProvider } from '../providers/UifeedBack/UifeedBack';
import { DynamicLinksApiProvider } from '../providers/DynamicLinksApi';
import { UserPlanProvider } from '../providers/UserPlan';
import { UserReputationProvider } from '../providers/UserReputation';
import { SacProvider } from '../providers/SAC';
import { PicProvider } from '../providers/Pic';
import { LocalizationProvider } from '../providers/Localization/Localization';
import { LocalDbProvider } from '../providers/LocalDbProvider';
import { PagseguroPgtoServiceProvider } from '../providers/PagSeguro/PagSeguro';

import { MyApp } from './app.component';
import { DatePipe } from '@angular/common';


const firebaseAuth = {
  apiKey: "AIzaSyDDNN_49-8oTn9pgeBtucMQ_w3kVnXUTHo",
  authDomain: "economizeiapp.firebaseapp.com",
  databaseURL: "https://economizeiapp.firebaseio.com",
  projectId: "economizeiapp",
  storageBucket: "economizeiapp.appspot.com",
  messagingSenderId: "668476234421"
};

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch (e) {
    }
  }

  handleError(err: any): void {
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    LottieAnimationViewModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebaseAuth, 'Economizei'),
    AngularFirestoreModule,
    AngularFireStorageModule,
    IonicModule.forRoot(MyApp, {
      spinner: 'dots',
      platforms: {
        ios: {
          backButtonText: 'Voltar'
        }
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [

    //Plugins
    FirebaseDynamicLinks,
    FCM,
    StatusBar,
    SplashScreen,
    Vibration,
    Camera,
    File,
    Crop,
    Calendar,
    Diagnostic,
    LocationAccuracy,
    Geolocation,
    NativeGeocoder,
    Firebase,
    LaunchNavigator,
    SocialSharing,
    Clipboard,
    Toast,
    CameraPreview,
    NativeStorage,
    GooglePlus,
    Facebook,
    DatePipe,
    
    //local Providers
    CoreProvider,
    EcoFirestore,
    TimeProvider,
    UtilProvider,
    UserDataProvider,
    EmojiProvider,
    FcmProvider,
    HapticProvider,
    UiFeedBackProvider,
    DynamicLinksApiProvider,
    UserPlanProvider,
    UserReputationProvider,
    SacProvider,
    PicProvider,
    LocalizationProvider,
    LocalDbProvider,
    PagseguroPgtoServiceProvider,

    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }