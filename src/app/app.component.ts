import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, App, Nav, ActionSheetButton, Events } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireAuth } from 'angularfire2/auth';
import { Firebase } from '@ionic-native/firebase';

import { FcmProvider } from '../providers/Fcm';
import { UiFeedBackProvider } from '../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../providers/UserData';
import { CoreProvider } from '../providers/CoreProvider';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPageName = 'HomePage';

  authSplash = true;
  openByDynamicLink = false;
  dynamicLinkData: any = null;
  openByNotification = false;
  notificationData: any = null;

  private menuServ = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/auth.json'
  }

  constructor(
    private fire: AngularFireAuth,
    private firebaseNative: Firebase,
    public fcm: FcmProvider,
    private platform: Platform,
    public splashScreen: SplashScreen,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public menuCtrl: MenuController,
    public appCrtl: App,
    public UserDataCtrl: UserDataProvider,
    private coreCtrl: CoreProvider,
    private events: Events
  ) {

    platform.ready()
      .then(() => {

        this.splashScreen.hide();

        this.menuCtrl.enable(true, 'guestMenu');

        this.initNavProvider();
        this.coreCtrl.init();

        //Page directioning and User data initializing
        this.fire.authState.subscribe(dataAuth => {
          this.authSplash = true;
          if (dataAuth != null) {
            //has a registrated user logged
            this.coreCtrl.db.users.getDoc(dataAuth.uid)
              .then(userDoc => {
                if (userDoc.exists) {
                  //has a registrated user logged
                  this.UserDataCtrl.init()
                    .then(() => {
                      if (this.platform.is('cordova')) {
                        fcm.updateToken(dataAuth.uid, this.UserDataCtrl.localUser.token);
                      }
                      this.preparteAppToUser();

                      if (!this.UserDataCtrl.localUser.emailVerified) {
                        if (this.fire.auth.currentUser.emailVerified) {
                          this.UiFeedBackCtrl.presenLoader('Verificando email...')
                            .then(() => {
                              userDoc.ref.update({ emailVerified: true }).then(() => {
                                this.UiFeedBackCtrl.dismissLoader();
                                this.UiFeedBackCtrl.presentPersistentToast('Recebemos a verificação do seu email!', 'success');
                              }).catch(e => {
                                this.UiFeedBackCtrl.dismissLoader();
                                this.UiFeedBackCtrl.presentErrorAlert('Erro ao tualizar seus dados', e);
                              });
                            });
                        } else {
                          this.UiFeedBackCtrl.presentAlert('Aviso!', 'Ainda não recebemos a confirmação de seu email ! Porfavor verifique seu email !', 'warning', true);
                        }
                      }

                    })
                    .catch(e => {
                      this.UiFeedBackCtrl.presentErrorAlert('Erro ao ler seu usuário', e);
                    });
                } else {
                  //has a logged user but the user does not have a register
                  if (this.fire.auth.currentUser.displayName != null)
                    this.UiFeedBackCtrl.presentAlert('Olá de novo ' + this.fire.auth.currentUser.displayName, 'Por favor Complete seu cadastro!', 'warning');

                  this.rootPageName = 'RegisterContinuePage';
                  this.nav.setRoot('RegisterContinuePage');
                  this.appCrtl.getRootNav().popToRoot();

                }
              })
              .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('Erro ao ler seu usuário', e);
              })
          } else {
            //Do not has a user logged on
            this.authSplash = false;
            if (this.menuCtrl.isEnabled('menu')) {
              if (this.menuCtrl.isOpen('menu')) {
                this.menuCtrl.close(this.menuCtrl.getOpen().id).then(() => {
                  this.menuCtrl.enable(true, 'guestMenu');
                });
              } else {
                this.menuCtrl.enable(true, 'guestMenu');
              }
            }
          }
        },
          error => {
            this.UiFeedBackCtrl.presentErrorAlert('Error at authState subscriber', error);
          });
      });
  }

  private initNavProvider() {
    console.log('initiating NavProvider listeners');

    this.events.subscribe('NavProvider:setRoot', data => {
      this.rootPageName = data.page;
      this.nav.setRoot(data.page);
      this.appCrtl.getRootNav().popToRoot();
    });

    this.events.subscribe('NavProvider:push', data => {
      this.nav.push(data.page, data.params);
    });

    this.events.subscribe('NavProvider:pop', () => {
      this.nav.pop();
    });

    this.events.subscribe('NavProvider:getActiveName', () => {
      if (this.nav.getActive() != undefined) {
        this.events.publish('NavProvider:getActiveName:response', {
          pageName: this.nav.getActive().name
        });
      } else {
        this.events.publish('NavProvider:getActiveName:response', {
          pageName: ''
        });
      }
    });

    this.events.subscribe('NavProvider:getRootName', () => {
      this.events.publish('NavProvider:getRootName:response', {
        rootName: this.rootPageName
      });
    });

  }

  menuServCore() {
    if (this.menuServ) {
      if (this.menuCtrl.isOpen('guestMenu')) {
        this.menuCtrl.close('guestMenu').then(() => {
          this.menuCtrl.enable(true, 'menu');
          this.authSplash = false;
        });
      } else {
        if (this.menuCtrl.isEnabled('guestMenu')) {
          this.menuCtrl.enable(true, 'menu');
          this.authSplash = false;
        }
      }
      setTimeout(() => {
        this.menuServCore();
      }, 1000)
    }
  }

  activateUserMenu() {
    this.menuServ = true;
    this.menuServCore();
    setTimeout(() => {
      this.menuServ = false;
    }, 5000);
  }

  preparteAppToUser() {
    this.activateUserMenu();
    if (this.platform.is('cordova')) {
      this.firebaseNative.setAnalyticsCollectionEnabled(true);
      this.firebaseNative.setUserId(this.UserDataCtrl.localUser.id);
      this.firebaseNative.setUserProperty('gender', this.UserDataCtrl.localUser.gender);
      this.firebaseNative.setUserProperty('fullName', this.UserDataCtrl.localUser.fullName);
      this.firebaseNative.setUserProperty('registerDate', this.UserDataCtrl.localUser.registerDate.toLocaleString('pt-BR'));
      this.firebaseNative.setUserProperty('type', this.UserDataCtrl.localUser.type.toString());
    }
  }

  setRootPage(page: string) {
    this.menuCtrl.close(this.menuCtrl.getOpen().id)
      .then(() => {
        this.rootPageName = page;
        this.nav.setRoot(page);
        this.appCrtl.getRootNav().popToRoot();
      });
  }

  pushPage(page: string) {
    this.menuCtrl.close(this.menuCtrl.getOpen().id)
      .then(() => {
        this.nav.push(page);
      });
  }

  goToLogin() {
    this.menuCtrl.close(this.menuCtrl.getOpen().id)
      .then(() => {
        this.nav.push('LoginPage');
      });
  }

  makeProvider() {
    this.menuCtrl.close(this.menuCtrl.getOpen().id)
      .then(() => {
        this.nav.push('EditUserProfilePage')
          .then(() => {
            this.UiFeedBackCtrl.presenLottietAlert(`Vire um prestador do economizei!`, `É facil virar um prestador de serviço do economizei basta selecionar o tipo de usuário "Prestador de Serviço" neste formulario e inserir os dados de profissao`, 'assets/animations/done.json', `success`, true);
          })
      });
  }

  shareApp() {
    let buttonsArray: ActionSheetButton[] = [];
    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.coreCtrl.socialSharing.share('Venha para o economizei!', null, null, 'https://economizei.page.link/economizeiapp');
      }
    });
    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.coreCtrl.clipboard.copy('https://economizei.page.link/economizeiapp')
          .then(() => {
            this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', '', 'success');
          });
      }
    });
    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl
      .create({
        title: 'Compartilhar o economizei',
        buttons: buttonsArray,
      });
    this.menuCtrl.close(this.menuCtrl.getOpen().id).then(() => {
      this.UiFeedBackCtrl.hapticCtrl.selection();
      actionSheet.present();
    });
  }

}

