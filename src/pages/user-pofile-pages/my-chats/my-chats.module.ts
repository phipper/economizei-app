import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyChatsPage } from './my-chats';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyChatsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyChatsPage),
    LottieAnimationViewModule
  ],
})
export class MyChatsPageModule {}
