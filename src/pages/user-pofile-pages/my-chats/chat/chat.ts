import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Content, TextInput, ActionSheetButton } from 'ionic-angular';

import { UserDataProvider } from '../../../../providers/UserData'
import { AngularFirestore } from 'angularfire2/firestore';

import { CoreProvider } from '../../../../providers/CoreProvider';

import { Camera } from '@ionic-native/camera';

import { User } from '../../../../models/User';
import { Message } from '../../../../models/Message';
import { Order } from '../../../../models/Order';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { Chat } from '../../../../models/Chat';
import { ChatUserData } from '../../../../models/ChatUserData';
import { SAC } from '../../../../models/SAC';
import { TimeProvider } from '../../../../providers/Time';

@IonicPage()
@Component({
	selector: 'page-chat',
	templateUrl: 'chat.html'
})
export class ChatPage {
	@ViewChild(Content) content: Content;
	@ViewChild('chat_input') messageInput: TextInput;

	hasHiddedSB = false;

	showSplash = true;
	hasMsg = false;
	showEmojiPicker = false;
	msgToSend: string = '';
	chatIndex: string;
	localMessages: Message[] = [];

	readedMsgIds: string[] = [];
	messagestToRead: Message[] = [];
	msgReaderService = false;
	msgReaderDelayInMs = 1000;
	msgReaderReq = 0;

	localChat = new Chat();
	localOrder = new Order();
	localSac = new SAC();
	actualUser = new User();
	otherUser = new User();
	messagesSub: any = null;
	initchatLimitNumber = 30; //parametro
	chatLimitNumber = this.initchatLimitNumber;
	chatMsgNum = 0;
	chatMsgNumSub: any = null;
	timeDif: number = 0;

	lottieSplash = {
		loop: true,
		autoplay: true,
		path: 'assets/animations/speechBubble.json'
	}

	constructor(
		public platform: Platform,
		public UserDataCtrl: UserDataProvider,
		public navCtrl: NavController,
		public navParams: NavParams,
		private camera: Camera,
		public afs: AngularFirestore,
		private UiFeedBackCtrl: UiFeedBackProvider,
		private timeCtrl: TimeProvider,
		public coreCtrl: CoreProvider
	) {
		this.localChat.id = navParams.get('chatId');
	}


	private getOrtherUserData(): Promise<ChatUserData> {
		return new Promise((resolve, reject) => {
			if (this.localChat.usersData.length > 2) {
				reject('chat of the type group');
			} else {
				this.UserDataCtrl.getUid()
					.then(actualUserUid => {
						for (var i = 0; i < this.localChat.usersData.length; ++i) {
							if (this.localChat.usersData[i].userId != actualUserUid) {
								resolve(this.localChat.usersData[i]);
							}
						}
					})
					.catch(e => {
						reject(e);
					})
			}
		});
	}


	initForOrderChat() {
		this.afs.collection('Orders').doc(this.localChat.orderId).ref.get()
			.then(orderDoc => {
				this.localOrder = orderDoc.data() as Order;
				this.loadMessages();
			});
	}

	loadMore() {
		this.chatLimitNumber = this.chatLimitNumber + 10;
		this.afs.collection('Chats').doc(this.localChat.id).collection('Messages').ref.orderBy('time', 'desc').limit(this.chatLimitNumber).get()
			.then(querySnapshot => {
				let tempMessageVet: Message[] = [];
				for (var i = (querySnapshot.docs.length - 1); i >= 0; --i) {
					tempMessageVet.push(querySnapshot.docs[i].data() as Message);
				}
				this.localMessages = tempMessageVet;
			})
			.catch(error => {
				console.log('Erro :( ' + error.message);
				this.UiFeedBackCtrl.presentAlert('Erro :(', error.message, 'error');
			});
	}

	msgReader() {
		if (this.messagestToRead.length > 0 && this.msgReaderReq == 0) {
			for (let tempMessage of this.messagestToRead) {
				if (this.readedMsgIds.indexOf(tempMessage.id, 0) == -1 || this.msgReaderReq == 0) {
					this.msgReaderReq++;
					this.readedMsgIds.push(tempMessage.id);
					this.afs.collection('Chats').doc(this.localChat.id).collection('Messages').doc<Message>(tempMessage.id).update({ readedOn: new Date() })
						.then(() => {
							this.msgReaderReq--;
						});
				}
			}
			this.messagestToRead = [];
		}
		setTimeout(() => {
			if (this.msgReaderService) {
				this.msgReader();
			}
		}, this.msgReaderDelayInMs);
	}

	activeMsgReaderService() {
		this.msgReaderService = true;
		this.msgReader();
	}

	desactiveMsgReaderService() {
		this.msgReaderService = false;
	}


	loadMessages() {

		this.chatMsgNumSub = this.afs.collection('Chats').doc(this.localChat.id).collection('Messages').ref
			.onSnapshot(querySnapshot1 => {
				this.chatMsgNum = querySnapshot1.size;
			},
				error => {
					console.log('Erro :( ' + error.message);
					this.UiFeedBackCtrl.presentAlert('Erro :(', error.message, 'error');
				});

		this.messagesSub = this.afs.collection('Chats').doc(this.localChat.id).collection('Messages').ref.orderBy('time', 'desc').limit(this.chatLimitNumber)
			.onSnapshot(querySnapshot => {

				let tempMessageVet: Message[] = [];
				for (var i = (querySnapshot.docs.length - 1); i >= 0; --i) {
					let tempMessage = querySnapshot.docs[i].data() as Message;
					if (tempMessage.recipientId == this.actualUser.id && tempMessage.readedOn == null) {
						this.messagestToRead.push(tempMessage);
					}
					tempMessageVet.push(tempMessage);
				}
				this.localMessages = tempMessageVet;

				if (!querySnapshot.empty) {
					this.hasMsg = true;
				}
				this.showSplash = false;
				this.scrollToBottom();

			},
				error => {
					this.UiFeedBackCtrl.presentErrorAlert('Erro :(', error);
				});

		this.activeMsgReaderService();

	}

	initChatPage() {
		this.afs.collection('Chats').doc(this.localChat.id).ref.get()
			.then(chatDoc => {
				this.localChat = chatDoc.data() as Chat;
				this.UserDataCtrl.getUser()
					.then(localUser => {
						this.actualUser = localUser;
						this.getOrtherUserData()
							.then(ortherUserData => {
								this.afs.collection('Users').doc(ortherUserData.userId).ref.get()
									.then(otherUserDoc => {
										this.otherUser = otherUserDoc.data() as User;
										this.timeCtrl.getRefTimeDif()
											.then(time => {
												this.timeDif = time;
												this.initForOrderChat();
											});
									});
							});
					});
			})
			.catch(e => {
				this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar conversa', e);
			})
	}

	ionViewDidEnter() {
		if (this.platform.is('cordova')) {
			this.coreCtrl.localDbCtrl.getAppConfig()
				.then(appConfig => {
					if (appConfig.statusBarMode == "") {
						this.coreCtrl.showStatusBar();
						this.hasHiddedSB = true;
					}
					this.loadChatPage();
				})
				.catch(e => {
					this.showSplash = false;
					this.UiFeedBackCtrl.presentErrorAlert('Erro ao sair', e);
				});
		} else {
			this.loadChatPage();
		}
	}


	loadChatPage() {

		this.UserDataCtrl.getUser()
			.then(localUser => {
				this.actualUser = localUser;
				if (this.actualUser.chats.indexOf(this.localChat.id, 0) != -1) {
					this.initChatPage();
				} else {
					this.UiFeedBackCtrl.presenLoader('Verificando autorização', 'assets/animations/speechBubble.json')
						.then(() => {
							this.afs.collection('Chats').doc(this.localChat.id).ref.get()
								.then(chatDoc => {
									let tempChat = chatDoc.data() as Chat;
									this.afs.collection('Orders').doc(tempChat.orderId).ref.get()
										.then(orderDoc => {
											let tempOrder = orderDoc.data() as Order;
											this.UiFeedBackCtrl.updateLoaderMessage('Autorização verificada!, atualizando seu usuário');
											if (tempOrder.providerId == this.actualUser.id || tempOrder.clientId == this.actualUser.id) {

												this.UserDataCtrl.localUser.chats.push(this.localChat.id);
												this.UserDataCtrl.update({ chats: this.UserDataCtrl.localUser.chats })
													.then(() => {
														this.UiFeedBackCtrl.dismissLoader();
														this.initChatPage();
													})
													.catch(e => {
														this.UiFeedBackCtrl.presentErrorAlert('Erro ao atualizar suas connversas', e);
													});

											} else {
												this.UiFeedBackCtrl.dismissLoader();
												this.navCtrl.pop();
												this.UiFeedBackCtrl.presentAlert('Computer says no!', 'Somente os usuários deste pedido tem acesso ao chat!', 'warning', true);
											}


										})
								})
								.catch(e => {
									this.UiFeedBackCtrl.presentErrorAlert('Erro ao ler Pedido', e);
								});
						});
				}
			})
			.catch(e => {
				this.UiFeedBackCtrl.presentErrorAlert('Erro ao ler seus dados', e);
			})

	}

	/**
   * busca e retorna as informações do usuário 
   * @param {userId} string SAC a ser salvo
   * @param {type} string Tipo do SAC `name` ou `thumbnailUrl`
   * @return `string`
   */
	getUserData(userId: string, type: string) {
		for (var i = 0; i < this.localChat.usersData.length; ++i) {
			if (this.localChat.usersData[i].userId != userId) {
				switch (type) {
					case 'thumbnailUrl':
						return this.localChat.usersData[i].userThumbnailUrl;

					case 'name':
						return this.localChat.usersData[i].userName;

					default:
						return `the paramiter type '${type}' is invalid`;

				}
			}
		}
		return null;
	}


	ionViewDidLeave() {
		if (this.hasHiddedSB) {
			this.coreCtrl.hideStatusBar();
		}

		if (this.messagesSub != null) {
			this.messagesSub();
		}
		if (this.chatMsgNumSub != null) {
			this.chatMsgNumSub();
		}
		this.desactiveMsgReaderService();
	}

	goToOrder() {
		if (this.localChat.orderId != '') {
			if (this.localOrder.clientId == this.actualUser.id) {
				this.navCtrl.push('MyOrderDetailPage', {
					orderId: this.localOrder.id,
				});
			} else {
				this.navCtrl.push('ReceivedOrderDetailPage', {
					orderId: this.localOrder.id,
				});
			}
		}
	}

	needThumbNaill(actualMessage: Message, index: number): boolean {
		var prevMsgIndex = 0;
		if (index > 0) {
			prevMsgIndex = index - 1;
		} else if (index == 0) {
			return true;
		}
		return (actualMessage.senderId != this.localMessages[prevMsgIndex].senderId)
	}


	sendMessage() {
		if (!this.showSplash) {
			if (this.msgToSend.length > 1) {

				this.chatLimitNumber = this.initchatLimitNumber;

				let actualTime = new Date(new Date().getTime() + this.timeDif);
				let actualDateTimeString = actualTime.toLocaleString('en-GB');
				var message: Message = new Message();

				message.id = this.afs.createId();
				message.chatId = this.localChat.id;
				message.senderId = this.actualUser.id;
				message.senderFullName = this.actualUser.fullName;
				message.recipientId = this.otherUser.id;
				message.message = this.msgToSend;
				message.dateString = actualDateTimeString.split(',')[0];
				message.timeString = actualDateTimeString.substr(12, 5);
				message.time = actualTime;
				message.type = 'text';

				this.messageSender(message);

			} else {
				this.msgToSend = '';
			}
		}
	}

	switchEmojiPicker() {
		this.showEmojiPicker = !this.showEmojiPicker;
		this.content.resize();
		this.scrollToBottom();
		if (!this.showEmojiPicker) {
			this.messageInput.setFocus();
		}
	}

	switchOffEmojiPicker() {
		if (this.showEmojiPicker) {
			this.showEmojiPicker = false;
		}
	}

	sendPicture(img) {

		this.chatLimitNumber = this.initchatLimitNumber;

		let actualTime = new Date(new Date().getTime() + this.timeDif);
		let actualDateTimeString = actualTime.toLocaleString('en-GB');
		var message: Message = new Message();

		message.id = this.afs.createId();
		message.chatId = this.localChat.id;
		message.senderId = this.actualUser.id;
		message.senderFullName = this.actualUser.fullName;
		message.recipientId = this.otherUser.id;
		message.message = `data:image/jpeg;base64,${img}`;
		message.dateString = actualDateTimeString.split(',')[0];
		message.timeString = actualDateTimeString.substr(12, 5);
		message.time = actualTime;
		message.type = 'image';

		this.messageSender(message);
	}

	takePicture() {
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageData) => {
			// imageData is a base64 encoded string
			this.sendPicture(imageData);

		}, (err) => {
			console.log(err);
		}).catch();
	}

	scrollToBottom() {
		setTimeout(() => {
			if (this.content._scroll)
				this.content.scrollToBottom(0);
		}, 100);
	}

	activateChat() {
		this.UiFeedBackCtrl.presentToastTop('ativando Chat', 'warning');
		this.localChat.status = 'active';
		this.afs.collection('Chats').doc<Chat>(this.localChat.id).update({ status: this.localChat.status })
			.then(() => {
				this.UiFeedBackCtrl.presentToastTop('Chat ativado com sucesso!', 'success');
			})
			.catch(e => {
				console.log(`erro: ${JSON.stringify(e)}`);
				this.UiFeedBackCtrl.presentAlert('erro', JSON.stringify(e), 'error');
			})
	}

	messageSender(message: Message) {
		this.localMessages.push(message);
		var tempMsg = Object.assign({}, message);
		tempMsg.onFirebase = true;
		this.afs.collection('Chats').doc(this.localChat.id).collection('Messages').doc(tempMsg.id).set(Object.assign({}, tempMsg));
		this.msgToSend = '';
		this.showEmojiPicker = false;
		this.content.resize();
		this.messageInput.setFocus();
		this.scrollToBottom();
		if (this.localChat.status == 'archived') {
			this.activateChat();
		}
	}

	msgTapped(message: Message) {
		this.navCtrl.push('MessageDetailPage', {
			message: message,
			chat: this.localChat,
			actualUser: this.actualUser
		});
	}

	messageDeleter(message: Message) {
		if (message.originalMessage == '') {
			message.originalMessage = message.message;
		}

		if (message.type == 'text') {
			message.message = '🗑 Esta mensagem foi apagada';
		} else if (message.type == 'image') {
			message.message = '🗑 Esta imagem foi apagada';
			message.type = 'text';
		}

		this.afs.collection('Chats').doc(this.localChat.id).collection('Messages').doc<Message>(message.id).update({ originalMessage: message.originalMessage, message: message.message, type: message.type });

	}


	msgPressed(message: Message) {
		if (message.senderId == this.actualUser.id) {
			let buttonsArray: ActionSheetButton[] = [];

			buttonsArray.push({
				text: 'Excluir',
				icon: !this.platform.is('ios') ? 'trash' : null,
				handler: () => {
					this.messageDeleter(message);
				}
			});

			buttonsArray.push({
				text: 'Cancelar',
				icon: null,
				role: 'cancel',
				handler: null,
			});

			let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
				title: 'Opções de mensagem',
				buttons: buttonsArray,
			});
			this.UiFeedBackCtrl.hapticCtrl.selection();
			actionSheet.present();
		}
	}

}