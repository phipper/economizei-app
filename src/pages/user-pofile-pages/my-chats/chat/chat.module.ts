import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatPage } from './chat';
import {EmojiPickerComponentModule} from "../../../../components/emoji-picker/emoji-picker.module";
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    ChatPage,
  ],
  imports: [
    EmojiPickerComponentModule,
    IonicPageModule.forChild(ChatPage),
    LottieAnimationViewModule
  ],
})
export class ChatPageModule {}
