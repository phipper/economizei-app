import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

import { Message } from '../../../../../models/Message';
import { User } from '../../../../../models/User';
import { Chat } from '../../../../../models/Chat';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';


@IonicPage()
@Component({
  selector: 'page-message-detail',
  templateUrl: 'message-detail.html',
})
export class MessageDetailPage {


  message: Message = new Message();
  localChat: Chat = new Chat();
  actualUser: User = new User();
  msgSub: any;

  constructor(
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.message = navParams.get("message");
    this.localChat = navParams.get("chat");
    this.actualUser = navParams.get("actualUser");
  }

  ionViewDidEnter() {
    if (this.message.onFirebase) {
      this.msgSub = this.afs.collection('Chats').doc(this.message.chatId).collection('Messages').doc(this.message.id).valueChanges()
        .subscribe(msgDoc => {
          this.message = msgDoc as Message;
        });
    } else {
      this.UiFeedBackCtrl.presentToastCenter('Mensagem ainda não enviada, tente novamente mais tarde!', 'warning');
    }
  }

  ionViewDidLeave() {
    if (this.msgSub != null) {
      this.msgSub.unsubscribe();
    }
  }

  /**
 * busca e retorna as informações do usuário 
 * @param {userId} string SAC a ser salvo
 * @param {type} string Tipo do SAC `name` ou `thumbnailUrl`
 * @return `string`
 */
  getUserData(userId: string, type: string) {
    for (var i = 0; i < this.localChat.usersData.length; ++i) {
      if (this.localChat.usersData[i].userId == userId) {
        switch (type) {
          case 'thumbnailUrl':
            return this.localChat.usersData[i].userThumbnailUrl;

          case 'name':
            return this.localChat.usersData[i].userName;

          default:
            return `the paramiter type '${type}' is invalid`;

        }
      }
    }
    return null;
  }

}
