import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetButton, Content, ItemSliding } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';

import { UserDataProvider } from '../../../providers/UserData';

import { Chat } from '../../../models/Chat';
import { ChatUserData } from '../../../models/ChatUserData';
import { Message } from '../../../models/Message';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
  selector: 'page-my-chats',
  templateUrl: 'my-chats.html',
})
export class MyChatsPage {
  @ViewChild(Content) content: Content;

  segment: string = 'Ativos';
  showSplash = true;
  localUserIsProvider = false;
  localChats: Chat[] = [];
  localLastMessages = new Map<string, Message>();
  myOrders = 0;
  receivedOrders = 0;
  limitOfChats = 10;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/speechBubble.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
  }

  getLastMsgs() {
    this.localChats.forEach((localChat, i) => {
      this.db.afs.collection('Chats').doc(localChat.id).collection('Messages').ref.orderBy('time', 'desc').limit(1).get()
        .then(querySnapshot => {
          if (!querySnapshot.empty) {
            this.localLastMessages.set(localChat.id, querySnapshot.docs[0].data() as Message);
          }
          if (this.localChats.length == i + 1) {
            this.showSplash = false;
          }
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar ultimas msgs', e);
          this.showSplash = false;
        });
    })

    if (this.localChats.length == 0) {
      this.showSplash = false;
    }
  }

  canLoadChat(chatStatus): boolean {
    switch (this.segment) {
      case 'Ativos':
        return chatStatus == 'active';
      case 'Arquivados':
        return chatStatus == 'archived';
    }
  }

  ionViewDidEnter() {
    this.showSplash = true;
    this.localChats = [];
    this.limitOfChats = 10;
    if (this.UserDataCtrl.localUser.chats.length > 0) {
      var reqCount = 0;
      var respCount = 0;
      for (let chatId of this.UserDataCtrl.localUser.chats) {
        if (this.localChats.length <= this.limitOfChats) {
          reqCount++;
          this.db.afs.collection('Chats').doc(chatId).ref.get()
            .then(doc => {
              respCount++;

              if (doc.exists) {

                let tempChat = doc.data() as Chat;
                if (this.canLoadChat(tempChat.status)) {
                  this.localChats.push(tempChat);
                }

              } else {
                console.log(`Apontamento para chat excluido encontrado no usuário. ID do chat: ${doc.id}`)
              }

              if (respCount == reqCount) {
                this.localChats = this.db.sortByCreatedOn(this.localChats, 'desc');
                this.getLastMsgs();
              }

            });
        } else {
          break;
        }
      }
    } else {
      this.showSplash = false;
    }
  }

  doRefreshLastMsgs(refresher) {
    for (var i = 0; i < this.localChats.length; ++i) {
      var count = 0;
      this.db.afs.collection('Chats').doc(this.localChats[i].id).collection('Messages').ref.orderBy('time', 'desc').limit(1).get()
        .then(querySnapshot => {
          if (!querySnapshot.empty) {
            this.localLastMessages.set(this.localChats[count].id, querySnapshot.docs[0].data() as Message);
          }
          count++;
          if (this.localChats.length == count) {
            refresher.complete();
            this.showSplash = false;
          }
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro ao atualizar ultimas msgs', e);
          refresher.complete();
          this.showSplash = false;
        });
    }
    if (this.localChats.length == 0) {
      refresher.complete();
      this.showSplash = false;
    }
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    this.showSplash = true;
    this.localChats = [];
    this.limitOfChats = 10;
    if (this.UserDataCtrl.localUser.chats.length > 0) {
      var reqCount = 0;
      var respCount = 0;
      for (let chatId of this.UserDataCtrl.localUser.chats) {
        if (this.localChats.length <= this.limitOfChats) {
          reqCount++;
          this.db.afs.collection('Chats').doc(chatId).ref.get()
            .then(doc => {
              respCount++;

              if (doc.exists) {

                let tempChat = doc.data() as Chat;
                if (this.canLoadChat(tempChat.status)) {
                  this.localChats.push(tempChat);
                }

              } else {
                console.log(`Apontamento para chat excluido encontrado no usuário. ID do chat: ${doc.id}`)
              }

              if (respCount == reqCount) {
                this.localChats = this.db.sortByCreatedOn(this.localChats, 'desc');
                this.doRefreshLastMsgs(refresher);
              }


            });
        } else {
          break;
        }
      }
    } else {
      refresher.complete();
      this.showSplash = false;
    }
  }

  doInfiniteLastMsgs(infiniteScroll) {
    for (var i = 0; i < this.localChats.length; ++i) {
      var count = 0;
      this.db.afs.collection('Chats').doc(this.localChats[i].id).collection('Messages').ref.orderBy('time', 'desc').limit(1).get()
        .then(querySnapshot => {
          if (!querySnapshot.empty) {
            this.localLastMessages.set(this.localChats[count].id, querySnapshot.docs[0].data() as Message);
          }
          count++;
          if (this.localChats.length == count) {
            infiniteScroll.complete();
          }
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar mais ultimas msgs', e);
          infiniteScroll.complete();
        });
    }
  }

  doInfinite(infiniteScroll) {
    this.limitOfChats = this.limitOfChats + 10;
    var OriginalNumberOfChats = this.localChats.length;

    if (this.UserDataCtrl.localUser.chats.length > OriginalNumberOfChats) {
      var reqCount = 0;
      var respCount = 0;
      for (var i = OriginalNumberOfChats; i < this.UserDataCtrl.localUser.chats.length; ++i) {
        if (this.localChats.length <= this.limitOfChats) {
          reqCount++;
          this.db.afs.collection('Chats').doc(this.UserDataCtrl.localUser.chats[i]).ref.get()
            .then(doc => {
              respCount++;
              let tempChat = doc.data() as Chat;
              if (this.canLoadChat(tempChat.status)) {
                this.localChats.push(tempChat);
              }
              if (respCount == reqCount) {
                this.localChats = this.db.sortByCreatedOn(this.localChats, 'desc');
                this.doInfiniteLastMsgs(infiniteScroll);
              }
            });
        } else {
          infiniteScroll.complete();
          break;
        }
      }
    } else {
      infiniteScroll.complete();
    }
  }

  getReadedStatus(chatId: string) {
    if (this.localLastMessages.has(chatId)) {
      return this.localLastMessages.get(chatId).readedOn != null
        || this.localLastMessages.get(chatId).senderId == this.UserDataCtrl.localUser.id;
    } else {
      return false;
    }
  }

  getOrtherUserData(chat: Chat): ChatUserData {
    if (chat.usersData.length > 2) {
      return null;
    } else {
      for (var i = 0; i < chat.usersData.length; ++i) {
        if (chat.usersData[i].userId != this.UserDataCtrl.localUser.id) {
          return chat.usersData[i];
        }
      }
    }
  }

  getDateString(data: Date): string {
    return data.toLocaleString('en-GB').substr(0, 10).split(',')[0];
  }

  getTimeString(data: Date): string {
    return data.toLocaleString('en-GB').substr(12, 5);
  }

  itemTapped(chat: Chat) {
    this.navCtrl.push('ChatPage', {
      chatId: chat.id
    });
  }

  itemPressed(chat: Chat, slidingItem: ItemSliding) {
    let buttonsArray: ActionSheetButton[] = [];
    if (chat.status == 'active') {
      buttonsArray.push({
        text: 'Arquivar',
        icon: !this.platform.is('ios') ? 'archive' : null,
        handler: () => {
          this.archiveChat(chat, slidingItem);
        }
      });
    }
    if (chat.status == 'archived') {
      buttonsArray.push({
        text: 'Ativar',
        icon: !this.platform.is('ios') ? 'bulb' : null,
        handler: () => {
          this.activateChat(chat, slidingItem);
        }
      });
    }
    buttonsArray.push({
      text: 'Excluir',
      icon: !this.platform.is('ios') ? 'trash' : null,
      handler: () => {
        this.deleteChatConfirmation(chat, slidingItem);
      }
    });
    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: chat.title,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

  archiveChat(chat: Chat, slidingItem: ItemSliding) {
    this.UiFeedBackCtrl.presenLoader('Alterando..', 'assets/animations/speechBubble.json')
      .then(() => {
        slidingItem.close();
        this.db.afs.collection('Chats').doc<Chat>(chat.id).update({ status: 'archived' })
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast(`${chat.title} Arquivado`, 'success');
            this.ionViewDidEnter();
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentAlert('erro :(', e.menssage, 'error');
          });
      });
  }

  activateChat(chat: Chat, slidingItem: ItemSliding) {
    this.UiFeedBackCtrl.presenLoader('Alterando..', 'assets/animations/speechBubble.json')
      .then(() => {
        slidingItem.close();
        this.db.afs.collection('Chats').doc<Chat>(chat.id).update({ status: 'active' })
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast(`${chat.title} Ativado`, 'success');
            this.ionViewDidEnter();
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentAlert('erro :(', e.menssage, 'error');
          });
      });
  }

  deleteChatConfirmation(chat: Chat, slidingItem: ItemSliding) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Tem certeza?',
      subTitle: `Você quer realmente deletar a conversa de ${chat.title}?`,
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.deleteChat(chat);
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    slidingItem.close();
    alert.present();
  }

  deleteChat(chat: Chat) {
    this.UiFeedBackCtrl.presenLoader('Excluindo..', 'assets/animations/speechBubble.json')
      .then(() => {
        this.db.afs.collection('Chats').doc(chat.id).delete()
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast(`${chat.title} Excluido com sucesso!`, 'success');
            this.ionViewDidEnter();
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentAlert('erro :(', e.menssage, 'error');
          });
      });
  }

}
