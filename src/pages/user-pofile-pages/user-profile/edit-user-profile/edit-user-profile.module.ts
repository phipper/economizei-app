import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditUserProfilePage } from './edit-user-profile';
import { LottieAnimationViewModule } from 'ng-lottie';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    EditUserProfilePage,
  ],
  imports: [
    LottieAnimationViewModule,
    BrMaskerModule,
    IonicPageModule.forChild(EditUserProfilePage),
  ],
})
export class EditUserProfilePageModule {}
