import { Component } from '@angular/core';
import { Platform, IonicPage, NavController } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { PicProvider } from '../../../../providers/Pic';
import { UserDataProvider } from '../../../../providers/UserData';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilProvider } from '../../../../providers/Util';


import { User } from '../../../../models/User';
import { AngularFireAuth } from 'angularfire2/auth';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';
import { Banner } from '../../../../models/Banner';

@IonicPage()
@Component({
  selector: 'page-edit-user-profile',
  templateUrl: 'edit-user-profile.html',
})
export class EditUserProfilePage {

  tempUser: User = new User();
  public localBirthDate = '';
  standarProfilePicUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582';

  public userForm: FormGroup = null;

  oldCpf = '';

  bannersToUpdate = 0;
  updatedBanners = 0;

  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/auth.json'
  }

  constructor(
    private fire: AngularFireAuth,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    private navCtrl: NavController,
    public platform: Platform,
    public UiFeedBackCtrl: UiFeedBackProvider,
    private PicCtrl: PicProvider,
    private formBuilder: FormBuilder,
    public utilCtrl: UtilProvider
  ) {
    this.userForm = this.formBuilder.group({
      firstName: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.text), Validators.required, Validators.minLength(3), Validators.maxLength(50)])
      ],
      lastName: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.text), Validators.required, Validators.minLength(3), Validators.maxLength(50)])
      ],
      cpf: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.cpf), Validators.required, Validators.minLength(11), Validators.maxLength(14)])
      ],
      gender: [
        '',
        Validators.compose([Validators.required])
      ],
      birthDate: [
        '',
        Validators.compose([Validators.required])
      ],
      type: [
        '',
      ],
      job: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.text), Validators.required, Validators.minLength(3), Validators.maxLength(50)])
      ],
      workPhone: [
        '',
        Validators.compose([Validators.minLength(14), Validators.maxLength(16)])
      ],
      cellPhone: [
        '',
        Validators.compose([Validators.required, Validators.minLength(14), Validators.maxLength(16)])
      ]
    });
  }

  ionViewDidLoad() {
    this.UserDataCtrl.getUser()
      .then(user => {
        this.tempUser = Object.assign({}, user);

        this.localBirthDate = this.tempUser.birthDate.toISOString().substring(0, 10).toString();

        this.oldCpf = this.tempUser.cpf;

        this.showSplash = false;
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }


  editUserProfPic() {
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Alterar Foto do perfil',
      subTitle: 'Selecione de onde deseja enviar a foto',
      buttons: [
        {
          text: 'Tirar foto',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.updateViaNewPhoto();
          }
        }, {
          text: 'Selecionar foto da galeria',
          icon: !this.platform.is('ios') ? 'images' : null,
          handler: () => {
            this.updateViaSelectedPhotoFromGallery();
          }
        }, {
          text: 'Cancelar',
          role: 'cancel'
        }
      ],
      enableBackdropDismiss: true
    });
    actionSheet.present();
  }

  updateBannersThumbnails(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.bannersToUpdate = 0;
      this.updatedBanners = 0;
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.tempUser.id)
        .get()
        .then(bannerQueryes => {

          if (bannerQueryes.empty) {
            console.log('Não possui anuncios para atualizar a thumbnail');
            resolve();
          } else {

            for (var i = 0; i < bannerQueryes.docs.length; i++) {
              let tempBanner = bannerQueryes.docs[i].data() as Banner;

              if (tempBanner.status != 13 && tempBanner.status != 14 && tempBanner.status != 101 && tempBanner.status != 102 && tempBanner.status != 103) {
                this.bannersToUpdate = this.bannersToUpdate + 1;

                setTimeout(() => {

                  this.db.banners.update(tempBanner.id, { providerThumbnailUrl: this.tempUser.pic.thumbnailUrl })
                    .then(() => {
                      this.updatedBanners = this.updatedBanners + 1;

                      if (this.updatedBanners == this.bannersToUpdate) {
                        resolve();
                      }
                    })
                    .catch(e => {
                      reject({ message: 'Erro ao atualizar Banner favor carregar foto novamente.', errro: e })
                    })

                }, 500);

              }
            }

            if (this.bannersToUpdate == 0) {
              console.log('Não possui anuncios para atualizar a thumbnail');
              resolve();
            }

          }

        })
        .catch(e => {
          reject({ message: 'Erro ao carregar Banners favor carregar foto novamente.', errro: e })
        })
    });
  }

  updateViaNewPhoto() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('camera')
      .then(newPic => {
        let tempPic = newPic;

        tempPic.info = 'updateViaNewPhoto';

        this.UiFeedBackCtrl.updateLoaderMessage('Fazendo upload...');

        this.PicCtrl.uploadUserImg(tempPic.url, this.UserDataCtrl.localUser.id)
          .then(url => {
            tempPic.url = url;

            this.UiFeedBackCtrl.updateLoaderMessage('Atualizando dados do Usuário...');

            this.tempUser.pic = Object.assign({}, tempPic);
            this.UserDataCtrl.update({ pic: this.tempUser.pic })
              .then(() => {
                this.fire.auth.currentUser.updateProfile({ displayName: this.fire.auth.currentUser.displayName, photoURL: this.tempUser.pic.url })
                  .then(() => {

                    if (this.tempUser.type = 10) {
                      this.UiFeedBackCtrl.updateLoaderMessage('Atualizando dados dos seus anuncios...');
                      this.updateBannersThumbnails()
                        .then(() => {
                          this.UiFeedBackCtrl.dismissLoader();
                          this.UiFeedBackCtrl.presentToast('Foto atualizada com sucesso!', 'success');
                        })
                        .catch(e => {
                          this.UiFeedBackCtrl.dismissLoader();
                          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                        });
                    } else {
                      this.UiFeedBackCtrl.dismissLoader();
                      this.UiFeedBackCtrl.presentToast('Foto atualizada com sucesso!', 'success');
                    }


                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não tirou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  updateViaSelectedPhotoFromGallery() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('galery')
      .then(newPic => {
        let tempPic = newPic;

        tempPic.info = 'updateViaPhotoFromGallery';

        this.UiFeedBackCtrl.updateLoaderMessage('Fazendo upload...');

        this.PicCtrl.uploadUserImg(tempPic.url, this.UserDataCtrl.localUser.id)
          .then(url => {
            tempPic.url = url;

            this.UiFeedBackCtrl.updateLoaderMessage('Atualizando dados do Usuário...');

            this.tempUser.pic = Object.assign({}, tempPic);
            this.UserDataCtrl.update({ pic: this.tempUser.pic })
              .then(() => {
                this.fire.auth.currentUser.updateProfile({ displayName: this.fire.auth.currentUser.displayName, photoURL: this.tempUser.pic.url })
                  .then(() => {

                    if (this.tempUser.type = 10) {
                      this.UiFeedBackCtrl.updateLoaderMessage('Atualizando dados dos seus anuncios...');
                      this.updateBannersThumbnails()
                        .then(() => {
                          this.UiFeedBackCtrl.dismissLoader();
                          this.UiFeedBackCtrl.presentToast('Foto atualizada com sucesso!', 'success');
                        })
                        .catch(e => {
                          this.UiFeedBackCtrl.dismissLoader();
                          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                        });
                    } else {
                      this.UiFeedBackCtrl.dismissLoader();
                      this.UiFeedBackCtrl.presentToast('Foto atualizada com sucesso!', 'success');
                    }

                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não selecionou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  canSave(): boolean {

    if (!this.userForm.controls.firstName.valid
      || !this.userForm.controls.lastName.valid
      || !this.userForm.controls.cpf.valid
      || !this.userForm.controls.gender.valid
      || !this.userForm.controls.birthDate.valid
      || !this.userForm.controls.cellPhone.valid)
      return false;

    if (this.tempUser.type == 10)
      if (!this.userForm.controls.job.valid || !this.userForm.controls.workPhone.valid)
        return false;

    return true;
  }

  tryUpdate() {
    this.UiFeedBackCtrl.presenLoader('Checkando dados...')
      .then(() => {
        console.log(`Inserted CPF: ${this.userForm.controls.cpf.value}`);

        let newCpf = this.userForm.controls.cpf.value;

        if (newCpf != this.oldCpf) {
          this.UiFeedBackCtrl.presenLoader('Checkando disponibilidade de CPF...');
          this.db.afs.collection('Users').ref
            .where('cpf', '==', newCpf)
            .get()
            .then(cpfQuerry => {
              this.db.validators.validateQuerySnapshot(cpfQuerry)
                .then(cpfQuerry => {
                  if (cpfQuerry.empty) {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.updateProfile();
                  } else {
                    this.UiFeedBackCtrl.dismissLoader();
                    let tempUser = cpfQuerry.docs[0].data() as User;
                    if (tempUser.loginMethod == 'password') {
                      const alert = this.UiFeedBackCtrl.alertCtrl.create({
                        title: 'CPF já utilizado!',
                        subTitle: `O CPF inserido já esta utilizado pelo usuário de nome ${tempUser.firstName}, e email: ${tempUser.email}! Deseja reperar a senha agora?`,
                        buttons: [
                          {
                            text: 'Sim',
                            handler: () => {
                              this.UiFeedBackCtrl.presenLoader('Checkando dados...')
                                .then(() => {
                                  this.resetPassword(tempUser.email);
                                })
                            }
                          },
                          'Cancelar'
                        ]
                      });
                      this.UiFeedBackCtrl.hapticCtrl.notification('error');
                      alert.present();
                    } else {
                      this.UiFeedBackCtrl.dismissLoader();
                      this.UiFeedBackCtrl.presentAlert(`CPF já utilizado!`, `O CPF inserido já esta utilizado pelo usuário de nome ${tempUser.firstName}, e email: ${tempUser.email}! Favor entrar via ${tempUser.loginMethod}!`, `warning`, true);
                    }
                  }
                })
            })
            .catch(e => {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            })
        } else {
          this.UiFeedBackCtrl.dismissLoader();
          this.updateProfile();
        }

      })
  }

  resetPassword(email: string) {
    this.UiFeedBackCtrl.presenLoader(`Enviando email para Reset de senha...`)
      .then(() => {
        this.fire.auth.sendPasswordResetEmail(email)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert("Reset Efetuado!", "Link de reset de senha enviado para: " + email, 'success', true);
          })
          .catch(e => {
            if (e.code === 'auth/invalid-email') {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentAlert("Erro ", "Email invalido !", 'warning', true);
            } else if (e.code === 'auth/user-not-found') {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentAlert("Erro ", "Não existe nehuma conta com este email !", 'warning', true);
            }
            else {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentErrorAlert("Erro ", e);
            }
          })
      })
  }

  updateProfile() {

    this.UiFeedBackCtrl.presenLoader('Salvando dados...')
      .then(() => {

        this.tempUser.birthDate = new Date(`${this.localBirthDate} 00:00:00 GMT-0300 (Hora oficial do Brasil)`);

        this.tempUser.fullName = this.tempUser.firstName + ' ' + this.tempUser.lastName;
        this.tempUser.id = this.fire.auth.currentUser.uid;

        this.fire.auth.currentUser.updateProfile({ displayName: this.tempUser.fullName, photoURL: this.fire.auth.currentUser.photoURL })
          .then(() => {
            this.UserDataCtrl.update(Object.assign({}, this.tempUser))
              .then(() => {
                this.navCtrl.pop();
                this.UiFeedBackCtrl.dismissLoader();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao salvar dados', e);
          })

      })
  }
}
