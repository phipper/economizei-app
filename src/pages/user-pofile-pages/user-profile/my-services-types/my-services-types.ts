import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Content, ItemSliding, ModalController, ActionSheetButton } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';

import { AngularFirestore } from 'angularfire2/firestore';
import { UserDataProvider } from '../../../../providers/UserData';
import { TimeProvider } from '../../../../providers/Time';
import { ServiceTypeSub } from '../../../../models/ServiceTypeSub';

@IonicPage()
@Component({
  selector: 'page-my-services-types',
  templateUrl: 'my-services-types.html',
})
export class MyServicesTypesPage {
  @ViewChild(Content) content: Content;

  localServicesTypeSubs: ServiceTypeSub[] = [];
  localUserServiceTypesIds: string[] = [];
  showSplash = true;
  limitOfServiceTypesParam = 10;
  limitOfServiceTypes = this.limitOfServiceTypesParam;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private timeCtrl: TimeProvider,
    public modalCtrl: ModalController
  ) {
  }

  ionViewDidEnter() {
    this.localServicesTypeSubs = [];
    this.showSplash = true;
    this.afs.collection('ServiceTypeSubs').ref
      .where('providerId', '==', this.UserDataCtrl.localUser.id)
      .orderBy('createdOn', 'desc').limit(this.limitOfServiceTypes).get()
      .then(querySnapshot => {
        for (let doc of querySnapshot.docs) {
          this.localServicesTypeSubs.push(doc.data() as ServiceTypeSub);
        }
        this.showSplash = false;
      })
      .catch(e => {
        this.showSplash = false;
        this.UiFeedBackCtrl.presentErrorAlert('Erro :(', e);
      });
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    this.limitOfServiceTypes = this.limitOfServiceTypesParam;
    this.localServicesTypeSubs = [];
    this.showSplash = true;
    this.afs.collection('ServiceTypeSubs').ref
      .where('providerId', '==', this.UserDataCtrl.localUser.id)
      .orderBy('createdOn', 'desc').limit(this.limitOfServiceTypes).get()
      .then(querySnapshot => {
        for (let doc of querySnapshot.docs) {
          this.localServicesTypeSubs.push(doc.data() as ServiceTypeSub);
        }
        refresher.complete();
        this.showSplash = false;
      })
      .catch(e => {
        refresher.complete();
        this.showSplash = false;
        this.UiFeedBackCtrl.presentErrorAlert('Erro :(', e);
      });
  }

  doInfinite(infiniteScroll) {
    this.limitOfServiceTypes = this.limitOfServiceTypes + this.limitOfServiceTypesParam;
    let initLocalServicesTypesLength = this.localUserServiceTypesIds.length;
    this.afs.collection('ServiceTypeSubs').ref
      .where('providerId', '==', this.UserDataCtrl.localUser.id).limit(this.limitOfServiceTypes).get()
      .then(querySnapshot => {
        for (var i = initLocalServicesTypesLength; i < querySnapshot.docs.length; i++) {
          this.localServicesTypeSubs.push(querySnapshot.docs[i].data() as ServiceTypeSub);
        }
        infiniteScroll.complete();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro :(', e);
      });
  }

  edit(serviceTypeSub: ServiceTypeSub, slidingItem: ItemSliding) {
    let modal = this.modalCtrl.create('EditServiceSubPage', {
      serviceTypeSub: Object.assign({}, serviceTypeSub)
    });
    if (slidingItem != null) {
      slidingItem.close();
    }
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  tryToDelete(serviceTypeSub: ServiceTypeSub) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Processando...',
    });
    loader.present()
      .then(() => {
        this.timeCtrl.getDate()
          .then(ecoDate => {
            const last24hTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1);
            if (serviceTypeSub.createdOn.getTime() < last24hTime) {
              this.afs.collection('ServiceTypeSubs').doc(serviceTypeSub.id).delete()
                .then(() => {
                  loader.dismiss();
                  this.UiFeedBackCtrl.presentToast(`${serviceTypeSub.serviceTypeTitle} excluida`, 'success');
                  this.ionViewDidEnter();
                }).catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert("erro ao excluir Inscrição", e);
                });
            } else {
              loader.dismiss();
              this.UiFeedBackCtrl.presentAlert('Computer says NO!', `Você só pode se desinscrever de um serviço 24h após a inscrição`, 'erro');
            }
          });
      });

  }

  delete(serviceTypeSub: ServiceTypeSub, slidingItem: ItemSliding) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: "Tem certeza?",
      subTitle: `Você quer desiscrever de ${serviceTypeSub.serviceTypeTitle}`,
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.tryToDelete(serviceTypeSub);
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    if (slidingItem != null) {
      slidingItem.close();
    }
    alert.present();
  }

  addServiceSubPage() {
    this.navCtrl.push('AddServiceSubPage');
  }

  itemPressed(serviceTypeSub: ServiceTypeSub) {

    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Editar',
      icon: !this.platform.is('ios') ? 'create' : null,
      handler: () => {
        this.edit(serviceTypeSub, null);
      }
    });

    buttonsArray.push({
      text: 'Finalizar',
      icon: !this.platform.is('ios') ? 'trash' : null,
      handler: () => {
        this.delete(serviceTypeSub, null);
      }
    });


    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções de ' + serviceTypeSub.serviceTypeTitle,
      buttons: buttonsArray,
    });
    actionSheet.present();
  }

}
