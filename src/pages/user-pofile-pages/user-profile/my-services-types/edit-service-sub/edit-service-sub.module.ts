import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditServiceSubPage } from './edit-service-sub';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    EditServiceSubPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(EditServiceSubPage),
  ],
})
export class EditServiceSubPageModule {}
