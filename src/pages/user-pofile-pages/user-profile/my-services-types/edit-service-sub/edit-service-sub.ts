import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';

import { ServiceTypeSub } from '../../../../../models/ServiceTypeSub';


@IonicPage()
@Component({
  selector: 'page-edit-service-sub',
  templateUrl: 'edit-service-sub.html',
})

export class EditServiceSubPage {

  hasChanges = false;

  localServiceTypeSub: ServiceTypeSub = new ServiceTypeSub();

  originalNotif: boolean = null;
  originalNotifRad: number = null;

  constructor(
    public viewCtrl: ViewController,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localServiceTypeSub = navParams.get('serviceTypeSub');
    this.originalNotif = this.localServiceTypeSub.notif;
    this.originalNotifRad = this.localServiceTypeSub.notifRad;
  }

  ionViewDidEnter() {
  }

  toggleNotif() {
    this.hasChanges = true;
    this.localServiceTypeSub.notif = !this.localServiceTypeSub.notif;
  }

  changeNotifRange() {
    this.hasChanges = true;
  }

  checkForChanges(): boolean {
    return this.originalNotif != this.localServiceTypeSub.notif
      || this.originalNotifRad != this.localServiceTypeSub.notifRad;
  }

  close() {
    this.viewCtrl.dismiss();
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  save() {
    if (this.checkForChanges()) {
      let loader = this.UiFeedBackCtrl.loadingCtrl.create({
        content: 'salvando...',
      });

      loader.present()
        .then(() => {
          this.afs.collection('ServiceTypeSubs').doc<ServiceTypeSub>(this.localServiceTypeSub.id).update({ notif: this.localServiceTypeSub.notif, notifRad: this.localServiceTypeSub.notifRad })
            .then(() => {
              loader.dismiss();
              this.navCtrl.pop();
              this.UiFeedBackCtrl.presentAlert('Inscrição Atualizada com sucesso!', '', 'success');
            });
        });
    } else {
      this.UiFeedBackCtrl.presentToastCenter('Não existem alterações para serem aplicadas', 'error');
    }
  }

}
