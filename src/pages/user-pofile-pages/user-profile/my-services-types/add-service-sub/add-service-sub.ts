import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, Content, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../../../providers/UserData';

import { AngularFirestore } from 'angularfire2/firestore';

import { Category } from '../../../../../models/Category';
import { ServiceType } from '../../../../../models/ServiceType';
import { ServiceTypeSub } from '../../../../../models/ServiceTypeSub';
import { Address } from '../../../../../models/Address';
import { UserPlanProvider } from '../../../../../providers/UserPlan';
import { TimeProvider } from '../../../../../providers/Time';

@IonicPage()
@Component({
  selector: 'page-add-service-sub',
  templateUrl: 'add-service-sub.html',
})
export class AddServiceSubPage {
  @ViewChild(Content) content: Content;

  localCategories: Category[] = [];
  selectedCategory: Category = null;
  localServiceTypes: ServiceType[] = [];
  serviceTypesToSelection: ServiceType[] = [];
  selectedServiceType: ServiceType = null;

  canSelectServiceType = false;

  showSplash = true;

  localServiceTypeSub: ServiceTypeSub = new ServiceTypeSub();

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    private UserPlanCtrl: UserPlanProvider,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private timeCtrl: TimeProvider
  ) {

  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'CreateRequestPge') {
      this.content.scrollToTop();
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  loadCategories() {
    this.afs.collection('Categories').ref.orderBy('index', 'asc').get()
      .then(CategoriesQuery => {
        for (let CategoriesDoc of CategoriesQuery.docs) {
          this.localCategories.push(CategoriesDoc.data() as Category);
        }
        this.loadServicesTypes();
      });
  }

  loadServicesTypes() {
    this.afs.collection('ServiceTypes').ref.get()
      .then(ServicesTypesQuery => {
        for (let ServicesTypeDoc of ServicesTypesQuery.docs) {
          this.localServiceTypes.push(ServicesTypeDoc.data() as ServiceType);
        }
        this.showSplash = false;
      });
  }

  ionViewDidEnter() {
    if (this.UserDataCtrl.localUser.verifiedProvider && this.UserDataCtrl.localUser.status == 'active') {
      if (this.UserDataCtrl.localUser.workAdressId == '') {
        const alert = this.UiFeedBackCtrl.alertCtrl.create({
          title: 'Você não possui endereço de trabaho',
          subTitle: 'Você precisa ter um endereço de trabalho cadastrado para se inscrever em um tipo de serviço. Deseja cadastrar agora?',
          buttons: [
            {
              text: 'Sim',
              handler: () => {
                this.navCtrl.push('MyAdressesPage');
              }
            },
            'Cancelar'
          ]
        });
        this.navCtrl.pop();
        this.showSplash = false;
        this.UiFeedBackCtrl.hapticCtrl.notification('error');
        alert.present();
      } else {
        this.UserPlanCtrl.canSubcribeToServiceType(this.UserDataCtrl.localUser)
          .then(canSubscribe => {
            if (canSubscribe) {

              this.loadCategories();
              this.localServiceTypeSub.id = this.afs.createId();
              this.localServiceTypeSub.providerId = this.UserDataCtrl.localUser.id;

            } else {
              this.UserPlanCtrl.getUserPlan()
                .then(plan => {
                  this.navCtrl.pop();
                  this.UiFeedBackCtrl.presentAlert(`Numero máximo de inscrição atingido para o plano ${plan.title} :(`,
                    `O plano ${plan.title} permite somente ${plan.maxServiceTypesSub} inscrições em tipos de serviço!`, 'error');
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                })
            }
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else if (this.UserDataCtrl.localUser.status == 'active') {
      this.navCtrl.pop();
      if (this.UserDataCtrl.localUser.providerVerificationSacId != '') {
        this.UiFeedBackCtrl.presentAlert('Ainda estamos verificando seus dados', 'Precisamos verificar os seus documentos antes de permitir a criação de anuncios para garantir a segurança de nossos usuários. Aguarde... esse processo demora no maximo 24horas, entraremos em contato com você se necessario.', 'warning')
      } else {
        const alert = this.UiFeedBackCtrl.alertCtrl.create({
          title: 'Você ainda não foi verificado !',
          subTitle: 'Você precisa nos enviar algums dados para que possamos verificar seu usuário de prestador. Esse processo é rápido e você só poderá fazer anuncios e receber pedidos depois de faze-lo. Deseja enviar agora?',
          buttons: [
            {
              text: 'Sim',
              handler: () => {
                this.navCtrl.push('ProviderVerificationPage');
              }
            },
            'Não'
          ]
        });
        this.UiFeedBackCtrl.hapticCtrl.notification('warning');
        alert.present();
      }
    } else {
      this.navCtrl.pop();
      this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error');
    }
  }

  selectCategory() {
    if (this.selectedCategory != null) {
      this.selectedServiceType = null
      this.serviceTypesToSelection = [];

      let tempSelectServices = this.localServiceTypes.filter((servicesType) => {
        return (this.selectedCategory.serviceTypesIds.indexOf(servicesType.id) != -1);
      });

      this.serviceTypesToSelection = tempSelectServices;
      this.canSelectServiceType = true;
    }
  }

  toggleNotif() {
    this.localServiceTypeSub.notif = !this.localServiceTypeSub.notif;
  }

  saveConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: "Atenção!",
      subTitle: `Você só pode se desinscrever de um serviço 24h após a inscrição`,
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.trySave();
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  trySave() {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'verificando seus dados...',
    });

    loader.present()
      .then(() => {
        this.afs.collection('ServiceTypeSubs').ref
          .where('providerId', '==', this.UserDataCtrl.localUser.id).where('serviceTypeId', '==', this.selectedServiceType.id).get()
          .then(querySnapshot => {
            if (querySnapshot.empty) {
              loader.dismiss();
              this.save();
            } else {
              loader.dismiss();
              this.UiFeedBackCtrl.presentAlert('Erro', `Você já esta inscrito para o tipo de serviço ${this.selectedServiceType.title}!`, 'error');
            }
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao verificar seus dados', e);
          });
      });

  }

  save() {

    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'salvando...',
    });

    loader.present()
      .then(() => {
        this.timeCtrl.getDate()
          .then(ecoDate => {

            this.localServiceTypeSub.createdOn = ecoDate;

            this.afs.collection('Addresses').doc(this.UserDataCtrl.localUser.workAdressId).ref.get()
              .then(workAddressDoc => {
                let tempWorkAddressDoc = workAddressDoc.data() as Address;

                this.localServiceTypeSub.providerWorkAddress = Object.assign({}, tempWorkAddressDoc);
                this.localServiceTypeSub.serviceTypeId = this.selectedServiceType.id;
                this.localServiceTypeSub.serviceTypeTitle = this.selectedServiceType.title;
                this.localServiceTypeSub.serviceTypeSymbol = this.selectedServiceType.symbol;

                this.afs.collection('ServiceTypeSubs').doc(this.localServiceTypeSub.id).set(Object.assign({}, this.localServiceTypeSub))
                  .then(() => {
                    loader.dismiss();
                    this.navCtrl.pop();
                    this.UiFeedBackCtrl.presentAlert('Inscrição realizada com sucesso!', '', 'success');
                  });
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao salvar dados', e);
          })

      });

  }

}