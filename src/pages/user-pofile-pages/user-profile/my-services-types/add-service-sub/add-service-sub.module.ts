import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddServiceSubPage } from './add-service-sub';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    AddServiceSubPage,
  ],
  imports: [
    IonicPageModule.forChild(AddServiceSubPage),
    LottieAnimationViewModule
  ],
})
export class AddServiceSubPageModule {}
