import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyServicesTypesPage } from './my-services-types';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyServicesTypesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyServicesTypesPage),
    LottieAnimationViewModule
  ],
})
export class MyServicesTypesPageModule {}
