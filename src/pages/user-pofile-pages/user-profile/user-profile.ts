import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { UserPlanProvider } from '../../../providers/UserPlan';

import { AngularFirestore } from 'angularfire2/firestore';
import { UserDataProvider } from '../../../providers/UserData';
import { Plan } from '../../../models/Plan';
import { CoreProvider } from '../../../providers/CoreProvider';

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {

  localUserId = '';

  next24AvailableProposalSubmissionStr = '';
  next24AvailableProposalSubmissionStrLoader = true;

  next24AvailableOrdersAcceptanceStr = '';
  next24AvailableOrdersAcceptanceStrLoader = true;

  availableBannersStr = ``;
  availableBannersStrLoader = true;

  standarProfilePicUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582';
  localUserPlan: Plan = new Plan();
  planDataLoader = true;

  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public afs: AngularFirestore,
    public UserDataCtrl: UserDataProvider,
    public platform: Platform,
    private coreCtrl: CoreProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private UserPlanCtrl: UserPlanProvider,
    private modalCtrl: ModalController
  ) {
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.showSplash = false;

        if (this.UserDataCtrl.localUserIsProvider) {
          this.UserPlanCtrl.getUserPlan()
            .then(userPlan => {
              this.localUserPlan = userPlan;
              this.planDataLoader = false;

              this.UserPlanCtrl.next24AvailableOrdersAcceptanceStr(this.UserDataCtrl.localUser)
                .then(next24AvailableOrdersAcceptanceStr => {
                  this.next24AvailableOrdersAcceptanceStr = next24AvailableOrdersAcceptanceStr;
                  this.next24AvailableOrdersAcceptanceStrLoader = false;
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                })

              this.UserPlanCtrl.next24AvailableProposalSubmissionStr(this.UserDataCtrl.localUser)
                .then(next24AvailableProposalSubmissionStr => {
                  this.next24AvailableProposalSubmissionStr = next24AvailableProposalSubmissionStr;
                  this.next24AvailableProposalSubmissionStrLoader = false;
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                })

              this.UserPlanCtrl.availableBannersStr(this.UserDataCtrl.localUser)
                .then(availableBannersStr => {
                  this.availableBannersStr = availableBannersStr;
                  this.availableBannersStrLoader = false;
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                })
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            })
        }

      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    this.showSplash = true;
    this.planDataLoader = true;
    this.next24AvailableProposalSubmissionStrLoader = true;
    this.next24AvailableOrdersAcceptanceStrLoader = true;
    this.availableBannersStrLoader = true;
    this.ionViewDidEnter();
    refresher.complete();
  }

  openUserPicViewerPage() {
    this.navCtrl.push('UserPicViewerPage', {
      user: this.UserDataCtrl.localUser
    });
  }

  logout() {
    this.UiFeedBackCtrl.presenLoader('Saindo...', 'assets/animations/logout.json')
      .then(() => {
        this.navCtrl.setRoot('HomePage');
        this.UserDataCtrl.logout()
          .then(() => {
            this.UiFeedBackCtrl.presentToast('Até logo!', 'success');
            this.UiFeedBackCtrl.dismissLoader();
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao sair', e);
          });
      });
  }

  goToAddBannerPage() {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Verificando seus dados...',
      dismissOnPageChange: true
    });

    loader.present().then(() => {
      if (this.UserDataCtrl.localUser.status == 'verified') {
        this.afs.collection('Addresses').ref
          .where('userId', '==', this.localUserId).get()
          .then(querySnapshot => {
            if (querySnapshot.empty) {
              const alert = this.UiFeedBackCtrl.alertCtrl.create({
                title: 'Não possuimos nenhum endereço seu !',
                subTitle: 'Você precisa cadastrar um endereço para poder criar um anuncio. Deseja cadastrar agora?',
                buttons: [
                  {
                    text: 'Sim',
                    handler: () => {
                      this.navCtrl.push('AddAddressPage');
                    }
                  },
                  'Cancelar'
                ]
              });
              loader.dismiss();
              alert.present();
            } else {
              this.navCtrl.push('AddBannerPage');
            }
          });
      } else {
        loader.dismiss();
        this.UiFeedBackCtrl.presentAlert('Você ainda não foi verificado :(', 'Precisamos verificar seus dados antes de permitir a criação de anuncios para garantir a segurança de nossos usuários. Aguarde... esse processo demora no maximo 24horas, entraremos em contato com você se necessario.', 'warning')
      }
    });
  }

  goToPage(page) {
    this.navCtrl.push(page, {
      localUser: this.UserDataCtrl.localUser
    });
  }

  setRootPage(page) {
    this.coreCtrl.navSetRoot(page)
  }

  openPlanDetails() {
    let modal = this.modalCtrl.create('PlanDetailsModal');
    modal.present();
  }

  showMyClientReputation() {
    this.UserDataCtrl.getUser()
      .then(data => {
        this.navCtrl.push('ClientDetailPage', {
          client: data
        });
      })
  }

  showMyProviderReputation() {
    this.UserDataCtrl.getUser()
      .then(data => {
        this.navCtrl.push('ProviderDetailPage', {
          provider: data
        });
      })
  }


}