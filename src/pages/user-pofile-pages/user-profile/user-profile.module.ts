import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfilePage } from './user-profile';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    UserProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfilePage),
    LottieAnimationViewModule
  ],
})
export class UserProfilePageModule {}
