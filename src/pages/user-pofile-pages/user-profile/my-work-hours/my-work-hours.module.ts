import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyWorkHoursPage } from './my-work-hours';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyWorkHoursPage,
  ],
  imports: [
    IonicPageModule.forChild(MyWorkHoursPage),
    LottieAnimationViewModule
  ],
})
export class MyWorkHoursPageModule {}
