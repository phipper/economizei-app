import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserDataProvider } from '../../../../providers/UserData';

import { Range } from '../../../../models/Range';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { OfficeHours } from '../../../../models/OfficeHours';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
  selector: 'page-my-work-hours',
  templateUrl: 'my-work-hours.html',
})
export class MyWorkHoursPage {

  localOfficeHours = new OfficeHours();
  showSplash = true;

  dom: Range = new Range();
  seg: Range = new Range();
  ter: Range = new Range();
  qua: Range = new Range();
  qui: Range = new Range();
  sex: Range = new Range();
  sab: Range = new Range();

  canLeaveCtrl = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
  }

  ionViewDidEnter() {
    this.showSplash = true;
    this.db.officeHours.get(this.UserDataCtrl.localUser.id)
      .then(officeHours => {
        if (officeHours == null) {
          let alert = this.UiFeedBackCtrl.alertCtrl.create({
            title: 'Você ainda não possui um horario de trabalho definido!',
            subTitle: 'Deseja criar um horario de trabalho?',
            buttons: [
              {
                text: 'SIM',
                handler: () => {
                  this.localOfficeHours.id = this.db.createId();
                  this.localOfficeHours.providerId = this.UserDataCtrl.localUser.id;
                  this.localOfficeHours.createdOn = new Date();
                  this.localOfficeHours.updatedOn = new Date();
                  this.db.officeHours.create(this.localOfficeHours)
                    .then(() => {
                      this.navCtrl.push('MyWorkHoursPage');
                    })
                    .catch(e => {
                      this.UiFeedBackCtrl.presentErrorAlert('erro ao criar Horario de trabalho', e);
                    });
                }
              },
              'Cancelar'
            ]
          });
          alert.present();
          this.canLeaveCtrl = true;
          this.navCtrl.pop();
        } else {
          this.localOfficeHours = officeHours;

          this.dom.lower = this.localOfficeHours.dom.begin;
          this.seg.lower = this.localOfficeHours.seg.begin;
          this.ter.lower = this.localOfficeHours.ter.begin;
          this.qua.lower = this.localOfficeHours.qua.begin;
          this.qui.lower = this.localOfficeHours.qui.begin;
          this.sex.lower = this.localOfficeHours.sex.begin;
          this.sab.lower = this.localOfficeHours.sab.begin;

          this.dom.upper = this.localOfficeHours.dom.end;
          this.seg.upper = this.localOfficeHours.seg.end;
          this.ter.upper = this.localOfficeHours.ter.end;
          this.qua.upper = this.localOfficeHours.qua.end;
          this.qui.upper = this.localOfficeHours.qui.end;
          this.sex.upper = this.localOfficeHours.sex.end;
          this.sab.upper = this.localOfficeHours.sab.end;

          this.showSplash = false;
        }
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('erro ao procurar seu horario de trabalho', e);
      });
  }

  ionViewCanLeave(): boolean {
    if (!this.canLeaveCtrl) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Tem certeza?',
        subTitle: 'Você quer voltar sem salvar os dados?',
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              this.canLeaveCtrl = true;
              this.navCtrl.pop();
            }
          },
          'Cancelar'
        ]
      });
      alert.present();
    } else {
      return true;
    }
    return false;
  }

  toggleDom() {
    this.canLeaveCtrl = false;
    this.localOfficeHours.dom.work = !this.localOfficeHours.dom.work;
    if (!this.localOfficeHours.dom.work)
      this.UiFeedBackCtrl.presentToastTop('Domingo removido do seu horário', 'success');
  }

  toggleSeg() {
    this.canLeaveCtrl = false;
    this.localOfficeHours.seg.work = !this.localOfficeHours.seg.work;
    if (!this.localOfficeHours.seg.work)
      this.UiFeedBackCtrl.presentToastTop('Segunda-feira removida do seu horário', 'success');
  }

  toggleTer() {
    this.canLeaveCtrl = false;
    this.localOfficeHours.ter.work = !this.localOfficeHours.ter.work;
    if (!this.localOfficeHours.ter.work)
      this.UiFeedBackCtrl.presentToastTop('Terça-feira removida do seu horário', 'success');
  }

  toggleQua() {
    this.canLeaveCtrl = false;
    this.localOfficeHours.qua.work = !this.localOfficeHours.qua.work;
    if (!this.localOfficeHours.qua.work)
      this.UiFeedBackCtrl.presentToastTop('Quarta-feira removida do seu horário', 'success');
  }

  toggleQui() {
    this.canLeaveCtrl = false;
    this.localOfficeHours.qui.work = !this.localOfficeHours.qui.work;
    if (!this.localOfficeHours.qui.work)
      this.UiFeedBackCtrl.presentToastTop('Quinta-feira removida do seu horário', 'success');
  }

  toggleSex() {
    this.canLeaveCtrl = false;
    this.localOfficeHours.sex.work = !this.localOfficeHours.sex.work;
    if (!this.localOfficeHours.sex.work)
      this.UiFeedBackCtrl.presentToastTop('Sexta-feira removida do seu horário', 'success');
  }

  toggleSab() {
    this.canLeaveCtrl = false;
    this.localOfficeHours.sab.work = !this.localOfficeHours.sab.work;
    if (!this.localOfficeHours.sab.work)
      this.UiFeedBackCtrl.presentToastTop('Sábado removido do seu horário', 'success');
  }

  checkDom() {
    this.canLeaveCtrl = false;
    if ((this.dom.upper - this.dom.lower) == 0) {
      this.toggleDom();
      this.dom.lower = 0;
      this.dom.upper = 24
    }
  }

  checkSeg() {
    this.canLeaveCtrl = false;
    if ((this.seg.upper - this.seg.lower) == 0) {
      this.toggleSeg();
      this.seg.lower = 0;
      this.seg.upper = 24
    }
  }

  checkTer() {
    this.canLeaveCtrl = false;
    if ((this.ter.upper - this.ter.lower) == 0) {
      this.toggleTer();
      this.ter.lower = 0;
      this.ter.upper = 24
    }
  }

  checkQua() {
    this.canLeaveCtrl = false;
    if ((this.qua.upper - this.qua.lower) == 0) {
      this.toggleQua();
      this.qua.lower = 0;
      this.qua.upper = 24
    }
  }

  checkQui() {
    this.canLeaveCtrl = false;
    if ((this.qui.upper - this.qui.lower) == 0) {
      this.toggleQui();
      this.qui.lower = 0;
      this.qui.upper = 24
    }
  }

  checkSex() {
    this.canLeaveCtrl = false;
    if ((this.sex.upper - this.sex.lower) == 0) {
      this.toggleSex();
      this.sex.lower = 0;
      this.sex.upper = 24
    }
  }

  checkSab() {
    this.canLeaveCtrl = false;
    if ((this.sab.upper - this.sab.lower) == 0) {
      this.toggleSab();
      this.sab.lower = 0;
      this.sab.upper = 24
    }
  }

  saveWorkHoursConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'ATENÇÃO !',
      subTitle: 'Os seus clientes só poderão abrir pedidos dentro deste horario de atendimento! Deseja continuar?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.saveWorkHours();
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.impact('medium');
    alert.present();
  }

  saveWorkHours() {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'salvando...',
    });

    loader.present()
      .then(() => {

        this.localOfficeHours.dom.begin = this.dom.lower;
        this.localOfficeHours.seg.begin = this.seg.lower;
        this.localOfficeHours.ter.begin = this.ter.lower;
        this.localOfficeHours.qua.begin = this.qua.lower;
        this.localOfficeHours.qui.begin = this.qui.lower;
        this.localOfficeHours.sex.begin = this.sex.lower;
        this.localOfficeHours.sab.begin = this.sab.lower;

        this.localOfficeHours.dom.end = this.dom.upper;
        this.localOfficeHours.seg.end = this.seg.upper;
        this.localOfficeHours.ter.end = this.ter.upper;
        this.localOfficeHours.qua.end = this.qua.upper;
        this.localOfficeHours.qui.end = this.qui.upper;
        this.localOfficeHours.sex.end = this.sex.upper;
        this.localOfficeHours.sab.end = this.sab.upper;

        this.localOfficeHours.updatedOn = new Date();

        this.db.officeHours.update(this.localOfficeHours.id, this.localOfficeHours)
          .then(() => {
            loader.dismiss();
            this.canLeaveCtrl = true;
            this.navCtrl.pop();
            this.UiFeedBackCtrl.presentToast('Horário de trabalho atualizado com sucesso', 'success');
          }).catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentErrorAlert('erro ao atualizar Horario de trabalho', e);
          });
      });
  }

}