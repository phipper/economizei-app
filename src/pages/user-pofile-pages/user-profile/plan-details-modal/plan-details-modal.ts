import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { UserPlanProvider } from '../../../../providers/UserPlan';
import { Plan } from '../../../../models/Plan';
import { UserDataProvider } from '../../../../providers/UserData';



@IonicPage()
@Component({
  selector: 'page-plan-details-modal',
  templateUrl: 'plan-details-modal.html',
})

export class PlanDetailsModal {

  localUserId = '';

  next24AvailableProposalSubmissionStr = '';
  next24AvailableOrdersAcceptanceStr = '';
  availableBannersStr = '';
  availableServiceTypeSubsStr = '';
  localUserPlan: Plan = new Plan();
  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public UserDataCtrl: UserDataProvider,
    private UserPlanCtrl: UserPlanProvider,
  ) {
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.UserPlanCtrl.getUserPlan()
          .then(userPlan => {
            this.localUserPlan = userPlan;
            this.UserPlanCtrl.availableBannersStr(this.UserDataCtrl.localUser)
              .then(availableBannersStr => {
                this.availableBannersStr = availableBannersStr;
                this.UserPlanCtrl.next24AvailableProposalSubmissionStr(this.UserDataCtrl.localUser)
                  .then(next24AvailableProposalSubmissionStr => {
                    this.next24AvailableProposalSubmissionStr = next24AvailableProposalSubmissionStr;
                    this.UserPlanCtrl.next24AvailableOrdersAcceptanceStr(this.UserDataCtrl.localUser)
                      .then(next24AvailableOrdersAcceptanceStr => {
                        this.next24AvailableOrdersAcceptanceStr = next24AvailableOrdersAcceptanceStr;
                        this.UserPlanCtrl.availableServiceTypeSubsStr(this.UserDataCtrl.localUser)
                          .then(availableServiceTypeSubsStr => {
                            this.availableServiceTypeSubsStr = availableServiceTypeSubsStr;

                            this.showSplash = false;
                          })
                      })
                  })
              })
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }
  
  translateBoolean(b: boolean): string {
    return b ? 'Sim' : 'Não';
  }

  close() {
    this.viewCtrl.dismiss({ success: false });
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

}
