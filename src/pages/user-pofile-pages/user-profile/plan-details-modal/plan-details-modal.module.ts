import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanDetailsModal } from './plan-details-modal';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    PlanDetailsModal,
  ],
  imports: [    
    LottieAnimationViewModule,
    IonicPageModule.forChild(PlanDetailsModal),
  ],
})
export class PlanDetailsModule { }
