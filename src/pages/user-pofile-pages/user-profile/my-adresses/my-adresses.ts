import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';

import { UserDataProvider } from '../../../../providers/UserData';

import { Address } from '../../../../models/Address';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-my-adresses',
  templateUrl: 'my-adresses.html',
})
export class MyAdressesPage {

  addresses: Address[] = [];
  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/location.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
  }

  ionViewDidEnter() {
    this.addresses = [];
    this.showSplash = true;
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.db.addresses.getUserAddresses(userId)
          .then(addresses => {
            this.addresses = addresses;
            if (addresses.length == 0) {
              this.UiFeedBackCtrl.presentAlert('Você não possui nenhum endereço cadastrado :(', 'Cadastre seus endereços de casa e do trabalho para que o Economizei possa te ajudar a escolher melhor os serviços!', 'warning');
            }
            this.showSplash = false;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  addressOptions(address: Address) {

    let buttonsArray: any = [
      {
        text: 'Deletar',
        role: 'destructive',
        icon: !this.platform.is('ios') ? 'trash' : null,
        handler: () => {
          this.deleteAddressConfirmation(address);
        }
      }
    ];

    if (this.UserDataCtrl.localUser.homeAdressId != address.id) {
      buttonsArray.push({
        text: 'Definir como endereço de Casa',
        icon: !this.platform.is('ios') ? 'home' : null,
        handler: () => {
          this.makeHomeAdressId(address);
        }
      });
    }

    if (this.UserDataCtrl.localUser.workAdressId != address.id) {
      buttonsArray.push({
        text: 'Definir como endereço de Trabalho',
        icon: !this.platform.is('ios') ? 'briefcase' : null,
        handler: () => {
          this.makeWorkAdressId(address);
        }
      });
    }

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções de ' + address.street + ', ' + address.numero,
      buttons: buttonsArray,
    });
    actionSheet.present();
  }

  deleteWorkAddress(address: Address) {

    this.UiFeedBackCtrl.updateLoaderMessage('Atualizando seus dados...');

    this.UserDataCtrl.localUser.workAdressId = '';
    this.UserDataCtrl.localUser.workBairro = '';
    this.UserDataCtrl.localUser.workCity = '';
    this.UserDataCtrl.localUser.workState = '';

    this.UserDataCtrl.update({
      workAdressId: this.UserDataCtrl.localUser.workAdressId,
      workBairro: this.UserDataCtrl.localUser.workBairro,
      workCity: this.UserDataCtrl.localUser.workCity,
      workState: this.UserDataCtrl.localUser.workState
    })
      .then(() => {
        this.UiFeedBackCtrl.updateLoaderMessage('Deletando Endereço...');
        this.db.addresses.delete(address.id)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Feito !', 'Endereço excluido', 'success');
            this.ionViewDidEnter();
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro :(', e);
      })
  }

  deleteHomeAddress(address: Address) {


    this.UiFeedBackCtrl.updateLoaderMessage('Atualizando seus dados...');

    this.UserDataCtrl.localUser.homeAdressId = '';

    this.UserDataCtrl.update({ homeAdressId: this.UserDataCtrl.localUser.homeAdressId })
      .then(() => {
        this.UiFeedBackCtrl.updateLoaderMessage('Deletando Endereço...');
        this.db.addresses.delete(address.id)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Feito !', 'Endereço excluido', 'success');
            this.ionViewDidEnter();
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.dismissLoader();
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao ercluir endereço de casa', e);
      });

  }

  deleteHomeAndWorkAddress(address: Address) {

    this.UiFeedBackCtrl.updateLoaderMessage('Atualizando seus dados...');

    this.UserDataCtrl.localUser.workAdressId = '';
    this.UserDataCtrl.localUser.workBairro = '';
    this.UserDataCtrl.localUser.workCity = '';
    this.UserDataCtrl.localUser.workState = '';

    this.UserDataCtrl.localUser.homeAdressId = '';

    this.UserDataCtrl.update({
      workAdressId: this.UserDataCtrl.localUser.workAdressId,
      workBairro: this.UserDataCtrl.localUser.workBairro,
      workCity: this.UserDataCtrl.localUser.workCity,
      workState: this.UserDataCtrl.localUser.workState,
      homeAdressId: this.UserDataCtrl.localUser.homeAdressId
    })
      .then(() => {
        this.UiFeedBackCtrl.updateLoaderMessage('Deletando Endereço...');
        this.db.addresses.delete(address.id)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Feito !', 'Endereço excluido', 'success');
            this.ionViewDidEnter();
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro :(', e);
      })
  }

  deleteAddress(address: Address) {
    this.UiFeedBackCtrl.presenLoader('Verificando seus dados...')
      .then(() => {
        if (this.UserDataCtrl.localUser.homeAdressId == address.id && this.UserDataCtrl.localUser.workAdressId != address.id) {
          this.deleteHomeAddress(address)
        } else if (this.UserDataCtrl.localUser.workAdressId == address.id && this.UserDataCtrl.localUser.homeAdressId != address.id) {
          this.deleteWorkAddress(address)
        } else if (this.UserDataCtrl.localUser.workAdressId == address.id && this.UserDataCtrl.localUser.homeAdressId == address.id) {
          this.deleteHomeAndWorkAddress(address)
        } else {
          this.UiFeedBackCtrl.updateLoaderMessage('Deletando Endereço...');
          this.db.addresses.delete(address.id)
            .then(() => {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentAlert('Feito !', 'Endereço excluido', 'success');
              this.ionViewDidEnter();
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro ao excluir endereço', e);
            });
        }
      });
  }


  deleteAddressConfirmation(address: Address) {

    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Deletar',
      subTitle: 'Você tem certeza que deseja excluir este endereço?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.deleteAddress(address);
          }
        },
        'Cancelar'
      ]
    });
    alert.present();
  }


  addAddressPage() {
    this.navCtrl.push('AddAddressPage');
  }

  makeHomeAdressId(address: Address) {
    this.UiFeedBackCtrl.presenLoader('Atualizando seus dados...')
      .then(() => {
        this.UserDataCtrl.localUser.homeAdressId = address.id;

        this.UserDataCtrl.update({ homeAdressId: this.UserDataCtrl.localUser.homeAdressId })
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Feito !', 'Endereço de Casa atualizado com sucesso', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Erro', JSON.stringify(e), 'error');
          });

      });

  }

  makeWorkAdressId(address: Address) {
    this.UiFeedBackCtrl.presenLoader('Atualizando seus dados...')
      .then(() => {

        this.UserDataCtrl.localUser.workAdressId = address.id;
        this.UserDataCtrl.localUser.workBairro = address.bairro;
        this.UserDataCtrl.localUser.workCity = address.city;
        this.UserDataCtrl.localUser.workState = address.state;

        this.UserDataCtrl.update({ workAdressId: this.UserDataCtrl.localUser.workAdressId, workBairro: this.UserDataCtrl.localUser.workBairro, workCity: this.UserDataCtrl.localUser.workCity, workState: this.UserDataCtrl.localUser.workState })
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Feito !', 'Endereço de Trabalho atualizado com sucesso', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Erro', JSON.stringify(e), 'error');
          });

      });
  }

}
