import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyAdressesPage } from './my-adresses';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyAdressesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyAdressesPage),
    LottieAnimationViewModule
  ],
})
export class MyAdressesPageModule {}
