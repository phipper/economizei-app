import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAddressPage } from './add-address';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    AddAddressPage,
  ],
  imports: [
    BrMaskerModule,
    IonicPageModule.forChild(AddAddressPage),
  ],
})
export class AddAddressPageModule {}
