import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import { NativeGeocoder } from '@ionic-native/native-geocoder';

import { Address } from '../../../../../models/Address';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { UtilProvider } from '../../../../../providers/Util';
import { EcoFirestore } from '../../../../../providers/EcoFirestore/EcoFirestore';
import { UserDataProvider } from '../../../../../providers/UserData';

@IonicPage()
@Component({
  selector: 'page-add-address',
  templateUrl: 'add-address.html',
})
export class AddAddressPage {

  address = new Address();
  validCep = false;

  constructor(
    private nativeGeocoder: NativeGeocoder,
    private http: Http,
    public userDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider,
  ) {
  }

  ionViewDidEnter() {
  }

  cleanAll() {
    this.address = new Address();
    this.validCep = false;
  }

  // checkCep() {
  //   if (this.localCep.indexOf('-', 0) != (-1) || this.localCep.indexOf('.', 0) != (-1)) {
  //     this.validCep = false;
  //     this.localCep = "";
  //     this.UiFeedBackCtrl.presentAlert("CEP invalido", "Favor inserir somente numeros!", 'warning', true);
  //   } else if (this.localCep.length == 8) {
  //     this.address.cep = this.localCep;
  //     this.searchCep();
  //   } else {
  //     this.validCep = false;
  //     this.localCep = "";
  //     this.UiFeedBackCtrl.presentAlert("CEP invalido", "favor inserir um cep valido!", 'warning', true);
  //   }
  // }

  searchCep() {
    this.UiFeedBackCtrl.presenLoader(`Procurando CEP...`, `assets/animations/location.json`)
      .then(() => {

        var addressServiceUrl = "https://viacep.com.br/ws/" + this.utilCtrl.unFormat(this.address.cep) + "/json";
        this.http.get(addressServiceUrl).toPromise()
          .then(response => {
            let viaCepAddress = response.json();
            if (!viaCepAddress.erro) {
              this.address.street = viaCepAddress.logradouro;
              this.address.complemento = viaCepAddress.complemento;
              this.address.bairro = viaCepAddress.bairro;
              this.address.city = viaCepAddress.localidade;
              this.address.state = this.utilCtrl.ufToState(viaCepAddress.uf);
              this.address.ibge = viaCepAddress.ibge;
              this.address.gia = viaCepAddress.gia;

              this.validCep = true;

              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentAlert("CEP encontrado!", "Favor verifique os demais dados!", `success`, true);
            } else {
              this.validCep = false;
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentAlert("CEP não encontrado", "Favor verifique se o CEP que foi digitado esta correto!", 'error');
            }
          })
          .catch(e => {
            this.validCep = false;
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert("CEP não encontrado", "Favor verifique se o CEP que foi digitado esta correto!", 'error');
          });
      });
  }

  AddAddress() {
    this.UiFeedBackCtrl.presenLoader(`Procurando as cordenadas para este endereço...`, `assets/animations/location.json`)
      .then(() => {
        this.nativeGeocoder.forwardGeocode(`${this.address.street} ${this.address.numero}, ${this.address.city} - ${this.address.state} ${this.address.cep}`, { defaultLocale: 'pt-BR', maxResults: 1, useLocale: true })
          .then(coordinates => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando...');
            this.address.latitude = Number(coordinates[0].latitude);
            this.address.longitude = Number(coordinates[0].longitude);
            this.address.id = this.db.createId();
            this.address.userId = this.userDataCtrl.localUser.id;
            this.db.addresses.create(this.address)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.navCtrl.pop();
              })
              .catch(error => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert("erro ao salvar endereço!", error);
              });
          }, error => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("erro ao procurar as coordenadas para este endereço!", error);
          });
      });
  }

}
