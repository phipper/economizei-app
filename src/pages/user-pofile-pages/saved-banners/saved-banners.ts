import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetButton, Content } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { AngularFirestore } from 'angularfire2/firestore';

import { Banner } from '../../../models/Banner';
import { UserDataProvider } from '../../../providers/UserData';

@IonicPage()
@Component({
  selector: 'page-saved-banners',
  templateUrl: 'saved-banners.html',
})
export class SavedBannersPage {
  @ViewChild(Content) content: Content;

  savedBanners: Banner[];
  localUserSavedBanners: string[] = [];
  showSplash = true;
  limitOfBannersParam = 10;
  limitOfBanners = this.limitOfBannersParam;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/favorite.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard
  ) {
  }

  ionViewDidEnter() {
    this.savedBanners = [];
    this.showSplash = true;

    this.localUserSavedBanners = this.UserDataCtrl.localUser.savedBanners;
    if (this.localUserSavedBanners.length > 0) {
      for (var i = 0; i < this.localUserSavedBanners.length; i++) {
        if (i < this.limitOfBanners) {
          this.afs.collection('Banners').doc(this.localUserSavedBanners[i]).ref.get()
            .then(doc => {
              this.savedBanners.push(doc.data() as Banner);
              if (this.savedBanners.length == this.localUserSavedBanners.length) {
                this.showSplash = false;
              }
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro ao ler um Anuncio', e);
            })
        } else {
          break;
        }
      }
    } else {
      setTimeout(() => {
        this.showSplash = false;
      }, 1000);
    }
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    this.limitOfBanners = this.limitOfBannersParam;
    this.savedBanners = [];
    this.showSplash = true;
    this.localUserSavedBanners = this.UserDataCtrl.localUser.savedBanners;
    if (this.localUserSavedBanners.length > 0) {
      for (var i = 0; i < this.localUserSavedBanners.length; i++) {
        if (i < this.limitOfBanners) {
          this.afs.collection('Banners').doc(this.localUserSavedBanners[i]).ref.get()
            .then(doc => {
              this.savedBanners.push(doc.data() as Banner);
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro ao ler um Anuncio', e);
            })
        } else {
          break;
        }
      }
      refresher.complete();
      this.showSplash = false;
    } else {
      refresher.complete();
      this.showSplash = false;
    }

    refresher.complete();
    this.showSplash = false;
  }

  doInfinite(infiniteScroll) {
    this.limitOfBanners = this.limitOfBanners + this.limitOfBannersParam;
    let initlocalBannersLength = this.savedBanners.length;
    for (var i = initlocalBannersLength; i < this.localUserSavedBanners.length; i++) {
      if (i < this.limitOfBanners) {
        this.afs.collection('Banners').doc(this.localUserSavedBanners[i]).ref.get()
          .then(doc => {
            this.savedBanners.push(doc.data() as Banner);
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentAlert('Erro :(', JSON.stringify(e), 'error');
          });
      } else {
        infiniteScroll.complete();
        break;
      }
    }
    infiniteScroll.complete();
  }

  unsaveBanner(bannerId: string) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Removendo dos favoritos...',
      dismissOnPageChange: true
    });
    loader.present().then(() => {
      this.UserDataCtrl.unsaveBanner(bannerId)
        .then(() => {
          loader.dismiss();
          this.UiFeedBackCtrl.presentToast('Anuncio removido dos favoritos', 'success');
          this.ionViewDidEnter();
        }).catch(e => {
          this.UiFeedBackCtrl.presentAlert('Erro :(', JSON.stringify(e), 'error');
        });
    });
  }

  itemPressed(banner: Banner) {
    let buttonsArray: ActionSheetButton[] = [];
    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share(`Olha esse anuncio que achei pra você no economizei, ${banner.title}`, banner.title, null, banner.dynamicLink);
      }
    });
    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy(banner.dynamicLink).then(() => {
          this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', banner.dynamicLink, 'success');
        });
      }
    });
    buttonsArray.push({
      text: 'Remover dos favoritos',
      icon: !this.platform.is('ios') ? 'heart' : null,
      handler: () => {
        this.unsaveBanner(banner.id);
      }
    });
    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: banner.title,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

  itemTapped(banner: Banner) {
    this.navCtrl.push('BannerDetailPage', {
      banner: banner
    });
  }

}
