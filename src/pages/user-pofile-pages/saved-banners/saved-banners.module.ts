import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavedBannersPage } from './saved-banners';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    SavedBannersPage,
  ],
  imports: [
    IonicPageModule.forChild(SavedBannersPage),
    LottieAnimationViewModule
  ],
})
export class SavedServicesPageModule {}
