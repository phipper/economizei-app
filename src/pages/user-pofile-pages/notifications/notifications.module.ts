import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationsPage } from './notifications';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    NotificationsPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationsPage),
    LottieAnimationViewModule
  ],
})
export class NotificationsPageModule {}
