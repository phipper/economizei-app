import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetButton, Content, ItemSliding } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';

import { UserDataProvider } from '../../../providers/UserData';
import { AngularFirestore } from 'angularfire2/firestore';

import { Notification } from '../../../models/Notification';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  localNotifications: Notification[] = [];
  showSplash = true;
  hasUnreadedNotif = false;
  loadingNotification = false;
  limitOfNotifications = 20;
  OriginalNumberOfNotifications = 0;
  markAsReadReqNum = 0;
  markAsReadRespNum = 0;
  deleteReqNum = 0;
  deleteRespNum = 0;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/notification.json'
  }

  noNotifLottie = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/no_notifications.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.showSplash = true;
        this.localNotifications = [];
        this.limitOfNotifications = 20;
        this.OriginalNumberOfNotifications = 0;
        this.markAsReadReqNum = 0;
        this.markAsReadRespNum = 0;
        this.afs.collection('Users').doc(this.localUserId).collection('Notifications').ref
          .orderBy('createdOn', 'desc').limit(this.limitOfNotifications).get()
          .then(querySnapshot => {
            this.hasUnreadedNotif = false;
            this.OriginalNumberOfNotifications = querySnapshot.docs.length;
            for (var i = 0; i < querySnapshot.docs.length; ++i) {
              let tempNotif = querySnapshot.docs[i].data() as Notification;
              if (!tempNotif.readed) {
                this.hasUnreadedNotif = true;
              }
              this.localNotifications.push(tempNotif);
            }
            this.showSplash = false;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
      });
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    this.showSplash = true;
    this.limitOfNotifications = 20;
    this.localNotifications = [];
    this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').ref
      .orderBy('createdOn', 'desc').limit(this.limitOfNotifications).get().then(querySnapshot => {
        this.hasUnreadedNotif = false;
        this.OriginalNumberOfNotifications = querySnapshot.docs.length;
        for (var i = 0; i < querySnapshot.docs.length; ++i) {
          let tempNotif = querySnapshot.docs[i].data() as Notification;
          if (!tempNotif.readed) {  
            this.hasUnreadedNotif = true;
          }
          this.localNotifications.push(tempNotif);
        }
        this.showSplash = false;
        refresher.complete();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
      });
  }

  doInfinite(infiniteScroll) {
    this.limitOfNotifications = this.limitOfNotifications + 10;
    this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').ref
      .orderBy('createdOn', 'desc').limit(this.limitOfNotifications).get().then(querySnapshot => {
        this.hasUnreadedNotif = false;
        for (var i = this.OriginalNumberOfNotifications; i < querySnapshot.docs.length; ++i) {
          let tempNotif = querySnapshot.docs[i].data() as Notification;
          if (!tempNotif.readed) {
            this.hasUnreadedNotif = true;
          }
          this.localNotifications.push(tempNotif);
        }
        this.OriginalNumberOfNotifications = querySnapshot.docs.length;
        infiniteScroll.complete();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
      });
  }

  markAsRead(notification: Notification, slidingItem: ItemSliding) {
    this.UiFeedBackCtrl.presenLoader('Alterando...', 'assets/animations/notification.json')
      .then(() => {
        slidingItem.close();
        notification.readed = true;
        this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').doc(notification.id).update(Object.assign({}, notification))
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast(`${notification.title} marcada como lida`, 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

  delete(notification: Notification, slidingItem: ItemSliding) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: "Tem certeza?",
      subTitle: `Você quer deletar ${notification.title}`,
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.UiFeedBackCtrl.presenLoader('Excluindo...', 'assets/animations/notification.json')
              .then(() => {
                this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').doc(notification.id).delete()
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast(`${notification.title} excluida`, 'success');
                    this.ionViewDidEnter();
                  })
                  .catch(e => {
                    this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
                  });
              });

          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    slidingItem.close();
    alert.present();
  }

  itemPressed(notification: Notification, slidingItem: ItemSliding) {
    let buttonsArray: ActionSheetButton[] = [];
    if (!notification.readed) {
      buttonsArray.push({
        text: 'Marcar como lida',
        icon: !this.platform.is('ios') ? 'done-all' : null,
        handler: () => {
          this.markAsRead(notification, slidingItem);
        }
      });
    }
    buttonsArray.push({
      text: 'Excluir',
      icon: !this.platform.is('ios') ? 'trash' : null,
      handler: () => {
        this.delete(notification, slidingItem);
      }
    });
    buttonsArray.push({
      text: "Cancelar",
      icon: null,
      role: "cancel",
      handler: null,
    });
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: notification.title,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

  itemTapped(notification: Notification) {
    if (!this.loadingNotification) {
      this.loadingNotification = true;
      switch (notification.type) {
        case 'termsOfUseViolation':
          this.navCtrl.push('TermsOfUsePage')
            .then(() => {
              this.loadingNotification = false;
              this.UiFeedBackCtrl.presentAlert(notification.title, notification.text, 'error');
            });
          break;
        case 'generalAlerts':
          switch (notification.action.type) {
            case 'gotoPage':
              this.navCtrl.push(notification.action.page)
                .then(() => {
                  this.loadingNotification = false;
                });
              break;
            case 'showMyOrder':
              this.navCtrl.push('MyOrderDetailPage', {
                orderId: notification.action.orderId
              })
                .then(() => {
                  this.loadingNotification = false;
                });
              break;
            case 'showReceivedOrder':
              this.navCtrl.push('ReceivedOrderDetailPage', {
                orderId: notification.action.orderId
              })
                .then(() => {
                  this.loadingNotification = false;
                });
              break;
            case 'showMySac':
              this.navCtrl.push('MySacDetailPage', {
                sacId: notification.action.sacId
              }).then(() => {
                this.loadingNotification = false;
              });
              break;

            default:
              break;
          }
          break;
        case 'newOrder':
          this.navCtrl.push('ReceivedOrderDetailPage', {
            orderId: notification.action.orderId
          })
            .then(() => {
              this.loadingNotification = false;
            });
          break;
        case 'showRequest':
          let loader = this.UiFeedBackCtrl.loadingCtrl.create({
            content: 'Carregando Solicitação...'
          });
          loader.present().then(() => {
            this.afs.collection('Requests').doc(notification.action.requestId).ref.get()
              .then(requestDoc => {
                var tempRequest = requestDoc.data() as Request;
                loader.dismiss();
                this.navCtrl.push('RequestDetailPage', {
                  request: tempRequest
                })
                  .then(() => {
                    this.loadingNotification = false;
                  });
              }).catch(e => {
                loader.dismiss();
                this.UiFeedBackCtrl.presentErrorAlert("Erro ao carregar solicitação", e);
              });
          });
          break;
        case 'showMyRequest':
          let loader1 = this.UiFeedBackCtrl.loadingCtrl.create({
            content: 'Carregando Solicitação...'
          });
          loader1.present().then(() => {
            this.afs.collection('Requests').doc(notification.action.requestId).ref.get()
              .then(requestDoc => {
                var tempRequest = requestDoc.data() as Request;
                loader1.dismiss();
                this.navCtrl.push('MyRequestDetailPage', {
                  request: tempRequest
                })
                  .then(() => {
                    this.loadingNotification = false;
                  });
              }).catch(e => {
                loader1.dismiss();
                this.UiFeedBackCtrl.presentErrorAlert("Erro ao carregar solicitação", e);
              });
          });
          break;
        default:
          break;
      }
      this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').doc(notification.id).update({ readed: true })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("erro ao registar a leitura da notifcação", e);
        });
    } else {
      this.UiFeedBackCtrl.presentToast('Ainda abrindo notificação!', 'warning');
    }
  }

  markAllReaded() {
    this.UiFeedBackCtrl.presenLoader('Alterando...', 'assets/animations/notification.json')
      .then(() => {
        for (var i = 0; i < this.localNotifications.length; ++i) {
          if (!this.localNotifications[i].readed) {
            this.markAsReadReqNum++;
            this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').doc(this.localNotifications[i].id).update({ readed: true })
              .then(() => {
                this.markAsReadRespNum++;
                this.UiFeedBackCtrl.updateLoaderMessage(`Marcando notificação como lida ${this.markAsReadRespNum}/${this.markAsReadReqNum} ...`);
                if (this.markAsReadReqNum == this.markAsReadRespNum) {
                  this.UiFeedBackCtrl.dismissLoader()
                  this.ionViewDidEnter();
                }
              })
              .catch(e => {
                this.UiFeedBackCtrl.dismissLoader();
                this.ionViewDidEnter();
                this.UiFeedBackCtrl.presentErrorAlert("erro ao atualizar notificação", e);
              });
          }
        }
      });
  }

  executeDeleteAll() {
    this.UiFeedBackCtrl.presenLoader('Excluindo...', 'assets/animations/notification.json')
      .then(() => {
        this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').ref
          .orderBy('createdOn', 'desc').get()
          .then(querySnapshot => {
            for (var i = 0; i < querySnapshot.docs.length; ++i) {
              this.deleteReqNum++;
              this.afs.collection('Users').doc(this.UserDataCtrl.localUser.id).collection('Notifications').doc(querySnapshot.docs[i].id).delete()
                .then(() => {
                  this.deleteRespNum++;
                  this.UiFeedBackCtrl.updateLoaderMessage(`Excluindo notificação ${this.deleteRespNum}/${querySnapshot.docs.length} ...`);
                  if (this.deleteReqNum == this.deleteRespNum) {
                    this.UiFeedBackCtrl.dismissLoader()
                    this.ionViewDidEnter();
                  }
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.ionViewDidEnter();
                  this.UiFeedBackCtrl.presentErrorAlert("erro ao excluir notificação", e);
                });
            }
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert("erro ao excluir notificação", e);
          });
      });
  }

  deleteAll() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: "Tem certeza?",
      subTitle: `Você quer deletar todas as notificações?`,
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.executeDeleteAll();
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  notificationsPageOptions() {
    let buttonsArray: ActionSheetButton[] = [];
    if (this.localNotifications.length > 0 && this.hasUnreadedNotif) {
      buttonsArray.push({
        text: 'Marcar todas como lida',
        icon: !this.platform.is('ios') ? 'done-all' : null,
        handler: () => {
          this.markAllReaded();
        }
      });
    }

    if (this.localNotifications.length > 0) {
      buttonsArray.push({
        text: 'Excluir todas',
        icon: !this.platform.is('ios') ? 'trash' : null,
        handler: () => {
          this.deleteAll();
        }
      });
    }

    buttonsArray.push({
      text: "Cancelar",
      icon: null,
      role: "cancel",
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções das notificações',
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();

  }

}
