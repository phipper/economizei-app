import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialPage } from './financial';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    FinancialPage,
  ],
  imports: [
    IonicPageModule.forChild(FinancialPage),
    LottieAnimationViewModule
  ],
})
export class FinancialPageModule {}
