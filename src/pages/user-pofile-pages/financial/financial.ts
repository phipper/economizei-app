import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { PlanSignature } from '../../../models/PlanSignature';
import { UtilProvider } from '../../../providers/Util';

import { UserDataProvider } from '../../../providers/UserData';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
	selector: 'page-financial',
	templateUrl: 'financial.html',
})
export class FinancialPage {


	localUserId = '';

	showSplash = true;
	localActivePlanSignature: PlanSignature = new PlanSignature();

	lottieSplash = {
		loop: true,
		autoplay: true,
		path: 'assets/animations/credit-card.json'
	}

	constructor(
		public platform: Platform,
		public UserDataCtrl: UserDataProvider,
		public db: EcoFirestore,
		public navCtrl: NavController,
		public navParams: NavParams,
		private UiFeedBackCtrl: UiFeedBackProvider,
		public utilCtrl: UtilProvider
	) {
	}

	ionViewDidLoad() {
		this.showSplash = true;
		this.UserDataCtrl.getUid()
			.then(userId => {
				this.localUserId = userId;
				this.db.afs.collection('PlanSignatures').ref
					.where('userId', '==', this.localUserId).where('status', '==', 'ACTIVE').get()
					.then(planSignaturesQuery => {
						this.db.validators.validateQuerySnapshot(planSignaturesQuery)
							.then(planSignaturesQuery => {
								if (!planSignaturesQuery.empty) {
									this.localActivePlanSignature = planSignaturesQuery.docs[0].data() as PlanSignature;
									this.showSplash = false;
								} else {
									this.showSplash = false;
								}
							})
					})
			})
			.catch(e => {
				this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
			})
	}

	PSDetail(PS: PlanSignature) {
		this.navCtrl.push('PlanSignatureDetailsPage', {
			planSignature: PS,
		})
	}

	goToPage(page) {
		this.navCtrl.push(page);
	}
}
