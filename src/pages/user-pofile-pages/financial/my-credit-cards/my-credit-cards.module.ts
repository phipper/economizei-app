import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyCreditCardsPage } from './my-credit-cards';
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    MyCreditCardsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyCreditCardsPage),
    LottieAnimationViewModule
  ],
})
export class MyCreditCardsPageModule {}
