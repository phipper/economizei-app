import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform, ActionSheetButton } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../../providers/UserData';

import { UtilProvider } from '../../../../providers/Util';
import { CreditCard } from '../../../../models/CreditCard';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-my-credit-cards',
  templateUrl: 'my-credit-cards.html',
})
export class MyCreditCardsPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  showSplash = true;
  localCreditCards: CreditCard[] = [];

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/credit-card.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'AddCreditCardPage') {
      this.content.scrollToTop();
    }
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.loadCreditCards();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }


  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    this.loadCreditCards();
    refresher.complete();
  }

  loadCreditCards() {
    this.showSplash = true;
    this.localCreditCards = [];
    this.db.afs.collection('Users').doc(this.localUserId).collection('CreditCards').ref
      .orderBy('createdOn', 'desc').get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              this.localCreditCards.push(doc.data() as CreditCard);
            }
            this.scrollToTop();
            this.showSplash = false;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
      });
  }

  deleteCC(cc: CreditCard) {
    this.UiFeedBackCtrl.presenLoader('Excluindo...', 'assets/animations/credit-card.json')
      .then(() => {
        this.db.afs.collection('Users').doc(this.localUserId).collection('CreditCards').doc(cc.id).delete()
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.loadCreditCards();
            this.UiFeedBackCtrl.presentAlert('Cartão exluido com sucesso!', '', 'success', true);
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao excluir cartão', e);
          })
      })
  }

  deleteConfirmation(cc: CreditCard) {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Tem certeza?',
      subTitle: `Você tem certeza que deseja excluir o cartão de numero ${this.utilCtrl.maskCreditCardNumber(cc.number)} ?`,
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.deleteCC(cc);
          }
        },
        'Cancelar'
      ]
    });
    alert.present();
  }

  cardOptions(cc: CreditCard) {
    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Detalhes',
      icon: !this.platform.is('ios') ? 'information-circle' : null,
      handler: () => {
        this.cardDetail(cc);
      }
    });

    buttonsArray.push({
      text: 'Excluir',
      icon: !this.platform.is('ios') ? 'trash' : null,
      handler: () => {
        this.deleteConfirmation(cc);
      }
    });

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções de ' + cc.brand + ' ' + this.utilCtrl.maskCreditCardNumber(cc.number),
      buttons: buttonsArray,
    });
    actionSheet.present();
  }

  cardDetail(cc: CreditCard) {
    this.UiFeedBackCtrl.presentAlert('Detalhes do cartão',
      `Operadora: ${cc.brand},
      Numero: ${this.utilCtrl.maskCreditCardNumber(cc.number)},
    Nome do Titular: ${cc.ownerName}`, '');
  }

  addCreditCardPage() {
    this.navCtrl.push('AddCreditCardPage');
  }

}
