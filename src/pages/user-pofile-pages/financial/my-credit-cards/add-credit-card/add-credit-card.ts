import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, Content, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';

import { EcoFirestore } from '../../../../../providers/EcoFirestore/EcoFirestore';
import { UserDataProvider } from '../../../../../providers/UserData';
import { CreditCard } from '../../../../../models/CreditCard';
import { PagseguroPgtoServiceProvider } from '../../../../../providers/PagSeguro/PagSeguro';
import { UtilProvider } from '../../../../../providers/Util';

@IonicPage()
@Component({
  selector: 'page-add-credit-card',
  templateUrl: 'add-credit-card.html',
})
export class AddCreditCardPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  localCreditCard: CreditCard = new CreditCard();
  lastLocalCreditCardNumberLength = 0;

  segment = 1;

  acceotedCreditCardBrands = ["visa", "mastercard", "amex", "elo", "hipercard", "diners"]

  displayCreditCardNumber = '•••• •••• •••• ••••';
  tempOwnerName = '';
  tempOwnerCpf = '';
  tempOwnerBirthDate = '';
  tempExpiringDate = '';

  DECIMAL_SEPARATOR = '.';
  GROUP_SEPARATOR = ',';
  pureResult: any;
  maskedId: any;

  maxDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMaxDate(3650);

  canLeaveCtrl = true;
  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/credit-card.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public pagseg: PagseguroPgtoServiceProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localCreditCard.ownerName = 'NOME SOBRENOME';
    this.localCreditCard.expirationMonth = 'MM';
    this.localCreditCard.expirationYear = 'AAAA';
    this.localCreditCard.ownerCPF = '•••.•••.•••-••';
    this.localCreditCard.ownerBirthDate = 'DD/MM/AAAA';
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'CreateRequestPge') {
      this.content.scrollToTop();
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  ionViewDidEnter() {

    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;

        if (this.UserDataCtrl.localUser.status == 'active') {

          this.pagseg.init()
            .then(() => {
              this.showSplash = false;
            })
            .catch(e => {

            })
        } else {
          this.navCtrl.pop();
          this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error', true);
        }

      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  ionViewCanLeave(): boolean {
    if (!this.canLeaveCtrl) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Tem certeza?',
        subTitle: 'Você quer descartar todos os dados e cancelar o cadastro do seu cartão de credito?',
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              this.canLeaveCtrl = true;
              this.navCtrl.pop();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      return true;
    }
    return false;
  }


  getCardData() {
    this.displayCardNumber();
    if (this.localCreditCard.number.length > this.lastLocalCreditCardNumberLength) {
      this.pagseg.getCreditCardBrandData(this.localCreditCard.number)
        .then(newCardBrandData => {
          if (this.acceotedCreditCardBrands.indexOf(newCardBrandData.name, 0) != -1) {
            this.localCreditCard.brand = newCardBrandData.name;
          } else {
            this.UiFeedBackCtrl.presentAlert('Não aceitamos este cartão!', `Infelizmente você não pode usar cartões da bandeira ${newCardBrandData.name}, Porfavor utilize outro cartão!`, 'warning', true);
            this.navCtrl.pop();
          }
        })
        .catch(e => {
          console.log(JSON.stringify(e));
          this.localCreditCard.brand = '';
          if (this.localCreditCard.number.length >= 6) {
            this.UiFeedBackCtrl.presentAlert('Numero do cartão incorreto!', 'Não encontramos dados validos para este cartão de credito. Favor confira o numero e tente novamente!', 'warning', true);
          }
        })
    }
    this.lastLocalCreditCardNumberLength = this.localCreditCard.number.length;
  }

  goForward() {
    this.segment++;
  }

  goBack() {
    if (this.segment > 1) {
      this.segment--;
    }
  }

  displayCardNumber() {
    var tempDisplayNumber = '';
    for (let i = 0; i < 16; i++) {
      if (i < this.localCreditCard.number.length) {
        if (i != 4 && i != 8 && i != 12) {
          tempDisplayNumber = tempDisplayNumber + this.localCreditCard.number[i];
        } else {
          if (i == 4 || i == 8 || i == 12) {
            tempDisplayNumber = tempDisplayNumber + ' ' + this.localCreditCard.number[i];
          } else {
            tempDisplayNumber = tempDisplayNumber + '•';
          }
        }
      } else {
        if (i != 4 && i != 8 && i != 12) {
          tempDisplayNumber = tempDisplayNumber + '•';
        } else {
          if (i == 4 || i == 8 || i == 12) {
            tempDisplayNumber = tempDisplayNumber + ' •';
          } else {
            tempDisplayNumber = tempDisplayNumber + '•';
          }
        }
      }
    }
    this.displayCreditCardNumber = tempDisplayNumber;
  }

  ownerNameInput() {
    if (this.tempOwnerName.length != 0) {
      this.localCreditCard.ownerName = this.tempOwnerName.toUpperCase();
    } else {
      this.localCreditCard.ownerName = 'NOME SOBRENOME';
    }
  }

  expiringDateInput() {
    this.localCreditCard.expirationMonth = this.tempExpiringDate.split('-')[1];
    this.localCreditCard.expirationYear = this.tempExpiringDate.split('-')[0];
  }

  ownerCpfInput() {
    var tempUserCpf = '';
    for (let i = 0; i < 11; i++) {
      if (i < this.tempOwnerCpf.length) {
        if (i != 3 && i != 6 && i != 9) {
          tempUserCpf = tempUserCpf + this.tempOwnerCpf[i];
        } else {
          if (i == 3 || i == 6) {
            tempUserCpf = tempUserCpf + '.' + this.tempOwnerCpf[i];
          } else if (i == 9) {
            tempUserCpf = tempUserCpf + '-' + this.tempOwnerCpf[i];
          } else {
            tempUserCpf = tempUserCpf + '•';
          }
        }
      } else {
        if (i != 3 && i != 6 && i != 9) {
          tempUserCpf = tempUserCpf + '•';
        } else {
          if (i == 3 || i == 6) {
            tempUserCpf = tempUserCpf + '.•';
          } else if (i == 9) {
            tempUserCpf = tempUserCpf + '-•';
          } else {
            tempUserCpf = tempUserCpf + '•';
          }
        }
      }
    }
    this.localCreditCard.ownerCPF = tempUserCpf;
  }

  ownerBirthDateInput() {
    this.localCreditCard.ownerBirthDate = `${this.tempOwnerBirthDate.split('-')[2]}/${this.tempOwnerBirthDate.split('-')[1]}/${this.tempOwnerBirthDate.split('-')[0]}`
  }

  validateCreditCardNumber() {
    this.UiFeedBackCtrl.presenLoader('Verificando...', 'assets/animations/credit-card.json')
      .then(() => {
        this.db.afs.collection('Users').doc(this.localUserId)
          .collection('CreditCards').ref.where(`number`, `==`, this.localCreditCard.number).get()
          .then(querySnapShot => {

            if (querySnapShot.empty) {

              this.pagseg.validateCreditCardNumber(this.localCreditCard.number, this.localCreditCard.brand)
                .then(cardIsValid => {
                  if (cardIsValid) {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.segment = 2;
                  } else {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentAlert('Dados invalidos!', 'Favor confira os dados inseridos e tente novamente!', 'warning', true);
                  }
                })

            } else {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentAlert('Numero do cartão Já cadastrado!', 'Você já possui um cartão com este numero cadastrado!', 'warning', true);
            }

          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao validar cartão', e);
          })
      })
  }



  save() {
    this.UiFeedBackCtrl.presenLoader('salvando...', 'assets/animations/credit-card.json')
      .then(() => {
        this.localCreditCard.id = this.db.createId();
        this.db.afs.collection('Users').doc(this.localUserId)
          .collection('CreditCards').doc(this.localCreditCard.id).set(Object.assign({}, this.localCreditCard))
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.navCtrl.pop();
            this.UiFeedBackCtrl.presentAlert('Cartão salvo com sucesso!', '', 'success', true);
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao salvar cartão', e);
          })
      })
  }

}