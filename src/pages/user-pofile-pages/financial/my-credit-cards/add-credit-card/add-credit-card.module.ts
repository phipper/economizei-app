import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCreditCardPage } from './add-credit-card';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    AddCreditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCreditCardPage),
    LottieAnimationViewModule
  ],
})
export class addCreditCardPageModule {}
