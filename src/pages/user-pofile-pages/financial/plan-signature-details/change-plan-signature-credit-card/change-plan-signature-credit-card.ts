import { Component, ViewChild } from '@angular/core';
import { trigger, transition, style, state, animate, keyframes } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Slides } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../../../providers/UserData';
import { CreditCard } from '../../../../../models/CreditCard';
import { UtilProvider } from '../../../../../providers/Util';
import { PagseguroPgtoServiceProvider } from '../../../../../providers/PagSeguro/PagSeguro';
import { Address } from '../../../../../models/Address';
import { User } from '../../../../../models/User';
import { PlanSignature } from '../../../../../models/PlanSignature';
import { EcoFirestore } from '../../../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
  selector: 'page-change-plan-signature-credit-card',
  templateUrl: 'change-plan-signature-credit-card.html',
  animations: [
    trigger('swipe', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(-361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ])))
    ])
  ]
})
export class ChangePlanSignatureCreditCardPage {

  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;
  state: string = 'x';
  swipeIndex: number = 0;

  showSplash = true;

  localUser: User = new User();

  segment = 'Cartão';

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/credit-card.json'
  }

  localPlanSignature: PlanSignature = new PlanSignature();


  tempCardCCV = '';
  selectedCreditCard: CreditCard = new CreditCard();
  localCreditCards: CreditCard[] = [];

  selectedAddress: Address = new Address();
  localAddresses: Address[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public UserDataCtrl: UserDataProvider,
    public pagseg: PagseguroPgtoServiceProvider,
    public db: EcoFirestore,
    public utilCtrl: UtilProvider
  ) {
    this.localPlanSignature = navParams.get('planSignature');
  }


  slideMoved() {
    if (this.swipeIndex != 0 && this.slides.getActiveIndex() >= this.slides.getPreviousIndex()) {
      this.state = 'rightSwipe';
      this.swipeIndex -= 361;
      this.slides.setElementStyle("transform", `translate3d(${this.swipeIndex}, 0px, 0px)`);
    }
    else {
      this.state = 'leftSwipe';
      this.swipeIndex += 361;
      this.slides.setElementStyle("transform", `translate3d(${this.swipeIndex}, 0px, 0px)`);
    }
    // console.log(this.slides.getActiveIndex());
    // console.log(this.state);
  }

  animationDone() {
    this.state = 'x';
  }


  scrollToTop() {
    this.content.scrollToTop();
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  ionViewDidEnter() {
    this.showSplash = true;
    this.UserDataCtrl.getUser()
      .then(user => {
        this.localUser = user;
        if (this.UserDataCtrl.localUser.status == 'active') {
          this.db.addresses.getUserAddresses(this.localUser.id)
            .then(addresses => {
              if (addresses.length == 0) {
                const alert = this.UiFeedBackCtrl.alertCtrl.create({
                  title: 'Não possuimos nehum endereço seu !',
                  subTitle: 'Você precisa cadastrar um endereço para Alterar o cartão da sua Assinatura. Deseja cadastrar agora?',
                  buttons: [
                    {
                      text: 'Sim',
                      handler: () => {
                        this.navCtrl.push('AddAddressPage');
                      }
                    },
                    'Não'
                  ]
                });
                this.navCtrl.pop();
                this.UiFeedBackCtrl.hapticCtrl.notification('warning');
                alert.present();
              } else {
                this.localAddresses = addresses;
                this.pagseg.init()
                  .then(() => {
                    this.db.afs.collection('Users').doc(this.localUser.id).collection('CreditCards').ref.get()
                      .then(ccQuery => {
                        this.db.validators.validateQuerySnapshot(ccQuery)
                          .then(ccQuery => {
                            this.localCreditCards = [];
                            for (let ccDoc of ccQuery.docs) {
                              this.localCreditCards.push(ccDoc.data() as CreditCard)
                            }
                            this.showSplash = false;
                          })
                      })
                  })
                  .catch(e => {
                    this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
                  })
              }
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
            })
        } else {
          this.navCtrl.pop();
          this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error', true);
        }
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
      })
  }

  cancel() {
    this.navCtrl.pop();
  }

  selectCC(cc: CreditCard) {
    if (cc.id != this.localPlanSignature.ceditCard.id) {
      const alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Confirmar Cartão',
        subTitle: `Você deseja utilizar o cartão de numero: •••• •••• •••• ${cc.number.substr(12, 4)} para a assinatura ?`,
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.UiFeedBackCtrl.hapticCtrl.selection();
              this.selectedCreditCard = cc;
            }
          },
          'Não'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();

    } else {
      this.UiFeedBackCtrl.presentAlert(`Este é o cartão atual!`, `Você não pode utilizar o cartão de numero: •••• •••• •••• ${cc.number.substr(12, 4)}, pois ele já é o cartão atual da assinatura`, `warning`);
    }
  }

  confirmCC_CCV() {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Confirmar Codigo de Segurança',
      subTitle: `Você confirma o Codigo de segurança: ${this.tempCardCCV} para o cartão de numero: •••• •••• •••• ${this.selectedCreditCard.number.substr(12, 4)}?`,
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.goForward();
          }
        },
        'Não'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  addCC() {
    this.navCtrl.push('AddCreditCardPage')
  }

  goBack() {
    switch (this.segment) {
      case 'Endereço':
        this.segment = 'Dados'
        this.scrollToTop();
        break;
      case 'Dados':
        this.segment = 'Cartão'
        this.scrollToTop();
        break;
    }
  }

  goForward() {
    switch (this.segment) {
      case 'Cartão':
        this.segment = 'Dados'
        this.scrollToTop();
        break;
      case 'Dados':
        this.segment = 'Endereço'
        this.scrollToTop();
        break;
    }
  }

  change() {
    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/credit-card.json')
      .then(() => {
        this.pagseg.changePlanSignatureCard(this.localPlanSignature.id, this.localUser, this.selectedAddress, this.selectedCreditCard, this.tempCardCCV)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presenLottietAlert(`Atenção!`, `Sua Solicitação de alteração foi cartão foi salva, estamos processando sua Solicitação!`, 'assets/animations/check_animation.json', 'success', true);
            this.navCtrl.pop();
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          })

      })
  }

}
