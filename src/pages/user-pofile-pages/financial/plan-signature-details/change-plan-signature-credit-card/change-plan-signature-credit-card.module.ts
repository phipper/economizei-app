import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangePlanSignatureCreditCardPage } from './change-plan-signature-credit-card';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ChangePlanSignatureCreditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangePlanSignatureCreditCardPage),
    LottieAnimationViewModule
  ],
})
export class ChangePlanSignatureCreditCardPageModule {}
