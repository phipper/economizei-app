import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, Content, NavController, NavParams, ActionSheetButton } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';

import { UserDataProvider } from '../../../../providers/UserData';
import { PagseguroPgtoServiceProvider } from '../../../../providers/PagSeguro/PagSeguro';
import { PlanSignature } from '../../../../models/PlanSignature';
import { UtilProvider } from '../../../../providers/Util';
import { PagSegPaymentOrder } from '../../../../providers/PagSeguro/models/PagSegPaymentOrder';
import { PagSegPaymentTransaction } from '../../../../providers/PagSeguro/models/PagSegPaymentTransaction';

@IonicPage()
@Component({
  selector: 'page-plan-signature-details',
  templateUrl: 'plan-signature-details.html',
})
export class PlanSignatureDetailsPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  segment = 'Detalhes';

  localPlanSignature: PlanSignature = new PlanSignature();

  localPlanPaymentOrders: PagSegPaymentOrder[] = []

  canLeaveCtrl = true;
  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/credit-card.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public pagseg: PagseguroPgtoServiceProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localPlanSignature = navParams.get('planSignature');
  }


  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;

        if (this.localPlanSignature.userId == this.localUserId) {
          this.pagseg.getPlanSignaturePaymentOrders(this.localPlanSignature.id)
            .then(paymentOrders => {
              this.localPlanPaymentOrders = paymentOrders;
              this.showSplash = false;
              if (this.localPlanSignature.status == 'PAYMENT_METHOD_CHANGE') {
                this.UiFeedBackCtrl.presentAlert(`Atenção: ${this.utilCtrl.translatePlanSignaturesStatus(this.localPlanSignature.status)}`, `Detalhes: ${this.utilCtrl.planSignaturesStatusDetail(this.localPlanSignature.status)}`, `warning`, true);
              }
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            })
        } else {
          this.navCtrl.pop();
          this.UiFeedBackCtrl.presentAlert('Acesso Negado!', 'Somente o usuário da Assinatura pode acessar esta pagina!', 'error', true);
        }

      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  TransactionDetail(T: PagSegPaymentTransaction) {
    this.UiFeedBackCtrl.presentAlert(`Status: ${this.utilCtrl.translatePaymentOrderTransactionStatus(T.status)}`, `Detalhes: ${this.utilCtrl.paymentOrderTransactionStatusDetail(T.status)}`, ``);
  }

  changeCreditCard() {
    this.navCtrl.push('ChangePlanSignatureCreditCardPage', {
      planSignature: this.localPlanSignature
    })
  }

  cancelPlanSignatureConfirmation() {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Tem Certeza?',
      subTitle: 'Você tem certeza que deseja cancelar a Assinatura?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.cancelPlanSignature();
          }
        },
        'Cancelar'
      ]
    });
    alert.present();
  }

  cancelPlanSignature() {
    this.UiFeedBackCtrl.presenLoader('Solicitando Cancelamento...', 'assets/animations/credit-card.json')
      .then(() => {
        this.pagseg.cancelPlanSignature(this.localPlanSignature.id)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert(`Cancelamento Solicitado!`, `Sua Assinatura será cancelada em-breve, enviaremos uma notificação para lhe avisar quando o cancelamento for concluido! Cobranças poderão ser devolvidas na proxima fatura do seu cartão.`, `success`, true);
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          })
      })
  }

  options() {
    let buttonsArray: ActionSheetButton[] = [];
    buttonsArray.push({
      text: 'Detalhes do Status da Assinatura',
      icon: !this.platform.is('ios') ? 'information-circle' : null,
      handler: () => {
        this.UiFeedBackCtrl.presentAlert(`Status: ${this.utilCtrl.translatePlanSignaturesStatus(this.localPlanSignature.status)}`, `Detalhes: ${this.utilCtrl.planSignaturesStatusDetail(this.localPlanSignature.status)}`, ``);
      }
    });
    if (this.localPlanSignature.status == 'ACTIVE' || this.localPlanSignature.status == 'PAYMENT_METHOD_CHANGE') {
      buttonsArray.push({
        text: 'Alterar cartão de credito',
        icon: !this.platform.is('ios') ? 'card' : null,
        handler: () => {
          this.changeCreditCard();
        }
      });
    }
    if (this.localPlanSignature.status == 'ACTIVE' || this.localPlanSignature.status == 'PAYMENT_METHOD_CHANGE') {
      buttonsArray.push({
        text: 'Cancelar Assinatura',
        icon: !this.platform.is('ios') ? 'close' : null,
        handler: () => {
          this.cancelPlanSignatureConfirmation();
        }
      });
    }
    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções da Assinatura',
      buttons: buttonsArray,
    });
    actionSheet.present();
  }


}