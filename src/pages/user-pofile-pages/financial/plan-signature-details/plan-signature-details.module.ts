import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanSignatureDetailsPage } from './plan-signature-details';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    PlanSignatureDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanSignatureDetailsPage),
    LottieAnimationViewModule
  ],
})
export class PlanSignatureDetailsPageModule {}
