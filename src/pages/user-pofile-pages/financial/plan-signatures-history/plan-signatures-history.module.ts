import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanSignaturesHistoryPage } from './plan-signatures-history';
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    PlanSignaturesHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanSignaturesHistoryPage),
    LottieAnimationViewModule
  ],
})
export class PlanSignaturesHistoryPageModule {}
