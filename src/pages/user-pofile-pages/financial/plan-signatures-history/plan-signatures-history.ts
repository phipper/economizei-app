import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../../providers/UserData';

import { UtilProvider } from '../../../../providers/Util';
import { PlanSignature } from '../../../../models/PlanSignature';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-plan-signatures-history',
  templateUrl: 'plan-signatures-history.html',
})
export class PlanSignaturesHistoryPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  showSplash = true;
  localPlanSignatures: PlanSignature[] = [];

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/credit-card.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'AddCreditCardPage') {
      this.content.scrollToTop();
    }
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.loadPlanSignatures();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }


  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    this.loadPlanSignatures();
    refresher.complete();
  }

  loadPlanSignatures() {
    this.showSplash = true;
    this.localPlanSignatures = [];
    this.db.afs.collection('PlanSignatures').ref
      .orderBy('createdOn', 'desc').where('userId', '==', this.localUserId).get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              this.localPlanSignatures.push(doc.data() as PlanSignature);
            }
            this.scrollToTop();
            this.showSplash = false;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
      });
  }

  PSDetail(PS: PlanSignature) {
    this.navCtrl.push('PlanSignatureDetailsPage', {
      planSignature: PS,
    })
  }


}
