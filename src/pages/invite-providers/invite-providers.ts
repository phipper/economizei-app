import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ActionSheetButton } from 'ionic-angular';
import { UserDataProvider } from '../../providers/UserData';
import { UiFeedBackProvider } from '../../providers/UifeedBack/UifeedBack';
import { ProviderInvite } from '../../models/ProviderInvite';
import { ProviderInviteUtilization } from '../../models/ProviderInviteUtilization';
import { DynamicLinksApiProvider } from '../../providers/DynamicLinksApi';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';
import { Plan } from '../../models/Plan';
import { PlanAssignment } from '../../models/PlanAssignment';
import { CoreProvider } from '../../providers/CoreProvider';
import { EcoFirestore } from '../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-invite-providers',
  templateUrl: 'invite-providers.html',
})
export class InviteProvidersPage {

  showSplash = true;

  localInvite: ProviderInvite = new ProviderInvite();

  inviteUtilizations: ProviderInviteUtilization[] = [];
  inviteUtilizationsSub: any = null;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/share.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    public platform: Platform,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private dynamicLinksCtrl: DynamicLinksApiProvider,
    public coreCtrl: CoreProvider
  ) {
  }

  initProviderInviteUtilizationSub() {
    this.inviteUtilizationsSub = this.db.afs.collection('ProviderInvites').doc(this.localInvite.id).collection('InviteUtilizations').ref
      .orderBy('createdOn')
      .onSnapshot(querySnapshot => {
        this.showSplash = true;
        this.inviteUtilizations = [];
        for (var i = 0; i < querySnapshot.docs.length; ++i) {
          this.inviteUtilizations.push(querySnapshot.docs[i].data() as ProviderInviteUtilization);
        }
        this.showSplash = false;
      },
        error => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro", error);
        });
  }

  createProviderInvite(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.localInvite.id = this.db.createId();
      this.localInvite.providerId = this.UserDataCtrl.localUser.id;
      this.localInvite.providerName = this.UserDataCtrl.localUser.fullName;
      this.localInvite.providerThumbnailUrl = this.UserDataCtrl.localUser.pic.url;
      this.dynamicLinksCtrl.createProviderInviteDynamicLink(this.localInvite.id, this.localInvite.providerName, this.localInvite.providerThumbnailUrl)
        .then(dynamicLink => {
          this.localInvite.dynamicLink = dynamicLink.shortLink;
          this.db.afs.collection('ProviderInvites').doc<ProviderInvite>(this.localInvite.id).set(Object.assign({}, this.localInvite))
            .then(() => {
              resolve();
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(uid => {
        this.db.afs.collection('ProviderInvites').ref
          .where('providerId', '==', uid).get()
          .then(query => {
            if (!query.empty) {
              this.localInvite = query.docs[0].data() as ProviderInvite;
              this.initProviderInviteUtilizationSub();
            } else {
              this.createProviderInvite()
                .then(() => {
                  this.initProviderInviteUtilizationSub();
                })
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })

  }

  ionViewDidLoad() {
    if (this.coreCtrl.platform.is(`cordova`)) {
      this.coreCtrl.hideStatusBar();
    }
  }

  ionViewWillLeave() {
    if (this.coreCtrl.platform.is(`cordova`)) {
      this.coreCtrl.showStatusBar();
    }
  }

  ionViewDidLeave() {
    if (this.inviteUtilizationsSub != null) {
      this.inviteUtilizationsSub();
    }
  }

  shareInvite() {

    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share(`Venha ser um profissional do Economizei, é de gratis!`, null, null, this.localInvite.dynamicLink);
      }
    });

    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy(this.localInvite.dynamicLink)
          .then(() => {
            this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', this.localInvite.dynamicLink, 'success');
          });
      }
    });

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Compartilhar Convite',
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

  useReward() {
    this.UiFeedBackCtrl.presenLoader('Resgatando')
      .then(() => {
        this.db.afs.collection('Plans').doc('ecobit').ref.get()
          .then(planDoc => {
            var ecobitPlan = planDoc.data() as Plan;
            var tempPlanAssignment = new PlanAssignment();
            tempPlanAssignment.id = this.db.createId();
            tempPlanAssignment.providerId = this.UserDataCtrl.localUser.id;
            tempPlanAssignment.plan = Object.assign({}, ecobitPlan);
            tempPlanAssignment.signatureId = `N/A Created by invite-providers Page`;
            this.db.afs.collection('PlanAssignments').doc<PlanAssignment>(tempPlanAssignment.id).set(Object.assign({}, tempPlanAssignment))
              .then(() => {
                this.localInvite.rewardUsed = true;
                this.db.afs.collection('ProviderInvites').doc<ProviderInvite>(this.localInvite.id).update({ rewardUsed: this.localInvite.rewardUsed })
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presenLottietAlert('Resgate Efetuado!', 'Bem-vindo ao plando Ecobit, Agradecemos a sua colaboração!', 'assets/animations/gift.json', 'success', true);
                  })
              })
              .catch(e => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
              })
          })
      })
  }


}
