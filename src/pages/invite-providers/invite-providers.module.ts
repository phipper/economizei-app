import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LottieAnimationViewModule } from 'ng-lottie';
import { InviteProvidersPage } from './invite-providers';

@NgModule({
  declarations: [
    InviteProvidersPage,
  ],
  imports: [
    IonicPageModule.forChild(InviteProvidersPage),
    LottieAnimationViewModule
  ],
})
export class InviteProvidersPageModule {}
