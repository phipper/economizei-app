import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBannersPage } from './my-banners';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyBannersPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBannersPage),
    LottieAnimationViewModule
  ],
})
export class MyBannersPageModule {}
