import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, Content, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../../providers/UserData';
import { SacProvider } from '../../../../providers/SAC';
import { UtilProvider } from '../../../../providers/Util';

import { Banner } from '../../../../models/Banner';
import { Characteristic } from '../../../../models/Characteristic';
import { Address } from '../../../..//models/Address';
import { Pic } from '../../../../models/Pic';
import { SAC } from '../../../../models/SAC';
import { PicProvider } from '../../../../providers/Pic';
import { LocalDbProvider } from '../../../../providers/LocalDbProvider';
import { UserPlanProvider } from '../../../../providers/UserPlan';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';
import { Plan } from '../../../../models/Plan';

@IonicPage()
@Component({
  selector: 'page-edit-banner',
  templateUrl: 'edit-banner.html',
})
export class EditBannerPage {
  @ViewChild(Content) content: Content;

  localUserPlan: Plan = new Plan();
  localBanner = new Banner();
  localPics: Pic[] = [];

  oldPicsIds: string[] = []
  deletedPicsFromDBNum = 0;

  picsToDelete: Pic[] = [];
  deletedPicsFromStorageNum = 0;

  picsToUpdate: Pic[] = [];
  uploadedPicsNum = 0;

  segment: string = "Dados";
  addresses: Address[] = [];
  descriptionCaracCountMin = 0;
  lastRadons: string[] = [];
  oldCharacteristicsIds: string[] = [];
  localCharacteristics: Characteristic[] = [];
  localCharacteristicsReady = true;
  localCharacteristicsModel: Characteristic[] = [];
  deletedCaracNum = 0;
  uploadedCaracNum = 0;

  localPrice = "";

  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private sacCtrl: SacProvider,
    private PicCtrl: PicProvider,
    private localDbCtrl: LocalDbProvider,
    private UserPlanCtrl: UserPlanProvider,
    private utilCtrl: UtilProvider
  ) {
    this.localBanner = navParams.get("banner");
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name != 'EditBannerPage') {
      this.content.scrollToTop();
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  ionViewDidEnter() {
    if (this.UserDataCtrl.localUser.verifiedProvider && this.UserDataCtrl.localUser.status == 'active') {
      this.db.addresses.getUserAddresses(this.UserDataCtrl.localUser.id)
        .then(addresses => {
          if (addresses.length == 0) {
            const alert = this.UiFeedBackCtrl.alertCtrl.create({
              title: 'Não possuimos nenhum endereço seu !',
              subTitle: 'Você precisa cadastrar um endereço para poder editar um anuncio. Deseja cadastrar agora?',
              buttons: [
                'Cancelar',
                {
                  text: 'Sim',
                  handler: () => {
                    this.navCtrl.push('AddAddressPage');
                  }
                }
              ]
            });
            this.navCtrl.pop();
            this.UiFeedBackCtrl.hapticCtrl.notification('error');
            alert.present();
          } else {
            this.addresses = addresses;

            this.UserPlanCtrl.getUserPlan()
              .then(plan => {

                this.localUserPlan = plan;
                this.localPrice = this.localBanner.price.toString();

                this.showSplash = false;

              })
              .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
              });
          }
        });

    } else if (this.UserDataCtrl.localUser.status == 'active') {
      this.navCtrl.pop();
      if (this.UserDataCtrl.localUser.providerVerificationSacId != '') {
        this.UiFeedBackCtrl.presentAlert('Ainda estamos verificando seus dados', 'Precisamos verificar os seus documentos antes de permitir a criação de anuncios para garantir a segurança de nossos usuários. Aguarde... esse processo demora no maximo 24horas, entraremos em contato com você se necessario.', 'warning')
      } else {
        const alert = this.UiFeedBackCtrl.alertCtrl.create({
          title: 'Você ainda não foi verificado !',
          subTitle: 'Você precisa nos enviar algums dados para que possamos verificar seu usuário de prestador. Esse processo é rápido e você só poderá fazer anuncios e receber pedidos depois de faze-lo. Deseja enviar agora?',
          buttons: [
            'Não',
            {
              text: 'Sim',
              handler: () => {
                this.navCtrl.push('ProviderVerificationPage');
              }
            }
          ]
        });
        this.UiFeedBackCtrl.hapticCtrl.notification('warning');
        alert.present();
      }
    } else {
      this.navCtrl.pop();
      this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error', true);
    }
  }

  loadPicsPage() {
    this.showSplash = true;
    this.localPics = [];
    this.db.banners.pics.getAll(this.localBanner.id)
      .then(pics => {
        this.localPics = pics;
        this.showSplash = false;
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar fotos', e);
      });
  }


  CheckPrice() {
    let priceValue = Number(this.localPrice);
    if (this.localPrice.indexOf(',', 0) != -1 || this.localPrice.indexOf('.', 0) != -1) {
      this.localBanner.price = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não pode conter casas decimais!", 'assets/animations/file_error.json', 'error');
    } else if (Number.isNaN(priceValue)) {
      this.localBanner.price = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não deve conter letras!", 'assets/animations/file_error.json', 'error');
    } else if (this.localPrice == "") {
      this.localBanner.price = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "Favor digite um preço valido!", 'assets/animations/file_error.json', 'error');
    } else if (priceValue <= 0) {
      this.localBanner.price = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço deve ser maior que 0!", 'assets/animations/file_error.json', 'error');
    } else {
      this.localBanner.price = priceValue;
    }
  }

  randomStr() {
    var s = '';
    var r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    do {
      for (var i = 0; i < 5; i++) {
        s += r.charAt(Math.floor(Math.random() * r.length));
      }
    } while (this.lastRadons.indexOf(s, 0) != -1)

    this.lastRadons.push(s);
    return s;
  };

  descriptionOnInput() {
    this.descriptionCaracCountMin = (20 - this.localBanner.description.length);
  }


  createSac(): Promise<void> {
    return new Promise((resolve, reject) => {
      let tempSac = new SAC();
      this.localDbCtrl.getEBVSModelData()
        .then(SacModelData => {

          tempSac.type = SacModelData.type;
          tempSac.priority = SacModelData.priority;
          tempSac.businessRole = SacModelData.businessRole;
          tempSac.category = SacModelData.category;
          tempSac.subCategory = SacModelData.subCategory;
          tempSac.assignedTeam = SacModelData.assignedTeam;

          tempSac.userId = this.UserDataCtrl.localUser.id;
          tempSac.userName = this.UserDataCtrl.localUser.fullName;
          tempSac.bannerId = this.localBanner.id;
          tempSac.title = `Verificação de Edição em Anuncio para: ${this.UserDataCtrl.localUser.firstName}`;
          tempSac.description = `Verificação de Anuncio para: ${this.UserDataCtrl.localUser.fullName},
                                      name: ${this.UserDataCtrl.localUser.fullName}, 
                                      cpf: ${this.UserDataCtrl.localUser.cpf}, 
                                      email: ${this.UserDataCtrl.localUser.email}, 
                                      cellPhone: ${this.UserDataCtrl.localUser.cellPhone}, 
                                      workPhone: ${this.UserDataCtrl.localUser.workPhone}, 
                                      birthDate: ${this.UserDataCtrl.localUser.birthDate}, 
                                      loginMethod: ${this.UserDataCtrl.localUser.loginMethod}, 
                                      job: ${this.UserDataCtrl.localUser.job}`;


          this.sacCtrl.saveSac(tempSac, 'RDS')
            .then(() => {
              resolve();
            })
            .catch(e => {
              reject(e);
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  saveDataConfirmation() {
    if (this.localBanner.status != 7) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: "ATENÇÃO !",
        subTitle: "Devido nossa politica de segurança se você alterar o dados do seu Anuncio ele deverá passar por uma verificação novamente! O processo de verificação leva no maximo 24h e seu anuncio ficará fora do ar enquanto isso! Deseja continuar?",
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.saveData();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      this.saveData();
    }

  }

  toggleWarranty() {
    this.localBanner.warranty = !this.localBanner.warranty;
  }

  saveData() {
    this.UiFeedBackCtrl.presenLoader(`Salvando dados...`)
      .then(() => {
        if (this.localBanner.price != null) {
          this.createSac()
            .then(() => {
              this.localBanner.status = 7;
              this.db.banners.update(this.localBanner.id, this.localBanner)
                .then(() => {
                  this.UiFeedBackCtrl.presentAlert("Alterações salvas!", "", 'success');
                  this.UiFeedBackCtrl.dismissLoader();
                  this.navCtrl.pop();
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentErrorAlert("Erro ao salvar alteraçoes do Banner favor tente novamente!", e);
                })
            })
            .catch(e => {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentErrorAlert("Erro ao criar SAC, favor contate nosso serviço de atendimento ao consumidor!", e);
            });
        } else {
          this.UiFeedBackCtrl.presentAlert("Erro", "Favor definir um preço valido!", 'success');
          this.UiFeedBackCtrl.dismissLoader();
        }
      });
  }

  saveAddressConfirmation() {
    for (let address of this.addresses) {
      if (address.id == this.localBanner.addressId) {
        let alert = this.UiFeedBackCtrl.alertCtrl.create({
          title: "Ótimo!",
          subTitle: "O edereço " + address.street + ", " + address.numero + " será utilizado para o seu novo anuncio!",
          buttons: [
            {
              text: 'Confirmar',
              handler: () => {
                this.saveAddress(address);
              }
            },
            'Cancelar'
          ]
        });
        this.UiFeedBackCtrl.hapticCtrl.notification('warning');
        alert.present();
      }
    }
  }

  saveAddress(address: Address) {
    this.UiFeedBackCtrl.presenLoader(`Atualizando endereço...`, `assets/animations/location.json`)
      .then(() => {
        this.localBanner.addressId = address.id;
        this.localBanner.address = Object.assign({}, address);
        this.db.banners.update(this.localBanner.id, { address: this.localBanner.address, addressId: this.localBanner.addressId })
          .then(() => {
            this.UiFeedBackCtrl.presentAlert("Feito!", "Endereço do Anuncio atualizado", 'success');
            this.UiFeedBackCtrl.dismissLoader();
            this.navCtrl.pop();
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro :(", e);
          });
      });
  }

  addPicFromCameraConfirmation() {

    if (this.localBanner.status != 9) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: "ATENÇÃO !",
        subTitle: "Devido nossa politica de segurança se você adicionar fotos ao seu Anuncio ele deverá passar por uma verificação novamente! O processo de verificação leva no maximo 24h e seu anuncio ficará fora do ar enquanto isso! Deseja continuar?",
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.addPicFromCamera();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      this.addPicFromCamera();
    }

  }

  addPicFromCamera() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('camera')
      .then(newPic => {
        let tempPic = newPic;
        tempPic.info = this.localBanner.id + '_' + this.randomStr() + '.jpeg';

        tempPic.index = this.localPics.length;
        this.localPics.push(tempPic);
        this.picsToUpdate.push(tempPic);

        this.UiFeedBackCtrl.dismissLoader();
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não tirou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  addPicFromGalleryConfirmation() {

    if (this.localBanner.status != 9) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: "ATENÇÃO !",
        subTitle: "Devido nossa politica de segurança se você adicionar fotos ao seu Anuncio ele deverá passar por uma verificação novamente! O processo de verificação leva no maximo 24h e seu anuncio ficará fora do ar enquanto isso! Deseja continuar?",
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.addPicFromGallery();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      this.addPicFromGallery();
    }

  }

  addPicFromGallery() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('galery')
      .then(newPic => {
        let tempPic = newPic;
        tempPic.info = this.localBanner.id + '_' + this.randomStr() + '.jpeg';

        tempPic.index = this.localPics.length;
        this.localPics.push(tempPic);
        this.picsToUpdate.push(tempPic);


        this.UiFeedBackCtrl.dismissLoader();
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não Selecionou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  deletePic(pic: Pic) {

    if (this.localPics.length > 1) {

      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: "Tem certeza?",
        subTitle: "",
        buttons: [
          {
            text: 'Sim',
            handler: () => {

              let loader = this.UiFeedBackCtrl.loadingCtrl.create({
                content: 'Excluindo...',
              });
              loader.present()
                .then(() => {
                  this.picsToDelete.push(pic);
                  loader.setContent("Atualizando dados...");
                  let tempPics: Pic[] = [];
                  this.localPics.forEach(picObject => {
                    if (picObject.info != pic.info) {
                      let tempPicObject = picObject;
                      tempPicObject.index = tempPics.length;
                      tempPics.push(tempPicObject);
                    }
                  });
                  this.localPics = tempPics;
                  loader.dismiss();
                });
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      this.UiFeedBackCtrl.presentAlert("Seu anuncio deve ter alguma foto!", "Você não pode deletar essa foto pois ela é a unica do seu Anuncio !", 'error');
    }
  }

  pics_IndexUp(pic: Pic) {

    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Alterando...',
    });

    loader.present()
      .then(() => {
        let originalIndex = pic.index;
        let tempPic = this.localPics[originalIndex];
        let tempPics: Pic[] = [];
        this.localPics.forEach(picObject => {
          if (picObject.info != pic.info) {
            tempPics.push(picObject);
          }
        });
        tempPic.index = originalIndex - 1;
        tempPics.splice(originalIndex - 1, 0, tempPic);
        this.localPics = tempPics;
        loader.dismiss();
      });

  }

  pics_IndexDown(pic: Pic) {

    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Alterando...',
    });

    loader.present()
      .then(() => {
        let originalIndex = pic.index;
        let tempPic = this.localPics[originalIndex];
        let tempPics: Pic[] = [];

        this.localPics.forEach(picObject => {
          if (picObject.info != pic.info) {
            tempPics.push(picObject);
          }
        });
        tempPic.index = originalIndex + 1;
        tempPics.splice(originalIndex + 1, 0, tempPic);
        this.localPics = tempPics;
        loader.dismiss();
      });
  }

  Pic_Options(pic: Pic) {

    let buttonsArray = [
      {
        text: 'Deletar',
        role: 'destructive',
        icon: !this.platform.is('ios') ? 'trash' : null,
        handler: () => {
          this.deletePic(pic);
        }
      }
    ];

    if (pic.index != 0) {
      buttonsArray.push({
        text: 'Mover para o ' + pic.index + 'ª lugar',
        role: null,
        icon: !this.platform.is('ios') ? 'arrow-dropup' : null,
        handler: () => {
          this.pics_IndexUp(pic);
        }
      });
    }

    if (pic.index != this.localPics.length - 1) {
      buttonsArray.push({
        text: 'Mover para o ' + (pic.index + 2) + 'ª lugar',
        role: null,
        icon: !this.platform.is('ios') ? 'arrow-dropdown' : null,
        handler: () => {
          this.pics_IndexDown(pic);
        }
      });
    }

    buttonsArray.push({
      text: "Cancelar",
      icon: null,
      role: "cancel",
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções da foto',
      buttons: buttonsArray,
    });
    actionSheet.present();
  }

  uploadPics(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (var i = 0; i < this.localPics.length; i++) {
        let tempPic = this.localPics[i];
        tempPic.index = i;
        this.PicCtrl.uploadBannertImg(tempPic.url, tempPic.info)
          .then(url => {
            tempPic.url = url;
            if (tempPic.index == 0) {
              this.localPics[0].url = tempPic.url;
            }
            this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).doc(this.localBanner.id).collection(this.db.COLLECTIONS.PICS_COLLECTION).add(Object.assign({}, tempPic))
              .then(() => {
                this.uploadedPicsNum++;
                if (this.localPics.length == this.uploadedPicsNum) {
                  resolve();
                }
              })
          })
          .catch(e => {
            reject({ log: 'erro ao fazer upload de uma imagem', error: JSON.stringify(e) });
          });
      }
      if (this.localPics.length == 0) {
        resolve();
      }
    });
  }

  deletePicsFromDB(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (let picId of this.oldPicsIds) {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).doc(this.localBanner.id).collection(this.db.COLLECTIONS.PICS_COLLECTION).doc(picId).delete()
          .then(() => {
            this.deletedPicsFromDBNum++;
            if (this.oldPicsIds.length == this.deletedPicsFromDBNum) {
              resolve();
            }
          })
          .catch(e => {
            reject({ log: 'erro ao deletar uma img', error: JSON.stringify(e) });
          });
      }
    })
  }

  deletePicsFromStorage(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.picsToDelete.length > 0) {
        for (let picToDel of this.picsToDelete) {
          this.PicCtrl.deleteBannertImg(picToDel.info)
            .then(() => {
              this.deletedPicsFromStorageNum++;
              if (this.deletedPicsFromStorageNum == this.picsToDelete.length) {
                resolve();
              }
            })
            .catch(e => {
              reject(e);
            })
        }

      } else {
        resolve();
      }
    })
  }

  savePics() {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Salvando...',
      dismissOnPageChange: true
    });
    loader.present()
      .then(() => {
        this.localBanner.status = 9;
        this.localBanner.thumbnail = this.localPics[0].thumbnailUrl;
        loader.setContent('atualizando Banner');
        this.db.afs.collection('Banners').doc<Banner>(this.localBanner.id).update({ status: this.localBanner.status, thumbnail: this.localPics[0].thumbnailUrl })
          .then(() => {
            loader.setContent('Criando SAC')
            this.createSac()
              .then(() => {
                loader.setContent('Apagando dados antigos');
                this.deletePicsFromDB()
                  .then(() => {
                    this.deletePicsFromStorage()
                      .then(() => {
                        loader.setContent('Fazendo Upload das imagens');
                        this.uploadPics()
                          .then(() => {
                            this.UiFeedBackCtrl.presentAlert("Alterações salvas!", "", 'success');
                            loader.dismiss();
                            this.navCtrl.pop();
                          })
                      })
                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao salvar fotos', e);
            loader.dismiss();
          });
      })
  }

  loadServiceCaracTab() {
    this.showSplash = true;
    this.localCharacteristicsModel = [];
    this.localCharacteristics = [];
    this.oldCharacteristicsIds = [];
    this.db.serviceTypes.getCharacteristics(this.localBanner.serviceTypeId)
      .then(serviceTypeCaracs => {
        this.localCharacteristicsModel = serviceTypeCaracs;

        this.db.banners.characteristics.getAll(this.localBanner.id)
          .then(bannerCaracs => {
            this.localCharacteristics = bannerCaracs;

            for (let oldCarac of bannerCaracs) {
              this.oldCharacteristicsIds.push(oldCarac.id);
            }

            this.scrollToTop();
            this.showSplash = false;
          });
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar Caracteristicas do tipo de serviço', e);
      });
  }

  findIndexByOrderNumber(CaracVector: Characteristic[], orderNumber: number): number {
    let count = 0;
    let foundIt = false;
    CaracVector.forEach(carac => {
      if (carac.orderNumber == orderNumber) {
        foundIt = true;
      } else if (!foundIt) {
        count++;
      }
    });
    if (foundIt) {
      return count;
    } else {
      return -1;
    }
  }

  answerCharac(characModelIndex: number) {

    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      subTitle: this.localCharacteristicsModel[characModelIndex].bcTitle + " (Selecione todas as opções que seu cliente poderá escolher)"
    });

    this.localCharacteristicsModel[characModelIndex].data.forEach(data => {
      alert.addInput({
        type: 'checkbox',
        label: data,
        value: data,
      });
    })

    alert.addButton({
      text: 'Cancelar',
      role: 'Cancel'
    });

    alert.addButton({
      text: 'Ok',
      handler: data => {
        if (data.length > 0) {

          let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, (characModelIndex + 1));

          this.localCharacteristics[localCharacIndex].data = data;

          if (this.localCharacteristicsModel.length == (characModelIndex + 1)) {
            this.localCharacteristicsReady = true;
            this.scrollToBottom();
            this.UiFeedBackCtrl.presentAlert("Bom trabalho!", "Caracteriticas do serviço preenchidas com sucesso!", 'success');
          } else {
            this.picCharacModel((characModelIndex + 1), (characModelIndex + 1));
            this.scrollToBottom();
          }
        } else {
          this.answerCharac(characModelIndex);
          this.UiFeedBackCtrl.presentAlert("Erro !", "Favor selecionar alguma Opção!", 'error');
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  picCharacModel(characModelIndex: number, orderNumber: number) {

    if (this.localCharacteristicsModel[characModelIndex].bcTitle == null) {

      let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

      if (this.localCharacteristics.length == (localCharacIndex + 1)) {
        this.localCharacteristics[(localCharacIndex + 1)] = this.localCharacteristicsModel[characModelIndex];
      } else {
        this.localCharacteristicsReady = false;
        var timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
        for (var i = 0; i < timesToPop; i++) {
          this.localCharacteristics.pop();
        }
        this.localCharacteristics[(localCharacIndex + 1)] = this.localCharacteristicsModel[characModelIndex];
      }


      if (this.localCharacteristicsModel.length == (characModelIndex + 1)) {
        this.localCharacteristicsReady = true;
        this.UiFeedBackCtrl.presentAlert("Bom trabalho!", "Caracteriticas do serviço preenchidas com sucesso!", 'success');
      } else {
        this.picCharacModel((characModelIndex + 1), (orderNumber + 1));
      }

    } else {
      if (!this.localCharacteristicsModel[characModelIndex].IsDependent) {

        if (this.localCharacteristicsModel.length >= (characModelIndex + 1)) {

          let tempCarac = new Characteristic();

          tempCarac.id = this.db.createId();
          tempCarac.title = this.localCharacteristicsModel[characModelIndex].title;
          tempCarac.bcTitle = this.localCharacteristicsModel[characModelIndex].bcTitle;
          tempCarac.orderNumber = this.localCharacteristicsModel[characModelIndex].orderNumber;
          tempCarac.IsDependent = this.localCharacteristicsModel[characModelIndex].IsDependent;
          tempCarac.rcDataSelection = this.localCharacteristicsModel[characModelIndex].rcDataSelection;
          tempCarac.dependencyCON = this.localCharacteristicsModel[characModelIndex].dependencyCON;
          tempCarac.dependencyD = this.localCharacteristicsModel[characModelIndex].dependencyD;

          tempCarac.data = [];

          let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

          if (this.localCharacteristics.length == (localCharacIndex + 1)) {
            this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
          } else {
            this.localCharacteristicsReady = false;
            let timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
            for (let i = 0; i < timesToPop; i++) {
              this.localCharacteristics.pop();
            }
            this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
          }

        } else {
          this.localCharacteristicsReady = true;
          this.UiFeedBackCtrl.presentAlert("Bom trabalho!", "Caracteriticas do serviço preenchidas com sucesso!", 'success');
        }

      } else {
        if ((this.localCharacteristics[(this.localCharacteristicsModel[characModelIndex].dependencyCON - 1)].data.indexOf(this.localCharacteristicsModel[characModelIndex].dependencyD, 0)) != (-1)) {

          if (this.localCharacteristicsModel.length >= (characModelIndex + 1)) {

            let tempCarac = new Characteristic();

            tempCarac.id = this.db.createId();
            tempCarac.title = this.localCharacteristicsModel[characModelIndex].title;
            tempCarac.bcTitle = this.localCharacteristicsModel[characModelIndex].bcTitle;
            tempCarac.orderNumber = this.localCharacteristicsModel[characModelIndex].orderNumber;
            tempCarac.IsDependent = this.localCharacteristicsModel[characModelIndex].IsDependent;
            tempCarac.rcDataSelection = this.localCharacteristicsModel[characModelIndex].rcDataSelection;
            tempCarac.dependencyCON = this.localCharacteristicsModel[characModelIndex].dependencyCON;
            tempCarac.dependencyD = this.localCharacteristicsModel[characModelIndex].dependencyD;

            tempCarac.data = [];

            let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

            if (this.localCharacteristics.length == (localCharacIndex + 1)) {
              this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
            } else {
              this.localCharacteristicsReady = false;
              let timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
              for (let i = 0; i < timesToPop; i++) {
                this.localCharacteristics.pop();
              }
              this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
            }

          } else {
            this.localCharacteristicsReady = true;
            this.UiFeedBackCtrl.presentAlert("Bom trabalho!", "Caracteriticas do serviço preenchidas com sucesso!", 'success');
          }

        } else {

          if (characModelIndex + 1 < this.localCharacteristicsModel.length) {
            this.picCharacModel((characModelIndex + 1), orderNumber);
          } else if (characModelIndex + 1 == this.localCharacteristicsModel.length) {
            this.localCharacteristicsReady = true;
            this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
          }

        }
      }
    }
  }


  private deleteCaracs(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (let caracId of this.oldCharacteristicsIds) {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).doc(this.localBanner.id).collection(this.db.COLLECTIONS.CHARACTERISTICS_COLLECTION).doc(caracId).delete()
          .then(() => {
            this.deletedCaracNum++;
            if (this.oldCharacteristicsIds.length == this.deletedCaracNum) {
              resolve();
            }
          })
          .catch(e => {
            reject({ log: 'erro ao deletar uma caracteristica do serviço', error: JSON.stringify(e) });
          });
      }
    });
  }

  private uploadCaracs(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (let carac of this.localCharacteristics) {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).doc(this.localBanner.id).collection(this.db.COLLECTIONS.CHARACTERISTICS_COLLECTION).doc(carac.id).set(Object.assign({}, carac))
          .then(() => {
            this.uploadedCaracNum++;
            if (this.localCharacteristics.length == this.uploadedCaracNum) {
              resolve();
            }
          })
          .catch(e => {
            reject({ log: 'erro ao fazer upload de uma caracteristica do serviço', error: JSON.stringify(e) });
          });
      }
    });
  }

  saveCharacs() {
    this.UiFeedBackCtrl.presenLoader('Atualizando caracteristicas...')
      .then(() => {
        this.UiFeedBackCtrl.updateLoaderMessage("Excluindo dados antigos...");
        this.deleteCaracs()
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage("Enviando dados novos...");
            this.uploadCaracs()
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage("Atualizando dados dos Banners!");
                this.utilCtrl.generateKeyWords(this.localCharacteristics)
                  .then(newKeyWords => {

                    this.db.banners.update(this.localBanner.id, { keyWords: newKeyWords })
                      .then(() => {
                        this.UiFeedBackCtrl.dismissLoader();
                        this.navCtrl.pop();
                        this.UiFeedBackCtrl.presentAlert('Dados atualizados com sucesso!', 'As caracteristicas do serviço anunciado foram atualizadas com sucesso!', 'success');
                      })

                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao salvar Caracteristicas', e);
          })
      });
  }

}
