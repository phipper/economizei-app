import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditBannerPage } from './edit-banner';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    EditBannerPage,
  ],
  imports: [
    IonicPageModule.forChild(EditBannerPage),
    LottieAnimationViewModule
  ],
})
export class EditBannerPageModule {}
