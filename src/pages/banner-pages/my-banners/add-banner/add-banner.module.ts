import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBannerPage } from './add-banner';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    AddBannerPage,
  ],
  imports: [
    IonicPageModule.forChild(AddBannerPage),
    LottieAnimationViewModule
  ],
})
export class AddBannerPageModule {}
