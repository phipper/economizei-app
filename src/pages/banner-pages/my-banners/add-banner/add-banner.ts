import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, Content, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { DynamicLinksApiProvider } from '../../../../providers/DynamicLinksApi';
import { SacProvider } from '../../../../providers/SAC';
import { PicProvider } from '../../../../providers/Pic';
import { UtilProvider } from '../../../../providers/Util';

import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';
import { UserDataProvider } from '../../../../providers/UserData';

import { Pic } from '../../../../models/Pic';
import { Banner } from '../../../../models/Banner';
import { Category } from '../../../../models/Category';
import { ServiceType } from '../../../../models/ServiceType';
import { Characteristic } from '../../../../models/Characteristic';
import { Address } from '../../../../models/Address';
import { UserPlanProvider } from '../../../../providers/UserPlan';
import { SAC } from '../../../../models/SAC';
import { LocalDbProvider } from '../../../../providers/LocalDbProvider';
import { Plan } from '../../../../models/Plan';

@IonicPage()
@Component({
  selector: 'page-add-banner',
  templateUrl: 'add-banner.html',
})
export class AddBannerPage {
  @ViewChild(Content) content: Content;

  localUserPlan: Plan = new Plan();
  localBanner = new Banner();
  tempPics: Pic[] = [];
  uploadedPicsNum = 0;

  localCategories: Category[] = [];
  selectedCategory: Category = null;
  localServiceTypes: ServiceType[] = [];
  serviceTypesToSelection: ServiceType[] = [];
  selectedServiceType: ServiceType = null;

  canSelectServiceType = false;

  serviceTypes: ServiceType[] = [];
  segment: string = 'Dados';
  addresses: Address[] = [];
  descriptionCaracCountMin = 0;
  lastRadons: string[] = [];
  localCharacteristics: Characteristic[] = [];
  localCharacteristicsReady = false;
  localCharacteristicsModel: Characteristic[] = [];
  uploadedCaracNum = 0;
  canLeaveCtrl = true;
  localPrice = '';
  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/newspaper.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private dynamicLinksApi: DynamicLinksApiProvider,
    private sacCtrl: SacProvider,
    private UserPlanCtrl: UserPlanProvider,
    private PicCtrl: PicProvider,
    private localDbCtrl: LocalDbProvider,
    private utilCtrl: UtilProvider
  ) {
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name != 'AddBannerPage') {
      this.content.scrollToTop();
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  ionViewDidEnter() {
    if (this.UserDataCtrl.localUser.verifiedProvider && this.UserDataCtrl.localUser.status == 'active') {
      this.db.addresses.getUserAddresses(this.UserDataCtrl.localUser.id)
        .then(addresses => {
          if (addresses.length == 0) {
            const alert = this.UiFeedBackCtrl.alertCtrl.create({
              title: 'Não possuimos nenhum endereço seu !',
              subTitle: 'Você precisa cadastrar um endereço para poder criar um anuncio. Deseja cadastrar agora?',
              buttons: [
                {
                  text: 'Sim',
                  handler: () => {
                    this.navCtrl.push('AddAddressPage');
                  }
                },
                'Cancelar'
              ]
            });
            this.navCtrl.pop();
            this.UiFeedBackCtrl.hapticCtrl.notification('error');
            alert.present();
          } else {
            this.addresses = addresses;
            this.UserPlanCtrl.canCreateBanner(this.UserDataCtrl.localUser)
              .then(canCreateBanner => {
                if (canCreateBanner) {

                  this.UserPlanCtrl.getUserPlan()
                    .then(plan => {

                      this.localUserPlan = plan;

                      this.localBanner.id = this.db.createId();
                      this.localBanner.addressId = this.UserDataCtrl.localUser.workAdressId;
                      this.loadCategories();

                    })
                    .catch(e => {
                      this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                    });

                } else {
                  this.UserPlanCtrl.getUserPlan()
                    .then(plan => {
                      this.navCtrl.pop();
                      this.UiFeedBackCtrl.presentAlert(`Numero máximo de Anuncios atingido para o plano ${plan.title} :(`,
                        `O plano ${plan.title} permite somente ${plan.maxNumOfBanners} anuncios não finalizados por vez, ê precisa finalizar algum Anuncio para poder criar um novo!`, 'error');
                    })
                    .catch(e => {
                      this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                    });
                }
              })
              .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
              });
          }
        });
    } else if (this.UserDataCtrl.localUser.status == 'active') {
      this.navCtrl.pop();
      if (this.UserDataCtrl.localUser.providerVerificationSacId != '') {
        this.UiFeedBackCtrl.presentAlert('Ainda estamos verificando seus dados', 'Precisamos verificar os seus documentos antes de permitir a criação de anuncios para garantir a segurança de nossos usuários. Aguarde... esse processo demora no maximo 24horas, entraremos em contato com você se necessario.', 'warning')
      } else {
        const alert = this.UiFeedBackCtrl.alertCtrl.create({
          title: 'Você ainda não foi verificado !',
          subTitle: 'Você precisa nos enviar algums dados para que possamos verificar seu usuário de prestador. Esse processo é rápido e você só poderá fazer anuncios e receber pedidos depois de faze-lo. Deseja enviar agora?',
          buttons: [
            {
              text: 'Sim',
              handler: () => {
                this.navCtrl.push('ProviderVerificationPage');
              }
            },
            'Não'
          ]
        });
        this.UiFeedBackCtrl.hapticCtrl.notification('warning');
        alert.present();
      }
    } else {
      this.navCtrl.pop();
      this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error', true);
    }
  }

  loadCategories() {
    if (this.platform.is('cordova')) {
      console.log('AddBannerPage says: É cordova');
      this.localDbCtrl.getCategories()
        .then(categories => {
          console.log('AddBannerPage says: categories Data received from local DB');
          this.localCategories = categories;
          this.loadServicesTypes();
        })
    } else {
      console.log('AddBannerPage says: Não É cordova');

      this.db.categories.getAll()
        .then(categories => {
          console.log('AddBannerPage says: categories Data received from Firebase');
          this.localCategories = categories;

          this.loadServicesTypes();
        });
    }
  }

  loadServicesTypes() {
    this.db.serviceTypes.getAll()
      .then(servicesTypes => {
        console.log('AddBannerPage says: ServiceTypes Data received from Firebase');
        this.localServiceTypes = servicesTypes;
        this.showSplash = false;
      });
  }

  ionViewCanLeave(): boolean {
    if (!this.canLeaveCtrl) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Tem certeza?',
        subTitle: 'Você quer descartar todos os dados e cancelar a criação de seu Anuncio?',
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              this.canLeaveCtrl = true;
              this.navCtrl.pop();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      return true;
    }
    return false;
  }

  selectCategory() {
    if (this.selectedCategory != null) {
      this.canLeaveCtrl = false;
      this.selectedServiceType = null
      this.serviceTypesToSelection = [];
      this.localCharacteristicsReady = false;

      let tempSelectServices = this.localServiceTypes.filter((servicesType) => {
        return (this.selectedCategory.serviceTypesIds.indexOf(servicesType.id) != -1);
      });

      this.serviceTypesToSelection = tempSelectServices;
      this.canSelectServiceType = true;

      this.scrollToBottom();
    }
  }

  selectServiceType() {
    this.localCharacteristicsReady = false;
    this.scrollToBottom();
  }

  CheckPrice() {
    let priceValue = Number(this.localPrice);
    if (this.localPrice.indexOf(',', 0) != -1 || this.localPrice.indexOf('.', 0) != -1) {
      this.localBanner.price = null;
      this.localPrice = '';
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não pode conter casas decimais!", 'assets/animations/file_error.json', 'error');
    } else if (Number.isNaN(priceValue)) {
      this.localBanner.price = null;
      this.localPrice = '';
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não deve conter letras!", 'assets/animations/file_error.json', 'error');
    } else if (this.localPrice == '') {
      this.localBanner.price = null;
      this.localPrice = '';
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "Favor digite um preço valido!", 'assets/animations/file_error.json', 'error');
    } else if (priceValue <= 0) {
      this.localBanner.price = null;
      this.localPrice = '';
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço deve ser maior que 0!", 'assets/animations/file_error.json', 'error');
    } else {
      this.localBanner.price = priceValue;
    }
  }

  randomStr() {
    var s = '';
    var r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    do {
      for (var i = 0; i < 5; i++) {
        s += r.charAt(Math.floor(Math.random() * r.length));
      }
    } while (this.lastRadons.indexOf(s, 0) != -1)

    this.lastRadons.push(s);
    return s;
  };

  descriptionOnInput() {
    this.descriptionCaracCountMin = (20 - this.localBanner.description.length);
  }

  goBack() {
    switch (this.segment) {
      case 'Serviço':
        this.segment = 'Dados'
        break;

      case 'Endereço':
        this.segment = 'Serviço'
        break;

      case 'Fotos':
        this.segment = 'Endereço'
        break;

    }
  }

  toggleWarranty() {
    this.localBanner.warranty = !this.localBanner.warranty;
  }

  toggleAcceptHomeDelivery() {
    this.localBanner.acceptHomeDelivery = !this.localBanner.acceptHomeDelivery;
  }

  loadServiceCaracTab() {
    this.showSplash = true;
    this.localCharacteristicsModel = [];
    this.localCharacteristics = [];

    this.db.serviceTypes.getCharacteristics(this.selectedServiceType.id)
      .then(characteristics => {

        this.localCharacteristicsModel = characteristics;

        let tempCarac = new Characteristic();

        tempCarac.id = this.db.createId();
        tempCarac.title = this.localCharacteristicsModel[0].title;
        tempCarac.bcTitle = this.localCharacteristicsModel[0].bcTitle;
        tempCarac.orderNumber = this.localCharacteristicsModel[0].orderNumber;
        tempCarac.IsDependent = this.localCharacteristicsModel[0].IsDependent;
        tempCarac.rcDataSelection = this.localCharacteristicsModel[0].rcDataSelection;
        tempCarac.dependencyCON = this.localCharacteristicsModel[0].dependencyCON;
        tempCarac.dependencyD = this.localCharacteristicsModel[0].dependencyD;

        tempCarac.data = [];

        this.localCharacteristics.push(tempCarac);
        this.scrollToTop();
        this.showSplash = false;

      });
  }

  saveData() {
    this.UiFeedBackCtrl.presenLoader('Verificando dados...')
      .then(() => {
        if (this.selectedServiceType.id != '' && this.localBanner.price != null) {

          this.localBanner.serviceTypeId = this.selectedServiceType.id;
          this.localBanner.serviceTypeTitle = this.selectedServiceType.title;
          this.localBanner.serviceTypeSymbol = this.selectedServiceType.symbol;
          this.localBanner.addressId = this.UserDataCtrl.localUser.workAdressId;
          this.localBanner.providerId = this.UserDataCtrl.localUser.id;
          this.localBanner.requiredHomeDelivery = this.selectedServiceType.requiredHomeDelivery;
          this.localBanner.acceptHomeDelivery = this.selectedServiceType.acceptHomeDelivery;
          this.localBanner.providerThumbnailUrl = this.UserDataCtrl.localUser.pic.thumbnailUrl;

          if (!this.UserDataCtrl.localUser.bannerCriationTutorial)
            this.UiFeedBackCtrl.presentAlert('Dados salvos!', 'O proximo passo é definir um endereço para usarmos como base para esse Anúncio. Como padrão utilizamos o seu endereço de trabalho.', 'success', true);

          this.canLeaveCtrl = false;
          if (!this.localCharacteristicsReady) {
            this.loadServiceCaracTab();
          }
          this.UiFeedBackCtrl.dismissLoader();
          this.segment = 'Serviço';

        } else {
          if (this.localBanner.price == null) {
            this.UiFeedBackCtrl.presentAlert('Erro', 'Favor definir um preço valido!', 'error', true);
            this.UiFeedBackCtrl.dismissLoader();
          } else {
            this.UiFeedBackCtrl.presentAlert('Erro', 'Favor selecionar um tipo de serviço!', 'error', true);
            this.UiFeedBackCtrl.dismissLoader();
          }
        }
      });
  }

  saveCarac() {
    this.segment = 'Endereço';
  }

  saveAddress() {
    const selectedAdress = this.addresses.filter((addres) => {
      return (addres.id == this.localBanner.addressId);
    });

    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Ótimo!',
      subTitle: 'O edereço ' + selectedAdress[0].street + ', ' + selectedAdress[0].numero + ' será utilizado para o seu novo anuncio!',
      buttons: [
        {
          text: 'Confirmar',
          handler: () => {
            this.localBanner.address = Object.assign({}, selectedAdress[0]);
            if (!this.UserDataCtrl.localUser.bannerCriationTutorial)
              this.UiFeedBackCtrl.presentAlert('Feito!', 'Agora chegou a hora de colocar algumas fotos no seu anuncio. Dica: utilize fotos de serviços anteriores para os seus clientes verem como você é um ótimo profissional! ;)', 'success', true);
            this.segment = 'Fotos';
            this.scrollToTop();
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  addPicFromCamera() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('camera')
      .then(newPic => {
        let tempPic = newPic;
        tempPic.info = this.localBanner.id + '_' + this.randomStr() + '.jpeg';
        this.tempPics.push(tempPic);
        this.UiFeedBackCtrl.dismissLoader();
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não tirou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  addPicFromGallery() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('galery')
      .then(newPic => {
        let tempPic = newPic;
        tempPic.info = this.localBanner.id + '_' + this.randomStr() + '.jpeg';
        this.tempPics.push(tempPic);
        this.UiFeedBackCtrl.dismissLoader();
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não selecionou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  deletePic(pic: Pic) {

    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Tem certeza?',
      subTitle: '',
      buttons: [
        {
          text: 'Sim',
          handler: () => {

            this.UiFeedBackCtrl.presenLoader('Excluindo...')
              .then(() => {

                this.UiFeedBackCtrl.updateLoaderMessage('Atualizando dados...');
                let tempPics: Pic[] = [];
                this.tempPics.forEach(picObject => {
                  if (picObject.info != pic.info) {
                    tempPics.push(picObject);
                  }
                });
                this.tempPics = tempPics;
                this.UiFeedBackCtrl.dismissLoader();

              });
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  pics_findIndexByInfo(picInfo: string): number {
    let index = 0;
    let foundIt = false;
    this.tempPics.forEach(picObject => {
      if (picObject.info == picInfo) {
        foundIt = true;
      } else if (!foundIt) {
        index++;
      }
    });
    if (foundIt) {
      return index;
    } else {
      return -1;
    }
  }

  pics_IndexUp(pic: Pic) {

    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/pic.json')
      .then(() => {
        let originalIndex = this.pics_findIndexByInfo(pic.info);
        let tempPic = this.tempPics[originalIndex];
        let tempPics: Pic[] = [];
        this.tempPics.forEach(picObject => {
          if (picObject.info != pic.info) {
            tempPics.push(picObject);
          }
        });
        tempPics.splice(originalIndex - 1, 0, tempPic);
        this.tempPics = tempPics;
        this.UiFeedBackCtrl.dismissLoader();
      });

  }

  pics_IndexDown(pic: Pic) {

    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/pic.json')
      .then(() => {
        let originalIndex = this.pics_findIndexByInfo(pic.info);
        let tempPic = this.tempPics[originalIndex];
        let tempPics: Pic[] = [];
        this.tempPics.forEach(picObject => {
          if (picObject.info != pic.info) {
            tempPics.push(picObject);
          }
        });
        tempPics.splice(originalIndex + 1, 0, tempPic);
        this.tempPics = tempPics;
        this.UiFeedBackCtrl.dismissLoader();
      });
  }

  Pic_Options(pic: Pic) {

    let buttonsArray = [
      {
        text: 'Deletar',
        role: 'destructive',
        icon: !this.platform.is('ios') ? 'trash' : null,
        handler: () => {
          this.deletePic(pic);
        }
      }
    ];

    if (this.pics_findIndexByInfo(pic.info) != 0) {
      buttonsArray.push({
        text: 'Mover para o ' + this.pics_findIndexByInfo(pic.info) + 'ª lugar',
        role: null,
        icon: !this.platform.is('ios') ? 'arrow-dropup' : null,
        handler: () => {
          this.pics_IndexUp(pic);
        }
      });
    }

    if (this.pics_findIndexByInfo(pic.info) != this.tempPics.length - 1) {
      buttonsArray.push({
        text: 'Mover para o ' + (this.pics_findIndexByInfo(pic.info) + 2) + 'ª lugar',
        role: null,
        icon: !this.platform.is('ios') ? 'arrow-dropdown' : null,
        handler: () => {
          this.pics_IndexDown(pic);
        }
      });
    }

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções da foto',
      buttons: buttonsArray,
    });
    actionSheet.present();
  }

  findIndexByOrderNumber(CaracVector: Characteristic[], orderNumber: number): number {
    let count = 0;
    let foundIt = false;
    CaracVector.forEach(carac => {
      if (carac.orderNumber == orderNumber) {
        foundIt = true;
      } else if (!foundIt) {
        count++;
      }
    });
    if (foundIt) {
      return count;
    } else {
      return -1;
    }
  }

  answerCharac(characModelIndex: number) {

    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      subTitle: this.localCharacteristicsModel[characModelIndex].bcTitle + ' (Selecione todas as opções que seu cliente poderá escolher)'
    });

    this.localCharacteristicsModel[characModelIndex].data.forEach(data => {
      alert.addInput({
        type: 'checkbox',
        label: data,
        value: data,
      });
    })

    alert.addButton({
      text: 'Cancelar',
      role: 'Cancel'
    });

    alert.addButton({
      text: 'Ok',
      handler: data => {
        if (data.length > 0) {

          let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, (characModelIndex + 1));

          this.localCharacteristics[localCharacIndex].data = data;

          if (this.localCharacteristicsModel.length == (characModelIndex + 1)) {
            this.localCharacteristicsReady = true;
            this.scrollToBottom();
            this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
          } else {
            this.picCharacModel((characModelIndex + 1), (characModelIndex + 1));
            this.scrollToBottom();
          }
        } else {
          this.answerCharac(characModelIndex);
          this.UiFeedBackCtrl.presentAlert('Erro !', 'Favor selecionar alguma Opção!', 'error');
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  picCharacModel(characModelIndex: number, orderNumber: number) {

    if (this.localCharacteristicsModel[characModelIndex].bcTitle == null) {

      let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

      if (this.localCharacteristics.length == (localCharacIndex + 1)) {
        this.localCharacteristics[(localCharacIndex + 1)] = this.localCharacteristicsModel[characModelIndex];
      } else {
        this.localCharacteristicsReady = false;
        var timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
        for (var i = 0; i < timesToPop; i++) {
          this.localCharacteristics.pop();
        }
        this.localCharacteristics[(localCharacIndex + 1)] = this.localCharacteristicsModel[characModelIndex];
      }


      if (this.localCharacteristicsModel.length == (characModelIndex + 1)) {
        this.localCharacteristicsReady = true;
        this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
      } else {
        this.picCharacModel((characModelIndex + 1), (orderNumber + 1));
      }

    } else {
      if (!this.localCharacteristicsModel[characModelIndex].IsDependent) {

        if (this.localCharacteristicsModel.length >= (characModelIndex + 1)) {

          let tempCarac = new Characteristic();

          tempCarac.id = this.db.createId();
          tempCarac.title = this.localCharacteristicsModel[characModelIndex].title;
          tempCarac.bcTitle = this.localCharacteristicsModel[characModelIndex].bcTitle;
          tempCarac.orderNumber = this.localCharacteristicsModel[characModelIndex].orderNumber;
          tempCarac.IsDependent = this.localCharacteristicsModel[characModelIndex].IsDependent;
          tempCarac.rcDataSelection = this.localCharacteristicsModel[characModelIndex].rcDataSelection;
          tempCarac.dependencyCON = this.localCharacteristicsModel[characModelIndex].dependencyCON;
          tempCarac.dependencyD = this.localCharacteristicsModel[characModelIndex].dependencyD;

          tempCarac.data = [];

          let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

          if (this.localCharacteristics.length == (localCharacIndex + 1)) {
            this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
          } else {
            this.localCharacteristicsReady = false;
            let timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
            for (let i = 0; i < timesToPop; i++) {
              this.localCharacteristics.pop();
            }
            this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
          }

        } else {
          this.localCharacteristicsReady = true;
          this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
        }

      } else {
        if ((this.localCharacteristics[(this.localCharacteristicsModel[characModelIndex].dependencyCON - 1)].data.indexOf(this.localCharacteristicsModel[characModelIndex].dependencyD, 0)) != (-1)) {

          if (this.localCharacteristicsModel.length >= (characModelIndex + 1)) {

            let tempCarac = new Characteristic();

            tempCarac.id = this.db.createId();
            tempCarac.title = this.localCharacteristicsModel[characModelIndex].title;
            tempCarac.bcTitle = this.localCharacteristicsModel[characModelIndex].bcTitle;
            tempCarac.orderNumber = this.localCharacteristicsModel[characModelIndex].orderNumber;
            tempCarac.IsDependent = this.localCharacteristicsModel[characModelIndex].IsDependent;
            tempCarac.rcDataSelection = this.localCharacteristicsModel[characModelIndex].rcDataSelection;
            tempCarac.dependencyCON = this.localCharacteristicsModel[characModelIndex].dependencyCON;
            tempCarac.dependencyD = this.localCharacteristicsModel[characModelIndex].dependencyD;

            tempCarac.data = [];

            let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

            if (this.localCharacteristics.length == (localCharacIndex + 1)) {
              this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
            } else {
              this.localCharacteristicsReady = false;
              let timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
              for (let i = 0; i < timesToPop; i++) {
                this.localCharacteristics.pop();
              }
              this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
            }

          } else {
            this.localCharacteristicsReady = true;
            this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
          }

        } else {
          if (characModelIndex + 1 < this.localCharacteristicsModel.length) {
            this.picCharacModel((characModelIndex + 1), orderNumber);
          } else if (characModelIndex + 1 == this.localCharacteristicsModel.length) {
            this.localCharacteristicsReady = true;
            this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
          }
        }
      }
    }
  }

  createSac() {
    return new Promise((resolve, reject) => {
      let tempSac = new SAC();
      this.localDbCtrl.getNBVSModelData()
        .then(SacModelData => {

          tempSac.type = SacModelData.type;
          tempSac.priority = SacModelData.priority;
          tempSac.businessRole = SacModelData.businessRole;
          tempSac.category = SacModelData.category;
          tempSac.subCategory = SacModelData.subCategory;
          tempSac.assignedTeam = SacModelData.assignedTeam;

          tempSac.userId = this.UserDataCtrl.localUser.id;
          tempSac.userName = this.UserDataCtrl.localUser.fullName;
          tempSac.bannerId = this.localBanner.id;
          tempSac.title = `Verificação de Anuncio para: ${this.UserDataCtrl.localUser.firstName}`;
          tempSac.description = `Verificação de Anuncio para: ${this.UserDataCtrl.localUser.fullName},
                                      name: ${this.UserDataCtrl.localUser.fullName}, 
                                      cpf: ${this.UserDataCtrl.localUser.cpf}, 
                                      email: ${this.UserDataCtrl.localUser.email}, 
                                      cellPhone: ${this.UserDataCtrl.localUser.cellPhone}, 
                                      workPhone: ${this.UserDataCtrl.localUser.workPhone}, 
                                      birthDate: ${this.UserDataCtrl.localUser.birthDate}, 
                                      loginMethod: ${this.UserDataCtrl.localUser.loginMethod}, 
                                      job: ${this.UserDataCtrl.localUser.job}`;


          return this.sacCtrl.saveSac(tempSac, 'RDS')
            .then(() => {
              resolve();
            })
            .catch(e => {
              reject(e);
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }


  private uploadPics(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (var i = 0; i < this.tempPics.length; i++) {
        let tempPic = this.tempPics[i];
        tempPic.index = i;
        this.PicCtrl.uploadBannertImg(tempPic.url, tempPic.info)
          .then(url => {
            tempPic.url = url;
            if (tempPic.index == 0) {
              this.tempPics[0].url = tempPic.url;
            }
            this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).doc(this.localBanner.id).collection(this.db.COLLECTIONS.PICS_COLLECTION).add(Object.assign({}, tempPic))
              .then(() => {
                this.uploadedPicsNum++;
                if (this.tempPics.length == this.uploadedPicsNum) {
                  resolve();
                }
              })
          })
          .catch(e => {
            reject(e)
          });
      }
      if (this.tempPics.length == 0) {
        resolve();
      }
    });
  }

  private uploadCaracs(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (let carac of this.localCharacteristics) {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).doc(this.localBanner.id).collection(this.db.COLLECTIONS.CHARACTERISTICS_COLLECTION).doc(carac.id).set(Object.assign({}, carac))
          .then(() => {
            this.uploadedCaracNum++;
            if (this.localCharacteristics.length == this.uploadedCaracNum) {
              resolve();
            }
          })
          .catch(e => {
            reject(e);
          });
      }
    });
  }


  finish() {
    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/newspaper.json')
      .then(() => {
        this.utilCtrl.generateKeyWords(this.localCharacteristics)
          .then(newKeyWords => {
            this.localBanner.keyWords = newKeyWords;
            this.localBanner.thumbnail = this.tempPics[0].thumbnailUrl;
            this.localBanner.status = 10;
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando Anúncio');
            this.db.banners.create(this.localBanner)
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando as caracteristicas do Serviço anunciado');
                this.uploadCaracs()
                  .then(() => {
                    this.UiFeedBackCtrl.updateLoaderMessage('Salvando Fotos');
                    this.uploadPics()
                      .then(() => {
                        this.UiFeedBackCtrl.updateLoaderMessage('Gerando link para compartilhamento');
                        this.dynamicLinksApi.createBannerDynamicLink(this.localBanner.id, this.localBanner.title, this.localBanner.description, this.tempPics[0].url)
                          .then(dynamicLink => {
                            this.localBanner.dynamicLink = dynamicLink.shortLink;
                            this.UiFeedBackCtrl.updateLoaderMessage('Salvando link para compartilhamento');
                            this.db.banners.update(this.localBanner.id, { dynamicLink: this.localBanner.dynamicLink })
                              .then(() => {
                                this.UiFeedBackCtrl.updateLoaderMessage('Criando SAC para Verificação do Anúncio');
                                this.createSac()
                                  .then(() => {
                                    if (!this.UserDataCtrl.localUser.bannerCriationTutorial) {
                                      this.UiFeedBackCtrl.updateLoaderMessage('atualizando seu usuário');
                                      let oldJobKeyWords = this.UserDataCtrl.localUser.jobKeyWords;
                                      this.UserDataCtrl.update({ bannerCriationTutorial: true, jobKeyWords: `${oldJobKeyWords} ${this.localBanner.keyWords}` })
                                        .then(() => {
                                          this.UiFeedBackCtrl.dismissLoader();
                                          this.canLeaveCtrl = true;
                                          this.navCtrl.pop();
                                          this.UiFeedBackCtrl.presentAlert('Maravilha :)', 'Seu Anuncio foi criado e irá passar por uma verificação para que possa receber pedidos!', 'success', true);
                                        });
                                    } else {
                                      this.UiFeedBackCtrl.updateLoaderMessage('atualizando seu usuário');
                                      let oldJobKeyWords = this.UserDataCtrl.localUser.jobKeyWords;
                                      this.UserDataCtrl.update({ jobKeyWords: `${oldJobKeyWords} ${this.localBanner.keyWords}` })
                                        .then(() => {
                                          this.UiFeedBackCtrl.dismissLoader();
                                          this.canLeaveCtrl = true;
                                          this.navCtrl.pop();
                                          this.UiFeedBackCtrl.presentAlert('Maravilha :)', 'Seu Anuncio foi criado e irá passar por uma verificação para que possa receber pedidos!', 'success', true);
                                        });
                                    }
                                  })
                              })
                          })
                      })
                  })
              })
              .catch(e => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert('erro', e);
              });
          });
      });
  }
}