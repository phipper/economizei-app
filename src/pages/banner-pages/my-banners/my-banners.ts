import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform, ActionSheetButton } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../providers/UserData';

import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';

import { Banner } from '../../../models/Banner';
import { UtilProvider } from '../../../providers/Util';
import { WhereFilterOp, DocumentSnapshot } from '@firebase/firestore-types';



@IonicPage()
@Component({
  selector: 'page-my-banners',
  templateUrl: 'my-banners.html',
})
export class MyBannersPage {
  @ViewChild(Content) content: Content;

  segment: string = 'Ativos';
  showSplash = true;
  economizeiLogoUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Flogo%20fundo%20branco%201000x1000.png?alt=media&token=6e70cbd9-032e-4b71-944e-cd7f9da61c66';


  isExecutingQuery = false;
  localBanners: Banner[] = [];
  queryLimit = 4;
  lastQueryDoc: DocumentSnapshot = null;

  activeBannersWhere = {
    id: 0,
    name: 'ActiveBanners',
    fieldPath: 'status',
    opStr: '==' as WhereFilterOp,
    value: 11
  };

  pausedBannersWhere = {
    id: 1,
    name: 'PausedBanners',
    fieldPath: 'status',
    opStr: '==' as WhereFilterOp,
    value: 12
  };

  pendingBannersWhere = {
    id: 2,
    name: 'PausedBanners',
    fieldPath: 'status',
    opStr: '<=' as WhereFilterOp,
    value: 10
  };

  otherBannersWhere = {
    id: 3,
    name: 'OtherBanners',
    fieldPath: 'status',
    opStr: '>=' as WhereFilterOp,
    value: 14
  };

  querryWhere = this.activeBannersWhere;


  localUserId = '';

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name != 'MyBannersPage') {
      this.content.scrollToTop();
    }
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.reLoadActiveSegment();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  reLoadActiveSegment() {
    switch (this.segment) {
      case 'Ativos':
        this.querryWhere = this.activeBannersWhere;
        this.loadBanners();
        break;

      case 'Pausados':
        this.querryWhere = this.pausedBannersWhere;
        this.loadBanners();
        break;

      case 'Pendentes':
        this.querryWhere = this.pendingBannersWhere;
        this.loadBanners();
        break;

      case 'Outros':
        this.querryWhere = this.otherBannersWhere;
        this.loadBanners();
        break;
    }
  }


  loadBanners() {
    console.log(`Executando loadBanners query: ${JSON.stringify(this.querryWhere)}`);
    this.showSplash = true;
    this.isExecutingQuery = true;
    this.localBanners = [];
    this.lastQueryDoc = null;
    if (this.querryWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy(this.querryWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    }
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    console.log(`Executando doRefresh query: ${JSON.stringify(this.querryWhere)}`);
    this.isExecutingQuery = true;
    this.lastQueryDoc = null;
    this.localBanners = [];
    if (this.querryWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy(this.querryWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              refresher.complete();
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              refresher.complete();
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doInfinite(infiniteScroll) {
    this.isExecutingQuery = true;
    if (this.lastQueryDoc != null) {
      console.log(`Executando doInfinite query: ${JSON.stringify(this.querryWhere)}`);
      if (this.querryWhere.opStr !== '==') {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('providerId', '==', this.localUserId)
          .orderBy(this.querryWhere.fieldPath, 'asc')
          .orderBy('createdOn', 'desc')
          .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
          .startAfter(this.lastQueryDoc)
          .limit(this.queryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localBanners.push(doc.data() as Banner);
                }
                if (querySnapshot.empty) {
                  this.lastQueryDoc = null;
                } else {
                  this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('providerId', '==', this.localUserId)
          .orderBy('createdOn', 'desc')
          .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
          .startAfter(this.lastQueryDoc)
          .limit(this.queryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localBanners.push(doc.data() as Banner);
                }
                if (querySnapshot.empty) {
                  this.lastQueryDoc = null;
                } else {
                  this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      console.log(`no need to execute doInfinite query for: ${JSON.stringify(this.querryWhere)}`);
      infiniteScroll.complete();
    }
  }

  addBannerPage() {
    this.navCtrl.push('AddBannerPage');
  }

  editBannerPage(banner: Banner) {
    this.navCtrl.push('EditBannerPage', {
      banner: banner
    });
  }

  cardTapped(banner) {
    this.navCtrl.push('BannerDetailPage', {
      banner: banner
    });
  }

  cardPressed(banner) {
    this.bannerOptions(banner);
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  pauseBanner(banner) {
    this.UiFeedBackCtrl.presenLoader('Pausando ' + banner.title + '...')
      .then(() => {
        banner.status = 12;
        this.db.banners.update(banner.id, { status: banner.status })
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.segment = 'Pausados';
            this.UiFeedBackCtrl.presentToast('Anuncio Pausado!', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao atualizar Anuncio', e);
          });
      });
  }

  resumeBanner(banner) {
    this.UiFeedBackCtrl.presenLoader('Publicando ' + banner.title + '...')
      .then(() => {
        banner.status = 11;
        this.db.banners.update(banner.id, { status: banner.status })
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.segment = 'Ativos';
            this.UiFeedBackCtrl.presentToast('Anuncio Publicado!', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao atualizar Anuncio', e);
          });
      });

  }

  bannerOptions(banner) {

    let buttonsArray: ActionSheetButton[] = [];

    if (banner.status == 11) {
      buttonsArray.push({
        text: 'Pausar',
        icon: !this.platform.is('ios') ? 'pause' : null,
        handler: () => {
          this.pauseBanner(banner);
        }
      });
    }

    if (banner.status == 12) {
      buttonsArray.push({
        text: 'Publicar Banner',
        icon: !this.platform.is('ios') ? 'play' : null,
        handler: () => {
          this.resumeBanner(banner);
        }
      });
    }

    if (banner.status != 14 && banner.status != 10 && banner.status != 7) {
      buttonsArray.push({
        text: 'Editar',
        icon: !this.platform.is('ios') ? 'create' : null,
        handler: () => {
          this.editBannerPage(banner);
        }
      });
    }

    if (banner.status != 14) {
      buttonsArray.push({
        text: 'Finalizar',
        icon: !this.platform.is('ios') ? 'trash' : null,
        handler: () => {
          this.deleteConfirmation(banner);
        }
      });
    }

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: 'Opções de ' + banner.title,
      buttons: buttonsArray,
    });
    actionSheet.present();
  }

  deleteBanner(banner: Banner) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Excluindo...',
    });

    loader.present()
      .then(() => {
        banner.status = 14;
        this.db.banners.update(banner.id, { status: banner.status })
          .then(() => {
            loader.dismiss();
            this.segment = 'Outros';
            this.UiFeedBackCtrl.presentToast('Anuncio Finalizado!', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao atualizar Anuncio', e);
          });
      });
  }

  deleteConfirmation(banner: Banner) {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Finalizar?',
      subTitle: 'Você tem certeza que deseja finalizar este anúncio?',
      buttons: [
        'Cancelar',
        {
          text: 'Sim',
          handler: () => {
            this.deleteBanner(banner);
          }
        }
      ]
    });
    alert.present();
  }

}
