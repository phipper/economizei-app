import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { trigger, transition, style, state, animate, keyframes } from '@angular/core';

import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';
import { Characteristic } from '../../../../models/Characteristic';



@IonicPage()
@Component({
  selector: 'page-banner-characs-detail-modal',
  templateUrl: 'banner-characs-detail-modal.html',
  animations: [
    trigger('swipe', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(-361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ])))
    ])
  ]
})

export class BannerCharacsDetailModal {

  showSplash = true;

  bannerId = ``;
  characteristics: Characteristic[] = [];

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public db: EcoFirestore,
  ) {
    this.bannerId = navParams.get('bannerId');

  }

  ionViewDidEnter() {
    this.db.banners.characteristics.getAll(this.bannerId)
      .then(characteristics => {
        this.characteristics = characteristics;
        this.showSplash = false;
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  translateBoolean(b: boolean): string {
    return b ? 'Sim' : 'Não';
  }

  close() {
    this.viewCtrl.dismiss({ success: false });
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

}
