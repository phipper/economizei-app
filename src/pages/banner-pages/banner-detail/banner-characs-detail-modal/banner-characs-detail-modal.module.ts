import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BannerCharacsDetailModal } from './banner-characs-detail-modal';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    BannerCharacsDetailModal,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(BannerCharacsDetailModal),
  ],
})
export class BannerCharacsDetailModule { }
