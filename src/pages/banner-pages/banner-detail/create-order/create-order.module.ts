import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateOrderPage } from './create-order';
import { LottieAnimationViewModule } from 'ng-lottie';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    CreateOrderPage,
  ],
  imports: [
    LottieAnimationViewModule,
    Ionic2RatingModule,
    IonicPageModule.forChild(CreateOrderPage),
  ],
})
export class CreateOrderPageModule {}
