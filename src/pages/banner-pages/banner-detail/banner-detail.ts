import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Slides, ModalController } from 'ionic-angular';


import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { LocalizationProvider } from '../../../providers/Localization/Localization';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';

import { User } from '../../../models/User';
import { Banner } from '../../../models/Banner';
import { Pic } from '../../../models/Pic';
import { BannerView } from '../../../models/BannerView';
import { Address } from '../../../models/Address';
import { GeoLocation } from '../../../models/Geolocation';
import { UserDataProvider } from '../../../providers/UserData';

import { AsProviderReview } from '../../../models/AsProviderReview';
import { ComplimentsReport } from '../../../models/ComplimentsReport';
import { DocumentSnapshot } from '@firebase/firestore-types';

@IonicPage()
@Component({
    selector: 'page-banner-detail',
    templateUrl: 'banner-detail.html',
})
export class BannerDetailPage {
    @ViewChild(Slides) SlidesCtrl: Slides;

    isLoaded = false;

    localBanner = new Banner();
    provider = new User();
    currentBannerIsSave = false;

    localbannerView: BannerView = new BannerView();
    localGeoLocation: GeoLocation = new GeoLocation();

    localPics: Pic[] = [];

    completedOrdersNumber = '⌛';

    localComplimentsReport: ComplimentsReport[] = [];
    hasCompliments = false;
    showComplimentsSplash = true;

    showItemSplash = true;
    showPicSplash = true;

    lastReviewDoc: DocumentSnapshot = null;
    localReviews: AsProviderReview[] = [];
    reviewQueryLimit = 2;
    showReviewsSplash = true;


    constructor(
        public UserDataCtrl: UserDataProvider,
        private LocalizationCtrl: LocalizationProvider,
        public platform: Platform,
        public db: EcoFirestore,
        public navCtrl: NavController,
        public navParams: NavParams,
        private socialSharing: SocialSharing,
        private clipboard: Clipboard,
        private UiFeedBackCtrl: UiFeedBackProvider,
        private modalCtrl: ModalController
    ) {
        this.localBanner = navParams.get('banner');
    }

    loadPics() {
        this.db.banners.pics.getAll(this.localBanner.id)
            .then(pics => {
                this.localPics = pics;
                this.SlidesCtrl.lockSwipes(this.localPics.length == 1);
                this.showPicSplash = false;
            })
            .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar fotos', e);
            });
    }

    ionViewDidEnter() {
        this.loadPics();
        this.loadReviews();

        this.db.users.history.getCompletedOrdersNumber(this.localBanner.providerId, this.localBanner.id)
            .then(num => {
                this.completedOrdersNumber = num.toString();
            });

        this.db.users.get(this.localBanner.providerId)
            .then(user => {
                this.provider = user;
                this.showItemSplash = false;
            })
            .catch(e => {
                this.showItemSplash = false;
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            });

        this.db.users.history.getAsProviderReceivedReviewsCompliments(this.localBanner.providerId)
            .then(data => {
                this.localComplimentsReport = data;
                for (let cump of this.localComplimentsReport) {
                    if (cump.quantity > 0) {
                        this.hasCompliments = true;
                        break;
                    }
                }
                this.showComplimentsSplash = false;
            })

        if (this.UserDataCtrl.hasUser) {
            this.currentBannerIsSave = this.UserDataCtrl.isBannerSaved(this.localBanner.id);
            if (this.localBanner.providerId != this.UserDataCtrl.localUser.id) {
                if (this.platform.is('cordova')) {

                    console.log('BannerDetailPage says: É cordova');
                    this.LocalizationCtrl.getCurrentAddress()
                        .then((currentAddress: Address) => {

                            this.localGeoLocation.latitude = currentAddress.latitude;
                            this.localGeoLocation.longitude = currentAddress.longitude;
                            this.localGeoLocation.accuracy = this.LocalizationCtrl.lastGeoLocAccuracy;

                            this.localGeoLocation.state = currentAddress.state;
                            this.localGeoLocation.city = currentAddress.city;
                            this.localGeoLocation.bairro = currentAddress.bairro;
                            this.localbannerView.geoLocation = Object.assign({}, this.localGeoLocation);
                            this.localbannerView.userId = this.UserDataCtrl.localUser.id;

                            this.db.banners.addView(this.localBanner.id, this.localbannerView);

                        })
                        .catch(e => {
                            this.UiFeedBackCtrl.presentToast('erro ao registrar view', `warning`);
                        });

                } else {
                    console.log('BannerDetailPage says: não é cordova');
                }
            }
        } else {
            this.currentBannerIsSave = false;
            if (this.platform.is('cordova')) {
                console.log('BannerDetailPage says: É cordova');
                this.LocalizationCtrl.getCurrentAddress()
                    .then((currentAddress: Address) => {

                        this.localGeoLocation.latitude = currentAddress.latitude;
                        this.localGeoLocation.longitude = currentAddress.longitude;
                        this.localGeoLocation.accuracy = this.LocalizationCtrl.lastGeoLocAccuracy;

                        this.localGeoLocation.state = currentAddress.state;
                        this.localGeoLocation.city = currentAddress.city;
                        this.localGeoLocation.bairro = currentAddress.bairro;
                        this.localbannerView.geoLocation = Object.assign({}, this.localGeoLocation);
                        this.localbannerView.userId = '';

                        this.db.banners.addView(this.localBanner.id, this.localbannerView);
                    })
                    .catch(e => {
                        this.UiFeedBackCtrl.presentToast('erro ao registrar view', `warning`);
                    });
            } else {
                console.log('BannerDetailPage says: não é cordova');
            }
        }
        this.isLoaded = true;

    }

    showprovider() {
        if (this.provider.id != '') {
            this.navCtrl.push('ProviderDetailPage', {
                provider: this.provider
            });
        }
    }

    favorite() {
        if (this.UserDataCtrl.hasUser) {
            if (!this.currentBannerIsSave) {
                this.UiFeedBackCtrl.presenLoader('Adiconando aos favoritos...')
                    .then(() => {
                        this.UserDataCtrl.saveBanner(this.localBanner.id)
                            .then(() => {
                                this.currentBannerIsSave = true;
                                this.UiFeedBackCtrl.dismissLoader();
                                this.UiFeedBackCtrl.presentToast('Anuncio adicionado aos favoritos!', 'success')
                            })
                            .catch(e => {
                                this.UiFeedBackCtrl.dismissLoader();
                                this.UiFeedBackCtrl.presentErrorAlert('Erro ao adicionar o anuncio aos favoritos', e)
                            })
                    });
            } else {
                this.UiFeedBackCtrl.presenLoader('Removendo dos favoritos...')
                    .then(() => {
                        this.UserDataCtrl.unsaveBanner(this.localBanner.id)
                            .then(() => {
                                this.currentBannerIsSave = false;
                                this.UiFeedBackCtrl.dismissLoader();
                                this.UiFeedBackCtrl.presentToast('Anuncio removido aos favoritos!', 'success')
                            })
                            .catch(e => {
                                this.UiFeedBackCtrl.dismissLoader();
                                this.UiFeedBackCtrl.presentErrorAlert('Erro ao remover o anuncio aos favoritos', e)
                            })
                    });
            }
        } else {
            this.UiFeedBackCtrl.presentToast('Faça Login para poder favoritar o anuncio', 'warning');
            this.navCtrl.push('LoginPage');
        }
    }


    share(banner: Banner) {
        let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Compartilhar',
                    icon: !this.platform.is('ios') ? 'md-share' : null,
                    handler: () => {
                        this.socialSharing.share(`Olha esse anuncio que achei pra você no economizei, ${banner.title}`, banner.title, null, banner.dynamicLink);
                    }
                },
                {
                    text: 'Copiar link',
                    icon: !this.platform.is('ios') ? 'link' : null,
                    handler: () => {
                        this.clipboard.copy(banner.dynamicLink).then(() => {
                            this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', banner.dynamicLink, 'success');
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                }
            ]
        });
        actionSheet.present();
    }

    startOrder() {
        if (this.UserDataCtrl.hasUser) {
            this.navCtrl.push('CreateOrderPage', {
                banner: this.localBanner,
                provider: this.provider
            });
        } else {
            this.UiFeedBackCtrl.presentToast('Faça Login para poder favoritar contratar um serviço do anuncio', 'warning');
            this.navCtrl.push('LoginPage');
        }
    }

    openBannerPicsViewerPage() {
        this.navCtrl.push('BannerPicsViewerPage', {
            picsArray: this.localPics
        });
    }

    translateRate(rate: number): string {
        switch (rate) {
            case 1:
                return 'Péssimo';
            case 2:
                return 'Ruim';
            case 3:
                return 'Regular';
            case 4:
                return 'Bom';
            case 5:
                return 'Excelente';
        }
    }

    openCharacsDetail() {
        let modal = this.modalCtrl.create('BannerCharacsDetailModal', {
            bannerId: this.localBanner.id
        });
        modal.present();
    }

    loadReviews() {
        this.lastReviewDoc = null;
        this.showReviewsSplash = true;
        this.localReviews = [];

        this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.localBanner.providerId)
            .collection(this.db.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
            .orderBy('createdOn', 'desc')
            .where('bannerId', '==', this.localBanner.id)
            .limit(this.reviewQueryLimit)
            .get()
            .then(querySnapshot => {
                this.db.validators.validateQuerySnapshot(querySnapshot)
                    .then(querySnapshot => {
                        for (let doc of querySnapshot.docs) {
                            let tempReview = doc.data() as AsProviderReview;
                            this.localReviews.push(tempReview);
                        }
                        if (querySnapshot.empty) {
                            this.showReviewsSplash = false;
                            this.lastReviewDoc = null;
                        } else if (this.localReviews.length > this.reviewQueryLimit - 1) {
                            this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                            this.showReviewsSplash = false;
                        } else {
                            this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                            this.loadMoreReviews();
                        }
                    })
            })
            .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            });
    }


    loadMoreReviews() {
        this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.localBanner.providerId)
            .collection(this.db.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
            .orderBy('createdOn', 'desc')
            .startAfter(this.lastReviewDoc)
            .where('bannerId', '==', this.localBanner.id)
            .limit(this.reviewQueryLimit)
            .get()
            .then(querySnapshot => {
                this.db.validators.validateQuerySnapshot(querySnapshot)
                    .then(querySnapshot => {
                        for (let doc of querySnapshot.docs) {
                            let tempReview = doc.data() as AsProviderReview;
                            this.localReviews.push(tempReview);
                        }
                        if (this.localReviews.length > this.reviewQueryLimit - 1) {
                            this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                            this.showReviewsSplash = false;
                        } else {
                            if (querySnapshot.docs.length > 0) {
                                this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                                this.loadMoreReviews();
                            } else {
                                this.showReviewsSplash = false;
                            }
                        }
                    })
            })
            .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            });
    }

    doInfinite(infiniteScroll) {
        this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.localBanner.providerId)
            .collection(this.db.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
            .orderBy('createdOn', 'desc')
            .startAfter(this.lastReviewDoc)
            .where('bannerId', '==', this.localBanner.id)
            .limit(this.reviewQueryLimit)
            .get()
            .then(querySnapshot => {
                this.db.validators.validateQuerySnapshot(querySnapshot)
                    .then(querySnapshot => {
                        for (let doc of querySnapshot.docs) {
                            let tempReview = doc.data() as AsProviderReview;
                            this.localReviews.push(tempReview);
                        }
                        if (this.localReviews.length > this.reviewQueryLimit - 1) {
                            this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                            infiniteScroll.complete();
                        } else {
                            if (querySnapshot.docs.length > 0) {
                                this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                                this.doInfinite(infiniteScroll);
                            } else {
                                infiniteScroll.complete();
                            }
                        }
                    })
            })
            .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            });
    }

}   