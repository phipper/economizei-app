import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BannerPicsViewerPage } from './banner-pics-viewer';

@NgModule({
  declarations: [
    BannerPicsViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(BannerPicsViewerPage),
  ],
})
export class BannerPicsViewerPageModule {}
