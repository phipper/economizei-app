import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { Pic } from '../../../../models/Pic';

@IonicPage()
@Component({
  selector: 'page-banner-pics-viewer',
  templateUrl: 'banner-pics-viewer.html',
})
export class BannerPicsViewerPage {
  @ViewChild(Slides) SlidesCtrl: Slides;


  localPics: Pic[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.localPics = navParams.get("picsArray");
  }

  ionViewDidEnter() {
    this.SlidesCtrl.lockSwipes(this.localPics.length == 1);
  }

}
