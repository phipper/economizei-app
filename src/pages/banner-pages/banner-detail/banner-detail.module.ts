import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BannerDetailPage } from './banner-detail';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    BannerDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(BannerDetailPage),
  ],
})
export class BannerDetailPageModule {}
