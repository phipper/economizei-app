import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigPage } from './config';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ConfigPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfigPage),
    LottieAnimationViewModule
  ],
})
export class ConfigPageModule {}
