import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../providers/UifeedBack/UifeedBack';

import { UserDataProvider } from '../../providers/UserData';
import { CoreProvider } from '../../providers/CoreProvider';
import { AppConfig } from '../../models/AppConfig';
import { LocalizationProvider } from '../../providers/Localization/Localization';


@IonicPage()
@Component({
	selector: 'page-config',
	templateUrl: 'config.html',
})
export class ConfigPage {

	showSplash = true;
	localAppConfig: AppConfig = new AppConfig();

	economizeiVersion = "0.6.89 - Beta 1";

	lottieSplash = {
		loop: true,
		autoplay: true,
		path: 'assets/animations/gears.json'
	}

	constructor(
		public UserDataCtrl: UserDataProvider,
		public platform: Platform,
		public navCtrl: NavController,
		public navParams: NavParams,
		private UiFeedBackCtrl: UiFeedBackProvider,
		public coreCtrl: CoreProvider,
		public localizationCtrl: LocalizationProvider
	) {
	}

	ionViewDidEnter() {
		if (this.platform.is('cordova')) {
			this.coreCtrl.localDbCtrl.getAppConfig()
				.then(appConfig => {
					this.localAppConfig = appConfig;
					this.showSplash = false;
				})
				.catch(e => {
					this.showSplash = false;
					this.UiFeedBackCtrl.presentErrorAlert('Erro ao sair', e);
				});
		} else {
			setTimeout(() => {
				this.showSplash = false;
			}, 2000)
		}
	}

	doRefresh(refresher) {
		this.showSplash = true;
		this.ionViewDidEnter();
		refresher.complete();
	}

	logout() {
		this.UiFeedBackCtrl.presenLoader('Saindo...', 'assets/animations/logout.json')
			.then(() => {
				this.navCtrl.setRoot('HomePage');
				this.UserDataCtrl.logout()
					.then(() => {
						this.UiFeedBackCtrl.presentToast('Até logo!', 'success');
						this.UiFeedBackCtrl.dismissLoader();
					})
					.catch(e => {
						this.UiFeedBackCtrl.dismissLoader();
						this.UiFeedBackCtrl.presentErrorAlert('Erro ao sair', e);
					});
			});
	}

	toggleUserParam(param: string) {

		this.UiFeedBackCtrl.presenLoader('Alterando...', 'assets/animations/toggle_switch.json')
			.then(() => {
				switch (param) {

					case 'generalAlertsNotifications':
						this.UserDataCtrl.localUser.generalAlertsNotifications = !this.UserDataCtrl.localUser.generalAlertsNotifications;
						break;

					case 'newsNotification':
						this.UserDataCtrl.localUser.newsNotification = !this.UserDataCtrl.localUser.newsNotification;
						break;

					case 'newMsgNotifications':
						this.UserDataCtrl.localUser.newMsgNotifications = !this.UserDataCtrl.localUser.newMsgNotifications;
						break;

					case 'newBudgetNotifications':
						this.UserDataCtrl.localUser.newBudgetNotifications = !this.UserDataCtrl.localUser.newBudgetNotifications;
						break;

					case 'newReviewNotifications':
						this.UserDataCtrl.localUser.newReviewNotifications = !this.UserDataCtrl.localUser.newReviewNotifications;
						break;

					case 'newOrderNotifications':
						this.UserDataCtrl.localUser.newOrderNotifications = !this.UserDataCtrl.localUser.newOrderNotifications;
						break;

					case 'emailNotifications':
						this.UserDataCtrl.localUser.emailNotifications = !this.UserDataCtrl.localUser.emailNotifications;
						break;

				}
				this.UserDataCtrl.update(Object.assign({}, this.UserDataCtrl.localUser))
					.then(() => {
						this.UiFeedBackCtrl.dismissLoader();
					})
					.catch(e => {
						this.UiFeedBackCtrl.dismissLoader();
						this.UiFeedBackCtrl.presentErrorAlert("Erro ao atualizar o usuário", e);
					});

			})

	}

	changeStatusBarMode() {
		this.UiFeedBackCtrl.presenLoader('Alterando...', 'assets/animations/toggle_switch.json')
			.then(() => {
				this.coreCtrl.setStatusBarMode(this.localAppConfig.statusBarMode)
					.then(() => {
						this.UiFeedBackCtrl.dismissLoader();
						if (this.localAppConfig.statusBarMode == 'default') {
							this.UiFeedBackCtrl.presentAlert('reboot necessario', 'Feche e abra o economizei para aplicar a configuraçao padrão da StatusBar!', `warning`, true);
						}
					})
					.catch(e => {
						this.UiFeedBackCtrl.dismissLoader();
						this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
					});
			})
	}

	toggleHapticFeedbackConfig() {
		this.UiFeedBackCtrl.presenLoader('Alterando...', 'assets/animations/toggle_switch.json')
			.then(() => {
				this.localAppConfig.hapticFeedback = !this.localAppConfig.hapticFeedback;
				this.coreCtrl.setHapticFeedbackConfig(this.localAppConfig.hapticFeedback)
					.then(() => {
						this.UiFeedBackCtrl.dismissLoader();
					})
					.catch(e => {
						this.localAppConfig.hapticFeedback = !this.localAppConfig.hapticFeedback;
						this.UiFeedBackCtrl.dismissLoader();
						this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
					});
			})
	}

	goToPage(page) {
		this.navCtrl.push(page, {
			localUser: this.UserDataCtrl.localUser
		});
	}

	changeLocalizationConfig() {
		this.localizationCtrl.requestManualInput()
			.then(data => {
				this.UiFeedBackCtrl.presenLoader('Alterando...', 'assets/animations/toggle_switch.json')
					.then(() => {
						this.localAppConfig.state = data.selectedState;
						this.localAppConfig.city = data.selectedCity;
						this.coreCtrl.localDbCtrl.updateAppConfig(this.localAppConfig)
							.then(() => {
								this.UiFeedBackCtrl.dismissLoader();
							})
							.catch(e => {
								this.UiFeedBackCtrl.dismissLoader();
								this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
							});
					})
			})
			.catch(e => {
				if (e.message !== "Ação cancelada!") {
					this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
				} else {
					this.UiFeedBackCtrl.presentToast("Ação cancelada!", `warning`);
				}
			});
	}
}