import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignPlanPage } from './sign-plan';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    SignPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(SignPlanPage),
    LottieAnimationViewModule
  ],
})
export class SignPlanPageModule {}
