import { Component, ViewChild } from '@angular/core';
import { trigger, transition, style, state, animate, keyframes } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Slides } from 'ionic-angular';
import { Plan } from '../../../../models/Plan';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../../providers/UserData';

import { CreditCard } from '../../../../models/CreditCard';
import { UtilProvider } from '../../../../providers/Util';
import { PagseguroPgtoServiceProvider } from '../../../../providers/PagSeguro/PagSeguro';
import { Address } from '../../../../models/Address';
import { User } from '../../../../models/User';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
  selector: 'page-sign-plan',
  templateUrl: 'sign-plan.html',
  animations: [
    trigger('swipe', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(-361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ])))
    ])
  ]
})
export class SignPlanPage {

  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;
  state: string = 'x';
  swipeIndex: number = 0;

  showSplash = true;

  localUser: User = new User();

  segment = 'Plano';

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/credit-card.json'
  }

  localPlan: Plan = new Plan();

  tempCardCCV = '';
  selectedCreditCard: CreditCard = new CreditCard();
  localCreditCards: CreditCard[] = [];

  selectedAddress: Address = new Address();
  localAddresses: Address[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public UserDataCtrl: UserDataProvider,
    public pagseg: PagseguroPgtoServiceProvider,
    public db: EcoFirestore,
    public utilCtrl: UtilProvider
  ) {
    this.localPlan.id = navParams.get('planId');
  }


  slideMoved() {
    if (this.swipeIndex != 0 && this.slides.getActiveIndex() >= this.slides.getPreviousIndex()) {
      this.state = 'rightSwipe';
      this.swipeIndex -= 361;
      this.slides.setElementStyle("transform", `translate3d(${this.swipeIndex}, 0px, 0px)`);
    }
    else {
      this.state = 'leftSwipe';
      this.swipeIndex += 361;
      this.slides.setElementStyle("transform", `translate3d(${this.swipeIndex}, 0px, 0px)`);
    }
    // console.log(this.slides.getActiveIndex());
    // console.log(this.state);
  }

  animationDone() {
    this.state = 'x';
  }


  scrollToTop() {
    this.content.scrollToTop();
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  ionViewDidEnter() {
    this.showSplash = true;
    this.UserDataCtrl.getUser()
      .then(user => {
        this.localUser = user;
        if (this.UserDataCtrl.localUserIsProvider) {
          if (this.localUser.verifiedProvider) {
            if (this.UserDataCtrl.localUser.status == 'active') {
              this.db.afs.collection('PlanSignatures').ref
                .where('userId', '==', this.localUser.id).where('status', '==', 'ACTIVE').get()
                .then(planSignaturesQuery => {
                  this.db.validators.validateQuerySnapshot(planSignaturesQuery)
                    .then(planSignaturesQuery => {
                      if (planSignaturesQuery.empty) {
                        this.db.afs.collection('PlanSignatures').ref
                          .where('userId', '==', this.localUser.id).where('status', '==', 'PAYMENT_METHOD_CHANGE').get()
                          .then(planSignaturesQuery1 => {
                            this.db.validators.validateQuerySnapshot(planSignaturesQuery1)
                              .then(planSignaturesQuery1 => {
                                if (planSignaturesQuery1.empty) {
                                  this.db.afs.collection('Addresses').ref
                                    .where('userId', '==', this.localUser.id).get()
                                    .then(addressesQuery => {
                                      if (addressesQuery.empty) {
                                        const alert = this.UiFeedBackCtrl.alertCtrl.create({
                                          title: 'Não possuimos nehum endereço seu !',
                                          subTitle: 'Você precisa cadastrar um endereço para Assinar um Plano. Deseja cadastrar agora?',
                                          buttons: [
                                            'Não',
                                            {
                                              text: 'Sim',
                                              handler: () => {
                                                this.navCtrl.push('AddAddressPage');
                                              }
                                            }
                                          ]
                                        });
                                        this.navCtrl.pop();
                                        this.UiFeedBackCtrl.hapticCtrl.notification('warning');
                                        alert.present();
                                      } else {
                                        this.localAddresses = [];
                                        for (let addressDoc of addressesQuery.docs) {
                                          this.localAddresses.push(addressDoc.data() as Address);
                                        }
                                        this.pagseg.init()
                                          .then(() => {
                                            this.db.afs.collection('Plans').doc(this.localPlan.id).ref.get()
                                              .then(planDoc => {
                                                this.localPlan = planDoc.data() as Plan;
                                                this.db.afs.collection('Users').doc(this.localUser.id).collection('CreditCards').ref.get()
                                                  .then(ccQuery => {
                                                    this.localCreditCards = [];
                                                    for (let ccDoc of ccQuery.docs) {
                                                      this.localCreditCards.push(ccDoc.data() as CreditCard)
                                                    }
                                                    this.showSplash = false;
                                                  })
                                              })
                                          })
                                          .catch(e => {
                                            this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
                                          })
                                      }
                                    })
                                    .catch(e => {
                                      this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
                                    })

                                } else {
                                  this.UiFeedBackCtrl.presentAlert('Você possui uma Assinatura Com o metodo de pagamento Pendente !', 'Você precisa Cancelar essa assinatura para poder Assinar um novo Plano.', 'warning', true);
                                  this.navCtrl.pop();
                                  this.navCtrl.push('PlanSignaturesHistoryPage');
                                }
                              })
                          })
                          .catch(e => {
                            this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
                          })
                      } else {
                        this.UiFeedBackCtrl.presentAlert('Você já possui uma Assinatura ativa !', 'Você precisa Cancelar sua assinatura atual para poder Assinar um novo Plano.', 'warning', true);
                        this.navCtrl.pop();
                        this.navCtrl.push('PlanSignaturesHistoryPage');
                      }
                    })
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
                })
            } else {
              this.navCtrl.pop();
              this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error', true);
            }
          } else {
            this.navCtrl.pop();
            this.UiFeedBackCtrl.presentAlert('Somente prestadores ativos podem assinar planos!', 'Favor realize a verificação de prestadores!', 'error', true);
          }
        } else {
          this.navCtrl.pop();
          this.UiFeedBackCtrl.presentAlert('Somente prestadores podem assinar planos!', 'Você não esta cadastrado como prestador de serviço!', 'error', true);
        }
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
      })
  }

  cancel() {
    this.navCtrl.pop();
  }

  selectCC(cc: CreditCard) {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Confirmar Cartão',
      subTitle: `Você deseja utilizar o cartão de numero: •••• •••• •••• ${cc.number.substr(12, 4)} para a assinatura ?`,
      buttons: [
        'Não',
        {
          text: 'Sim',
          handler: () => {
            this.UiFeedBackCtrl.hapticCtrl.selection();
            this.selectedCreditCard = cc;
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  confirmCC_CCV() {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Confirmar Codigo de Segurança',
      subTitle: `Você confirma o Codigo de segurança: ${this.tempCardCCV} para o cartão de numero: •••• •••• •••• ${this.selectedCreditCard.number.substr(12, 4)}?`,
      buttons: [
        'Não',
        {
          text: 'Sim',
          handler: () => {
            this.goForward();
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  addCC() {
    this.navCtrl.push('AddCreditCardPage')
  }

  goBack() {
    switch (this.segment) {
      case 'Endereço':
        this.segment = 'Dados'
        this.scrollToTop();
        break;
      case 'Dados':
        this.segment = 'Cartão'
        this.scrollToTop();
        break;
      case 'Cartão':
        this.segment = 'Plano'
        this.scrollToTop();
        break;
    }
  }

  goForward() {
    switch (this.segment) {
      case 'Plano':
        this.segment = 'Cartão'
        this.scrollToTop();
        break;
      case 'Cartão':
        this.segment = 'Dados'
        this.scrollToTop();
        break;
      case 'Dados':
        this.segment = 'Endereço'
        this.scrollToTop();
        break;
    }
  }

  sign() {
    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/credit-card.json')
      .then(() => {

        this.pagseg.registerPlanSignature(this.localPlan, this.localUser, this.selectedAddress, this.selectedCreditCard, this.tempCardCCV)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presenLottietAlert(`Maravilha!`, `Sua assinatura foi salva, estamos processando sua assinatura e embreve iremos lhe notificar quando seu plano estiver disponivel para uso!`, 'assets/animations/check_animation.json', 'success', true);
            this.navCtrl.pop();
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          })

      })
  }

}
