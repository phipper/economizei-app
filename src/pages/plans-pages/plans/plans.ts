import { Component, ViewChild, style } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { UserDataProvider } from '../../../providers/UserData';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { trigger, transition, state, animate, keyframes } from '@angular/core';


@IonicPage()
@Component({
  selector: 'page-plans',
  templateUrl: 'plans.html',
  animations: [
    trigger('swipe', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(-361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ])))
    ])
  ]
})
export class PlansPage {
  @ViewChild(Slides) slides: Slides;

  swipeIndex: number = 0;
  state: string = 'x';
  planId: string = 'ecofree';

  bulletElement = document.getElementsByClassName("swiper-pagination-bullet-active") as HTMLCollectionOf<HTMLElement>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public UserDataCtrl: UserDataProvider,
    public UiFeedBackCtrl: UiFeedBackProvider
  ) {
  }

  ionViewDidEnter() {
  }

  buyPlan() {
    if (this.UserDataCtrl.hasUser) {
      this.navCtrl.push('SignPlanPage', {
        planId: this.planId,
      })
    } else {
      this.UiFeedBackCtrl.presentToast('Você precisa Fazer login para Assinar um plano', 'warning');
      this.navCtrl.push('LoginPage');
    }
  }

  slideMoved() {
    switch(this.slides.getActiveIndex()) {
      case 0:
      this.planId = 'ecofree';
      this.bulletElement[0].style.backgroundColor = '#d45654';
        break;
      case 1:
      this.planId = 'ecobit';
      this.bulletElement[0].style.backgroundColor = '#00be71';
        break;
      case 2:
      this.planId = 'ecomega';
      this.bulletElement[0].style.backgroundColor = '#ffa220';
        break;
      case 3:
      this.planId = 'ecogiga';
      this.bulletElement[0].style.backgroundColor = '#004c90';
        break;
    }
  }

  animationDone() {
    this.state = 'x';
  }

}
