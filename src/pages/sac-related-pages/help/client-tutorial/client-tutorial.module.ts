import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTutorialPage } from './client-tutorial';

@NgModule({
  declarations: [
    ClientTutorialPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientTutorialPage),
  ],
})
export class ClientTutorialPageModule {}
