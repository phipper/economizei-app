import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-client-tutorial',
	templateUrl: 'client-tutorial.html',
})
export class ClientTutorialPage {


	constructor(
		public navCtrl: NavController,
		public navParams: NavParams
	) {
	}

	ionViewDidLoad() {
	}

	goToPage(page) {
		this.navCtrl.push(page);
	}
}
