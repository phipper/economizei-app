import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySacsPage } from './my-sacs';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MySacsPage,
  ],
  imports: [
    IonicPageModule.forChild(MySacsPage),
    LottieAnimationViewModule
  ],
})
export class MySacsPageModule {}
