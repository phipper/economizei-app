import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';

import { AngularFirestore } from 'angularfire2/firestore';

import { SAC } from '../../../../models/SAC';
import { UtilProvider } from '../../../../providers/Util';
import { UserDataProvider } from '../../../../providers/UserData';

@IonicPage()
@Component({
	selector: 'page-my-sacs',
	templateUrl: 'my-sacs.html',
})
export class MySacsPage {
	@ViewChild(Content) content: Content;

	localUserId = '';

	segment: string = "Abertos";
	showSplash = true;
	localSacs: SAC[] = [];
	isLoaded = false;

	lottieSplash = {
		loop: true,
		autoplay: true,
		path: 'assets/animations/servishero_loading.json'
	}

	constructor(
		public platform: Platform,
		public UserDataCtrl: UserDataProvider,
		public afs: AngularFirestore,
		public navCtrl: NavController,
		public navParams: NavParams,
		private UiFeedBackCtrl: UiFeedBackProvider,
		public utilCtrl: UtilProvider
	) {
	}

	scrollToTop() {
		if (this.navCtrl.getActive().name == 'MySacsPage') {
			this.content.scrollToTop();
		}
	}

	ionViewDidEnter() {
		if (!this.isLoaded) {
			this.UserDataCtrl.getUid()
				.then(userId => {
					this.localUserId = userId;
					this.reLoadActiveSegment();
				})
				.catch(e => {
					this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
				})
		}
	}

	translateStatus(status: number): string {
		switch (status) {
			case 1:
				return 'Aguardando atendimento'
			case 1303:
				return 'Cancelado pelo usúario'
			case 1302:
				return 'Cancelado pelo economizei'
			case 1301:
				return 'Rejeitado'
			case 2:
				return 'Em atendimento'
			case 3:
				return 'Pendente usúario'
			case 1300:
				return 'Finalizado'
		}
	}

	reLoadActiveSegment() {
		switch (this.segment) {
			case "Abertos":
				this.loadOpenSacs();
				break;

			case "Encerrados":
				this.loadFinishedSacs();
				break;
		}
	}

	doRefresh(refresher) {
		this.reLoadActiveSegment();
		refresher.complete();
	}

	loadOpenSacs() {
		this.showSplash = true;
		this.localSacs = [];
		this.afs.collection('SACs').ref
			.where('userId', '==', this.localUserId)
			.orderBy('status', 'asc')
			.where('status', '<', 1300)
			.orderBy('createdOn', 'desc').get()
			.then(querySnapshot => {
				for (let sacDoc of querySnapshot.docs) {
					this.localSacs.push(sacDoc.data() as SAC);
				}
				this.scrollToTop();
				this.showSplash = false;
				this.isLoaded = true;
			})
			.catch(e => {
				console.log("Erro :( " + JSON.stringify(e));
				this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
			});
	}

	loadFinishedSacs() {
		this.showSplash = true;
		this.localSacs = [];
		this.afs.collection('SACs').ref
			.where('userId', '==', this.localUserId)
			.orderBy('status', 'asc')
			.where('status', '>=', 1300)
			.orderBy('createdOn', 'desc').get()
			.then(querySnapshot => {
				for (let sacDoc of querySnapshot.docs) {
					this.localSacs.push(sacDoc.data() as SAC);
				}
				this.scrollToTop();
				this.showSplash = false;
				this.isLoaded = true;
			})
			.catch(e => {
				console.log("Erro :( " + JSON.stringify(e));
				this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
			});
	}

	cardTapped(sac) {
		this.navCtrl.push('MySacDetailPage', {
			sacId: sac.id,
		});
	}

	newSacPage() {
		this.navCtrl.push('NewSacPage');
	}
}
