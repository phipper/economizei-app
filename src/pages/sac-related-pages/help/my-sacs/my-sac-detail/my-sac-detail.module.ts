import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySacDetailPage } from './my-sac-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MySacDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MySacDetailPage),
    LottieAnimationViewModule
  ],
})
export class MySacDetailPageModule {}
