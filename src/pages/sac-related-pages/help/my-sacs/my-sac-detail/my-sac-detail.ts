import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { UserDataProvider } from '../../../../../providers/UserData';
import { AngularFirestore } from 'angularfire2/firestore';

import { User } from '../../../../../models/User';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { SAC } from '../../../../../models/SAC';
import { SacActivity } from '../../../../../models/SacActivity';
import { UtilProvider } from '../../../../../providers/Util';

@IonicPage()
@Component({
  selector: 'page-my-sac-detail',
  templateUrl: 'my-sac-detail.html',
})
export class MySacDetailPage {

  userLoaded = false;
  showSplash = true;
  localUser = new User();
  localSac = new SAC();
  sacSub: any;
  sacActivitySub: any;
  localSacActivity: SacActivity[] = [];

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  localUserId = '';

  constructor(
    public afs: AngularFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localSac.id = navParams.get("sacId");
  }


  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.sacSub = this.afs.collection('SACs').doc(this.localSac.id).valueChanges()
          .subscribe(data => {
            this.localSac = data as SAC;
            if (this.localSac.userId == this.localUserId) {
              this.UserDataCtrl.getUser()
                .then(user => {
                  this.localUser = user;
                  this.showSplash = false;
                })
            } else {
              this.navCtrl.pop().then(() => {
                this.UiFeedBackCtrl.presentAlert('Acesso NEGADO!', 'Somente o usuário deste pedido tem acesso à esta pagina!', 'error');
              });
            }
          },
            error => {
              this.UiFeedBackCtrl.presentErrorAlert("Erro ao subscrever no documento do SAC", error);
            });

        this.sacActivitySub = this.afs.collection('SACs').doc(this.localSac.id).collection('SacActivities').ref
          .orderBy('createdOn', 'desc')
          .onSnapshot(querySnapshot => {
            this.showSplash = true;
            this.localSacActivity = [];
            for (var i = 0; i < querySnapshot.docs.length; ++i) {
              this.localSacActivity.push(querySnapshot.docs[i].data() as SacActivity);
            }
            this.showSplash = false;
          },
            error => {
              this.UiFeedBackCtrl.presentErrorAlert("Erro ao subscrever nas atividades do SAC", error);
            });
      })
  }

  ionViewDidLeave() {
    if (this.sacSub != null) {
      this.sacSub.unsubscribe();
    }
    if (this.sacActivitySub != null) {
      this.sacActivitySub();
    }
  }

  openAddCommentModal() {
    let modal = this.modalCtrl.create('AddCommentPage', {
      sac: this.localSac,
      user: this.localUser
    });
    modal.present();
  }

}
