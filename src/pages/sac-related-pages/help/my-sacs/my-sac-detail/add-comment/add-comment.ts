import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';

import { SAC } from '../../../../../../models/SAC';
import { SacActivity } from '../../../../../../models/SacActivity';
import { User } from '../../../../../../models/User';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';



@IonicPage()
@Component({
  selector: 'page-add-comment',
  templateUrl: 'add-comment.html',
})

export class AddCommentPage {

  localSac: SAC = new SAC();
  localUser: User = new User();
  localSacActivity: SacActivity = new SacActivity();

  constructor(
    public viewCtrl: ViewController,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localSac = navParams.get('sac');
    this.localUser = navParams.get('user');
  }

  ionViewDidEnter() {

  }


  close() {
    this.viewCtrl.dismiss();
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  saveComment() {
    this.UiFeedBackCtrl.presenLoader(`Salvando comentario...`)
      .then(() => {
        this.localSacActivity.id = this.db.createId();
        this.localSacActivity.userId = this.localUser.id;
        this.localSacActivity.userName = this.localUser.fullName;
        this.localSacActivity.userThumbUrl = this.localUser.pic.thumbnailUrl;

        this.db.afs.collection('SACs').doc(this.localSac.id).collection('SacActivities').doc(this.localSacActivity.id).set(Object.assign({}, this.localSacActivity))
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.viewCtrl.dismiss();
            this.UiFeedBackCtrl.presentAlert('Comentario adicionado com sucesso!', '', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

}
