import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-provider-tutorial',
	templateUrl: 'provider-tutorial.html',
})
export class ProviderTutorialPage {


	constructor(
		public navCtrl: NavController,
		public navParams: NavParams
	) {
	}

	ionViewDidLoad() {
	}

	goToPage(page) {
		this.navCtrl.push(page);
	}
}
