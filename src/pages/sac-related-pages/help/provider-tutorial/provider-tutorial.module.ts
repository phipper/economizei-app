import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderTutorialPage } from './provider-tutorial';

@NgModule({
  declarations: [
    ProviderTutorialPage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderTutorialPage),
  ],
})
export class ProviderTutorialPageModule {}
