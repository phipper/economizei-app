import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserDataProvider } from '../../../providers/UserData';

@IonicPage()
@Component({
	selector: 'page-help',
	templateUrl: 'help.html',
})
export class HelpPage {


	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public userDataCtrl: UserDataProvider
	) {
	}

	ionViewDidLoad() {
	}

	goToPage(page) {
		this.navCtrl.push(page);
	}
}
