import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewSacPage } from './new-sac';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    NewSacPage,
  ],
  imports: [
    IonicPageModule.forChild(NewSacPage),
    LottieAnimationViewModule
  ],
})
export class NewSacPageModule {}
