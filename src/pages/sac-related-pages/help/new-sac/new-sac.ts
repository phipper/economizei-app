import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';

import { UserDataProvider } from '../../../../providers/UserData';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { SacProvider, SacType } from '../../../../providers/SAC';

import { SAC } from '../../../../models/SAC';
import { SacBusinessRole } from '../../../../models/SacBusinessRole';
import { SacCategory } from '../../../../models/SacCategory';
import { SacSolverTeam } from '../../../../models/SacSolverTeam';

@IonicPage()
@Component({
	selector: 'page-new-sac',
	templateUrl: 'new-sac.html',
})
export class NewSacPage {
	@ViewChild(Content) content: Content;

	segment = "Categorizacao";

	sacType: SacType;

	sacTypes = [
		{
			title: 'Solicitação',
			type: 'RDS'
		},
		{
			title: 'Incidente / Erro',
			type: 'INC'
		},
	];

	newSac: SAC = new SAC();
	selectedSacBR: SacBusinessRole = new SacBusinessRole();
	selectedSacCat: SacCategory = new SacCategory();
	selectedSacSubCat: SacCategory = new SacCategory();

	localSacBusinessRoles: SacBusinessRole[] = [];

	canSelectSacCategory = false;
	localSacCategories: SacCategory[] = [];

	needToSelectSacSubCategory = false;
	canSelectSacSubCategory = false;
	localSacSubCategories: SacCategory[] = [];

	tempDesignedGroupId = ``;

	showSplash = true;

	loadingQuery = false;

	canLeaveCtrl = false;

	lottieSplash = {
		loop: true,
		autoplay: true,
		path: 'assets/animations/newspaper.json'
	}

	constructor(
		public UserDataCtrl: UserDataProvider,
		public db: EcoFirestore,
		public navCtrl: NavController,
		public navParams: NavParams,
		private UiFeedBackCtrl: UiFeedBackProvider,
		private sacCtrl: SacProvider,
	) {
	}


	ionViewDidLoad() {
		this.db.afs.collection(this.db.COLLECTIONS.SAC_BUSINESS_ROLES_COLLECTION).ref.get()
			.then(sacBRquery => {

				for (let SacBRDoc of sacBRquery.docs) {
					this.localSacBusinessRoles.push(SacBRDoc.data() as SacBusinessRole)
				}

				this.showSplash = false;
			})
			.catch(e => {
				this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
			});
	}

	ionViewCanLeave(): boolean {
		if (!this.canLeaveCtrl) {
			let alert = this.UiFeedBackCtrl.alertCtrl.create({
				title: 'Tem certeza?',
				subTitle: 'Você quer descartar todos os dados e cancelar a criação de seu Anuncio?',
				buttons: [
					{
						text: 'SIM',
						handler: () => {
							this.canLeaveCtrl = true;
							this.navCtrl.pop();
						}
					},
					'Cancelar'
				]
			});
			this.UiFeedBackCtrl.hapticCtrl.notification('warning');
			alert.present();
		} else {
			return true;
		}
		return false;
	}

	goBack() {
		switch (this.segment) {
			case 'Dados':
				this.segment = 'Categorizacao'
				break;
		}
	}

	goForward() {
		switch (this.segment) {
			case 'Categorizacao':
				this.segment = 'Dados'
				break;
		}
	}

	selectSBR() {
		if (this.selectedSacBR.id != '') {

			this.loadingQuery = true;

			this.needToSelectSacSubCategory = false;
			this.canSelectSacCategory = false;
			this.selectedSacCat = new SacCategory();
			this.selectedSacSubCat = new SacCategory();

			this.localSacCategories = [];
			this.localSacSubCategories = [];

			this.newSac.priority = this.selectedSacBR.priority;
			this.tempDesignedGroupId = this.selectedSacBR.designedGroupId;

			this.db.afs.collection(this.db.COLLECTIONS.SAC_BUSINESS_ROLES_COLLECTION).doc(this.selectedSacBR.id)
				.collection(this.db.COLLECTIONS.SAC_CATEGORIES_COLLECTION)
				.ref.get()
				.then(sacCategoriesquery => {

					for (let SacCat of sacCategoriesquery.docs) {
						this.localSacCategories.push(SacCat.data() as SacCategory);
					}

					this.canSelectSacCategory = true;
					this.loadingQuery = false;

				})
				.catch(e => {
					this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
				});
		}
	}

	selectSC() {
		if (this.selectedSacCat.id != '') {

			this.loadingQuery = true;

			this.canSelectSacSubCategory = false;
			this.selectedSacSubCat = new SacCategory();
			this.localSacSubCategories = [];

			this.newSac.priority = this.selectedSacCat.priority;
			this.tempDesignedGroupId = this.selectedSacCat.designedGroupId;

			this.db.afs.collection(this.db.COLLECTIONS.SAC_BUSINESS_ROLES_COLLECTION).doc(this.selectedSacBR.id)
				.collection(this.db.COLLECTIONS.SAC_CATEGORIES_COLLECTION).doc(this.selectedSacCat.id)
				.collection(this.db.COLLECTIONS.SAC_SUB_CATEGORIES_COLLECTION).ref.get()
				.then(sacCategoriesquery => {

					for (let SacCat of sacCategoriesquery.docs) {
						this.localSacSubCategories.push(SacCat.data() as SacCategory);
					}

					if (this.localSacSubCategories.length > 0) {
						this.needToSelectSacSubCategory = true;
					}else{
						this.needToSelectSacSubCategory = false;
					}

					this.canSelectSacSubCategory = true;
					this.loadingQuery = false;

				})
				.catch(e => {
					this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
				});
		}
	}

	selectSsC() {
		if (this.selectedSacSubCat.id != '') {

			this.newSac.priority = this.selectedSacSubCat.priority;
			this.tempDesignedGroupId = this.selectedSacSubCat.designedGroupId;

		}
	}

	canSaveCategorization(): boolean {
		if (this.loadingQuery || (this.sacType != 'INC' && this.sacType != 'RDS') || this.selectedSacBR.id == '' || this.selectedSacCat.id == '' || (this.needToSelectSacSubCategory && this.selectedSacSubCat.id == '')) {
			return false;
		}
		return true;
	}

	save() {
		this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/newspaper.json')
			.then(() => {
				this.db.afs.collection(this.db.COLLECTIONS.SAC_SOLVER_TEAMS_COLLECTION).doc(this.tempDesignedGroupId).ref.get()
					.then(stdoc => {
						this.newSac.assignedTeam = stdoc.data() as SacSolverTeam;

						this.newSac.userId = this.UserDataCtrl.localUser.id;
						this.newSac.userName = this.UserDataCtrl.localUser.fullName;
						this.newSac.businessRole = Object.assign({}, this.selectedSacBR);
						this.newSac.category = Object.assign({}, this.selectedSacCat);

						if (this.selectedSacSubCat.id != '') {
							this.newSac.subCategory = Object.assign({}, this.selectedSacSubCat);
						} else {
							this.newSac.subCategory = null;
						}

						this.newSac.type = this.sacType;

						this.sacCtrl.saveSac(this.newSac, this.sacType)
							.then(() => {
								this.UiFeedBackCtrl.dismissLoader();
								this.canLeaveCtrl = true;
								this.navCtrl.pop();
								this.UiFeedBackCtrl.presentAlert('Pronto !', 'Seu Chamado foi salvo em breve entraremos em contato!', 'success', true);
							})
							.catch(e => {
								this.UiFeedBackCtrl.dismissLoader();
								this.UiFeedBackCtrl.presentErrorAlert('erro', e);
							})
					})
					.catch(e => {
						this.UiFeedBackCtrl.dismissLoader();
						this.UiFeedBackCtrl.presentErrorAlert('erro', e);
					})
			});
	}
}
