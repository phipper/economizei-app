import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderServiceDetailPage } from './order-service-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    OrderServiceDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderServiceDetailPage),
    LottieAnimationViewModule
  ],
})
export class OrderServiceDetailPageModule {}
