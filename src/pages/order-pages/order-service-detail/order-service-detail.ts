import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Order } from '../../../models/Order';
import { Characteristic } from '../../../models/Characteristic';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-order-service-detail',
  templateUrl: 'order-service-detail.html',
})
export class OrderServiceDetailPage {

  showSplash = true;
  localOrder: Order = new Order();
  localCharacteristics: Characteristic[] = [];

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("order");
  }

  ionViewDidEnter() {
    this.db.orders.characteristics.getAll(this.localOrder.id)
      .then(characteristics => {
        this.localCharacteristics = characteristics;
        this.showSplash = false;
      })
      .catch(e => {
        this.showSplash = false;
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao procurar caracteristicas do pedido', e);
      });
  }

  ionViewDidLeave() {
  }

}
