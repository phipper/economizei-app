import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrdersPage } from './my-orders';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyOrdersPage,
  ],
  imports: [
    IonicPageModule.forChild(MyOrdersPage),
    LottieAnimationViewModule
  ],
})
export class MyOrdersPagePageModule {}
