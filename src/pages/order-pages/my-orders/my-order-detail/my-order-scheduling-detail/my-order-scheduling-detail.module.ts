import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderSchedulingDetailPage } from './my-order-scheduling-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyOrderSchedulingDetailPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(MyOrderSchedulingDetailPage),
  ],
})
export class MyOrderSchedulingDetailPageModule {}
