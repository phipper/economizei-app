import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

import { Order } from '../../../../../models/Order';
import { User } from '../../../../../models/User';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { Schedule } from '../../../../../models/Schedule';
import { Notification } from '../../../../../models/Notification';
import { Calendar, CalendarOptions } from '@ionic-native/calendar';
import { UtilProvider } from '../../../../../providers/Util';


@IonicPage()
@Component({
  selector: 'page-my-order-scheduling-detail',
  templateUrl: 'my-order-scheduling-detail.html',
})
export class MyOrderSchedulingDetailPage {

  showSplash = true;
  localOrder: Order = new Order();
  localUser: User = new User();
  localProvider = new User();
  localSchedules: Schedule[] = [];
  expiredSchedules: Schedule[] = [];
  schedulesSub: any;
  expiringSchedules = false;
  economizeiNotifThumbUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2FeconomizeiNotifThumb.png?alt=media&token=a206e2d7-8856-42b9-b777-561fd651240d';

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/calendar.json'
  }

  constructor(
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private calendar: Calendar,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localOrder = navParams.get("myOrder");
    this.localUser = navParams.get("client");
    this.localProvider = navParams.get("provider");
  }

  expireSchedules() {
    this.expiringSchedules = true;
    this.UiFeedBackCtrl.presenLoader('Atualizando Agendamento expirado...')
      .then(() => {
        for (var i = 0; i < this.expiredSchedules.length; ++i) {
          this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(this.expiredSchedules[i].id).update({ status: 'expired' })
            .catch(e => {
              this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
            });
        }
        this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
        let notif = new Notification();
        notif.id = this.afs.createId();
        notif.title = 'Agendamento EXPIRADO!';
        notif.text = `O agendamento do seu pedido para ${this.localOrder.title} expirou!`;
        notif.type = 'generalAlerts';
        notif.action = {
          type: 'showMyOrder',
          orderId: this.localOrder.id,
        };
        notif.senderName = "economizei";
        this.afs.collection('Users').doc(this.localOrder.clientId).collection('Notifications').doc(notif.id).set(Object.assign({}, notif))
          .then(() => {
            let notif1 = new Notification();
            notif1.id = this.afs.createId();
            notif1.title = 'Agendamento EXPIRADO!';
            notif1.text = `O agendamento do seu pedido para ${this.localOrder.clientName} expirou!`;
            notif1.type = 'generalAlerts';
            notif1.action = {
              type: 'showReceivedOrder',
              orderId: this.localOrder.id,
            };
            notif1.senderName = "economizei";
            this.afs.collection('Users').doc(this.localOrder.providerId).collection('Notifications').doc(notif1.id).set(Object.assign({}, notif1))
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentToast('Um dos agendamentos expirou!', 'error');
              });
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
          });
      });
  }

  ionViewDidEnter() {
    const currentTime = new Date().getTime();
    this.schedulesSub = this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').ref
      .orderBy("createdOn", 'desc').onSnapshot(querySnapshot => {
        this.showSplash = true;
        this.localSchedules = [];
        querySnapshot.forEach(doc => {
          this.localSchedules.push(doc.data() as Schedule);
          let tempStatus = doc.get('status');
          if (tempStatus == 'pending_provider' || tempStatus == 'pending_client') {
            let tempDuedate = doc.get('dueDate');
            if (tempDuedate.getTime() < currentTime) {
              this.expiredSchedules.push(doc.data() as Schedule);
            }
          }
          if (this.localSchedules.length == querySnapshot.size && !querySnapshot.empty) {
            if (this.expiredSchedules.length > 0) {
              if (!this.expiringSchedules) {
                this.expireSchedules();
              }
              this.showSplash = false;
            } else {
              this.showSplash = false;
            }
          }
        });
        if (querySnapshot.empty) {
          this.showSplash = false;
        }
      },
        error => {
          console.log("Erro :( " + error.message);
          this.UiFeedBackCtrl.presentAlert("Erro :(", error.message, 'error');
        });
  }

  ionViewDidLeave() {
    if (this.schedulesSub != null) {
      this.schedulesSub();
    }
  }

  addSchedulePage() {
    this.navCtrl.push('MyOrderCreateSchedulingPage', {
      receivedOrder: this.localOrder,
      provider: this.localProvider
    });
  }

  acceptSchedule(schedule: Schedule) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'Ao aceitar o agendamento você se compromete a receber o prestador e estar disponivel no horário e local marcado no agendamento. Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.UiFeedBackCtrl.presenLoader('Salvando..')
              .then(() => {
                schedule.status = 'approved';
                this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(schedule.id).update({ status: 'approved' })
                  .then(() => {
                    this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                    if (this.localOrder.acceptedBudget != null) {
                      this.localOrder.step = 3;
                      this.localOrder.status = 4;
                    } else {
                      this.localOrder.step = 2;
                      this.localOrder.status = 3;
                    }
                    this.localOrder.acceptedSchedule = schedule;
                    this.afs.collection('Orders').doc<Order>(this.localOrder.id).update({ step: this.localOrder.step, status: this.localOrder.status, acceptedSchedule: this.localOrder.acceptedSchedule })
                      .then(() => {
                        let notif = new Notification();
                        notif.id = this.afs.createId();
                        notif.title = 'Agendamento Aceito!';
                        notif.text = `O agendamento do seu pedido para ${this.localOrder.title} foi aceito!`;
                        notif.type = 'generalAlerts';
                        notif.action = {
                          type: 'showReceivedOrder',
                          orderId: this.localOrder.id,
                        };
                        notif.senderName = "economizei";
                        this.afs.collection('Users').doc(this.localOrder.providerId).collection('Notifications').doc(notif.id).set(Object.assign({}, notif))
                          .then(() => {
                            this.UiFeedBackCtrl.dismissLoader();
                            this.ionViewDidEnter();
                            this.UiFeedBackCtrl.presentToast('Agendamento aceito com sucesso!', 'success');
                          });
                      });
                  }).catch(e => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
                  });
              });
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  rejectSchedule(schedule: Schedule) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O pedido não poderá ser concluido até que seja gerado um novo agendamento. Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.UiFeedBackCtrl.presenLoader('Salvando...')
              .then(() => {
                schedule.status = 'rejected_by_client';
                this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(schedule.id).update({ status: 'rejected_by_client' })
                  .then(() => {
                    this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                    let notif = new Notification();
                    notif.id = this.afs.createId();
                    notif.title = 'Agendamento REJEITADO!';
                    notif.text = `O agendamento do seu pedido para ${this.localOrder.title} foi Rejeitado!`;
                    notif.type = 'generalAlerts';
                    notif.action = {
                      type: 'showReceivedOrder',
                      orderId: this.localOrder.id,
                    };
                    notif.senderName = "economizei";
                    this.afs.collection('Users').doc(this.localOrder.providerId).collection('Notifications').doc(notif.id).set(Object.assign({}, notif))
                      .then(() => {
                        this.UiFeedBackCtrl.dismissLoader();
                        this.ionViewDidEnter();
                        this.UiFeedBackCtrl.presentToast('Agendamento REJEITADO com sucesso!', 'success');
                      });
                  }).catch(e => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
                  });
              });
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelAprovedSchedule(schedule: Schedule) {
    this.UiFeedBackCtrl.presenLoader('Cancelando...')
      .then(() => {
        if (schedule.onClientCalendar) {
          this.UiFeedBackCtrl.updateLoaderMessage('Removendo do seu calendario...');
          let enventTitle = this.localOrder.title;
          let eventLocation = `${this.localOrder.address.street}, ${this.localOrder.address.numero} ${this.localOrder.address.bairro} - ${this.localOrder.address.city} ${this.localOrder.address.state}`;
          let eventNotes = `Entrega do ${this.localOrder.title} feito via economizei`;
          let eventStartDate = schedule.dueDate;
          let eventEndDate = new Date(eventStartDate.getTime() + (1000 * 60 * 60));
          this.calendar.deleteEvent(enventTitle, eventLocation, eventNotes, eventStartDate, eventEndDate)
            .then(hasRemoved => {
              schedule.onClientCalendar = false;
              if (hasRemoved) {
                this.UiFeedBackCtrl.presentToast('Evento REMOVIDO com sucesso!', 'success');
              } else {
                this.UiFeedBackCtrl.presentToast('Evento ja havia sido Removido manualmente!', 'success');
              }
              schedule.status = 'canceled_by_client';
              this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(schedule.id).update({ status: 'canceled_by_client' })
                .then(() => {
                  this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                  this.localOrder.step = 2;
                  this.localOrder.status = 2;
                  this.localOrder.acceptedSchedule = null;
                  this.afs.collection('Orders').doc<Order>(this.localOrder.id).update({ step: 2, status: 2, acceptedSchedule: null });
                  let notif = new Notification();
                  notif.id = this.afs.createId();
                  notif.title = 'Agendamento CANCELADO!';
                  notif.text = `O agendamento do seu pedido para ${this.localOrder.title} foi Cancelado!`;
                  notif.type = 'generalAlerts';
                  notif.action = {
                    type: 'showReceivedOrder',
                    orderId: this.localOrder.id,
                  };
                  notif.senderName = "economizei";
                  this.afs.collection('Users').doc(this.localOrder.providerId).collection('Notifications').doc(notif.id).set(Object.assign({}, notif))
                    .then(() => {
                      this.UiFeedBackCtrl.dismissLoader();
                      this.ionViewDidEnter();
                      this.UiFeedBackCtrl.presentToast('Agendamento CANCELADO com sucesso!', 'success');
                    });
                });
            }).catch(e => {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
            });
        } else {
          schedule.status = 'canceled_by_client';
          this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(schedule.id).update({ status: 'canceled_by_client' })
            .then(() => {
              this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
              this.localOrder.step = 2;
              this.localOrder.status = 2;
              this.localOrder.acceptedSchedule = null;
              this.afs.collection('Orders').doc<Order>(this.localOrder.id).update({ step: 2, status: 2, acceptedSchedule: null });
              let notif = new Notification();
              notif.id = this.afs.createId();
              notif.title = 'Agendamento CANCELADO!';
              notif.text = `O agendamento do seu pedido para ${this.localOrder.title} foi Cancelado!`;
              notif.type = 'generalAlerts';
              notif.action = {
                type: 'showReceivedOrder',
                orderId: this.localOrder.id,
              };
              notif.senderName = "economizei";
              this.afs.collection('Users').doc(this.localOrder.providerId).collection('Notifications').doc(notif.id).set(Object.assign({}, notif))
                .then(() => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.ionViewDidEnter();
                  this.UiFeedBackCtrl.presentToast('Agendamento CANCELADO com sucesso!', 'success');
                });
            }).catch(e => {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
            });
        }
      });
  }

  cancelAprovedScheduleConfirmation(schedule: Schedule) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O pedido não poderá ser concluido até que seja gerado um novo agendamento. Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.cancelAprovedSchedule(schedule);
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelSchedule(schedule: Schedule) {
    this.UiFeedBackCtrl.presenLoader('Cancelando...')
      .then(() => {
        this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc(schedule.id).ref.get().then(scheduleDoc => {
          if (scheduleDoc.get('status') != 'approved') {
            schedule.status = 'canceled_by_client';
            this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(schedule.id).update({ status: 'canceled_by_client' })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.afs.createId();
                notif.title = 'Agendamento CANCELADO!';
                notif.text = `O agendamento do seu pedido para ${this.localOrder.title} foi Cancelado!`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showReceivedOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = "economizei";
                this.afs.collection('Users').doc(this.localOrder.providerId).collection('Notifications').doc(notif.id).set(Object.assign({}, notif))
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.ionViewDidEnter();
                    this.UiFeedBackCtrl.presentToast('Agendamento CANCELADO com sucesso!', 'success');
                  });
              })
          } else {
            this.UiFeedBackCtrl.presentAlert("Erro :(", 'O agendamento já foi aprovado, tente novamente!', 'error');
          }
        }).catch(e => {
          this.UiFeedBackCtrl.dismissLoader();
          this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
        });
      });
  }

  cancelScheduleConfirmation(schedule: Schedule) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O pedido não poderá ser concluido até que seja gerado um novo agendamento. Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.cancelSchedule(schedule);
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  addToCalendar(schedule: Schedule) {
    this.UiFeedBackCtrl.presenLoader('Adicionando ao seu calendario', 'assets/animations/calendar.json')
      .then(() => {
        let enventTitle = this.localOrder.title;
        let eventLocation = `${this.localOrder.address.street}, ${this.localOrder.address.numero} ${this.localOrder.address.bairro} - ${this.localOrder.address.city} ${this.localOrder.address.state}`;
        let eventNotes = `Entrega do ${this.localOrder.title} feito via economizei`;
        let eventStartDate = schedule.dueDate;
        let eventEndDate = new Date(eventStartDate.getTime() + (1000 * 60 * 60));
        let eventOptions: CalendarOptions = {
          id: schedule.id,
          firstReminderMinutes: 30,
          secondReminderMinutes: 15
        };
        this.calendar.createEventWithOptions(enventTitle, eventLocation, eventNotes, eventStartDate, eventEndDate, eventOptions)
          .then(() => {
            schedule.onClientCalendar = true;
            this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(schedule.id).update({ onClientCalendar: true })
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presenLottietAlert('Evento criado com sucesso!', '', 'assets/animations/calendar.json', 'success');
              })
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

  removeFromCalendar(schedule: Schedule) {
    this.UiFeedBackCtrl.presenLoader('Removendo do seu calendario...')
      .then(() => {
        let enventTitle = this.localOrder.title;
        let eventLocation = `${this.localOrder.address.street}, ${this.localOrder.address.numero} ${this.localOrder.address.bairro} - ${this.localOrder.address.city} ${this.localOrder.address.state}`;
        let eventNotes = `Entrega do ${this.localOrder.title} feito via economizei`;
        let eventStartDate = schedule.dueDate;
        let eventEndDate = new Date(eventStartDate.getTime() + (1000 * 60 * 60));
        this.calendar.deleteEvent(enventTitle, eventLocation, eventNotes, eventStartDate, eventEndDate)
          .then(hasRemoved => {
            schedule.onClientCalendar = false;
            this.afs.collection('Orders').doc(this.localOrder.id).collection('Schedules').doc<Schedule>(schedule.id).update({ onClientCalendar: false })
              .then(() => {
                if (hasRemoved) {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentToast('Evento REMOVIDO com sucesso!', 'success');
                } else {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentToast('Evento ja havia sido Removido manualmente!', 'success');
                }
              })
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

}
