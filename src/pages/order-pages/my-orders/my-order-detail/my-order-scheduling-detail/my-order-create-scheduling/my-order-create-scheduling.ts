import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Order } from '../../../../../../models/Order';
import { User } from '../../../../../../models/User';
import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';
import { Schedule } from '../../../../../../models/Schedule';
import { Notification } from '../../../../../../models/Notification';
import { OfficeHours } from '../../../../../../models/OfficeHours';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';
import { UtilProvider } from '../../../../../../providers/Util';


@IonicPage()
@Component({
  selector: 'page-my-order-create-scheduling',
  templateUrl: 'my-order-create-scheduling.html',
})
export class MyOrderCreateSchedulingPage {

  showSplash = true;
  localOrder: Order = new Order();
  provider: User = new User();
  localDate = "";
  localDateValid = false;
  localHourValues: string[] = [];
  localTime = "";
  scheduleReady = false;
  localSchedule: Schedule = new Schedule();
  minDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMinDate();
  maxDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMaxDate(60);
  requiredHomeDelivery = false;
  acceptHomeDelivery = false;
  economizeiNotifThumbUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2FeconomizeiNotifThumb.png?alt=media&token=a206e2d7-8856-42b9-b777-561fd651240d';
  localOfficeHours = new OfficeHours();
  hasOfficeHours = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/calendar.json'
  }

  constructor(
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
    this.provider = navParams.get("provider");
  }

  loadProviderOfficeHours() {
    return new Promise((resolve, reject) => {
      this.db.officeHours.get(this.provider.id)
        .then(officeHours => {
          if (officeHours != null) {
            this.localOfficeHours = officeHours;
            this.hasOfficeHours = true;
            resolve();
          } else {
            resolve();
          }
        })
        .catch(e => {
          reject(e);
        });
    });
  }

  ionViewDidEnter() {
    if (this.localOrder.step == 2) {
      this.db.orders.schedules.getAll(this.localOrder.id)
        .then(schedules => {

          for (var i = 0; i < schedules.length; i++) {
            let tempSchedule = schedules[i];
            if (tempSchedule.status == 'pending_provider'
              || tempSchedule.status == 'pending_client'
              || tempSchedule.status == 'approved') {
              this.UiFeedBackCtrl.presentAlert('O pedido possui agendamentos em abertos', 'Favor dar as devidas trativas aos tratamentos já existentes!', 'warning', true);
              this.navCtrl.pop();
              break;
            }
            if (i == (schedules.length - 1) && !(schedules.length == 0)) {
              this.loadProviderOfficeHours()
                .then(() => {
                  this.loadParameters();
                  this.showSplash = false;
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler horarios de atendimento do prestador", e);
                })
            }
          }
          if (schedules.length == 0) {
            this.loadProviderOfficeHours()
              .then(() => {
                this.loadParameters();
                this.showSplash = false;
              })
              .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler horarios de atendimento do prestador", e);
              })
          }
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro :(", e);
        });
    } else {
      this.UiFeedBackCtrl.presentAlert('O Passo do pedido não permite a criação de agendamentos', 'A criação de agendamentos deve ser feita no passo 2 (Planejamento)', 'warning', true);
      this.navCtrl.pop();
    }
  }

  loadParameters() {
    this.checkForRequiredHomeDelivery();
    this.checkForAcceptHomeDelivery();
  }

  checkForRequiredHomeDelivery() {
    this.requiredHomeDelivery = this.localOrder.requiredHomeDelivery;
  }

  checkForAcceptHomeDelivery() {
    this.acceptHomeDelivery = this.localOrder.acceptHomeDelivery;
    this.localSchedule.homeDelivery = this.acceptHomeDelivery;
    this.localSchedule.geoCheckin = this.acceptHomeDelivery;
  }

  toggleHomeDelivery() {
    this.localSchedule.homeDelivery = !this.localSchedule.homeDelivery;
    if (this.localSchedule.homeDelivery) {
      this.localSchedule.geoCheckin = true;
      this.UiFeedBackCtrl.presenLottietAlert('Atendimento em domicílio adicionada ao seu agendamento', 'O prestador terá de se deslocar até o endereço do seu pedido no horario agendado', 'assets/animations/location.json', 'warning');
    } else {
      this.UiFeedBackCtrl.presentAlert('Atendimento em domicilio removido do seu agendamento', 'Isto significa que você irá se deslocar até o prestador ou deverá receber seu serviço por outros meios', 'warning');
    }
  }

  toggleCheckIn() {
    this.localSchedule.geoCheckin = !this.localSchedule.geoCheckin;
    if (this.localSchedule.geoCheckin) {
      this.UiFeedBackCtrl.presenLottietAlert('Check-in via GPS adicionado ao seu agendamento', 'O prestador terá de fazer Check-in ao chegar no local marcado', 'assets/animations/location_map_pin.json', 'warning');
    } else {
      this.UiFeedBackCtrl.presentAlert('Check-in via GPS removido do seu agendamento', '', 'warning');
    }
  }


  cleanUpAgendamentoSegment() {
    this.scheduleReady = false;
    this.localDateValid = false;
    this.localDate = "";
    this.localTime = "";
  }

  checkDate() {
    let dateToValidate = new Date(`${this.localDate} 00:00:00 GMT-0300 (Hora oficial do Brasil)`);

    if (this.hasOfficeHours) {
      switch (dateToValidate.getDay()) {
        case 0:
          if (!this.localOfficeHours.dom.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende no domingo... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.dom.begin; i < this.localOfficeHours.dom.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 1:
          if (!this.localOfficeHours.seg.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na segunda-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.seg.begin; i < this.localOfficeHours.seg.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 2:
          if (!this.localOfficeHours.ter.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na terça-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.ter.begin; i < this.localOfficeHours.ter.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 3:
          if (!this.localOfficeHours.qua.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na quarta-feira...  Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.qua.begin; i < this.localOfficeHours.qua.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 4:
          if (!this.localOfficeHours.qui.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na quinta-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.qui.begin; i < this.localOfficeHours.qui.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 5:
          if (!this.localOfficeHours.sex.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na sexta-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.sex.begin; i < this.localOfficeHours.sex.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 6:
          if (!this.localOfficeHours.sab.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na sabado... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.sab.begin; i < this.localOfficeHours.sab.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;
      }
    } else {
      this.scheduleReady = false;
      this.localTime = '';
      this.localHourValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
      this.localDateValid = true;
    }
  }

  checkTime() {
    if (this.localTime != "") {
      const currentTime = new Date().getTime();
      let tempDuedate = new Date(`${this.localDate} ${this.localTime}:00 GMT-0300 (Hora oficial do Brasil)`);
      if (tempDuedate.getTime() < currentTime) {
        this.scheduleReady = false;
        this.localTime = '';
        this.UiFeedBackCtrl.presentAlert('O horario selecionado já passou!', 'Favor selecionar um hórario valido!', 'error');
      } else {
        this.scheduleReady = true;
      }
    }
  }

  save() {
    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/calendar.json')
      .then(() => {
        this.localSchedule.id = this.db.createId();
        this.localSchedule.status = 'pending_provider';
        this.localSchedule.createdBy = this.localOrder.clientId;
        this.localSchedule.orderId = this.localOrder.id;
        this.localSchedule.clientId = this.localOrder.clientId;
        this.localSchedule.providerId = this.localOrder.providerId;
        this.localSchedule.dueDate = new Date(`${this.localDate} ${this.localTime}:00 GMT-0300 (Hora oficial do Brasil)`);

        this.db.orders.schedules.create(this.localOrder.id, this.localSchedule.id, this.localSchedule)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Novo Agendamento!';
            notif.text = `Foi criado um novo agendamento para seu pedido de ${this.localOrder.title}!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showReceivedOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = "economizei";
            this.db.users.notifications.create(this.localOrder.providerId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentAlert('Agendamento criado com sucesso!', '', 'success', true);
                this.navCtrl.pop();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

}
