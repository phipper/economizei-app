import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderCreateSchedulingPage } from './my-order-create-scheduling';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyOrderCreateSchedulingPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(MyOrderCreateSchedulingPage),
  ],
})
export class MyOrderCreateSchedulingPageModule {}
