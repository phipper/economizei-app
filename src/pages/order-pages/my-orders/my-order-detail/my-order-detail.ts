import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions, AppSelectionOptions, RememberChoiceOptions } from '@ionic-native/launch-navigator';

import { UserDataProvider } from '../../../../providers/UserData';

import { User } from '../../../../models/User';
import { Order } from '../../../../models/Order';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { Address } from '../../../../models/Address';
import { Notification } from '../../../../models/Notification';

import { Chat } from '../../../../models/Chat';
import { UtilProvider } from '../../../../providers/Util';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
  selector: 'page-my-order-detail',
  templateUrl: 'my-order-detail.html',
})
export class MyOrderDetailPage {

  showSplash = true;
  providerLoaded = false;
  localProvider = new User();
  localOrder = new Order();
  orderSub: any;
  localOrderElapsedTime = '--:--:--';
  localOrderElapsedTimeService = false;

  localProviderNumberOfReviews = 0;

  hasWarrantyTime = true;
  warrantyEndTime: Date = null;

  needToUnsub = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    private launchNavigator: LaunchNavigator,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localOrder.id = navParams.get("orderId");
    this.localOrder.address = new Address();
  }

  loadProviderData() {
    this.db.users.get(this.localOrder.providerId)
      .then(user => {
        this.localProvider = user;
        this.providerLoaded = true;
        this.showSplash = false;
      }).catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar dados do prestador', e);
      });
  }

  ionViewDidLoad() {
    this.orderSub = this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).doc(this.localOrder.id).valueChanges()
      .subscribe(data => {
        this.showSplash = true;
        this.localOrder = data as Order;
        this.UserDataCtrl.getUid()
          .then(localUserUid => {
            if (this.localOrder.clientId == localUserUid) {

              this.db.users.history.getUserAsProviderReviewsNumber(this.localOrder.providerId)
                .then(numberOfReviews => {

                  this.localProviderNumberOfReviews = numberOfReviews;

                  if (this.localOrder.step == 3 && this.localOrder.status == 6 && this.localOrder.acceptedSchedule != null && this.localOrder.acceptedSchedule.actualCheckinDate != null) {
                    this.localOrderElapsedTimeService = true;
                    this.getLocalOrderElapsedTimeService();
                  }

                  if (this.localOrder.status != 1300 && this.localOrder.step == 4 && this.localOrder.clientReviewId != '' && this.localOrder.providerReviewId != '') {
                    this.finishOrder();
                  }

                  if (this.localOrder.status != 1302 && this.localOrder.status != 1303 && this.localOrder.step == 4) {
                    if (this.localOrder.acceptedBudget.warrantyTime != '') {
                      let tempWarrantyDays = 0;
                      switch (this.localOrder.acceptedBudget.warrantyTime) {
                        case '15 dias':
                          tempWarrantyDays = 15;
                          break;
                        case '1 mês':
                          tempWarrantyDays = 30;
                          break;
                        case '3 mêses':
                          tempWarrantyDays = 30 * 3;
                          break;
                        case '6 mêses':
                          tempWarrantyDays = 30 * 6;
                          break;
                        case '1 ano':
                          tempWarrantyDays = 30 * 12;
                          break;
                      }
                      let actualTime = new Date().getTime();
                      this.warrantyEndTime = new Date(this.localOrder.acceptedSchedule.actualCheckinDate.getTime() + (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * tempWarrantyDays));
                      this.hasWarrantyTime = actualTime < this.warrantyEndTime.getTime();
                    } else {
                      this.hasWarrantyTime = false;
                      this.warrantyEndTime = null;
                    }
                  }

                  if (!this.providerLoaded) {
                    this.loadProviderData();
                  } else {
                    this.showSplash = false;
                  }

                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                })

            } else {
              this.navCtrl.pop()
                .then(() => {
                  this.UiFeedBackCtrl.presenLottietAlert('Acesso NEGADO!', 'Somente o cliente deste pedido tem acesso à esta pagina!', 'assets/animations/lock.json', 'error');
                });
            }
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar seus dados', e);
          })
      },
        error => {
          console.log("Erro :( " + error.message);
          this.UiFeedBackCtrl.presentAlert("Erro :(", error.message, 'error');
        });
  }

  ionViewDidLeave() {
    if (this.needToUnsub) {
      if (this.orderSub != null) {
        this.orderSub.unsubscribe();
      }
      if (this.localOrderElapsedTimeService) {
        this.localOrderElapsedTimeService = false;
      }
    } else {
      this.needToUnsub = true
    }
  }

  getLocalOrderElapsedTimeService() {
    let d = new Date();
    this.localOrderElapsedTime = this.utilCtrl.convertMiliSecondsToHMmSs(d.getTime() - this.localOrder.acceptedSchedule.actualCheckinDate.getTime());
    setTimeout(() => {
      if (this.localOrderElapsedTimeService) {
        this.getLocalOrderElapsedTimeService();
      }
    }, 1000);
  }


  finishOrder() {
    this.UiFeedBackCtrl.presenLoader(`Finalizando Pedido!`)
      .then(() => {
        this.localOrder.status = 1300;
        this.db.orders.update(this.localOrder.id, { status: this.localOrder.status })
          .then(() => {
            this.db.users.history.registerOrderCompletion(this.localOrder)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentAlert('Pedido finalizado!', 'Obrigado por utilizar o economizei', 'success', true);
              })
          })
      });
  }

  localOrderIsActive(): boolean {
    return (this.localOrder.status <= 1300)
  }

  deactivateGPSCheckinConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'Desative o checkin via GPS somente se o prestador já estivar no local do serviço. Deseja continuar?',
      buttons: [
        'Cancelar',
        {
          text: 'SIM',
          handler: () => {
            this.deactivateGPSCheckin();
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  deactivateGPSCheckin() {
    this.UiFeedBackCtrl.presenLoader('Modificando algumas coisas...', 'assets/animations/location.json')
      .then(() => {
        this.localOrder.acceptedSchedule.geoCheckin = false;
        this.db.orders.schedules.update(this.localOrder.id, this.localOrder.acceptedSchedule.id, { geoCheckin: this.localOrder.acceptedSchedule.geoCheckin })
          .then(() => {
            this.db.orders.update(this.localOrder.id, { acceptedSchedule: this.localOrder.acceptedSchedule })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.db.createId();
                notif.title = 'Checkin via GPS desativado!';
                notif.text = `O checkin via GPS do pedido para ${this.localOrder.clientName} foi desativado!`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showReceivedOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = "economizei";
                this.db.users.notifications.create(this.localOrder.providerId, notif)
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast('Checkin via GPS desativado!', 'success');
                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
          });
      });
  }

  openMap() {
    var tempAddress: Address;
    if (this.localOrder.address != null) {
      tempAddress = this.localOrder.address;
    } else {
      tempAddress = this.localOrder.banner.address;
    }
    let myRememberChoiceOptions: RememberChoiceOptions = {
      enabled: false
    };
    let myAppSelectionOptions: AppSelectionOptions = {
      dialogHeaderText: 'Selecione um aplicativo para abrir o endereço',
      cancelButtonText: 'Cancelar',
      rememberChoice: myRememberChoiceOptions
    };
    let options: LaunchNavigatorOptions = {
      appSelection: myAppSelectionOptions
    };
    this.launchNavigator.navigate([tempAddress.latitude, tempAddress.longitude], options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  cancelOrder() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Motivo:');

    alert.addInput({
      type: 'radio',
      label: 'Indisponibilidade de horário',
      value: 'Indisponibilidade de horário',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Orçamento',
      value: 'Orçamento'
    });

    alert.addInput({
      type: 'radio',
      label: 'Prestador',
      value: 'Prestador'
    });

    alert.addInput({
      type: 'radio',
      label: 'Outros',
      value: 'Outros',
      handler: () => {
        this.cancelOrderOtherMotive();
        alert.dismiss();
      }
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        this.cancelOrderStep2(data);
      }
    });

    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelOrderOtherMotive() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro motivo',
      message: "Digite o Motivo para cancelar o pedido",
      inputs: [
        {
          name: 'motive',
          placeholder: 'Motivo'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.cancelOrderStep2(data.motive)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    prompt.present();
  }

  cancelOrderStep2(reason) {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrder.status = 1303;
        this.localOrder.rejectReason = reason;
        this.db.orders.update(this.localOrder.id, { status: this.localOrder.status, rejectReason: reason })
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Pedido CANCELADO!';
            notif.text = `O pedido para ${this.localOrder.title} de ${this.UserDataCtrl.localUser.fullName} foi CANCELADO!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showReceivedOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = "economizei";
            this.db.users.notifications.create(this.localOrder.providerId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentToast('Pedido CANCELADO com sucesso!', 'success');
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
          });
      });
  }

  showProvider() {
    this.needToUnsub = false;
    this.navCtrl.push('ProviderDetailPage', {
      provider: this.localProvider
    });
  }

  showServiceDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('OrderServiceDetailPage', {
      order: this.localOrder
    });
  }

  showSchedulingDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('MyOrderSchedulingDetailPage', {
      myOrder: this.localOrder,
      client: this.UserDataCtrl.localUser,
      provider: this.localProvider
    });
  }

  showBudgetDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('MyOrderBudgetDetailPage', {
      receivedOrder: this.localOrder
    });
  }


  createChat(): Promise<any> {
    return new Promise((resolve, reject) => {
      let tempChat = new Chat;
      tempChat.id = this.db.createId();
      tempChat.orderId = this.localOrder.id;
      tempChat.title = this.localOrder.title;
      tempChat.usersData.push({
        userId: this.localOrder.clientId,
        userName: this.localOrder.clientName,
        userThumbnailUrl: this.localOrder.clientThumbnailUrl
      });
      tempChat.usersData.push({
        userId: this.localOrder.providerId,
        userName: this.localOrder.providerName,
        userThumbnailUrl: this.localOrder.providerThumbnailUrl
      });
      this.db.chats.create(tempChat)
        .then(() => {
          this.localOrder.chatId = tempChat.id;
          this.db.orders.update(this.localOrder.id, { chatId: this.localOrder.chatId })
            .then(() => {
              this.db.users.getDoc(this.localProvider.id)
                .then(providerDoc => {
                  let tempProviderChats = providerDoc.get('chats') as string[];
                  tempProviderChats.push(tempChat.id);
                  this.localProvider.chats = tempProviderChats;
                  providerDoc.ref.update({ chats: this.localProvider.chats })
                    .then(() => {
                      this.UserDataCtrl.localUser.chats.push(tempChat.id);
                      this.UserDataCtrl.update({ chats: this.UserDataCtrl.localUser.chats })
                        .then(() => {
                          resolve();
                        })
                    })
                })
            })
        })
        .catch(e => {
          reject(e)
        })
    });
  }

  openChat() {
    if (this.localOrder.chatId != '') {
      this.needToUnsub = false;
      this.navCtrl.push('ChatPage', {
        chatId: this.localOrder.chatId
      });
    } else {
      let loader = this.UiFeedBackCtrl.loadingCtrl.create({
        content: 'Criando Conversa...'
      });
      loader.present()
        .then(() => {
          this.createChat()
            .then(() => {
              loader.dismiss()
                .then(() => {
                  this.openChat();
                });
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('erro ao criar Chat', e);
            })
        });
    }
  }

  showClientReviewDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('myOrderClientReviewDetailPage', {
      myOrder: this.localOrder
    });
  }

  showProviderReviewDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('MyOrderProviderReviewDetailPage', {
      myOrder: this.localOrder
    });
  }

  cancelScheduleConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O Cancelamento do atendimento irá cancelar o agendamento aprovado, um novo agendamento terá de ser criado e aprovado para continuar o pedido. Deseja continuar?',
      buttons: [
        'Cancelar',
        {
          text: 'SIM',
          handler: () => {
            this.cancelScheduleMotiveSelection();
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelScheduleMotiveSelection() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Motivo:');

    alert.addInput({
      type: 'radio',
      label: 'Cliente Ausente',
      value: 'Cliente Ausente',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Orçamento',
      value: 'Orçamento'
    });

    alert.addInput({
      type: 'radio',
      label: 'Prestador',
      value: 'Prestador'
    });

    alert.addInput({
      type: 'radio',
      label: 'Outros',
      value: 'Outros',
      handler: data => {
        this.cancelScheduleOtherMotive();
        alert.dismiss();
      }
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        this.cancelSchedule(data);
      }
    });
    alert.present();
  }

  cancelScheduleOtherMotive() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro motivo',
      message: "Digite o Motivo para cancelar o Atendimento",
      inputs: [
        {
          name: 'motive',
          placeholder: 'Motivo'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.cancelSchedule(data.motive)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    prompt.present();
  }

  cancelSchedule(reason: string) {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrderElapsedTimeService = false;
        this.localOrderElapsedTime = '--:--:--';
        this.localOrder.acceptedSchedule.status = 'canceled_by_client';
        this.localOrder.acceptedSchedule.rejectReason = reason;
        this.localOrder.step = 2;
        this.localOrder.status = 2;
        this.db.orders.schedules.update(this.localOrder.id, this.localOrder.acceptedSchedule.id, { status: this.localOrder.acceptedSchedule.status, rejectReason: this.localOrder.acceptedSchedule.rejectReason })
          .then(() => {
            this.localOrder.acceptedSchedule = null;
            this.db.orders.update(this.localOrder.id, { step: this.localOrder.step, status: this.localOrder.status, acceptedSchedule: this.localOrder.acceptedSchedule })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.db.createId();
                notif.title = 'Agendamento cancelado!';
                notif.text = `O agendamento para seu pedido para ${this.localOrder.title} Acabou de ser cancelado!`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showMyOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = "economizei";
                this.db.users.notifications.create(this.localOrder.clientId, notif)
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast('Agendamento cancelado com sucesso!', 'success');
                  });
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
          });
      });
  }

}
