import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Order } from '../../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';
import { Notification } from '../../../../../../models/Notification';
import { AsProviderReview } from '../../../../../../models/AsProviderReview';
import { Compliment } from '../../../../../../models/Compliment';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-my-order-edit-provider-review',
  templateUrl: 'my-order-edit-provider-review.html',
})
export class MyOrderEditProviderReviewPage {

  reviewStep = 1;
  localOrder: Order = new Order();
  localReview: AsProviderReview = new AsProviderReview();
  hasChanges = false;
  clientOpinionText = '';
  originalPunctuality = null;
  originalClientOpinion = null;
  originalBudgetAccuracy = null;
  originalServiceDelivery = null;
  originalText = '';

  constructor(
    public viewCtrl: ViewController,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("myOrder");
    this.localReview = navParams.get("providerReview");
  }

  ionViewDidEnter() {
    this.originalPunctuality = this.localReview.punctuality;
    this.originalClientOpinion = this.localReview.clientOpinion;
    this.originalBudgetAccuracy = this.localReview.budgetAccuracy;
    this.originalServiceDelivery = this.localReview.serviceDelivery;
    this.originalText = this.localReview.text;
    switch (this.localReview.clientOpinion) {
      case 1:
        this.clientOpinionText = 'Péssimo';
        break;
      case 2:
        this.clientOpinionText = 'Ruim';
        break;
      case 3:
        this.clientOpinionText = 'Regular';
        break;
      case 4:
        this.clientOpinionText = 'Bom';
        break;
      case 5:
        this.clientOpinionText = 'Excelente';
        break;
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  goBack() {
    if (this.reviewStep > 0) {
      this.reviewStep = this.reviewStep - 1;
    }
  }

  goForward() {
    if (this.reviewStep < 3) {
      this.reviewStep = this.reviewStep + 1;
    } else {
      this.checkForChangesAndSave();
    }
  }

  selectPunctuality(punctuality: number) {
    this.hasChanges = true;
    this.localReview.punctuality = punctuality;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  selectServiceDelivery(serviceDelivery: number) {
    this.hasChanges = true;
    this.localReview.serviceDelivery = serviceDelivery;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  selectBudgetAccuracy(budgetAccuracy: number) {
    this.hasChanges = true;
    this.localReview.budgetAccuracy = budgetAccuracy;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  changeRating() {
    this.hasChanges = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    switch (this.localReview.clientOpinion) {
      case 1:
        this.clientOpinionText = 'Péssimo';
        break;
      case 2:
        this.clientOpinionText = 'Ruim';
        break;
      case 3:
        this.clientOpinionText = 'Regular';
        break;
      case 4:
        this.clientOpinionText = 'Bom';
        break;
      case 5:
        this.clientOpinionText = 'Excelente';
        break;
    }
  }

  checkForChanges(): boolean {
    return this.localReview.punctuality != this.originalPunctuality
      || this.localReview.budgetAccuracy != this.originalBudgetAccuracy
      || this.localReview.serviceDelivery != this.originalServiceDelivery
      || this.localReview.clientOpinion != this.originalClientOpinion
      || this.localReview.text != this.originalText;
  }

  checkForChangesAndSave() {
    if (this.checkForChanges()) {
      this.save();
    } else {
      this.UiFeedBackCtrl.presentAlert('Não existem alterações à serem salvas', '', 'error');
    }
  }

  save() {
    this.UiFeedBackCtrl.presenLoader('Salvando', 'assets/animations/star.json')
      .then(() => {
        this.localReview.processed = false;
        this.localReview.moderated = false;
        this.db.users.asProviderReviews.update(this.localReview)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Avaliação alterada!';
            notif.text = `A sua avalição como prestador para o pedido ${this.localOrder.title} foi alterada!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showReceivedOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = "economizei";
            this.db.users.notifications.create(this.localOrder.providerId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentAlert('Avaliação Alterada com sucesso!', '', 'success', true);
                this.viewCtrl.dismiss();
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

  selectStandardComment() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Adicionar comentário:');

    for (let compliment of this.db.users.history.STANDARD_PROVIDER_COMPLIMENTS) {
      alert.addInput({
        type: 'radio',
        label: compliment.text,
        value: JSON.stringify(compliment),
      });
    }

    alert.addInput({
      type: 'radio',
      label: 'Outro',
      value: 'Outro',
      handler: () => {
        this.selectCustomComment();
        alert.dismiss();
      }
    });
    
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        if (data) {
          this.defineCompliment(data);
        } else {
          this.UiFeedBackCtrl.presentAlert(`Erro!`, `Favor selecionar alguma opção`, `warning`, true);
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  selectCustomComment() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro',
      message: "Digite o seu comentário",
      inputs: [
        {
          name: 'comment',
          placeholder: 'Comentário',
          max: 250
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.defineCustomComment(data.comment)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    prompt.present();
  }

  defineCompliment(data: string) {
    let tempCompliment = JSON.parse(data) as Compliment;
    this.localReview.verified = true;
    this.localReview.text = tempCompliment.text;
    this.localReview.complimentCode = tempCompliment.code;
    this.hasChanges = true;
  }

  defineCustomComment(comment: string) {
    this.hasChanges = true;
    this.localReview.verified = false;
    this.localReview.text = comment;
  }

}
