import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderEditProviderReviewPage } from './my-order-edit-provider-review';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    MyOrderEditProviderReviewPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(MyOrderEditProviderReviewPage),
  ],
})
export class MyOrderEditProviderReviewPageModule {}
