import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Order } from '../../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';
import { Notification } from '../../../../../../models/Notification';
import { AsProviderReview } from '../../../../../../models/AsProviderReview';
import { Compliment } from '../../../../../../models/Compliment';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-my-order-create-provider-review',
  templateUrl: 'my-order-create-provider-review.html',
})
export class MyOrderCreateProviderReviewPage {

  reviewStep = 1;
  localOrder: Order = new Order();
  localReview: AsProviderReview = new AsProviderReview();
  punctualityReady = false;
  serviceDeliveryReady = false;
  budgetAccuracyReady = false;
  clientOpinionReady = false;
  clientOpinionText = '';

  constructor(
    public viewCtrl: ViewController,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("myOrder");
    this.localReview.id = this.db.createId();
    this.localReview.title = this.localOrder.title;
    this.localReview.clientId = this.localOrder.clientId;
    this.localReview.clientName = this.localOrder.clientName;
    this.localReview.providerId = this.localOrder.providerId;
    this.localReview.orderId = this.localOrder.id;
    if (this.localOrder.banner != null) {
      this.localReview.bannerId = this.localOrder.banner.id;
    }
  }

  ionViewDidEnter() {
  }

  close() {
    this.viewCtrl.dismiss();
  }

  goBack() {
    if (this.reviewStep > 0) {
      this.reviewStep = this.reviewStep - 1;
    }
  }

  goForward() {
    if (this.reviewStep < 3) {
      this.reviewStep = this.reviewStep + 1;
    } else {
      this.save();
    }
  }

  selectPunctuality(punctuality: number) {
    this.localReview.punctuality = punctuality;
    this.punctualityReady = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  selectServiceDelivery(serviceDelivery: number) {
    this.localReview.serviceDelivery = serviceDelivery;
    this.serviceDeliveryReady = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  selectBudgetAccuracy(budgetAccuracy: number) {
    this.localReview.budgetAccuracy = budgetAccuracy;
    this.budgetAccuracyReady = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  changeRating() {
    this.clientOpinionReady = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    switch (this.localReview.clientOpinion) {
      case 1:
        this.clientOpinionText = 'Péssimo';
        break;
      case 2:
        this.clientOpinionText = 'Ruim';
        break;
      case 3:
        this.clientOpinionText = 'Regular';
        break;
      case 4:
        this.clientOpinionText = 'Bom';
        break;
      case 5:
        this.clientOpinionText = 'Excelente';
        break;
    }
  }

  canFinishOrder(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.db.orders.get(this.localOrder.id)
        .then(order => {
          resolve(order.clientReviewId != '')
        })
        .catch(e => {
          reject(e);
        });
    })
  }

  save() {
    this.UiFeedBackCtrl.presenLoader('Salvando', 'assets/animations/star.json')
      .then(() => {
        this.db.users.asProviderReviews.create(this.localReview)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            this.localOrder.providerReviewId = this.localReview.id;
            this.canFinishOrder()
              .then(canFinishOrder => {
                if (canFinishOrder) {
                  this.localOrder.status = 1300;
                }
                this.db.orders.update(this.localOrder.id, { providerReviewId: this.localOrder.providerReviewId, status: this.localOrder.status })
                  .then(() => {
                    let notif = new Notification();
                    notif.id = this.db.createId();
                    notif.title = 'Nova Avaliação!';
                    notif.text = `Você recebeu uma nova avaliação de ${this.localOrder.clientName}!`;
                    notif.type = 'generalAlerts';
                    notif.action = {
                      type: 'showReceivedOrder',
                      orderId: this.localOrder.id,
                    };
                    notif.senderName = "economizei";
                    this.db.users.notifications.create(this.localOrder.providerId, notif)
                      .then(() => {
                        if (this.localOrder.status == 1300) {
                          this.db.users.history.registerOrderCompletion(this.localOrder)
                            .then(() => {
                              this.UiFeedBackCtrl.presentAlert('Pedido finalizado!', 'Obrigado por utilizar o economizei', 'success', true);
                              this.UiFeedBackCtrl.dismissLoader();
                              this.UiFeedBackCtrl.presenLottietAlert('Maravilha!', 'Avaliação criada com sucesso!', 'assets/animations/rating.json', 'success');
                              this.viewCtrl.dismiss();
                            });
                        } else {
                          this.UiFeedBackCtrl.dismissLoader();
                          this.UiFeedBackCtrl.presenLottietAlert('Maravilha!', 'Avaliação criada com sucesso!', 'assets/animations/rating.json', 'success');
                          this.viewCtrl.dismiss();
                        }
                      })
                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }


  selectStandardComment() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Adicionar comentário:');

    for (let compliment of this.db.users.history.STANDARD_PROVIDER_COMPLIMENTS) {
      alert.addInput({
        type: 'radio',
        label: compliment.text,
        value: JSON.stringify(compliment),
      });
    }

    alert.addInput({
      type: 'radio',
      label: 'Outro',
      value: 'Outro',
      handler: () => {
        this.selectCustomComment();
        alert.dismiss();
      }
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        if (data) {
          this.defineCompliment(data);
        } else {
          this.UiFeedBackCtrl.presentAlert(`Erro`, `Favor selecionar alguma opção`, `warning`, true);
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  selectCustomComment() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro',
      message: "Digite o seu comentário",
      inputs: [
        {
          name: 'comment',
          placeholder: 'Comentário',
          max: 250
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.defineCustomComment(data.comment)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    prompt.present();
  }

  defineCompliment(data: string) {
    let tempCompliment = JSON.parse(data) as Compliment;
    this.localReview.verified = true;
    this.localReview.text = tempCompliment.text;
    this.localReview.complimentCode = tempCompliment.code;
  }

  defineCustomComment(comment: string) {
    this.localReview.verified = false;
    this.localReview.text = comment;
    this.localReview.complimentCode = '';
  }

}
