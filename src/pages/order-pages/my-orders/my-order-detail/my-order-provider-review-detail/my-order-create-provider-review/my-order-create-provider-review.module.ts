import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderCreateProviderReviewPage } from './my-order-create-provider-review';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    MyOrderCreateProviderReviewPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(MyOrderCreateProviderReviewPage),
  ],
})
export class MyOrderCreateProviderReviewPageModule {}
