import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

import { Order } from '../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { AsProviderReview } from '../../../../../models/AsProviderReview';



@IonicPage()
@Component({
  selector: 'page-my-order-provider-review-detail',
  templateUrl: 'my-order-provider-review-detail.html',
})
export class MyOrderProviderReviewDetailPage {

  showSplash = true;
  localOrder: Order = new Order();
  hasReview = false;
  localReview: AsProviderReview = new AsProviderReview;
  reviewSub: any;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("myOrder");
  }

  ionViewDidEnter() {
    this.reviewSub = this.afs.collection('Users').doc(this.localOrder.providerId).collection('AsProviderReviews').ref
      .orderBy("createdOn", 'desc').where('orderId', '==', this.localOrder.id).onSnapshot(querySnapshot => {
        this.showSplash = true;
        if (querySnapshot.empty) {
          this.showSplash = false;
        } else {
          this.localReview = querySnapshot.docs[0].data() as AsProviderReview;
          this.hasReview = true;
          this.showSplash = false;
        }
      },
        error => {
          console.log("Erro :( " + error.message);
          this.UiFeedBackCtrl.presentAlert("Erro :(", error.message, 'error');
        });
  }

  ionViewDidLeave() {
    if (this.reviewSub != null) {
      this.reviewSub();
    }
  }

  createReviewModal() {
    let modal = this.modalCtrl.create('MyOrderCreateProviderReviewPage', {
      myOrder: this.localOrder
    });
    modal.present();
  }

  editProviderReview() {
    let modal = this.modalCtrl.create('MyOrderEditProviderReviewPage', {
      myOrder: this.localOrder,
      providerReview: this.localReview
    });
    modal.onDidDismiss(() => {
      this.ionViewDidEnter();
    });
    modal.present();
    this.ionViewDidLeave();
  }

  translateRate(rate: number): string {
    switch (rate) {
      case 1:
        return 'Péssimo';
      case 2:
        return 'Ruim';
      case 3:
        return 'Regular';
      case 4:
        return 'Bom';
      case 5:
        return 'Excelente';
    }
  }

}
