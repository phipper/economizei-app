import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderProviderReviewDetailPage } from './my-order-provider-review-detail';
import { Ionic2RatingModule } from 'ionic2-rating';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyOrderProviderReviewDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(MyOrderProviderReviewDetailPage),
  ],
})
export class MyOrderProviderReviewDetailPageModule {}
