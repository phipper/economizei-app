import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { myOrderClientReviewDetailPage } from './my-order-client-review-detail';
import { Ionic2RatingModule } from 'ionic2-rating';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    myOrderClientReviewDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(myOrderClientReviewDetailPage),
  ],
})
export class myOrderClientReviewDetailPageModule {}
