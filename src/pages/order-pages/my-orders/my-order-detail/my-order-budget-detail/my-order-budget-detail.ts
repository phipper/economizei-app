import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Order } from '../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { Notification } from '../../../../../models/Notification';
import { Budget } from '../../../../../models/Budget';
import { UtilProvider } from '../../../../../providers/Util';
import { EcoFirestore } from '../../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-my-order-budget-detail',
  templateUrl: 'my-order-budget-detail.html',
})
export class MyOrderBudgetDetailPage {

  showSplash = true;
  localOrder: Order = new Order();
  localBudgets: Budget[] = [];
  budgetsSub: any;
  expiringSchedules = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
  }

  ionViewDidEnter() {
    this.budgetsSub = this.db.afs.collection('Orders').doc(this.localOrder.id).collection('Budgets').ref
      .orderBy("createdOn", 'desc').onSnapshot(querySnapshot => {
        this.showSplash = true;
        this.localBudgets = [];
        querySnapshot.forEach(doc => {
          this.localBudgets.push(doc.data() as Budget);
          if (this.localBudgets.length == querySnapshot.size && !querySnapshot.empty) {
            this.showSplash = false;
          }
        });
        if (querySnapshot.empty) {
          this.showSplash = false;
        }
      },
        error => {
          console.log("Erro :( " + error.message);
          this.UiFeedBackCtrl.presentAlert("Erro :(", error.message, 'error');
        });
  }

  ionViewDidLeave() {
    if (this.budgetsSub != null) {
      this.budgetsSub();
    }
  }

  canCancelBudget(): boolean {
    return (this.localOrder.step == 2 || (this.localOrder.step == 3 && this.localOrder.status == 4));
  }

  acceptBudget(budget: Budget) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'Ao aceitar o orçamento você confirma que esta ciente que o valor do orçamento trata-se de uma estimativa e que o valor final do serviço poderá sofrer alterações! Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.UiFeedBackCtrl.presenLoader('Salvando')
              .then(() => {
                budget.status = 'approved';
                this.db.orders.budgets.update(this.localOrder.id, budget.id, { status: budget.status })
                  .then(() => {
                    this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                    if (this.localOrder.acceptedSchedule != null) {
                      this.localOrder.step = 3;
                      this.localOrder.status = 4;
                    } else {
                      this.localOrder.step = 2;
                      this.localOrder.status = 2;
                    }
                    this.localOrder.acceptedBudget = budget;
                    this.db.orders.update(this.localOrder.id, { step: this.localOrder.step, status: this.localOrder.status, acceptedBudget: this.localOrder.acceptedBudget })
                    let notif = new Notification();
                    notif.id = this.db.createId();
                    notif.title = 'Orçamento Aceito!';
                    notif.text = `O orçamento para o pedido de ${this.localOrder.clientName} foi aceito!`;
                    notif.type = 'generalAlerts';
                    notif.action = {
                      type: 'showReceivedOrder',
                      orderId: this.localOrder.id,
                    };
                    notif.senderName = "economizei";
                    this.db.users.notifications.create(this.localOrder.providerId, notif)
                      .then(() => {
                        this.UiFeedBackCtrl.dismissLoader();
                        this.ionViewDidEnter();
                        this.UiFeedBackCtrl.presentToast('Orçamento aceito com sucesso!', 'success');
                      });
                  })
                  .catch(e => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
                  });
              });
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  rejectBudget(budget: Budget) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O pedido não poderá ser concluido até que seja gerado um novo orçamento. Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.UiFeedBackCtrl.presenLoader('Salvando...')
              .then(() => {
                budget.status = 'rejected_by_client';
                this.db.orders.budgets.update(this.localOrder.id, budget.id, { status: budget.status })
                  .then(() => {
                    this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                    let notif = new Notification();
                    notif.id = this.db.createId();
                    notif.title = 'Orçamento REJEITADO!';
                    notif.text = `O orçamento do seu pedido para ${this.localOrder.clientName} foi Rejeitado!`;
                    notif.type = 'generalAlerts';
                    notif.action = {
                      type: 'showReceivedOrder',
                      orderId: this.localOrder.id,
                    };
                    notif.senderName = "economizei";
                    this.db.users.notifications.create(this.localOrder.providerId, notif)
                      .then(() => {
                        this.UiFeedBackCtrl.dismissLoader();
                        this.ionViewDidEnter();
                        this.UiFeedBackCtrl.presentToast('Agendamento REJEITADO com sucesso!', 'success');
                      });
                  }).catch(e => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
                  });
              });
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelBudget(budget: Budget) {
    this.UiFeedBackCtrl.presenLoader('Cancelando...')
      .then(() => {
        budget.status = 'canceled_by_client';
        this.db.orders.budgets.update(this.localOrder.id, budget.id, { status: budget.status })
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            if (this.localOrder.acceptedSchedule == null) {
              this.localOrder.status = 2;
            } else {
              this.localOrder.status = 3;
            }
            this.localOrder.step = 2;
            this.localOrder.acceptedBudget = null;
            this.db.orders.update(this.localOrder.id, { step: 2, status: this.localOrder.status, acceptedBudget: this.localOrder.acceptedBudget });
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Orçamento CANCELADO!';
            notif.text = `O orçamento do seu pedido para ${this.localOrder.clientName} foi Cancelado!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showReceivedOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = "economizei";
            this.db.users.notifications.create(this.localOrder.providerId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.ionViewDidEnter();
                this.UiFeedBackCtrl.presentToast('Agendamento Orçamento com sucesso!', 'success');
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

  cancelBudgetConfirmation(budget: Budget) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O pedido não poderá ser concluido até que seja gerado um novo Orçamento. Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.cancelBudget(budget);
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

}
