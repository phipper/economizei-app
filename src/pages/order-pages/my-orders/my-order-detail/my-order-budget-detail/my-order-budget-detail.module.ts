import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderBudgetDetailPage } from './my-order-budget-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyOrderBudgetDetailPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(MyOrderBudgetDetailPage),
  ],
})
export class MyOrderBudgetDetailPageModule {}
