import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrderDetailPage } from './my-order-detail';
import { LottieAnimationViewModule } from 'ng-lottie';
import { Ionic2RatingModule } from 'ionic2-rating';


@NgModule({
  declarations: [
    MyOrderDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    LottieAnimationViewModule,  
    IonicPageModule.forChild(MyOrderDetailPage),
  ],
})
export class MyOrderDetailPageModule {}
