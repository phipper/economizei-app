import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderProviderReviewDetailPage } from './received-order-provider-review-detail';
import { Ionic2RatingModule } from 'ionic2-rating';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrderProviderReviewDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(ReceivedOrderProviderReviewDetailPage),
  ],
})
export class ReceivedOrderProviderReviewDetailPageModule {}
