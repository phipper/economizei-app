import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';


import { UserDataProvider } from '../../../../providers/UserData';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { LocalizationProvider } from '../../../../providers/Localization/Localization';
import { UserPlanProvider } from '../../../../providers/UserPlan';

import { Address } from '../../../../models/Address';
import { Notification } from '../../../../models/Notification';
import { Chat } from '../../../../models/Chat';
import { User } from '../../../../models/User';
import { Order } from '../../../../models/Order';
import { Subscription } from 'rxjs/Subscription';
import { UtilProvider } from '../../../../providers/Util';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
  selector: 'page-received-order-detail',
  templateUrl: 'received-order-detail.html',
})
export class ReceivedOrderDetailPage {

  showSplash = true;
  clientLoaded = false;
  localClient = new User();
  localOrder = new Order();
  canGeoCheckin = false;
  actLat: number;
  actLong: number;
  actDistInKm: number;
  geoAccuracy: number;
  orderSub: Subscription = null;
  geoSub: Subscription = null;
  schedulesSub: any = null;
  checkinCode = '';
  checkinCodeInputPlaceholder = 'Codigo do checkin';
  checkinCodeRight = null;
  showGeoSplash = true;
  localOrderElapsedTime = '--:--:--';
  localOrderElapsedTimeService = false;
  localClientNumberOfReviews = 0;

  hasWarrantyTime = true;
  warrantyEndTime: Date = null;

  needToUnsub = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private UserPlanCtrl: UserPlanProvider,
    private LocalizationCtrl: LocalizationProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localOrder.id = navParams.get('orderId');
    this.localOrder.address = new Address();
  }

  loadClientData() {
    this.db.users.get(this.localOrder.clientId)
      .then(user => {
        this.localClient = user;
        this.showSplash = false;
        this.clientLoaded = true;
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar dados do cliente', e);
      });
  }

  ionViewDidLoad() {

    this.orderSub = this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).doc(this.localOrder.id).valueChanges()
      .subscribe(data => {
        this.showSplash = true;
        this.localOrder = data as Order;

        this.UserDataCtrl.getUid()
          .then(localUserUid => {
            if (this.localOrder.providerId == localUserUid) {

              this.db.users.history.getUserAsClientReviewsNumber(this.localOrder.clientId)
                .then(numberOfReviews => {

                  this.localClientNumberOfReviews = numberOfReviews;

                  if (this.localOrder.step == 3 && this.localOrder.status == 6 && this.localOrder.acceptedSchedule != null && this.localOrder.acceptedSchedule.actualCheckinDate != null) {
                    this.localOrderElapsedTimeService = true;
                    this.getLocalOrderElapsedTimeService();
                  }
                  if (this.localOrder.step == 3 && this.localOrder.status == 4) {
                    if (this.localOrder.acceptedSchedule.homeDelivery && this.localOrder.acceptedSchedule.geoCheckin) {
                      if (this.platform.is('cordova')) {
                        console.log('ReceivedOrderDetailPage says: É cordova');
                        this.geoSub = this.LocalizationCtrl.watchPosition()
                          .subscribe(data => {
                            this.showGeoSplash = false;
                            this.actLat = data.coords.latitude
                            this.actLong = data.coords.longitude
                            this.geoAccuracy = Number(data.coords.accuracy.toFixed(2));
                            this.actDistInKm = this.LocalizationCtrl.getDistanceFromCoordinatesInKm(this.actLat, this.actLong, this.localOrder.address.latitude, this.localOrder.address.longitude);
                            if (this.actDistInKm < 0.5) {
                              this.canGeoCheckin = true;
                            }
                          },
                            error => {
                              console.log(JSON.stringify(error));
                              this.UiFeedBackCtrl.presentAlert('GPS INDISPONIVEL 😭', 'Precisamos da sua localização para continuar o fluxo do pedido! Habilite a localização e tente novamente!', 'error');
                              this.navCtrl.pop();
                            });
                      } else {
                        console.log('ReceivedOrderDetailPage says: não é cordova 😭');
                      }
                    }
                  }

                  if (this.localOrder.status != 1300 && this.localOrder.step == 4 && this.localOrder.clientReviewId != '' && this.localOrder.providerReviewId != '') {
                    this.finishOrder();
                  }

                  if (this.localOrder.status != 1302 && this.localOrder.status != 1303 && this.localOrder.step == 4) {
                    if (this.localOrder.acceptedBudget.warrantyTime != '') {
                      let tempWarrantyDays = 0;
                      switch (this.localOrder.acceptedBudget.warrantyTime) {
                        case '15 dias':
                          tempWarrantyDays = 15;
                          break;
                        case '1 mês':
                          tempWarrantyDays = 30;
                          break;
                        case '3 mêses':
                          tempWarrantyDays = 30 * 3;
                          break;
                        case '6 mêses':
                          tempWarrantyDays = 30 * 6;
                          break;
                        case '1 ano':
                          tempWarrantyDays = 30 * 12;
                          break;
                      }
                      let actualTime = new Date().getTime();
                      this.warrantyEndTime = new Date(this.localOrder.acceptedSchedule.actualCheckinDate.getTime() + (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * tempWarrantyDays));
                      this.hasWarrantyTime = actualTime < this.warrantyEndTime.getTime();
                    } else {
                      this.hasWarrantyTime = false;
                      this.warrantyEndTime = null;
                    }
                  }

                  if (!this.clientLoaded) {
                    this.loadClientData();
                  } else {
                    this.showSplash = false;
                  }

                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                })

            } else {
              this.navCtrl.pop().then(() => {
                this.UiFeedBackCtrl.presenLottietAlert('Acesso NEGADO!', 'Somente o prestador deste pedido tem acesso à esta pagina!', 'assets/animations/lock.json', 'error');
              });
            }
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar seus dados', e);
          })

      },
        error => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar pedido', error);
        });

  }

  ionViewDidLeave() {
    if (this.needToUnsub) {
      if (this.orderSub != null) {
        this.orderSub.unsubscribe();
      }
      if (this.geoSub != null) {
        this.geoSub.unsubscribe();
      }
      if (this.schedulesSub != null) {
        this.schedulesSub();
      }
      if (this.localOrderElapsedTimeService) {
        this.localOrderElapsedTimeService = false;
      }
    } else {
      this.needToUnsub = true
    }
  }

  getLocalOrderElapsedTimeService() {
    let d = new Date();
    this.localOrderElapsedTime = this.utilCtrl.convertMiliSecondsToHMmSs(d.getTime() - this.localOrder.acceptedSchedule.actualCheckinDate.getTime());
    setTimeout(() => {
      if (this.localOrderElapsedTimeService) {
        this.getLocalOrderElapsedTimeService();
      }
    }, 1000);
  }

  finishOrder() {
    this.UiFeedBackCtrl.presenLoader(`Finalizando Pedido!`)
      .then(() => {
        this.localOrder.status = 1300;
        this.db.orders.update(this.localOrder.id, { status: this.localOrder.status })
          .then(() => {
            this.db.users.history.registerOrderCompletion(this.localOrder)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentAlert('Pedido finalizado!', 'Obrigado por utilizar o economizei', 'success', true);
              })
          })
      });
  }

  localOrderIsActive(): boolean {
    return (this.localOrder.status <= 1300)
  }

  showSchedulingDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('ReceivedOrderSchedulingDetailPage', {
      receivedOrder: this.localOrder,
      provider: this.UserDataCtrl.localUser
    });
  }

  showBudgetDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('ReceivedOrderBudgetDetailPage', {
      receivedOrder: this.localOrder
    });
  }

  showClient() {
    this.needToUnsub = false;
    this.navCtrl.push('ClientDetailPage', {
      client: this.localClient
    });
  }

  showLocalizationDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('ReceivedOrderLocDetailPage', {
      receivedOrder: this.localOrder,
      provider: this.UserDataCtrl.localUser
    });
  }

  showServiceDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('OrderServiceDetailPage', {
      order: this.localOrder
    });
  }

  showClientReviewDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('ReceivedOrderClientReviewDetailPage', {
      receivedOrder: this.localOrder
    });
  }

  showProviderReviewDetail() {
    this.needToUnsub = false;
    this.navCtrl.push('ReceivedOrderProviderReviewDetailPage', {
      receivedOrder: this.localOrder
    });
  }

  acceptOrderConfirmation() {
    this.UiFeedBackCtrl.presenLoader('Carregando...')
      .then(() => {
        this.UserPlanCtrl.canAcceptOrder(this.UserDataCtrl.localUser)
          .then(canAcceptOrder => {
            if (canAcceptOrder) {
              this.UserPlanCtrl.next24AvailableOrdersAcceptance(this.UserDataCtrl.localUser)
                .then(next24AvailableOrdersAcceptance => {
                  let alert = this.UiFeedBackCtrl.alertCtrl.create({
                    title: 'Atenção!',
                    subTitle: `Você ainda possui ${next24AvailableOrdersAcceptance} aceitações de pedidos disponiveis nas proximas 24h. Deseja continuar?`,
                    buttons: [
                      'Cancelar',
                      {
                        text: 'SIM',
                        handler: () => {
                          this.acceptOrder();
                        }
                      }
                    ]
                  });
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.hapticCtrl.notification('warning');
                  alert.present();
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentErrorAlert(`Erro ao carregar seu plano!`, e);
                })
            } else {
              this.UserPlanCtrl.getUserPlan()
                .then(uP => {
                  let alert = this.UiFeedBackCtrl.alertCtrl.create({
                    title: 'Limite de aceitação de pedidos atingido!',
                    subTitle: `Você atingiu o limite de aceitações de pedidos para o plano ${uP.title}. Faça Upgrade para poder aceitar o pedido ou aguarde até a renovação de sua cota diaria`,
                    buttons: [
                      'Cancelar',
                      {
                        text: 'Planos',
                        handler: () => {
                          this.navCtrl.pop()
                            .then(() => {
                              this.navCtrl.push('PlansPage');
                            })
                        }
                      }
                    ]
                  });
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.hapticCtrl.notification('warning');
                  alert.present();
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentErrorAlert(`Erro ao carregar seu plano!`, e);
                })
            }
          });
      });
  }

  acceptOrder() {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrder.step = 2;
        this.localOrder.status = 2;
        this.db.orders.update(this.localOrder.id, { step: 2, status: 2 })
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            this.UserPlanCtrl.registrateOrderAcceptance(this.UserDataCtrl.localUser.id, this.localOrder.id).then(() => {
              let notif = new Notification();
              notif.id = this.db.createId();
              notif.title = 'Pedido Aceito!';
              notif.text = `O o seu pedido para ${this.localOrder.title} foi aceito!`;
              notif.type = 'generalAlerts';
              notif.action = {
                type: 'showMyOrder',
                orderId: this.localOrder.id,
              };
              notif.senderName = 'economizei';
              this.db.users.notifications.create(this.localOrder.clientId, notif)
                .then(() => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentToast('Pedido aceito com sucesso!', 'success');
                });
            });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  createChat(): Promise<any> {
    return new Promise((resolve, reject) => {
      let tempChat = new Chat;
      tempChat.id = this.db.createId();
      tempChat.orderId = this.localOrder.id;
      tempChat.title = this.localOrder.title;
      tempChat.usersData.push({
        userId: this.localOrder.clientId,
        userName: this.localOrder.clientName,
        userThumbnailUrl: this.localOrder.clientThumbnailUrl
      });
      tempChat.usersData.push({
        userId: this.localOrder.providerId,
        userName: this.localOrder.providerName,
        userThumbnailUrl: this.localOrder.providerThumbnailUrl
      });
      this.db.chats.create(tempChat)
        .then(() => {
          this.localOrder.chatId = tempChat.id;
          this.db.orders.update(this.localOrder.id, { chatId: this.localOrder.chatId })
            .then(() => {
              this.db.users.getDoc(this.localClient.id)
                .then(providerDoc => {
                  let tempProviderChats = providerDoc.get('chats') as string[];
                  tempProviderChats.push(tempChat.id);
                  this.localClient.chats = tempProviderChats;
                  providerDoc.ref.update({ chats: this.localClient.chats })
                    .then(() => {
                      this.UserDataCtrl.localUser.chats.push(tempChat.id);
                      this.UserDataCtrl.update({ chats: this.UserDataCtrl.localUser.chats })
                        .then(() => {
                          resolve();
                        })
                    })
                })
            })
        })
        .catch(e => {
          reject(e)
        })
    })
  }

  openChat() {
    if (this.localOrder.chatId != '') {
      this.needToUnsub = false;
      this.navCtrl.push('ChatPage', {
        chatId: this.localOrder.chatId
      });
    } else {
      this.UiFeedBackCtrl.presenLoader('Criando Conversa...')
        .then(() => {
          this.createChat()
            .then(() => {
              this.UiFeedBackCtrl.dismissLoader()
                .then(() => {
                  this.openChat();
                });
            })
            .catch(e => {
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentErrorAlert('erro ao criar Chat', e);
            })
        });
    }
  }

  rejectOrder() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Motivo:');

    alert.addInput({
      type: 'radio',
      label: 'Cliente suspeito',
      value: 'Cliente suspeito',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Dados incoerentes',
      value: 'Dados incoerentes'
    });

    alert.addInput({
      type: 'radio',
      label: 'Horário inadequado',
      value: 'Horário inadequado'
    });

    alert.addInput({
      type: 'radio',
      label: 'Local inseguro',
      value: 'Local inseguro'
    });

    alert.addInput({
      type: 'radio',
      label: 'Outros',
      value: 'Outros'
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        this.rejectOrderStep2(data);
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  rejectOrderStep2(reason) {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrder.status = 1301;
        this.localOrder.rejectReason = reason;
        this.db.orders.update(this.localOrder.id, { status: this.localOrder.status, rejectReason: reason })
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Pedido REJEITADO!';
            notif.text = `O o seu pedido para ${this.localOrder.title} foi REJEITADO!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showMyOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = 'economizei';
            this.db.users.notifications.create(this.localOrder.clientId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentToast('Pedido REJEITADO com sucesso!', 'success');
              });
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  cancelOrder() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Motivo:');

    alert.addInput({
      type: 'radio',
      label: 'Indisponibilidade de horário',
      value: 'Indisponibilidade de horário',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Orçamento',
      value: 'Orçamento'
    });

    alert.addInput({
      type: 'radio',
      label: 'Prestador',
      value: 'Prestador'
    });

    alert.addInput({
      type: 'radio',
      label: 'Outros',
      value: 'Outros',
      handler: () => {
        this.cancelOrderOtherMotive();
        alert.dismiss();
      }
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        this.cancelOrderStep2(data);
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelOrderOtherMotive() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro motivo',
      message: 'Digite o Motivo para cancelar o pedido',
      inputs: [
        {
          name: 'motive',
          placeholder: 'Motivo'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.cancelOrderStep2(data.motive)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    prompt.present();
  }

  cancelOrderStep2(reason) {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrder.status = 1302;
        this.localOrder.rejectReason = reason;
        this.db.orders.update(this.localOrder.id, { status: this.localOrder.status, rejectReason: reason })
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Pedido CANCELADO!';
            notif.text = `O o seu pedido para ${this.localOrder.title} foi CANCELADO!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showMyOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = 'economizei';
            this.db.users.notifications.create(this.localOrder.clientId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentToast('Pedido CANCELADO com sucesso!', 'success');
              });
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  checkinCodeOnInput() {
    if (this.checkinCode != '') {
      this.checkinCode = this.checkinCode.toUpperCase();
      if (this.checkinCode == this.localOrder.acceptedSchedule.checkinCode) {
        this.checkinCodeRight = true;
        this.checkinCodeInputPlaceholder = ''
      } else {
        this.checkinCodeRight = false;
        this.checkinCodeInputPlaceholder = ''
        this.UiFeedBackCtrl.presentAlert('Codigo de checkin incorreto', 'Favor insira o codigo de checkin correto', 'error');
      }
    }
  }

  startServiceViaCodeConfirmation() {
    this.UiFeedBackCtrl.presenLoader('Fazendo checkin...', 'assets/animations/loc-point.json')
      .then(() => {
        if (this.checkinCode != '') {
          this.checkinCode = this.checkinCode.toUpperCase();
          if (this.checkinCode == this.localOrder.acceptedSchedule.checkinCode) {
            this.checkinCodeRight = true;
            this.checkinCodeInputPlaceholder = '';
            let alert = this.UiFeedBackCtrl.alertCtrl.create({
              title: 'Atenção!',
              subTitle: 'Ao realizar o checkin você também inicia o Serviço. Deseja continuar?',
              buttons: [
                'Cancelar',
                {
                  text: 'SIM',
                  handler: () => {
                    this.startServiceViaCode();
                  }
                }
              ]
            });
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.hapticCtrl.notification('warning');
            alert.present();
          } else {
            this.checkinCodeRight = false;
            this.checkinCodeInputPlaceholder = ''
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Codigo de checkin incorreto', 'Favor insira o codigo de checkin correto', 'error');
          }
        } else {
          this.checkinCodeRight = false;
          this.checkinCodeInputPlaceholder = ''
          this.UiFeedBackCtrl.dismissLoader();
          this.UiFeedBackCtrl.presentAlert('Codigo de checkin vazio', 'Favor insira o codigo de checkin correto', 'error');
        }
      });
  }

  startServiceViaCode() {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrder.acceptedSchedule.actualCheckinDate = new Date();
        this.localOrder.acceptedSchedule.delayInMilSec = (this.localOrder.acceptedSchedule.actualCheckinDate.getTime() - this.localOrder.acceptedSchedule.dueDate.getTime());
        this.localOrder.acceptedSchedule.status = 'checkin_done';
        this.localOrder.status = 6;
        this.db.orders.schedules.update(this.localOrder.id, this.localOrder.acceptedSchedule.id, { actualCheckinDate: this.localOrder.acceptedSchedule.actualCheckinDate, delayInMilSec: this.localOrder.acceptedSchedule.delayInMilSec, status: this.localOrder.acceptedSchedule.status })
          .then(() => {
            this.db.orders.update(this.localOrder.id, { status: this.localOrder.status, acceptedSchedule: this.localOrder.acceptedSchedule })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.db.createId();
                notif.title = 'Serviço iniciado!';
                notif.text = `O o seu pedido para ${this.localOrder.title} Acabou de ser iniciado!`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showMyOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = 'economizei';
                this.db.users.notifications.create(this.localOrder.clientId, notif)
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast('Pedido iniciado com sucesso!', 'success');
                  });
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  startServiceViaGPSCheckinConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'Ao realizar o checkin você também inicia o Serviço. Deseja continuar?',
      buttons: [
        'Cancelar',
        {
          text: 'SIM',
          handler: () => {
            this.startServiceViaGPSCheckin();
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  startServiceViaGPSCheckin() {
    this.UiFeedBackCtrl.presenLoader('Fazendo checkin...', 'assets/animations/loc-point.json')
      .then(() => {
        this.localOrder.acceptedSchedule.actualCheckinDate = new Date();
        this.localOrder.acceptedSchedule.delayInMilSec = (this.localOrder.acceptedSchedule.actualCheckinDate.getTime() - this.localOrder.acceptedSchedule.dueDate.getTime());
        this.localOrder.acceptedSchedule.status = 'checkin_done';
        this.localOrder.status = 6;
        this.db.orders.schedules.update(this.localOrder.id, this.localOrder.acceptedSchedule.id, { actualCheckinDate: this.localOrder.acceptedSchedule.actualCheckinDate, delayInMilSec: this.localOrder.acceptedSchedule.delayInMilSec, status: this.localOrder.acceptedSchedule.status })
          .then(() => {
            this.db.orders.update(this.localOrder.id, { status: this.localOrder.status, acceptedSchedule: this.localOrder.acceptedSchedule })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.db.createId();
                notif.title = 'Serviço iniciado!';
                notif.text = `O o seu pedido para ${this.localOrder.title} Acabou de ser iniciado!`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showMyOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = 'economizei';
                this.db.users.notifications.create(this.localOrder.clientId, notif)
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast('Pedido iniciado com sucesso!', 'success');
                  });
              });
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  cancelScheduleConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O Cancelamento do atendimento irá cancelar o agendamento aprovado, um novo agendamento terá de ser criado e aprovado para continuar o pedido. Deseja continuar?',
      buttons: [
        'Cancelar',
        {
          text: 'SIM',
          handler: () => {
            this.cancelScheduleMotiveSelection();
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelScheduleMotiveSelection() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Motivo:');

    alert.addInput({
      type: 'radio',
      label: 'Cliente Ausente',
      value: 'Cliente Ausente',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Orçamento',
      value: 'Orçamento'
    });

    alert.addInput({
      type: 'radio',
      label: 'Prestador',
      value: 'Prestador'
    });

    alert.addInput({
      type: 'radio',
      label: 'Outros',
      value: 'Outros',
      handler: data => {
        this.cancelScheduleOtherMotive();
        alert.dismiss();
      }
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        this.cancelSchedule(data);
      }
    });
    alert.present();
  }

  cancelScheduleOtherMotive() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro motivo',
      message: 'Digite o Motivo para cancelar o Atendimento',
      inputs: [
        {
          name: 'motive',
          placeholder: 'Motivo'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.cancelSchedule(data.motive)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    prompt.present();
  }

  cancelSchedule(reason: string) {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrderElapsedTimeService = false;
        this.localOrderElapsedTime = '--:--:--';
        this.localOrder.acceptedSchedule.status = 'canceled_by_provider';
        this.localOrder.acceptedSchedule.rejectReason = reason;
        this.localOrder.step = 2;
        this.localOrder.status = 2;
        this.db.orders.schedules.update(this.localOrder.id, this.localOrder.acceptedSchedule.id, { status: this.localOrder.acceptedSchedule.status, rejectReason: this.localOrder.acceptedSchedule.rejectReason })
          .then(() => {
            this.localOrder.acceptedSchedule = null;
            this.db.orders.update(this.localOrder.id, { step: this.localOrder.step, status: this.localOrder.status, acceptedSchedule: this.localOrder.acceptedSchedule })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.db.createId();
                notif.title = 'Agendamento cancelado!';
                notif.text = `O agendamento para seu pedido para ${this.localOrder.title} Acabou de ser cancelado!`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showMyOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = 'economizei';
                this.db.users.notifications.create(this.localOrder.clientId, notif)
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast('Agendamento cancelado com sucesso!', 'success');
                  });
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  completeServiceConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'Tem certeza que deseja concluir o serviço?',
      buttons: [
        'Cancelar',
        {
          text: 'SIM',
          handler: () => {
            this.completeService();
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  completeService() {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrderElapsedTimeService = false;
        this.localOrderElapsedTime = '--:--:--';
        this.localOrder.acceptedSchedule.status = 'realized';
        this.localOrder.step = 4;
        this.localOrder.status = 7;
        this.db.orders.schedules.update(this.localOrder.id, this.localOrder.acceptedSchedule.id, { status: this.localOrder.acceptedSchedule.status })
          .then(() => {
            this.db.orders.update(this.localOrder.id, { step: this.localOrder.step, status: this.localOrder.status, acceptedSchedule: this.localOrder.acceptedSchedule })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.db.createId();
                notif.title = 'Serviço concluido!';
                notif.text = `O o seu pedido para ${this.localOrder.title} foi concluido, !`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showMyOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = 'economizei';
                this.db.users.notifications.create(this.localOrder.clientId, notif)
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast('Serviço concluido com sucesso!', 'success');
                  });
              });
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  completeSheduleAndOpeNewScheduleConfirmation() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'Tem certeza que deseja concluir o Atendimento e liberar a abertura de um novo agendamento?',
      buttons: [
        'Cancelar',
        {
          text: 'SIM',
          handler: () => {
            this.completeSheduleAndOpeNewSchedule();
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  completeSheduleAndOpeNewSchedule() {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localOrderElapsedTimeService = false;
        this.localOrderElapsedTime = '--:--:--';
        this.localOrder.acceptedSchedule.status = 'realized';
        this.localOrder.step = 2;
        this.localOrder.status = 2;
        this.db.orders.schedules.update(this.localOrder.id, this.localOrder.acceptedSchedule.id, { status: this.localOrder.acceptedSchedule.status })
          .then(() => {
            this.localOrder.acceptedSchedule = null;
            this.db.orders.update(this.localOrder.id, { step: this.localOrder.step, status: this.localOrder.status, acceptedSchedule: this.localOrder.acceptedSchedule })
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                let notif = new Notification();
                notif.id = this.db.createId();
                notif.title = 'Atendimento Concluido!';
                notif.text = `O atendimento seu pedido: ${this.localOrder.title} Acabou de ser concluido!`;
                notif.type = 'generalAlerts';
                notif.action = {
                  type: 'showMyOrder',
                  orderId: this.localOrder.id,
                };
                notif.senderName = 'economizei';
                this.db.users.notifications.create(this.localOrder.clientId, notif)
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.UiFeedBackCtrl.presentToast('Agendamento cancelado com sucesso!', 'success');
                  });
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

}
