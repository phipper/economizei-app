import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderDetailPage } from './received-order-detail';
import { LottieAnimationViewModule } from 'ng-lottie';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    ReceivedOrderDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(ReceivedOrderDetailPage),
    LottieAnimationViewModule
  ],
})
export class ReceivedOrderDetailPageModule {}
