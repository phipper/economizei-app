import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderSchedulingDetailPage } from './received-order-scheduling-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrderSchedulingDetailPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(ReceivedOrderSchedulingDetailPage),
  ],
})
export class ReceivedOrderSchedulingDetailPageModule {}
