import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderCreateSchedulingPage } from './received-order-create-scheduling';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrderCreateSchedulingPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(ReceivedOrderCreateSchedulingPage),
  ],
})
export class ReceivedOrderCreateSchedulingPageModule {}
