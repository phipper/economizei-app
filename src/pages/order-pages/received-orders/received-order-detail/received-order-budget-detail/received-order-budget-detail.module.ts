import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderBudgetDetailPage } from './received-order-budget-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrderBudgetDetailPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(ReceivedOrderBudgetDetailPage),
  ],
})
export class ReceivedOrderBudgetDetailPageModule {}
