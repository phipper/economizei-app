import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderCreateBudgetPage } from './received-order-create-budget';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrderCreateBudgetPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(ReceivedOrderCreateBudgetPage),
  ],
})
export class ReceivedOrderCreateBudgetPageModule {}
