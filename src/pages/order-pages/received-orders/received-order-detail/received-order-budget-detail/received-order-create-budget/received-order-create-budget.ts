import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Order } from '../../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';
import { Notification } from '../../../../../../models/Notification';
import { Budget } from '../../../../../../models/Budget';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-received-order-create-budget',
  templateUrl: 'received-order-create-budget.html',
})
export class ReceivedOrderCreateBudgetPage {

  showSplash = true;
  localOrder: Order = new Order();
  budgetReady = false;
  localBudget: Budget = new Budget();
  descriptionCaracCountMin = 0;
  localPrice = "";

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
  }

  ionViewDidEnter() {
    if (this.localOrder.step == 2) {
      let count = 0;
      this.db.orders.budgets.getAll(this.localOrder.id)
        .then(budgets => {
          budgets.forEach(budget => {
            count++;
            if (budget.status == 'pending' || budget.status == 'approved') {
              this.UiFeedBackCtrl.presentAlert('O pedido possui orçamentos em abertos', 'Favor dar as devidas trativas aos tratamentos já existentes!', 'warning', true);
              this.navCtrl.pop();
            }
            if (count == budgets.length && !(budgets.length == 0)) {
              this.showSplash = false;
            }
          });
          if (budgets.length == 0) {
            this.showSplash = false;
          }
        })
        .catch(e => {
          console.log("Erro :( " + JSON.stringify(e));
          this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
        });
    } else {
      this.UiFeedBackCtrl.presentAlert('O Passo do pedido não permite a criação de orçamento', 'A criação de orçamentos deve ser feita no passo 2 (Planejamento)', 'warning', true);
      this.navCtrl.pop();
    }
  }

  CheckPrice() {
    let priceValue = Number(this.localPrice);
    if (this.localPrice.indexOf(',', 0) != -1 || this.localPrice.indexOf('.', 0) != -1) {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não pode conter casas decimais!", 'assets/animations/file_error.json', 'error');
    } else if (Number.isNaN(priceValue)) {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não deve conter letras!", 'assets/animations/file_error.json', 'error');
    } else if (this.localPrice == "") {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "Favor digite um preço valido!", 'assets/animations/file_error.json', 'error');
    } else if (priceValue <= 0) {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço deve ser maior que 0!", 'assets/animations/file_error.json', 'error');
    } else {
      this.localBudget.value = priceValue;
    }
  }

  toggleWarranty() {
    this.localBudget.warranty = !this.localBudget.warranty;
  }

  // descriptionOnInput(){
  //   this.descriptionCaracCountMin = (10 - this.localBudget.description.length);
  // }

  save() {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        this.localBudget.id = this.db.createId();
        this.localBudget.status = 'pending';
        this.localBudget.orderId = this.localOrder.id;
        this.localBudget.clientId = this.localOrder.clientId;
        this.localBudget.providerId = this.localOrder.providerId;

        this.db.orders.budgets.create(this.localOrder.id, this.localBudget.id, this.localBudget)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Novo Orçamento!';
            notif.text = `Foi criado um novo orçamento foi criado para seu pedido de ${this.localOrder.title}!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showMyOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = "economizei";
            this.db.users.notifications.create(this.localOrder.clientId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentAlert('Orçamento criado com sucesso!', 'Aguarde o retorno do cliente e fique atendo as duvidas que o mesmo venha ter no chat.', 'success', true);
                this.navCtrl.pop();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
          });
      });
  }

}
