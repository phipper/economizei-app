import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

import { Order } from '../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { Notification } from '../../../../../models/Notification';
import { Budget } from '../../../../../models/Budget';
import { UtilProvider } from '../../../../../providers/Util';


@IonicPage()
@Component({
  selector: 'page-received-order-budget-detail',
  templateUrl: 'received-order-budget-detail.html',
})
export class ReceivedOrderBudgetDetailPage {

  showSplash = true;
  localOrder: Order = new Order();
  localBudgets: Budget[] = [];
  budgetsSub: any;
  expiringSchedules = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
  }

  ionViewDidEnter() {
    this.budgetsSub = this.afs.collection('Orders').doc(this.localOrder.id).collection('Budgets').ref
      .orderBy("createdOn", 'desc').onSnapshot(querySnapshot => {
        this.showSplash = true;
        this.localBudgets = [];
        querySnapshot.forEach(doc => {
          this.localBudgets.push(doc.data() as Budget);
          if (this.localBudgets.length == querySnapshot.size && !querySnapshot.empty) {
            this.showSplash = false;
          }
        });
        if (querySnapshot.empty) {
          this.showSplash = false;
        }
      },
        error => {
          console.log("Erro :( " + error.message);
          this.UiFeedBackCtrl.presentErrorAlert("Erro", error);
        });
  }

  ionViewDidLeave() {
    if (this.budgetsSub != null) {
      this.budgetsSub();
    }
  }

  canCancelBudget(): boolean {
    return (this.localOrder.step == 2 || (this.localOrder.step == 3 && this.localOrder.status == 4));
  }

  createBudgetPage() {
    this.navCtrl.push('ReceivedOrderCreateBudgetPage', {
      receivedOrder: this.localOrder
    });
  }

  cancelBudget(budget: Budget) {
    this.UiFeedBackCtrl.presenLoader('Salvando...')
      .then(() => {
        budget.status = 'canceled_by_provider';
        this.afs.collection('Orders').doc(this.localOrder.id).collection('Budgets').doc<Budget>(budget.id).update({ status: 'canceled_by_provider' })
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            if (this.localOrder.acceptedSchedule == null) {
              this.localOrder.status = 2;
            } else {
              this.localOrder.status = 3;
            }
            this.localOrder.step = 2;
            this.localOrder.acceptedBudget = null;
            this.afs.collection('Orders').doc<Order>(this.localOrder.id).update({ step: 2, status: this.localOrder.status, acceptedBudget: this.localOrder.acceptedBudget });
            let notif = new Notification();
            notif.id = this.afs.createId();
            notif.title = 'Orçamento CANCELADO!';
            notif.text = `O orçamento do seu pedido para ${this.localOrder.title} foi Cancelado!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showMyOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = "economizei";
            this.afs.collection('Users').doc(this.localOrder.clientId).collection('Notifications').doc(notif.id).set(Object.assign({}, notif))
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.ionViewDidEnter();
                this.UiFeedBackCtrl.presentToast('Agendamento Orçamento com sucesso!', 'success');
              });
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

  cancelBudgetConfirmation(budget: Budget) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Atenção!',
      subTitle: 'O pedido não poderá ser concluido até que seja gerado um novo Orçamento. Deseja continuar?',
      buttons: [
        {
          text: 'SIM',
          handler: () => {
            this.cancelBudget(budget);
          }
        },
        'Cancelar'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

}
