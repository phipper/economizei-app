import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator, LaunchNavigatorOptions, AppSelectionOptions, RememberChoiceOptions } from '@ionic-native/launch-navigator';

import { Order } from '../../../../../models/Order';
import { User } from '../../../../../models/User';
import { Address } from '../../../../../models/Address';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';


@IonicPage()
@Component({
  selector: 'page-received-order-loc-detail',
  templateUrl: 'received-order-loc-detail.html',
})
export class ReceivedOrderLocDetailPage {

  showSplash = true;
  showItemSplash = true;
  localOrder: Order = new Order();
  localProvider: User = new User();
  actLat: number;
  actLong: number;
  actDistInKm: number;
  geoAccuracy: number;
  workDistInKm: number;
  homeDistInKm: number;
  localWorkAdress: Address = new Address();
  localHomeAdress: Address = new Address();
  geoSub: any;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    private geolocation: Geolocation,
    private launchNavigator: LaunchNavigator,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
    this.localProvider = navParams.get("provider");
  }

  ionViewDidEnter() {
    this.geoSub = this.geolocation.watchPosition().subscribe(data => {
      this.actLat = data.coords.latitude
      this.actLong = data.coords.longitude
      this.geoAccuracy = Number(data.coords.accuracy.toFixed(2));
      this.actDistInKm = Number(this.getDistanceFromLatLonInKm(this.actLat, this.actLong, this.localOrder.address.latitude, this.localOrder.address.longitude).toFixed(2));
      this.showItemSplash = false;
    },
      error => {
        console.log("Erro :( " + error.message);
        this.UiFeedBackCtrl.presentAlert("Erro :(", error.message, 'error');
      });
    if (this.localProvider.workAdressId != "" || this.localProvider.homeAdressId != "") {
      if (this.localProvider.workAdressId != "") {
        this.afs.collection('Addresses').doc(this.localProvider.workAdressId).ref.get().then(workaddresDoc => {
          this.localWorkAdress = workaddresDoc.data() as Address;
          this.workDistInKm = Number(this.getDistanceFromLatLonInKm(this.localWorkAdress.latitude, this.localWorkAdress.longitude, this.localOrder.address.latitude, this.localOrder.address.longitude).toFixed(2));
          if (this.localProvider.homeAdressId != "") {
            this.afs.collection('Addresses').doc(this.localProvider.homeAdressId).ref.get().then(homeaddresDoc => {
              this.localHomeAdress = homeaddresDoc.data() as Address;
              this.homeDistInKm = Number(this.getDistanceFromLatLonInKm(this.localHomeAdress.latitude, this.localHomeAdress.longitude, this.localOrder.address.latitude, this.localOrder.address.longitude).toFixed(2));
              this.showSplash = false;
            });
          } else {
            this.showSplash = false;
          }
        })
          .catch(e => {
            console.log("Erro :( " + JSON.stringify(e));
            this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
          });
      } else {
        this.afs.collection('Addresses').doc(this.localProvider.homeAdressId).ref.get().then(homeaddresDoc => {
          this.localHomeAdress = homeaddresDoc.data() as Address;
          this.homeDistInKm = Number(this.getDistanceFromLatLonInKm(this.localHomeAdress.latitude, this.localHomeAdress.longitude, this.localOrder.address.latitude, this.localOrder.address.longitude).toFixed(2));
          this.showSplash = false;
        })
          .catch(e => {
            console.log("Erro :( " + JSON.stringify(e));
            this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
          });
      }
    } else {
      this.showSplash = false;
    }
  }

  ionViewDidLeave() {
    if (this.geoSub != null) {
      this.geoSub.unsubscribe();
    }
  }

  openMap(address: Address) {
    let myRememberChoiceOptions: RememberChoiceOptions = {
      enabled: false
    };
    let myAppSelectionOptions: AppSelectionOptions = {
      dialogHeaderText: 'Selecione um aplicativo para abrir o endereço',
      cancelButtonText: 'Cancelar',
      rememberChoice: myRememberChoiceOptions
    };
    let options: LaunchNavigatorOptions = {
      appSelection: myAppSelectionOptions
    };
    this.launchNavigator.navigate([address.latitude, address.longitude], options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2): number {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg): number {
    return deg * (Math.PI / 180)
  }

}
