import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderLocDetailPage } from './received-order-loc-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrderLocDetailPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(ReceivedOrderLocDetailPage),
  ],
})
export class ReceivedOrderLocDetailPageModule {}
