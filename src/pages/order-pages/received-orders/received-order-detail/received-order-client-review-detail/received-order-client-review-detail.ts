import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

import { Order } from '../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { AsClientReview } from '../../../../../models/AsClientReview';



@IonicPage()
@Component({
  selector: 'page-received-order-client-review-detail',
  templateUrl: 'received-order-client-review-detail.html',
})
export class ReceivedOrderClientReviewDetailPage {

  showSplash = true;
  localOrder: Order = new Order();
  hasReview = false;
  localReview: AsClientReview = new AsClientReview;
  reviewSub: any;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
  }

  ionViewDidEnter() {
    this.reviewSub = this.afs.collection('Users').doc(this.localOrder.clientId).collection('AsClientReviews').ref
      .orderBy("createdOn", 'desc').where('orderId', '==', this.localOrder.id).onSnapshot(querySnapshot => {
        this.showSplash = true;
        if (querySnapshot.empty) {
          this.showSplash = false;
        } else {
          this.localReview = querySnapshot.docs[0].data() as AsClientReview;
          this.hasReview = true;
          this.showSplash = false;
        }
      },
        error => {
          console.log("Erro :( " + error.message);
          this.UiFeedBackCtrl.presentAlert("Erro :(", error.message, 'error');
        });
  }

  ionViewDidLeave() {
    if (this.reviewSub != null) {
      this.reviewSub();
    }
  }

  createReviewModal() {
    let modal = this.modalCtrl.create('ReceivedOrderCreateClientReviewPage', {
      receivedOrder: this.localOrder
    });
    modal.present();
  }

  editClientReview() {
    let modal = this.modalCtrl.create('ReceivedOrderEditClientReviewPage', {
      receivedOrder: this.localOrder,
      clientReview: this.localReview
    });
    modal.onDidDismiss(() => {
      this.ionViewDidEnter();
    });
    modal.present();
    this.ionViewDidLeave();
  }

  translateRate(rate: number): string {
    switch (rate) {
      case 1:
        return 'Péssimo';
      case 2:
        return 'Ruim';
      case 3:
        return 'Regular';
      case 4:
        return 'Bom';
      case 5:
        return 'Excelente';
    }
  }

}
