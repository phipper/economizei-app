import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Order } from '../../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';
import { AsClientReview } from '../../../../../../models/AsClientReview';
import { Notification } from '../../../../../../models/Notification';
import { Compliment } from '../../../../../../models/Compliment';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-received-order-edit-client-review',
  templateUrl: 'received-order-edit-client-review.html',
})
export class ReceivedOrderEditClientReviewPage {

  reviewStep = 1;
  localOrder: Order = new Order();
  localReview: AsClientReview = new AsClientReview();
  hasChanges = false;
  providerOpinionText = '';
  originalPunctuality = null;
  originalProviderOpinion = null;
  originalText = '';

  constructor(
    public viewCtrl: ViewController,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
    this.localReview = navParams.get("clientReview");
  }

  ionViewDidEnter() {
    this.originalPunctuality = this.localReview.punctuality;
    this.originalProviderOpinion = this.localReview.providerOpinion;
    this.originalText = this.localReview.text;
    switch (this.localReview.providerOpinion) {
      case 1:
        this.providerOpinionText = 'Péssimo';
        break;
      case 2:
        this.providerOpinionText = 'Ruim';
        break;
      case 3:
        this.providerOpinionText = 'Regular';
        break;
      case 4:
        this.providerOpinionText = 'Bom';
        break;
      case 5:
        this.providerOpinionText = 'Excelente';
        break;
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  goBack() {
    if (this.reviewStep > 0) {
      this.reviewStep = this.reviewStep - 1;
    }
  }

  goForward() {
    if (this.reviewStep < 2) {
      this.reviewStep = this.reviewStep + 1;
    } else {
      this.checkForChangesAndSave();
    }
  }

  selectPunctuality(punctuality: number) {
    this.hasChanges = true;
    this.localReview.punctuality = punctuality;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  changeRating() {
    this.hasChanges = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    switch (this.localReview.providerOpinion) {
      case 1:
        this.providerOpinionText = 'Péssimo';
        break;
      case 2:
        this.providerOpinionText = 'Ruim';
        break;
      case 3:
        this.providerOpinionText = 'Regular';
        break;
      case 4:
        this.providerOpinionText = 'Bom';
        break;
      case 5:
        this.providerOpinionText = 'Excelente';
        break;
    }
  }

  changingText() {
    this.hasChanges = true;
  }

  checkForChanges(): boolean {
    return this.localReview.punctuality != this.originalPunctuality
      || this.localReview.providerOpinion != this.originalProviderOpinion
      || this.localReview.text != this.originalText;
  }

  checkForChangesAndSave() {
    if (this.checkForChanges()) {
      this.save();
    } else {
      this.UiFeedBackCtrl.presentAlert('Não existem alterações à serem salvas', '', 'error');
    }
  }

  save() {
    this.UiFeedBackCtrl.presenLoader('Salvando', 'assets/animations/star.json')
      .then(() => {
        this.localReview.processed = false;
        this.localReview.moderated = false;
        this.db.users.asClientReviews.update(this.localReview)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            let notif = new Notification();
            notif.id = this.db.createId();
            notif.title = 'Avaliação alterada!';
            notif.text = `A sua avalição como cliente para o pedido ${this.localOrder.title} foi alterada!`;
            notif.type = 'generalAlerts';
            notif.action = {
              type: 'showMyOrder',
              orderId: this.localOrder.id,
            };
            notif.senderName = "economizei";
            this.db.users.notifications.create(this.localOrder.clientId, notif)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentAlert('Avaliação Alterada com sucesso!', '', 'success', true);
                this.viewCtrl.dismiss();
              });
          }).catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

  selectStandardComment() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Adicionar comentário:');

    for (let compliment of this.db.users.history.STANDARD_CLIENT_COMPLIMENTS) {
      alert.addInput({
        type: 'radio',
        label: compliment.text,
        value: JSON.stringify(compliment),
      });
    }

    alert.addInput({
      type: 'radio',
      label: 'Outro',
      value: 'Outro',
      handler: () => {
        this.selectCustomComment();
        alert.dismiss();
      }
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        if (data) {
          this.defineCompliment(data);
        } else {
          this.UiFeedBackCtrl.presentAlert(`Erro`, `Favor selecionar alguma opção`, `warning`, true);
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  selectCustomComment() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro',
      message: "Digite o seu comentário",
      inputs: [
        {
          name: 'comment',
          placeholder: 'Comentário',
          max: 250
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.defineCustomComment(data.comment)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    prompt.present();
  }

  defineCompliment(data: string) {
    let tempCompliment = JSON.parse(data) as Compliment;
    this.localReview.verified = true;
    this.localReview.text = tempCompliment.text;
    this.localReview.complimentCode = tempCompliment.code;
    this.hasChanges = true;
  }

  defineCustomComment(comment: string) {
    this.hasChanges = true;
    this.localReview.verified = false;
    this.localReview.text = comment;
    this.localReview.complimentCode = '';
  }
}
