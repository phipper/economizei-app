import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderEditClientReviewPage } from './received-order-edit-client-review';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    ReceivedOrderEditClientReviewPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(ReceivedOrderEditClientReviewPage),
  ],
})
export class ReceivedOrderEditClientReviewPageModule {}
