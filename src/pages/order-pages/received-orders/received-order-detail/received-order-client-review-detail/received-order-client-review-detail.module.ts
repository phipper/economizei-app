import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderClientReviewDetailPage } from './received-order-client-review-detail';
import { Ionic2RatingModule } from 'ionic2-rating';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrderClientReviewDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(ReceivedOrderClientReviewDetailPage),
  ],
})
export class ReceivedOrderClientReviewDetailPageModule {}
