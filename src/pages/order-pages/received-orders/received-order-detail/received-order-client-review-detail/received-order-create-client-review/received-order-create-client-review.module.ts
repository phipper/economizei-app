import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrderCreateClientReviewPage } from './received-order-create-client-review';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    ReceivedOrderCreateClientReviewPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(ReceivedOrderCreateClientReviewPage),
  ],
})
export class ReceivedOrderCreateClientReviewPageModule {}
