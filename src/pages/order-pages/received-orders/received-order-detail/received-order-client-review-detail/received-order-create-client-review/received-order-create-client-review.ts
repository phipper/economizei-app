import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Order } from '../../../../../../models/Order';
import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';
import { AsClientReview } from '../../../../../../models/AsClientReview';
import { Notification } from '../../../../../../models/Notification';
import { Compliment } from '../../../../../../models/Compliment';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-received-order-create-client-review',
  templateUrl: 'received-order-create-client-review.html',
})
export class ReceivedOrderCreateClientReviewPage {

  reviewStep = 1;
  localOrder: Order = new Order();
  localReview: AsClientReview = new AsClientReview();
  punctualityReady = false;
  providerOpinionReady = false;
  providerOpinionText = '';

  constructor(
    public viewCtrl: ViewController,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localOrder = navParams.get("receivedOrder");
    this.localReview.id = this.db.createId();
    this.localReview.title = this.localOrder.title;
    this.localReview.clientId = this.localOrder.clientId;
    this.localReview.providerId = this.localOrder.providerId;
    this.localReview.providerName = this.localOrder.providerName;
    this.localReview.orderId = this.localOrder.id;
  }

  ionViewDidEnter() {
  }

  close() {
    this.viewCtrl.dismiss();
  }

  goBack() {
    if (this.reviewStep > 0) {
      this.reviewStep = this.reviewStep - 1;
    }
  }

  goForward() {
    if (this.reviewStep < 2) {
      this.reviewStep = this.reviewStep + 1;
    } else {
      this.save();
    }
  }

  selectPunctuality(punctuality: number) {
    this.localReview.punctuality = punctuality;
    this.punctualityReady = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  changeRating() {
    this.providerOpinionReady = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    switch (this.localReview.providerOpinion) {
      case 1:
        this.providerOpinionText = 'Péssimo';
        break;
      case 2:
        this.providerOpinionText = 'Ruim';
        break;
      case 3:
        this.providerOpinionText = 'Regular';
        break;
      case 4:
        this.providerOpinionText = 'Bom';
        break;
      case 5:
        this.providerOpinionText = 'Excelente';
        break;
    }
  }

  canFinishOrder(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.db.orders.get(this.localOrder.id)
        .then(order => {
          resolve(order.providerReviewId != '')
        })
        .catch(e => {
          reject(e);
        });
    })
  }


  save() {
    this.UiFeedBackCtrl.presenLoader('Salvando', 'assets/animations/star.json')
      .then(() => {
        this.db.users.asClientReviews.create(this.localReview)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
            this.localOrder.clientReviewId = this.localReview.id;
            this.canFinishOrder()
              .then(canFinishOrder => {
                if (canFinishOrder) {
                  this.localOrder.status = 1300;
                }
                this.db.orders.update(this.localOrder.id, { clientReviewId: this.localOrder.clientReviewId, status: this.localOrder.status })
                  .then(() => {
                    let notif = new Notification();
                    notif.id = this.db.createId();
                    notif.title = 'Nova Avaliação!';
                    notif.text = `Você recebeu uma nova avaliação para o pedido ${this.localOrder.title} 🎉`;
                    notif.type = 'generalAlerts';
                    notif.action = {
                      type: 'showMyOrder',
                      orderId: this.localOrder.id,
                    };
                    notif.senderName = "economizei";
                    this.db.users.notifications.create(this.localOrder.clientId, notif)
                      .then(() => {
                        if (this.localOrder.status == 1300) {
                          this.db.users.history.registerOrderCompletion(this.localOrder)
                            .then(() => {
                              this.UiFeedBackCtrl.presentAlert('Pedido finalizado!', 'Obrigado por utilizar o economizei', 'success', true);
                              this.UiFeedBackCtrl.dismissLoader();
                              this.UiFeedBackCtrl.presenLottietAlert('Maravilha!', 'Avaliação criada com sucesso!', 'assets/animations/rating.json', 'success');
                              this.viewCtrl.dismiss();
                            });
                        } else {
                          this.UiFeedBackCtrl.dismissLoader();
                          this.UiFeedBackCtrl.presenLottietAlert('Maravilha!', 'Avaliação criada com sucesso!', 'assets/animations/rating.json', 'success');
                          this.viewCtrl.dismiss();
                        }
                      });
                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          });
      });
  }

  selectStandardComment() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Adicionar comentário:');

    for (let compliment of this.db.users.history.STANDARD_CLIENT_COMPLIMENTS) {
      alert.addInput({
        type: 'radio',
        label: compliment.text,
        value: JSON.stringify(compliment),
      });
    }

    alert.addInput({
      type: 'radio',
      label: 'Outro',
      value: 'Outro',
      handler: () => {
        this.selectCustomComment();
        alert.dismiss();
      }
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Confimar',
      handler: data => {
        if (data) {
          this.defineCompliment(data);
        } else {
          this.UiFeedBackCtrl.presentAlert(`Erro`, `Favor selecionar alguma opção`, `warning`, true);
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  selectCustomComment() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro',
      message: "Digite o seu comentário",
      inputs: [
        {
          name: 'comment',
          placeholder: 'Comentário',
          max: 250
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.defineCustomComment(data.comment)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    prompt.present();
  }

  defineCompliment(data: string) {
    let tempCompliment = JSON.parse(data) as Compliment;
    this.localReview.verified = true;
    this.localReview.text = tempCompliment.text;
    this.localReview.complimentCode = tempCompliment.code;
  }

  defineCustomComment(comment: string) {
    this.localReview.verified = false;
    this.localReview.text = comment;
    this.localReview.complimentCode = '';
  }
}
