import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../providers/UserData';

import { Order } from '../../../models/Order';
import { UtilProvider } from '../../../providers/Util';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { DocumentSnapshot, WhereFilterOp } from '@firebase/firestore-types';

@IonicPage()
@Component({
  selector: 'page-received-orders',
  templateUrl: 'received-orders.html',
})
export class ReceivedOrdersPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  segment: string = 'Abertos';
  showSplash = true;

  isExecutingQuery = false;
  localOrders: Order[] = [];
  queryLimit = 4;
  lastQueryDoc: DocumentSnapshot = null;

  openOrdersWhere = {
    id: 0,
    name: 'Open Orders',
    fieldPath: 'status',
    opStr: '<' as WhereFilterOp,
    value: 1300
  };

  finishedOrdersWhere = {
    id: 1,
    name: 'Finished Orders',
    fieldPath: 'status',
    opStr: '>=' as WhereFilterOp,
    value: 1300
  };

  querryWhere = this.openOrdersWhere;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'ReceivedOrdersPage') {
      this.content.scrollToTop();
    }
  }

  ionViewDidLoad() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.reLoadActiveSegment();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  reLoadActiveSegment() {
    switch (this.segment) {
      case 'Abertos':
        this.querryWhere = this.openOrdersWhere;
        this.loadOrders();
        break;

      case 'Encerrados':
        this.querryWhere = this.finishedOrdersWhere;
        this.loadOrders();
        break;
    }
  }

  loadOrders() {
    console.log(`Executando loadOrders query: ${JSON.stringify(this.querryWhere)}`);
    this.showSplash = true;
    this.isExecutingQuery = true;
    this.localOrders = [];
    this.lastQueryDoc = null;
    if (this.querryWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy(this.querryWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localOrders.push(doc.data() as Order);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localOrders.push(doc.data() as Order);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    }
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    console.log(`Executando doRefresh query: ${JSON.stringify(this.querryWhere)}`);
    this.isExecutingQuery = true;
    this.lastQueryDoc = null;
    this.localOrders = [];
    if (this.querryWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy(this.querryWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localOrders.push(doc.data() as Order);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              refresher.complete();
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localOrders.push(doc.data() as Order);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              refresher.complete();
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doInfinite(infiniteScroll) {
    if (this.lastQueryDoc != null) {
      this.isExecutingQuery = true;
      console.log(`Executando doInfinite query: ${JSON.stringify(this.querryWhere)}`);
      if (this.querryWhere.opStr !== '==') {
        this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).ref
          .where('providerId', '==', this.localUserId)
          .orderBy(this.querryWhere.fieldPath, 'asc')
          .orderBy('createdOn', 'desc')
          .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
          .startAfter(this.lastQueryDoc)
          .limit(this.queryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localOrders.push(doc.data() as Order);
                }
                if (querySnapshot.empty) {
                  this.lastQueryDoc = null;
                } else {
                  this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).ref
          .where('providerId', '==', this.localUserId)
          .orderBy('createdOn', 'desc')
          .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
          .startAfter(this.lastQueryDoc)
          .limit(this.queryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localOrders.push(doc.data() as Order);
                }
                if (querySnapshot.empty) {
                  this.lastQueryDoc = null;
                } else {
                  this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      console.log(`no need to execute doInfinite query for: ${JSON.stringify(this.querryWhere)}`);
      infiniteScroll.complete();
    }
  }

  cardTapped(order) {
    this.navCtrl.push('ReceivedOrderDetailPage', {
      orderId: order.id,
    });
  }

}
