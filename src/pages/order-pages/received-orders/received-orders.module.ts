import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedOrdersPage } from './received-orders';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedOrdersPage,
  ],
  imports: [
    IonicPageModule.forChild(ReceivedOrdersPage),
    LottieAnimationViewModule
  ],
})
export class ReceivedOrdersPageModule {}
