import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPicViewerPage } from './user-pic-viewer';

@NgModule({
  declarations: [
    UserPicViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPicViewerPage),
  ],
})
export class UserPicViewerPageModule {}
