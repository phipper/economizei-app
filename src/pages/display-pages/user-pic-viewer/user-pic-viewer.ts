import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../../models/User';
import { Slides } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-user-pic-viewer',
  templateUrl: 'user-pic-viewer.html',
})
export class UserPicViewerPage {
  @ViewChild(Slides) SlidesCtrl: Slides;


  localUser: User;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.localUser = navParams.get("user");
  }

  ionViewDidEnter() {
    this.SlidesCtrl.lockSwipes(true);
  }

}
