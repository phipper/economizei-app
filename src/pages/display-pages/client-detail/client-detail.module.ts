import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientDetailPage } from './client-detail';
import { Ionic2RatingModule } from 'ionic2-rating';
import { LottieAnimationViewModule } from 'ng-lottie';
import { ProgressBarComponentModule } from '../../../components/progress-bar/progress-bar.module';
import { RoundProgressModule } from '../../../components/round-progress';

@NgModule({
  declarations: [
    ClientDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    RoundProgressModule,
    ProgressBarComponentModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(ClientDetailPage),
  ],
})
export class ClientDetailPageModule { }
