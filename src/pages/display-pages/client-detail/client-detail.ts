import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../../models/User';
import { UserReputationProvider } from '../../../providers/UserReputation';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { AsClientReview } from '../../../models/AsClientReview';
import { DocumentSnapshot } from '@firebase/firestore-types';
import { AsClientReviewFilterValues } from '../../../models/AsClientReviewFilterValues';
import { Range } from '../../../models/Range';
import { ComplimentsReport } from '../../../models/ComplimentsReport';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';

@IonicPage()
@Component({
  selector: 'page-client-detail',
  templateUrl: 'client-detail.html',
})
export class ClientDetailPage {

  client: User;
  onTimeOrdersPercentage = 0;
  contractedServices = '⌛';
  last90DaysCompletedOrders = '⌛';
  asClientReviewsNumber = '⌛';
  hasReviews = false;
  hasCumpliments = false;

  radius: number = 40;
  semicircle: boolean = false;
  color: string = 'rgb(0, 224, 171)';
  background: string = '#eaeaea';
  animationDelay: number = 0;
  animations: string[] = [];
  gradient: boolean = false;
  realCurrent: number = 0;

  localCumplimentsReport: ComplimentsReport[] = [];

  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  lastReviewDoc: DocumentSnapshot = null;
  localReviews: AsClientReview[] = [];
  reviewQueryLimit = 4;
  hasFilter = false;
  showFilters = false;
  filterValues: AsClientReviewFilterValues = new AsClientReviewFilterValues();
  reviewRange: Range = {
    lower: 0,
    upper: 5
  };


  originalPunctuality = 0;
  originalMinClientOpinion = 1;
  originalMaxClientOpinion = 5;
  originalOnlyWithText = 0;
  originalOnlyModerated = 0;



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private db: EcoFirestore,
    private UserReputationCtrl: UserReputationProvider,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.client = navParams.get("client");
  }


  getOverlayStyle() {
    let isSemi = this.semicircle;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '5%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': this.radius / 3.5 + 'px'
    };
  }


  ionViewDidEnter() {

    this.db.users.get(this.client.id)
      .then(clientUser => {

        this.client = clientUser as User;

        this.onTimeOrdersPercentage = this.UserReputationCtrl.getAsClientOnTimeOrdersPercentage(this.client);

        this.db.users.history.getAsClientCompletedOrderTotalNumber(this.client.id)
          .then(AsClientCompletedOrderTotalNumber => {
            this.contractedServices = AsClientCompletedOrderTotalNumber.toString();
          });

        this.db.users.history.getAsClientCompletedOrderNumber(this.client.id, 90)
          .then(last90DaysCompletedOrdersNumber => {
            this.last90DaysCompletedOrders = last90DaysCompletedOrdersNumber.toString();
          });

        this.db.users.history.getAsClientReceivedReviewsNumber(this.client.id)
          .then(asClientReviewsNumber => {
            if (asClientReviewsNumber > 0) {
              this.hasReviews = true;
            }
            this.asClientReviewsNumber = asClientReviewsNumber.toString();
          });

        this.db.users.history.getAsClientReceivedReviewsCompliments(this.client.id)
          .then(data => {
            this.localCumplimentsReport = data;
            for (let cump of this.localCumplimentsReport) {
              if (cump.quantity > 0) {
                this.hasCumpliments = true;
                break;
              }
            }
          });

        this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.client.id)
          .collection(this.db.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).ref
          .orderBy("createdOn", 'desc').get()
          .then(querySnapshot => {
            for (var i = 0; i < querySnapshot.docs.length; ++i) {
              this.localReviews.push(querySnapshot.docs[i].data() as AsClientReview);
            }
            this.loadReviews();
          })

      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
      });

  }

  openUserPicViewerPage() {
    this.navCtrl.push('UserPicViewerPage', {
      user: this.client
    });
  }


  translateRate(rate: number): string {
    switch (rate) {
      case 1:
        return 'Péssimo';
      case 2:
        return 'Ruim';
      case 3:
        return 'Regular';
      case 4:
        return 'Bom';
      case 5:
        return 'Excelente';
    }
  }


  toggleFilters() {
    this.showFilters = !this.showFilters;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    if (!this.showFilters) {
      this.initFiltersParams();
    }
  }

  changeReviewRange() {
    this.filterValues.minClientOpinion = this.reviewRange.lower;
    this.filterValues.maxClientOpinion = this.reviewRange.upper;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  selectFilterValue() {
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  initFiltersParams() {
    this.reviewRange = { lower: this.filterValues.minClientOpinion, upper: this.filterValues.maxClientOpinion };
    this.originalPunctuality = Number(this.filterValues.punctuality);
    this.originalMinClientOpinion = Number(this.filterValues.minClientOpinion);
    this.originalMaxClientOpinion = Number(this.filterValues.maxClientOpinion);
    this.originalOnlyWithText = Number(this.filterValues.onlyWithText);
    this.originalOnlyModerated = Number(this.filterValues.onlyModerated);
  }

  cleanFilters() {
    this.showFilters = false;
    this.filterValues = new AsClientReviewFilterValues();
    this.hasFilter = false;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.loadReviews();
  }

  canFilter(): boolean {
    return this.filterValues.punctuality != this.originalPunctuality
      || this.filterValues.minClientOpinion != this.originalMinClientOpinion
      || this.filterValues.maxClientOpinion != this.originalMaxClientOpinion
      || this.filterValues.onlyWithText != this.originalOnlyWithText
      || this.filterValues.onlyModerated != this.originalOnlyModerated;
  }

  filter() {
    this.showFilters = false;
    this.hasFilter = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.loadReviews();
  }

  checkForFilterAcceptation(review: AsClientReview): boolean {
    if (this.filterValues.punctuality != 0) {
      if (this.filterValues.punctuality != review.punctuality) {
        return false;
      }
    }
    if (this.filterValues.punctuality != 0) {
      if (this.filterValues.punctuality != review.punctuality) {
        return false;
      }
    }
    if (this.filterValues.minClientOpinion > review.providerOpinion) {
      return false;
    }
    if (this.filterValues.maxClientOpinion < review.providerOpinion) {
      return false;
    }
    if (this.filterValues.onlyWithText != 0) {
      if (this.filterValues.onlyWithText == 1 && review.text == '') {
        return false;
      }
      if (this.filterValues.onlyWithText == -1 && review.text != '') {
        return false;
      }
    }
    if (this.filterValues.onlyWithText != 0 && this.filterValues.onlyModerated != 0) {
      if (this.filterValues.onlyModerated == 1 && review.moderated == false) {
        return false;
      }
      if (this.filterValues.onlyModerated == -1 && review.moderated == true) {
        return false;
      }
    }
    return true;
  }

  loadReviews() {
    this.lastReviewDoc = null;
    this.showSplash = true;
    this.localReviews = [];

    this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.client.id).collection(this.db.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).ref
      .orderBy('createdOn', 'desc')
      .limit(this.reviewQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              let tempReview = doc.data() as AsClientReview;
              if (this.checkForFilterAcceptation(tempReview)) {
                this.localReviews.push(tempReview);
              }
            }
            if (querySnapshot.empty) {
              this.showSplash = false;
              this.lastReviewDoc = null;
            } else if (this.localReviews.length > this.reviewQueryLimit - 1) {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.showSplash = false;
            } else {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.loadMoreReviews();
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }


  loadMoreReviews() {
    this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.client.id).collection(this.db.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).ref
      .orderBy('createdOn', 'desc')
      .startAfter(this.lastReviewDoc)
      .limit(this.reviewQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              let tempReview = doc.data() as AsClientReview;
              if (this.hasFilter) {
                if (this.checkForFilterAcceptation(tempReview)) {
                  this.localReviews.push(tempReview);
                }
              } else {
                this.localReviews.push(tempReview);
              }
            }
            if (this.localReviews.length > this.reviewQueryLimit - 1) {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.showSplash = false;
            } else {
              if (querySnapshot.docs.length > 0) {
                this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.loadMoreReviews();
              } else {
                this.showSplash = false;
              }
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }

  doInfinite(infiniteScroll) {
    this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.client.id).collection(this.db.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).ref
      .orderBy('createdOn', 'desc')
      .startAfter(this.lastReviewDoc)
      .limit(this.reviewQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              let tempReview = doc.data() as AsClientReview;
              if (this.hasFilter) {
                if (this.checkForFilterAcceptation(tempReview)) {
                  this.localReviews.push(tempReview);
                }
              } else {
                this.localReviews.push(tempReview);
              }
            }
            if (this.localReviews.length > this.reviewQueryLimit - 1) {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              infiniteScroll.complete();
            } else {
              if (querySnapshot.docs.length > 0) {
                this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.doInfinite(infiniteScroll);
              } else {
                infiniteScroll.complete();
              }
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }



}
