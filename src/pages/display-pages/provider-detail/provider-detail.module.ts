import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderDetailPage } from './provider-detail';
import { Ionic2RatingModule } from 'ionic2-rating';
import { ProgressBarComponentModule } from '../../../components/progress-bar/progress-bar.module';
import { RoundProgressModule } from '../../../components/round-progress';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ProviderDetailPage
  ],
  imports: [
    Ionic2RatingModule,
    RoundProgressModule,
    ProgressBarComponentModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(ProviderDetailPage),
  ],
})
export class ProviderDetailPageModule { }
