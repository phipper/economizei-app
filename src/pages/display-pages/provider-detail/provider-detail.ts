import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserReputationProvider } from '../../../providers/UserReputation';

import { User } from '../../../models/User';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { AsProviderReview } from '../../../models/AsProviderReview';
import { AsProviderReviewFilterValues } from '../../../models/AsProviderReviewFilterValues';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { DocumentSnapshot } from '@firebase/firestore-types';
import { Range } from '../../../models/Range';
import { ComplimentsReport } from '../../../models/ComplimentsReport';
import { UserDataProvider } from '../../../providers/UserData';

@IonicPage()
@Component({
  selector: 'page-provider-detail',
  templateUrl: 'provider-detail.html',
})
export class ProviderDetailPage {

  radius: number = 40;
  semicircle: boolean = false;
  color: string = 'rgb(0, 224, 171)';
  background: string = '#eaeaea';
  animationDelay: number = 0;
  animations: string[] = [];
  gradient: boolean = false;
  realCurrent: number = 0;

  provider = new User();
  localCumplimentsReport: ComplimentsReport[] = [];

  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  lastReviewDoc: DocumentSnapshot = null;
  localReviews: AsProviderReview[] = [];
  reviewQueryLimit = 4;
  hasFilter = false;
  showFilters = false;
  filterValues: AsProviderReviewFilterValues = new AsProviderReviewFilterValues();
  reviewRange: Range = {
    lower: 0,
    upper: 5
  };
  originalPunctuality = 0;
  originalServiceDelivery = 0;
  originalBudgetAccuracy = 0;
  originalMinClientOpinion = 1;
  originalMaxClientOpinion = 5;
  originalOnlyWithText = 0;
  originalOnlyModerated = 0;

  onTimeOrdersPercentage = 0;
  onTimeDeliveredOrdersPercentage = 0;
  metBudgetOrdersPercentage = 0;

  performedServices = '⌛';
  last90DaysCompletedOrders = '⌛';
  asProviderReviewsNumber = '⌛';
  hasReviews = false;
  hasCumpliments = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private db: EcoFirestore,
    private UserReputationCtrl: UserReputationProvider,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public UserDataCtrl: UserDataProvider
  ) {
    let tempUser = navParams.get("provider");
    if (tempUser) {
      this.provider = tempUser;
    }
  }

  ionViewDidEnter() {

    this.db.users.get(this.provider.id)
      .then(provUser => {

        this.provider = provUser as User;

        this.onTimeOrdersPercentage = this.UserReputationCtrl.getAsProviderOnTimeOrdersPercentage(this.provider);
        this.onTimeDeliveredOrdersPercentage = this.UserReputationCtrl.getAsProviderOnTimeDeliveredOrdersPercentage(this.provider);
        this.metBudgetOrdersPercentage = this.UserReputationCtrl.getAsProviderMetBudgetOrdersPercentage(this.provider);


        this.db.users.history.getAsProviderCompletedOrderTotalNumber(this.provider.id)
          .then(AsProviderPerformedServices => {
            this.performedServices = AsProviderPerformedServices.toString();
          })

        this.db.users.history.getAsProviderCompletedOrderNumber(this.provider.id, 90)
          .then(last90DaysCompletedOrdersNumber => {
            this.last90DaysCompletedOrders = last90DaysCompletedOrdersNumber.toString();
          })

        this.db.users.history.getAsProviderReceivedReviewsNumber(this.provider.id)
          .then(asProviderReviewsNumber => {
            if (asProviderReviewsNumber > 0) {
              this.hasReviews = true;
            }
            this.asProviderReviewsNumber = asProviderReviewsNumber.toString();
          })

        this.db.users.history.getAsProviderReceivedReviewsCompliments(this.provider.id)
          .then(data => {
            this.localCumplimentsReport = data;
            for (let cump of this.localCumplimentsReport) {
              if (cump.quantity > 0) {
                this.hasCumpliments = true;
                break;
              }
            }
          })

        this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.provider.id)
          .collection(this.db.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
          .orderBy("createdOn", 'desc').get()
          .then(querySnapshot => {
            for (var i = 0; i < querySnapshot.docs.length; ++i) {
              this.localReviews.push(querySnapshot.docs[i].data() as AsProviderReview);
            }
            this.loadReviews();
          })

      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
      });
  }

  getOverlayStyle() {
    let isSemi = this.semicircle;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '5%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': this.radius / 3.5 + 'px'
    };
  }

  openUserPicViewerPage() {
    this.navCtrl.push('UserPicViewerPage', {
      user: this.provider
    });
  }


  translateRate(rate: number): string {
    switch (rate) {
      case 1:
        return 'Péssimo';
      case 2:
        return 'Ruim';
      case 3:
        return 'Regular';
      case 4:
        return 'Bom';
      case 5:
        return 'Excelente';
    }
  }


  toggleFilters() {
    this.showFilters = !this.showFilters;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    if (!this.showFilters) {
      this.initFiltersParams();
    }
  }

  changeReviewRange() {
    this.filterValues.minClientOpinion = this.reviewRange.lower;
    this.filterValues.maxClientOpinion = this.reviewRange.upper;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  selectFilterValue() {
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  initFiltersParams() {
    this.reviewRange = { lower: this.filterValues.minClientOpinion, upper: this.filterValues.maxClientOpinion };
    this.originalPunctuality = Number(this.filterValues.punctuality);
    this.originalServiceDelivery = Number(this.filterValues.serviceDelivery);
    this.originalBudgetAccuracy = Number(this.filterValues.budgetAccuracy);
    this.originalMinClientOpinion = Number(this.filterValues.minClientOpinion);
    this.originalMaxClientOpinion = Number(this.filterValues.maxClientOpinion);
    this.originalOnlyWithText = Number(this.filterValues.onlyWithText);
    this.originalOnlyModerated = Number(this.filterValues.onlyModerated);
  }

  cleanFilters() {
    this.showFilters = false;
    this.filterValues = new AsProviderReviewFilterValues();
    this.hasFilter = false;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.loadReviews();
  }

  canFilter(): boolean {
    return this.filterValues.punctuality != this.originalPunctuality
      || this.filterValues.serviceDelivery != this.originalServiceDelivery
      || this.filterValues.budgetAccuracy != this.originalBudgetAccuracy
      || this.filterValues.minClientOpinion != this.originalMinClientOpinion
      || this.filterValues.maxClientOpinion != this.originalMaxClientOpinion
      || this.filterValues.onlyWithText != this.originalOnlyWithText
      || this.filterValues.onlyModerated != this.originalOnlyModerated;
  }

  filter() {
    this.showFilters = false;
    this.hasFilter = true;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.loadReviews();
  }

  checkForFilterAcceptation(review: AsProviderReview): boolean {
    if (this.filterValues.punctuality != 0) {
      if (this.filterValues.punctuality != review.punctuality) {
        return false;
      }
    }
    if (this.filterValues.punctuality != 0) {
      if (this.filterValues.punctuality != review.punctuality) {
        return false;
      }
    }
    if (this.filterValues.serviceDelivery != 0) {
      if (this.filterValues.serviceDelivery != review.serviceDelivery) {
        return false;
      }
    }
    if (this.filterValues.budgetAccuracy != 0) {
      if (this.filterValues.budgetAccuracy != review.budgetAccuracy) {
        return false;
      }
    }
    if (this.filterValues.minClientOpinion > review.clientOpinion) {
      return false;
    }
    if (this.filterValues.maxClientOpinion < review.clientOpinion) {
      return false;
    }
    if (this.filterValues.onlyWithText != 0) {
      if (this.filterValues.onlyWithText == 1 && review.text == '') {
        return false;
      }
      if (this.filterValues.onlyWithText == -1 && review.text != '') {
        return false;
      }
    }
    if (this.filterValues.onlyWithText != 0 && this.filterValues.onlyModerated != 0) {
      if (this.filterValues.onlyModerated == 1 && review.moderated == false) {
        return false;
      }
      if (this.filterValues.onlyModerated == -1 && review.moderated == true) {
        return false;
      }
    }
    return true;
  }

  loadReviews() {
    this.lastReviewDoc = null;
    this.showSplash = true;
    this.localReviews = [];

    this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.provider.id).collection(this.db.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
      .orderBy('createdOn', 'desc')
      .limit(this.reviewQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              let tempReview = doc.data() as AsProviderReview;
              if (this.checkForFilterAcceptation(tempReview)) {
                this.localReviews.push(tempReview);
              }
            }
            if (querySnapshot.empty) {
              this.showSplash = false;
              this.lastReviewDoc = null;
            } else if (this.localReviews.length > this.reviewQueryLimit - 1) {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.showSplash = false;
            } else {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.loadMoreReviews();
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }


  loadMoreReviews() {
    this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.provider.id).collection(this.db.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
      .orderBy('createdOn', 'desc')
      .startAfter(this.lastReviewDoc)
      .limit(this.reviewQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              let tempReview = doc.data() as AsProviderReview;
              if (this.hasFilter) {
                if (this.checkForFilterAcceptation(tempReview)) {
                  this.localReviews.push(tempReview);
                }
              } else {
                this.localReviews.push(tempReview);
              }
            }
            if (this.localReviews.length > this.reviewQueryLimit - 1) {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.showSplash = false;
            } else {
              if (querySnapshot.docs.length > 0) {
                this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.loadMoreReviews();
              } else {
                this.showSplash = false;
              }
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }

  doInfinite(infiniteScroll) {
    this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).doc(this.provider.id).collection(this.db.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
      .orderBy('createdOn', 'desc')
      .startAfter(this.lastReviewDoc)
      .limit(this.reviewQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              let tempReview = doc.data() as AsProviderReview;
              if (this.hasFilter) {
                if (this.checkForFilterAcceptation(tempReview)) {
                  this.localReviews.push(tempReview);
                }
              } else {
                this.localReviews.push(tempReview);
              }
            }
            if (this.localReviews.length > this.reviewQueryLimit - 1) {
              this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              infiniteScroll.complete();
            } else {
              if (querySnapshot.docs.length > 0) {
                this.lastReviewDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.doInfinite(infiniteScroll);
              } else {
                infiniteScroll.complete();
              }
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }

  openProvidersBannersAndServicesSubs() {
    this.navCtrl.push('ProviderBannersAndServicesSubs', {
      provider: this.provider
    });
  }

}

