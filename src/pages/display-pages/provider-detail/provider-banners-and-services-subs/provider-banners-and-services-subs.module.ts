import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderBannersAndServicesSubs } from './provider-banners-and-services-subs';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ProviderBannersAndServicesSubs,
  ],
  imports: [
    IonicPageModule.forChild(ProviderBannersAndServicesSubs),
    LottieAnimationViewModule
  ],
})
export class ProviderBannersAndServicesSubsModule { }
