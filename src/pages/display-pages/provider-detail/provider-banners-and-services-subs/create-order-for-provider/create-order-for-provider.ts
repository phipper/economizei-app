import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform } from 'ionic-angular';

import { User } from '../../../../../models/User';
import { Address } from '../../../../../models/Address';
import { Order } from '../../../../../models/Order';
import { Characteristic } from '../../../../../models/Characteristic';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';
import { Schedule } from '../../../../../models/Schedule';
import { OfficeHours } from '../../../../../models/OfficeHours';
import { UserDataProvider } from '../../../../../providers/UserData';
import { EcoFirestore } from '../../../../../providers/EcoFirestore/EcoFirestore';
import { ServiceType } from '../../../../../models/ServiceType';
import { ServiceTypeSub } from '../../../../../models/ServiceTypeSub';
import { UtilProvider } from '../../../../../providers/Util';

@IonicPage()
@Component({
  selector: 'page-create-order-for-provider',
  templateUrl: 'create-order-for-provider.html',
})

export class CreateOrderForProviderPage {

  @ViewChild(Content) content: Content;

  localUser: User = new User();
  provider: User = new User();

  selectedServiceTypeSub: ServiceTypeSub = new ServiceTypeSub()

  selectedServiceType: ServiceType = new ServiceType();

  localOfficeHours = new OfficeHours();
  hasOfficeHours = false;

  localOrder = new Order();

  descriptionCaracCountMin = 0;

  segment: string = 'Dados';

  selectedScheduleType: string = '';

  localSchedule: Schedule = new Schedule();

  localDate = '';
  localDateValid = false;
  localHourValues: string[] = [];
  localTime = '';
  scheduleReady = false;
  minDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMinDate();
  maxDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMaxDate(60);

  addresses: Address[] = [];
  lastRadons: string[] = [];
  localCharacteristics: Characteristic[] = [];
  localCharacteristicsReady = false;
  localCharacteristicsModel: Characteristic[] = [];
  uploadedCaracNum = 0;

  canLeaveCtrl = true;
  showSplash = true;

  localProviderNumberOfReviews = '⌛';


  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public platform: Platform,
    public utilCtrl: UtilProvider
  ) {
    this.provider = navParams.get('provider');
    this.selectedServiceTypeSub = navParams.get('selectedServiceTypeSub');
  }

  scrollToTop() {
    this.content.scrollToTop();
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  loadProviderOfficeHours() {
    return new Promise((resolve, reject) => {
      this.db.officeHours.get(this.provider.id)
        .then(officeHours => {
          if (officeHours != null) {
            this.localOfficeHours = officeHours;
            this.hasOfficeHours = true;
            resolve();
          } else {
            resolve();
          }
        })
        .catch(e => {
          reject(e);
        });
    });
  }

  ionViewDidEnter() {

    this.UserDataCtrl.getUser()
      .then(UserDoc => {
        this.localUser = UserDoc;
        if (this.localUser.status == 'active') {
          this.db.addresses.getUserAddresses(this.localUser.id)
            .then(addresses => {
              if (addresses.length == 0) {
                const alert = this.UiFeedBackCtrl.alertCtrl.create({
                  title: 'Não possuimos nenhum endereço seu !',
                  subTitle: 'Você precisa cadastrar um endereço para poder criar um pedido. Deseja cadastrar agora?',
                  buttons: [
                    'Não',
                    {
                      text: 'Sim',
                      handler: () => {
                        this.navCtrl.push('AddAddressPage');
                      }
                    }
                  ]
                });
                this.navCtrl.pop();
                this.UiFeedBackCtrl.hapticCtrl.notification('warning');
                alert.present();
              } else {
                this.addresses = addresses;

                this.localOrder.id = this.db.createId();

                this.localOrder.addressId = this.localUser.homeAdressId;
                this.localOrder.clientId = this.localUser.id;
                this.localOrder.clientName = this.localUser.fullName;
                this.localOrder.clientThumbnailUrl = this.localUser.pic.thumbnailUrl;
                this.localOrder.providerId = this.provider.id;
                this.localOrder.providerName = this.provider.fullName;
                this.localOrder.providerThumbnailUrl = this.provider.pic.thumbnailUrl;

                this.localSchedule.id = this.db.createId();
                this.localSchedule.clientId = this.localUser.id;
                this.localSchedule.providerId = this.provider.id;
                this.localSchedule.orderId = this.localOrder.id;
                this.localSchedule.createdBy = this.localUser.id;
                this.localSchedule.status = 'pending_provider';

                this.db.serviceTypes.get(this.selectedServiceTypeSub.serviceTypeId)
                  .then(serviceType => {
                    this.selectedServiceType = serviceType;

                    this.localOrder.serviceTypeId = this.selectedServiceType.id;
                    this.localOrder.serviceTypeSymbol = this.selectedServiceType.symbol;
                    this.localOrder.serviceTypeTitle = this.selectedServiceType.title;
                    this.localOrder.acceptHomeDelivery = this.selectedServiceType.acceptHomeDelivery;
                    this.localOrder.requiredHomeDelivery = this.selectedServiceType.requiredHomeDelivery;

                    this.loadProviderOfficeHours()
                      .then(() => {
                        this.showSplash = false;
                      })
                  })
                  .catch(e => {
                    this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                  })

                this.db.users.history.getUserAsProviderReviewsNumber(this.provider.id)
                  .then(numberOfReviews => {
                    this.localProviderNumberOfReviews = numberOfReviews.toString();
                  })
                  .catch(e => {
                    this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                  })

              }
            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro ', e);
            })
        } else {
          this.navCtrl.pop();
          this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error', true);
        }
      })
      .catch(e => {
        this.navCtrl.pop();
        this.UiFeedBackCtrl.presentErrorAlert('erro', e);
      });
  }

  ionViewCanLeave(): boolean {
    if (!this.canLeaveCtrl) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Tem certeza?',
        subTitle: 'Você quer descartar todos os dados e cancelar a criação de seu Pedido?',
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              this.canLeaveCtrl = true;
              this.navCtrl.pop();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      return true;
    }
    return false;
  }

  toggleHomeDelivery() {
    this.localSchedule.homeDelivery = !this.localSchedule.homeDelivery;
    if (this.localSchedule.homeDelivery) {
      this.localSchedule.geoCheckin = true;
      this.UiFeedBackCtrl.presentAlert('Atendimento em domicílio adicionada ao seu agendamento', 'O prestador terá de se deslocar até o endereço do seu pedido no horario agendado', 'warning');
    } else {
      this.UiFeedBackCtrl.presentAlert('Atendimento em domicílio removida do seu agendamento', 'Isto significa que você irá se deslocar até o prestador ou deverá receber seu serviço por outros meios', 'warning');
    }
  }

  toggleCheckIn() {
    this.localSchedule.geoCheckin = !this.localSchedule.geoCheckin;
    if (this.localSchedule.geoCheckin) {
      this.UiFeedBackCtrl.presentAlert('Check-in via GPS adicionado ao seu agendamento', 'O prestador terá de fazer Check-in ao chegar no local marcado', 'warning');
    } else {
      this.UiFeedBackCtrl.presentAlert('Check-in via GPS removido do seu agendamento', '', 'warning');
    }
  }

  goForward() {
    switch (this.segment) {
      case 'Dados':
        this.segment = 'Serviço'
        if (!this.localCharacteristicsReady) {
          this.loadServiceCaracTab();
        }
        break;

      case 'Serviço':
        this.segment = 'Agendamento'
        break;

      case 'Agendamento':
        this.segment = 'Endereco'
        break;

    }
  }

  goBack() {
    switch (this.segment) {

      case 'Serviço':
        this.segment = 'Dados'
        break;

      case 'Agendamento':
        this.segment = 'Serviço'
        break;

      case 'Endereco':
        this.segment = 'Agendamento'
        break;

    }
  }

  loadServiceCaracTab() {
    this.showSplash = true;
    this.localCharacteristicsModel = [];
    this.localCharacteristics = [];
    this.localCharacteristicsReady = false;

    this.db.serviceTypes.getCharacteristics(this.selectedServiceType.id)
      .then(characteristics => {

        this.localCharacteristicsModel = characteristics;


        let tempCarac = new Characteristic();

        tempCarac.id = this.db.createId();
        tempCarac.title = this.localCharacteristicsModel[0].title;
        tempCarac.bcTitle = this.localCharacteristicsModel[0].bcTitle;
        tempCarac.orderNumber = this.localCharacteristicsModel[0].orderNumber;
        tempCarac.IsDependent = this.localCharacteristicsModel[0].IsDependent;
        tempCarac.rcDataSelection = this.localCharacteristicsModel[0].rcDataSelection;
        tempCarac.dependencyCON = this.localCharacteristicsModel[0].dependencyCON;
        tempCarac.dependencyD = this.localCharacteristicsModel[0].dependencyD;

        tempCarac.data = [];

        this.localCharacteristics.push(tempCarac);
        this.scrollToTop();
        this.showSplash = false;

      });
  }

  findIndexByOrderNumber(CaracVector: Characteristic[], orderNumber: number): number {
    let count = 0;
    let foundIt = false;
    CaracVector.forEach(carac => {
      if (carac.orderNumber == orderNumber) {
        foundIt = true;
      } else if (!foundIt) {
        count++;
      }
    });
    if (foundIt) {
      return count;
    } else {
      return -1;
    }
  }

  answerCharac(orderNumber: number) {
    let characModelIndex = this.findIndexByOrderNumber(this.localCharacteristicsModel, orderNumber);

    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      subTitle: this.localCharacteristicsModel[characModelIndex].title
    });

    this.localCharacteristicsModel[characModelIndex].data.forEach(data => {
      alert.addInput({
        type: this.localCharacteristicsModel[characModelIndex].rcDataSelection,
        label: data,
        value: data,
      });
    })

    alert.addButton({
      text: 'Cancelar',
      role: 'Cancel'
    });

    alert.addButton({
      text: 'Ok',
      handler: data => {
        if (this.localCharacteristicsModel[characModelIndex].rcDataSelection == 'checkbox') {
          if (data.length > 0) {
            if (this.canLeaveCtrl)
              this.canLeaveCtrl = false;
            let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);
            this.localCharacteristics[localCharacIndex].data = data;
            if (this.localCharacteristicsModel.length == (characModelIndex + 1)) {
              this.localCharacteristicsReady = true;
              this.scrollToBottom();
              this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
            } else {
              this.picCharacModel((characModelIndex + 1), orderNumber);
              this.scrollToBottom();
            }
          } else {
            this.answerCharac(orderNumber);
            this.UiFeedBackCtrl.presentAlert('Erro !', 'Favor selecionar alguma Opção!', 'error');
          }
        } else {
          if (data) {
            if (this.canLeaveCtrl)
              this.canLeaveCtrl = false;
            let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);
            this.localCharacteristics[localCharacIndex].data = data;
            if (this.localCharacteristicsModel.length == (characModelIndex + 1)) {
              this.localCharacteristicsReady = true;
              this.scrollToBottom();
              this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
            } else {
              this.picCharacModel((characModelIndex + 1), orderNumber);
              this.scrollToBottom();
            }
          } else {
            this.answerCharac(orderNumber);
            this.UiFeedBackCtrl.presentAlert('Erro !', 'Favor selecionar alguma Opção!', 'error');
          }
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  picCharacModel(characModelIndex: number, orderNumber: number) {

    if (!this.localCharacteristicsModel[characModelIndex].IsDependent) {

      if (this.localCharacteristicsModel.length >= (characModelIndex + 1)) {

        let tempCarac = new Characteristic();

        tempCarac.id = this.db.createId();
        tempCarac.title = this.localCharacteristicsModel[characModelIndex].title;
        tempCarac.bcTitle = this.localCharacteristicsModel[characModelIndex].bcTitle;
        tempCarac.orderNumber = this.localCharacteristicsModel[characModelIndex].orderNumber;
        tempCarac.IsDependent = this.localCharacteristicsModel[characModelIndex].IsDependent;
        tempCarac.rcDataSelection = this.localCharacteristicsModel[characModelIndex].rcDataSelection;
        tempCarac.dependencyCON = this.localCharacteristicsModel[characModelIndex].dependencyCON;
        tempCarac.dependencyD = this.localCharacteristicsModel[characModelIndex].dependencyD;

        tempCarac.data = [];

        let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

        if (this.localCharacteristics.length == (localCharacIndex + 1)) {
          this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
        } else {
          this.localCharacteristicsReady = false;
          let timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
          for (let i = 0; i < timesToPop; i++) {
            this.localCharacteristics.pop();
          }
          this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
        }

      } else {
        this.localCharacteristicsReady = true;
        this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
      }

    } else {
      if ((this.localCharacteristics[(this.localCharacteristicsModel[characModelIndex].dependencyCON - 1)].data.indexOf(this.localCharacteristicsModel[characModelIndex].dependencyD, 0)) != (-1)) {

        if (this.localCharacteristicsModel.length >= (characModelIndex + 1)) {

          let tempCarac = new Characteristic();

          tempCarac.id = this.db.createId();
          tempCarac.title = this.localCharacteristicsModel[characModelIndex].title;
          tempCarac.bcTitle = this.localCharacteristicsModel[characModelIndex].bcTitle;
          tempCarac.orderNumber = this.localCharacteristicsModel[characModelIndex].orderNumber;
          tempCarac.IsDependent = this.localCharacteristicsModel[characModelIndex].IsDependent;
          tempCarac.rcDataSelection = this.localCharacteristicsModel[characModelIndex].rcDataSelection;
          tempCarac.dependencyCON = this.localCharacteristicsModel[characModelIndex].dependencyCON;
          tempCarac.dependencyD = this.localCharacteristicsModel[characModelIndex].dependencyD;

          tempCarac.data = [];

          let localCharacIndex = this.findIndexByOrderNumber(this.localCharacteristics, orderNumber);

          if (this.localCharacteristics.length == (localCharacIndex + 1)) {
            this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
          } else {
            this.localCharacteristicsReady = false;
            let timesToPop = (this.localCharacteristics.length - (localCharacIndex + 1));
            for (let i = 0; i < timesToPop; i++) {
              this.localCharacteristics.pop();
            }
            this.localCharacteristics[(localCharacIndex + 1)] = tempCarac;
          }

        } else {
          this.localCharacteristicsReady = true;
          this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
        }

      } else {

        if (characModelIndex + 1 < this.localCharacteristicsModel.length) {
          this.picCharacModel((characModelIndex + 1), orderNumber);
        } else if (characModelIndex + 1 == this.localCharacteristicsModel.length) {
          this.localCharacteristicsReady = true;
          this.UiFeedBackCtrl.presentToastTop('Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!', 'success');
        }

      }
    }

  }

  cleanUpAgendamentoSegment() {
    this.scheduleReady = false;
    this.localDateValid = false;
    this.localDate = '';
    this.localTime = '';
  }

  checkDate() {
    let dateToValidate = new Date(`${this.localDate} 00:00:00 GMT-0300 (Hora oficial do Brasil)`);

    if (this.hasOfficeHours) {
      switch (dateToValidate.getDay()) {
        case 0:
          if (!this.localOfficeHours.dom.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende no domingo... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.dom.begin; i < this.localOfficeHours.dom.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 1:
          if (!this.localOfficeHours.seg.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na segunda-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.seg.begin; i < this.localOfficeHours.seg.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 2:
          if (!this.localOfficeHours.ter.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na terça-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.ter.begin; i < this.localOfficeHours.ter.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 3:
          if (!this.localOfficeHours.qua.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na quarta-feira...  Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.qua.begin; i < this.localOfficeHours.qua.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 4:
          if (!this.localOfficeHours.qui.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na quinta-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.qui.begin; i < this.localOfficeHours.qui.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 5:
          if (!this.localOfficeHours.sex.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na sexta-feira... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.sex.begin; i < this.localOfficeHours.sex.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;

        case 6:
          if (!this.localOfficeHours.sab.work) {
            this.cleanUpAgendamentoSegment();
            this.UiFeedBackCtrl.presentAlert('', 'Hum... infelizmente o prestador de serviço não atende na sabado... Favor escolher um outro dia da semana!', 'error');
          } else {
            this.scheduleReady = false;
            this.localTime = '';
            this.localHourValues = [];
            for (let i = this.localOfficeHours.sab.begin; i < this.localOfficeHours.sab.end; i++) {
              this.localHourValues.push(i.toString());
            }
            this.localDateValid = true;
          }
          break;
      }
    } else {
      this.scheduleReady = false;
      this.localTime = '';
      this.localHourValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
      this.localDateValid = true;
    }
  }

  checkTime() {
    if (this.localTime != '') {
      const currentTime = new Date().getTime();
      let tempDuedate = new Date(`${this.localDate} ${this.localTime}:00 GMT-0300 (Hora oficial do Brasil)`);
      if (tempDuedate.getTime() < currentTime) {
        this.scheduleReady = false;
        this.localTime = '';
        this.UiFeedBackCtrl.presentAlert('O horario selecionado já passou!', 'Favor selecionar um hórario valido!', 'error');
      } else {
        this.scheduleReady = true;
      }
    }
  }

  private uploadCaracs(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (let carac of this.localCharacteristics) {
        this.db.afs.collection(this.db.COLLECTIONS.ORDERS_COLLECTION).doc(this.localOrder.id).collection(this.db.COLLECTIONS.CHARACTERISTICS_COLLECTION).doc(carac.id).set(Object.assign({}, carac))
          .then(() => {
            this.uploadedCaracNum++;
            if (this.localCharacteristics.length == this.uploadedCaracNum) {
              resolve();
            }
          })
          .catch(e => {
            reject({ log: 'erro ao fazer upload de uma caracteristica do serviço', error: JSON.stringify(e) });
          });
      }
    });
  }

  saveOrder() {


    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/newspaper.json')
      .then(() => {

        if (this.localOrder.acceptHomeDelivery) {
          const selectedAdress = this.addresses.filter((addres) => {
            return (addres.id == this.localOrder.addressId);
          });
          this.localOrder.address = Object.assign({}, selectedAdress[0]);
        } else {
          this.localOrder.address = Object.assign({}, this.selectedServiceTypeSub.providerWorkAddress);
        }

        this.localSchedule.dueDate = new Date(`${this.localDate} ${this.localTime}:00 GMT-0300 (Hora oficial do Brasil)`);

        this.UiFeedBackCtrl.updateLoaderMessage('Salvando Pedido...');

        this.db.orders.create(this.localOrder)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando Agendamento...');
            this.db.orders.schedules.create(this.localOrder.id, this.localSchedule.id, this.localSchedule)
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando caracteristicas...');
                this.uploadCaracs()
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.canLeaveCtrl = true;
                    this.navCtrl.pop();
                    this.UiFeedBackCtrl.presentAlert('Maravilha :)', 'Seu pedido já foi enviado, agora é só aguardar o retorno do prestador! ', 'success');
                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.canLeaveCtrl = true;
            this.navCtrl.pop();
            this.UiFeedBackCtrl.presentErrorAlert('erro', e);
          });

      });
  }

}