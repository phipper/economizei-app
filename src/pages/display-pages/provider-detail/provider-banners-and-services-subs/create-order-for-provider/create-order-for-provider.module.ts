import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateOrderForProviderPage } from './create-order-for-provider';
import { LottieAnimationViewModule } from 'ng-lottie';
import { Ionic2RatingModule } from 'ionic2-rating';


@NgModule({
  declarations: [
    CreateOrderForProviderPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateOrderForProviderPage),
    LottieAnimationViewModule,
    Ionic2RatingModule
  ],
})
export class CreateOrderForProviderPageModule { }
