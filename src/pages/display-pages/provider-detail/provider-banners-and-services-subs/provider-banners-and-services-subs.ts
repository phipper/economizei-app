import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Content, ActionSheetButton } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { UserDataProvider } from '../../../../providers/UserData';
import { ServiceTypeSub } from '../../../../models/ServiceTypeSub';
import { DocumentSnapshot, WhereFilterOp } from '@firebase/firestore-types';
import { Banner } from '../../../../models/Banner';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';
import { User } from '../../../../models/User';

@IonicPage()
@Component({
  selector: 'page-provider-banners-and-services-subs',
  templateUrl: 'provider-banners-and-services-subs.html',
})
export class ProviderBannersAndServicesSubs {

  @ViewChild(Content) content: Content;

  isTheOwner = false;

  provider = new User();

  localServiceTypeSub: ServiceTypeSub[] = [];

  showSplash = true;

  isExecutingQuery = false;
  localBanners: Banner[] = [];
  arrangedBanners: Banner[] = [];

  queryLimit = 4;
  lastQueryDoc: DocumentSnapshot = null;

  activeBannersWhere = {
    id: 0,
    name: 'ActiveBanners',
    fieldPath: 'status',
    opStr: '==' as WhereFilterOp,
    value: 11
  };

  querryWhere = this.activeBannersWhere;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard
  ) {
    let tempUser = navParams.get("provider");
    if (tempUser) {
      this.provider = tempUser;
    }
  }



  loadlocalServiceTypeSub() {
    this.localServiceTypeSub = [];
    this.db.afs.collection('ServiceTypeSubs').ref
      .where('providerId', '==', this.provider.id)
      .orderBy('createdOn')
      .get()
      .then(ServicesTypesQuery => {
        for (let ServicesTypeSubDoc of ServicesTypesQuery.docs) {
          this.localServiceTypeSub.push(ServicesTypeSubDoc.data() as ServiceTypeSub);
        }
        this.loadBanners();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar suas inscrições em serviço', e);
      });
  }

  ionViewDidLoad() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        if (userId == this.provider.id) {
          this.isTheOwner = true;
        }
        this.loadlocalServiceTypeSub();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar suas inscrições em serviço', e);
      });
  }

  arrangeBanners(): Promise<void> {
    return new Promise((resolve) => {
      let bannersSize = this.localBanners.length;
      let tempArrangedBanners: Banner[] = [];
      tempArrangedBanners[0] = this.localBanners[0];
      for (var i = 0; i < bannersSize; i++) {
        if (i != 0) {
          if (i % 2 == 0)
            tempArrangedBanners[i / 2] = this.localBanners[i];
          else
            tempArrangedBanners[bannersSize / 2 + Math.floor(i / 2)] = this.localBanners[i];
        }
      }
      this.arrangedBanners = tempArrangedBanners;
      resolve();
    });
  }

  loadBanners() {
    console.log(`Executando loadBanners query: ${JSON.stringify(this.querryWhere)}`);
    this.showSplash = true;
    this.isExecutingQuery = true;
    this.localBanners = [];
    this.lastQueryDoc = null;
    if (this.querryWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.provider.id)
        .orderBy(this.querryWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.arrangeBanners()
                .then(() => {
                  this.showSplash = false;
                });
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.provider.id)
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.arrangeBanners()
                .then(() => {
                  this.showSplash = false;
                });
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    }
  }

  tryToRefresh(refresher) {
    if (!this.showSplash) {
      this.doRefresh(refresher);
    } else {
      refresher.complete();
      this.UiFeedBackCtrl.presentToastTop('Aguarde o carregamento', 'warning');
    }
  }

  doRefresh(refresher) {
    console.log(`Executando doRefresh query: ${JSON.stringify(this.querryWhere)}`);
    this.isExecutingQuery = true;
    this.lastQueryDoc = null;
    this.localBanners = [];
    if (this.querryWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.provider.id)
        .orderBy(this.querryWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.arrangeBanners()
                .then(() => {
                  refresher.complete();
                });
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('providerId', '==', this.provider.id)
        .orderBy('createdOn', 'desc')
        .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastQueryDoc = null;
              } else {
                this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.arrangeBanners()
                .then(() => {
                  refresher.complete();
                });
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doInfinite(infiniteScroll) {
    if (this.lastQueryDoc != null) {
      this.showSplash = true;
      console.log(`Executando doInfinite query: ${JSON.stringify(this.querryWhere)}`);
      if (this.querryWhere.opStr !== '==') {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('providerId', '==', this.provider.id)
          .orderBy(this.querryWhere.fieldPath, 'asc')
          .orderBy('createdOn', 'desc')
          .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
          .startAfter(this.lastQueryDoc)
          .limit(this.queryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localBanners.push(doc.data() as Banner);
                }
                if (querySnapshot.empty) {
                  this.lastQueryDoc = null;
                } else {
                  this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.arrangeBanners()
                  .then(() => {
                    this.showSplash = false;
                    console.log(`query Executed`);
                    infiniteScroll.complete();
                  });
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('providerId', '==', this.provider.id)
          .orderBy('createdOn', 'desc')
          .where(this.querryWhere.fieldPath, this.querryWhere.opStr, this.querryWhere.value)
          .startAfter(this.lastQueryDoc)
          .limit(this.queryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localBanners.push(doc.data() as Banner);
                }
                if (querySnapshot.empty) {
                  this.lastQueryDoc = null;
                } else {
                  this.lastQueryDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.arrangeBanners()
                  .then(() => {
                    this.showSplash = false;
                    console.log(`query Executed`);
                    infiniteScroll.complete();
                  });
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      console.log(`no need to execute doInfinite query for: ${JSON.stringify(this.querryWhere)}`);
      infiniteScroll.complete();
    }
  }


  serviceTypeSubTapped(serviceTypeSub: ServiceTypeSub) {
    if (this.isTheOwner) {
      this.UiFeedBackCtrl.presentInfoAlert(`Ao selecionar um dos seus tipos de serviço o cliente poderá abrir um pedido avulso para você!`)
    } else {
      this.navCtrl.push('CreateOrderForProviderPage', {
        provider: this.provider,
        selectedServiceTypeSub: serviceTypeSub
      });
    }
  }


  bannerTapped(banner) {
    this.navCtrl.push('BannerDetailPage', {
      banner: banner
    });
  }

  saveBanner(bannerId: string) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Adiconando aos favoritos...',
      dismissOnPageChange: true
    });

    loader.present()
      .then(() => {
        this.UserDataCtrl.saveBanner(bannerId)
          .then(() => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentToast('Anuncio adicionado aos favoritos', 'success');
          })
          .catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  unsaveBanner(bannerId: string) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Removendo dos favoritos...',
      dismissOnPageChange: true
    });
    loader.present()
      .then(() => {
        this.UserDataCtrl.unsaveBanner(bannerId)
          .then(() => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentToast('Anuncio removido dos favoritos', 'success');
          })
          .catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentAlert('Erro', JSON.stringify(e), 'error');
          });
      });
  }

  bannerPressed(banner: Banner) {

    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share(`Olha esse anuncio que achei pra você no economizei, ${banner.title}`, banner.title, null, banner.dynamicLink);
      }
    });

    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy(banner.dynamicLink).then(() => {
          this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', banner.dynamicLink, 'success');
        });
      }
    });

    if (this.UserDataCtrl.hasUser && !this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Adicionar aos favoritos',
        icon: !this.platform.is('ios') ? 'heart-outline' : null,
        handler: () => {
          this.saveBanner(banner.id);
        }
      });
    }

    if (this.UserDataCtrl.hasUser != null && this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Remover dos favoritos',
        icon: !this.platform.is('ios') ? 'heart' : null,
        handler: () => {
          this.unsaveBanner(banner.id);
        }
      });
    }

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: banner.title,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }



}
