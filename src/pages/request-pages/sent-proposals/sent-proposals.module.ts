import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SentProposalsPage } from './sent-proposals';
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    SentProposalsPage,
  ],
  imports: [
    IonicPageModule.forChild(SentProposalsPage),
    LottieAnimationViewModule
  ],
})
export class SentProposalsPageModule {}
