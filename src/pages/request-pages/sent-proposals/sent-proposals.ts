import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../providers/UserData';

import { UtilProvider } from '../../../providers/Util';
import { Proposal } from '../../../models/Proposal';
import { WhereFilterOp, DocumentSnapshot } from '@firebase/firestore-types';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-sent-proposals',
  templateUrl: 'sent-proposals.html',
})
export class SentProposalsPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  segment: string = "Pendentes";
  showSplash = true;
  localProposals: Proposal[] = [];
  isLoaded = false;
  proposalsQueryLimit = 5;
  lastProposalDoc: DocumentSnapshot = null;
  isExecutingQuery = false;

  pendingProposalsWhere = {
    id: 0,
    name: 'pendingProposals',
    fieldPath: 'status',
    opStr: '==' as WhereFilterOp,
    value: 1
  };

  acceptProposalsWhere = {
    id: 0,
    name: 'AceeptedProposals',
    fieldPath: 'status',
    opStr: '==' as WhereFilterOp,
    value: 11
  };

  othersProposalsWhere = {
    id: 0,
    name: 'OtherProposals',
    fieldPath: 'status',
    opStr: '>=' as WhereFilterOp,
    value: 13
  };

  proposalsWhere = this.pendingProposalsWhere;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'MyRequestsPage') {
      this.content.scrollToTop();
    }
  }

  ionViewDidEnter() {
    if (!this.isLoaded) {
      this.UserDataCtrl.getUid()
        .then(userId => {
          this.localUserId = userId;
          this.loadProposals();
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        })
    }
  }

  reLoadActiveSegment() {
    switch (this.segment) {
      case "Pendentes":
        this.proposalsWhere = this.pendingProposalsWhere;
        this.loadProposals();
        break;

      case "Aceitas":
        this.proposalsWhere = this.acceptProposalsWhere;
        this.loadProposals();
        break;

      case "Outras":
        this.proposalsWhere = this.othersProposalsWhere;
        this.loadProposals();
        break;
    }
  }

  proposalTapped(proposal: Proposal) {
    this.UiFeedBackCtrl.presenLoader('Carregando solicitação...')
      .then(() => {
        this.db.requests.get(proposal.requestId)
          .then(request => {
            this.navCtrl.push('RequestDetailPage', {
              request: request
            })
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          })
      })
  }

  loadProposals() {
    console.log(`Executando loadRequest query: ${JSON.stringify(this.proposalsWhere)}`);
    this.isExecutingQuery = true;
    this.showSplash = true;
    this.localProposals = [];
    if (this.proposalsWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy(this.proposalsWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.proposalsWhere.fieldPath, this.proposalsWhere.opStr, this.proposalsWhere.value)
        .limit(this.proposalsQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localProposals.push(doc.data() as Proposal);
              }
              if (querySnapshot.empty) {
                this.lastProposalDoc = null;
              } else {
                this.lastProposalDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.proposalsWhere.fieldPath, this.proposalsWhere.opStr, this.proposalsWhere.value)
        .limit(this.proposalsQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localProposals.push(doc.data() as Proposal);
              }
              if (querySnapshot.empty) {
                this.lastProposalDoc = null;
              } else {
                this.lastProposalDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    }
  }

  doRefresh(refresher) {
    console.log(`Executando doRefresh query: ${JSON.stringify(this.proposalsWhere)}`);
    this.isExecutingQuery = true;
    this.lastProposalDoc = null;
    this.localProposals = [];
    if (this.proposalsWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy(this.proposalsWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.proposalsWhere.fieldPath, this.proposalsWhere.opStr, this.proposalsWhere.value)
        .limit(this.proposalsQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localProposals.push(doc.data() as Proposal);
              }
              if (querySnapshot.empty) {
                this.lastProposalDoc = null;
              } else {
                this.lastProposalDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              console.log(`query Executed`);
              refresher.complete();
              this.isExecutingQuery = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
        .where('providerId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.proposalsWhere.fieldPath, this.proposalsWhere.opStr, this.proposalsWhere.value)
        .limit(this.proposalsQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localProposals.push(doc.data() as Proposal);
              }
              if (querySnapshot.empty) {
                this.lastProposalDoc = null;
              } else {
                this.lastProposalDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              console.log(`query Executed`);
              refresher.complete();
              this.isExecutingQuery = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doInfinite(infiniteScroll) {
    if (this.lastProposalDoc != null) {
      this.isExecutingQuery = true;
      console.log(`Executando doRefresh query: ${JSON.stringify(this.proposalsWhere)}`);
      if (this.proposalsWhere.opStr !== '==') {
        this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
          .where('providerId', '==', this.localUserId)
          .orderBy(this.proposalsWhere.fieldPath, 'asc')
          .orderBy('createdOn', 'desc')
          .where(this.proposalsWhere.fieldPath, this.proposalsWhere.opStr, this.proposalsWhere.value)
          .startAfter(this.lastProposalDoc)
          .limit(this.proposalsQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localProposals.push(doc.data() as Proposal);
                }
                if (querySnapshot.empty) {
                  this.lastProposalDoc = null;
                } else {
                  this.lastProposalDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
          .where('providerId', '==', this.localUserId)
          .orderBy('createdOn', 'desc')
          .where(this.proposalsWhere.fieldPath, this.proposalsWhere.opStr, this.proposalsWhere.value)
          .startAfter(this.lastProposalDoc)
          .limit(this.proposalsQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localProposals.push(doc.data() as Proposal);
                }
                if (querySnapshot.empty) {
                  this.lastProposalDoc = null;
                } else {
                  this.lastProposalDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      console.log(`no need to execute doInfinite query for: ${JSON.stringify(this.proposalsWhere)}`);
      infiniteScroll.complete();
    }
  }

}
