import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Slides, ModalController } from 'ionic-angular';

import { UserDataProvider } from '../../../providers/UserData';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { Request } from '../../../models/Request';
import { Proposal } from '../../../models/Proposal';
import { Pic } from '../../../models/Pic';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { UtilProvider } from '../../../providers/Util';

@IonicPage()
@Component({
  selector: 'page-request-detail',
  templateUrl: 'request-detail.html',
})
export class RequestDetailPage {
  @ViewChild(Slides) SlidesCtrl: Slides;

  localUserId = '';
  showSplash = true;
  localRequest = new Request();
  localPics: Pic[] = [];
  showPicSplash = true;
  receivedProposalsNumber = '⌛';

  sentProposals: Proposal[] = [];
  requestSub: any;
  descTrucated = false;

  constructor(
    public platform: Platform,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public modalCtrl: ModalController,
    public utilCtrl: UtilProvider
  ) {
    this.localRequest = navParams.get("request");
    if(this.localRequest.description.length > 250) {
      this.descTrucated = true;
    }
  }

  toggleTruncateDesc() {
    if(this.localRequest.description.length > 250) {
      this.descTrucated = !this.descTrucated;
    }
  }

  loadPics() {
    this.db.requests.pics.getAll(this.localRequest.id)
      .then(picQuery => {
        this.localPics = picQuery;
        this.SlidesCtrl.lockSwipes(this.localPics.length == 1);
        this.showPicSplash = false;
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar fotos', e);
      });
  }

  loadProposals() {
    this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
      .where('requestId', '==', this.localRequest.id)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            this.receivedProposalsNumber = querySnapshot.size.toString();
            if (!querySnapshot.empty) {
              this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
                .where('requestId', '==', this.localRequest.id)
                .where('providerId', '==', this.localUserId)
                .orderBy('createdOn', 'asc')
                .get()
                .then(querySnapshot => {
                  this.db.validators.validateQuerySnapshot(querySnapshot)
                    .then(querySnapshot => {
                      let tempProposalsVet: Proposal[] = [];
                      for (let tempPropDoc of querySnapshot.docs) {
                        tempProposalsVet.push(tempPropDoc.data() as Proposal);
                      }
                      this.sentProposals = tempProposalsVet;
                    })
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                })
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }


  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.loadPics();
        this.loadProposals();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  ionViewDidLeave() {
    if (this.requestSub != null) {
      this.requestSub.unsubscribe();
    }
  }

  numberPadding(number: number, size: number): string {
    var s = number.toString();
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }

  share() {
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Compartilhar',
          icon: !this.platform.is('ios') ? 'md-share' : null,
          handler: () => {
            this.socialSharing.share(`Olha essa Solcitação que achei pra você no economizei, ${this.localRequest.serviceTypeTitle}`, this.localRequest.serviceTypeTitle, null, this.localRequest.dynamicLink);
          }
        },
        {
          text: 'Copiar link',
          icon: !this.platform.is('ios') ? 'link' : null,
          handler: () => {
            this.clipboard.copy(this.localRequest.dynamicLink)
              .then(() => {
                this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', this.localRequest.dynamicLink, 'success');
              });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  startProposal() {
    this.navCtrl.push('CreateProposalPage', {
      request: this.localRequest
    });
  }


  showServiceDetail() {
    this.navCtrl.push('RequestServiceDetailPage', {
      request: this.localRequest
    });
  }

  showSchedulingDetail() {
    let modal = this.modalCtrl.create('RequestSchedulingDetailPage', {
      schedule: Object.assign({}, this.localRequest.schedule)
    });
    modal.present();
  }

  showClient() {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Carregando cliente...',
    });
    loader.present()
      .then(() => {
        this.db.users.get(this.localRequest.clientId)
          .then(user => {
            this.navCtrl.push('ClientDetailPage', {
              client: user
            })
              .then(() => {
                loader.dismiss();
              });
          })
          .catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentErrorAlert('erro ao carregar dados do cliente', e);
          })
      });
  }

}
