import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

import { Request } from '../../../../models/Request';
import { Characteristic } from '../../../../models/Characteristic';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';


@IonicPage()
@Component({
  selector: 'page-request-service-detail',
  templateUrl: 'request-service-detail.html',
})
export class RequestServiceDetailPage {

  showSplash = true;
  localRequest: Request = new Request();
  localCharacteristics: Characteristic[] = [];

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localRequest = navParams.get("request");
  }

  ionViewDidEnter() {
    this.afs.collection('Requests').doc(this.localRequest.id).collection('Characteristics').ref
      .orderBy("orderNumber").get()
      .then(querySnapshot => {
        for (let caracDoc of querySnapshot.docs) {
          this.localCharacteristics.push(caracDoc.data() as Characteristic);
        }
        this.showSplash = false;
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao procurar caracteristicas do pedido', e);
      });
  }

  ionViewDidLeave() {
  }

}
