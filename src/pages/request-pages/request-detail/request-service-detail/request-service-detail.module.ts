import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestServiceDetailPage } from './request-service-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    RequestServiceDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestServiceDetailPage),
    LottieAnimationViewModule
  ],
})
export class RequestServiceDetailPageModule {}
