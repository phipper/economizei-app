import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateProposalPage } from './create-proposal';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    CreateProposalPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateProposalPage),
    LottieAnimationViewModule
  ],
})
export class CreateProposalPageModule {}
