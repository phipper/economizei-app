import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, Content, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../../providers/UserData';
import { UserPlanProvider } from '../../../../providers/UserPlan';

import { Request } from '../../../../models/Request';
import { Schedule } from '../../../../models/Schedule';
import { Proposal } from '../../../../models/Proposal';
import { Budget } from '../../../../models/Budget';
import { Notification } from '../../../../models/Notification';
import { UtilProvider } from '../../../../providers/Util';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-create-proposal',
  templateUrl: 'create-proposal.html',
})
export class CreateProposalPage {
  @ViewChild(Content) content: Content;

  localRequest: Request = new Request();
  localProposal: Proposal = new Proposal();

  segment: string = 'Agendamento';

  selectedScheduleType: string = '';

  proposeSchedule = null;
  localSchedule: Schedule = new Schedule();

  localDate = '';
  localDateValid = false;
  localHourValues: string[] = [];
  localTime = '';
  scheduleReady = false;
  minDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMinDate();
  maxDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMaxDate(60);

  budgetReady = false;
  localBudget: Budget = new Budget();
  localPrice = "";

  uploadedCaracNum = 0;

  canLeaveCtrl = true;
  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/newspaper.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private UserPlanCtrl: UserPlanProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localRequest = navParams.get("request");
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'CreateRequestPge') {
      this.content.scrollToTop();
    }
  }

  scrollToBottom() {
    this.canLeaveCtrl = false;
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  initiatePage() {
    this.localProposal.id = this.db.createId();
    this.localProposal.providerId = this.UserDataCtrl.localUser.id;
    this.localProposal.providerName = this.UserDataCtrl.localUser.fullName;
    this.localProposal.providerJob = this.UserDataCtrl.localUser.job;
    this.localProposal.providerThumbnailUrl = this.UserDataCtrl.localUser.pic.thumbnailUrl;
    this.localProposal.providerPoints = this.UserDataCtrl.localUser.providerPoints;
    this.localProposal.providerReputation = this.UserDataCtrl.localUser.providerReputation;
    this.localProposal.requestId = this.localRequest.id;
    this.localProposal.requestScheduleType = this.localRequest.scheduleType;
    this.localProposal.requestAddress = Object.assign({}, this.localRequest.address);

    this.localProposal.clientName = this.localRequest.clientName;
    this.localProposal.clientThumbnailUrl = this.localRequest.clientThumbnailUrl;
    this.localProposal.serviceTypeId = this.localRequest.serviceTypeId;
    this.localProposal.serviceTypeTitle = this.localRequest.serviceTypeTitle;
    this.localProposal.serviceTypeSymbol = this.localRequest.serviceTypeSymbol;

    this.localSchedule.homeDelivery = this.localRequest.acceptHomeDelivery;
    this.localSchedule.clientId = this.localRequest.clientId;
    this.localSchedule.id = this.db.createId();
    this.localSchedule.clientId = this.localRequest.clientId;
    this.localSchedule.createdBy = this.UserDataCtrl.localUser.id;
    this.localSchedule.providerId = this.UserDataCtrl.localUser.id;
    this.localSchedule.requestId = this.localRequest.id;
    this.localSchedule.status = 'pending_client';

    this.localBudget.id = this.db.createId();
    this.localBudget.clientId = this.localRequest.clientId;
    this.localBudget.requestId = this.localRequest.id;
    this.localBudget.providerId = this.UserDataCtrl.localUser.id;

    this.showSplash = false;
  }

  ionViewDidEnter() {
    if (this.UserDataCtrl.localUser.verifiedProvider && this.UserDataCtrl.localUser.status == 'active') {
      if (this.UserDataCtrl.localUser.workAdressId == '') {
        const alert = this.UiFeedBackCtrl.alertCtrl.create({
          title: 'Não possuimos nenhum endereço de trabalho seu !',
          subTitle: 'Você precisa cadastrar um endereço para poder criar um anuncio. Deseja cadastrar agora?',
          buttons: [
            {
              text: 'Sim',
              handler: () => {
                this.navCtrl.push('AddAddressPage');
              }
            },
            'Cancelar'
          ]
        });
        this.navCtrl.pop();
        this.UiFeedBackCtrl.hapticCtrl.notification('error');
        alert.present();
      } else {
        this.UserPlanCtrl.canSubmitProposal(this.UserDataCtrl.localUser)
          .then(canSubmitProposal => {
            if (canSubmitProposal) {
              this.initiatePage();
            } else {
              this.UserPlanCtrl.getUserPlan()
                .then(plan => {
                  this.navCtrl.pop();
                  this.UiFeedBackCtrl.presentAlert(`Numero máximo de propostas atingidas para o plano ${plan.title} 😭`,
                    `O plano ${plan.title} permite somente ${plan.maxSentProposalsPerDay} de propostas por dia`, 'error');
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                });
            }
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar', e);
          });
      }
    } else if (this.UserDataCtrl.localUser.status == 'active') {
      this.navCtrl.pop();
      if (this.UserDataCtrl.localUser.providerVerificationSacId != '') {
        this.UiFeedBackCtrl.presentAlert('Ainda estamos verificando seus dados', 'Precisamos verificar os seus documentos antes de permitir a criação de anuncios para garantir a segurança de nossos usuários. Aguarde... esse processo demora no maximo 24horas, entraremos em contato com você se necessario.', 'warning')
      } else {
        const alert = this.UiFeedBackCtrl.alertCtrl.create({
          title: 'Você ainda não foi verificado !',
          subTitle: 'Você precisa nos enviar algums dados para que possamos verificar seu usuário de prestador. Esse processo é rápido e você só poderá fazer anuncios e receber pedidos depois de faze-lo. Deseja enviar agora?',
          buttons: [
            {
              text: 'Sim',
              handler: () => {
                this.navCtrl.push('ProviderVerificationPage');
              }
            },
            'Não'
          ]
        });
        this.UiFeedBackCtrl.hapticCtrl.notification('warning');
        alert.present();
      }
    } else {
      this.navCtrl.pop();
      this.UiFeedBackCtrl.presentAlert('Seu usuário não esta ativo!', 'Favor entrar em contato com o nosso suporte para mais esclarecimentos!', 'error');
    }
  }

  ionViewCanLeave(): boolean {
    if (!this.canLeaveCtrl) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Tem certeza?',
        subTitle: 'Você quer descartar todos os dados e cancelar a criação da proposta?',
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              this.canLeaveCtrl = true;
              this.navCtrl.pop();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      return true;
    }
    return false;
  }


  goForward() {
    this.segment = 'Orçamento';
    this.canLeaveCtrl = false;
  }

  goBack() {
    this.segment = 'Agendamento';
  }

  checkDate() {
    this.localDateValid = true;
    this.scrollToBottom();
  }

  checkTime() {
    if (this.localTime != '') {
      const currentTime = new Date().getTime();
      let tempDuedate = new Date(`${this.localDate} ${this.localTime}:00 GMT-0300 (Hora oficial do Brasil)`);
      if (tempDuedate.getTime() < currentTime) {
        this.scheduleReady = false;
        this.localTime = '';
        this.UiFeedBackCtrl.presentAlert('O horario selecionado já passou!', 'Favor selecionar um hórario valido!', 'error');
      } else {
        this.scheduleReady = true;
        this.scrollToBottom();
      }
    }
  }

  toggleHomeDelivery() {
    this.canLeaveCtrl = false;
    this.localSchedule.homeDelivery = !this.localSchedule.homeDelivery;
    if (this.localSchedule.homeDelivery) {
      this.localSchedule.geoCheckin = true;
      this.UiFeedBackCtrl.presenLottietAlert('Atendimento em domicílio adicionada ao seu agendamento', 'O prestador terá de se deslocar até o endereço do seu pedido no horario agendado', 'assets/animations/location.json', 'warning');
    } else {
      this.UiFeedBackCtrl.presentAlert('Atendimento em domicílio removida do seu agendamento', 'Isto significa que você irá se deslocar até o prestador ou deverá receber seu serviço por outros meios', 'warning');
    }
  }

  toggleCheckIn() {
    this.canLeaveCtrl = false;
    this.localSchedule.geoCheckin = !this.localSchedule.geoCheckin;
    if (this.localSchedule.geoCheckin) {
      this.UiFeedBackCtrl.presenLottietAlert('Check-in via GPS adicionado ao seu agendamento', 'O prestador terá de fazer Check-in ao chegar no local marcado', 'assets/animations/location_map_pin.json', 'warning');
    } else {
      this.UiFeedBackCtrl.presentAlert('Check-in via GPS removido do seu agendamento', '', 'warning');
    }
  }

  toggleRequiredHomeDelivery() {
    this.canLeaveCtrl = false;
    this.localRequest.requiredHomeDelivery = !this.localRequest.requiredHomeDelivery;
  }

  CheckPrice() {
    let priceValue = Number(this.localPrice);
    if (this.localPrice.indexOf(',', 0) != -1 || this.localPrice.indexOf('.', 0) != -1) {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não pode conter casas decimais!", 'assets/animations/file_error.json', 'error');
    } else if (Number.isNaN(priceValue)) {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço não deve conter letras!", 'assets/animations/file_error.json', 'error');
    } else if (this.localPrice == "") {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "Favor digite um preço valido!", 'assets/animations/file_error.json', 'error');
    } else if (priceValue <= 0) {
      this.localBudget.value = null;
      this.localPrice = "";
      this.UiFeedBackCtrl.presenLottietAlert("Preço invalido!", "O preço deve ser maior que 0!", 'assets/animations/file_error.json', 'error');
    } else {
      this.localBudget.value = priceValue;
    }
  }

  toggleWarranty() {
    this.localBudget.warranty = !this.localBudget.warranty;
  }

  finish() {

    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/newspaper.json')
      .then(() => {

        this.UiFeedBackCtrl.updateLoaderMessage('Registrando o envio da proposta');
        this.UserPlanCtrl.registrateProposalSubmission(this.UserDataCtrl.localUser.id, this.localRequest.id, this.localProposal.id)
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando orçamento');
            this.localProposal.budgetValue = this.localBudget.value;
            this.localProposal.budget = Object.assign({}, this.localBudget);

            if (this.localRequest.scheduleType == 'close' && this.localProposal.acceptedSchedule == false) {
              this.UiFeedBackCtrl.updateLoaderMessage('Salvando agendamento');
              this.localSchedule.id = this.db.createId();
              this.localSchedule.dueDate = new Date(`${this.localDate} ${this.localTime}:00 GMT-0300 (Hora oficial do Brasil)`);
              this.localProposal.schedule = Object.assign({}, this.localSchedule);
            }

            if (this.localRequest.scheduleType == 'open' && this.proposeSchedule == true) {
              this.UiFeedBackCtrl.updateLoaderMessage('Salvando agendamento');
              this.localSchedule.id = this.db.createId();
              this.localSchedule.dueDate = new Date(`${this.localDate} ${this.localTime}:00 GMT-0300 (Hora oficial do Brasil)`);
              this.localProposal.schedule = Object.assign({}, this.localSchedule);
            }

            this.UiFeedBackCtrl.updateLoaderMessage('Salvando seu endereço de trabalho');
            this.db.addresses.get(this.UserDataCtrl.localUser.workAdressId)
              .then(addressData => {
                let tempAddress = addressData;
                this.localProposal.providerWorkAddress = Object.assign({}, tempAddress);
                this.UiFeedBackCtrl.updateLoaderMessage('Salvando Proposta');
                this.db.proposals.create(this.localProposal)
                  .then(() => {
                    this.UiFeedBackCtrl.updateLoaderMessage('Salvando outras coisas...');
                    let notif = new Notification();
                    notif.id = this.db.createId();
                    notif.title = 'Nova proposta!';
                    notif.text = `${this.UserDataCtrl.localUser.firstName} acabou de enviar uma proposta para sua solicitação "${this.localRequest.serviceTypeTitle}"`;
                    notif.type = 'showMyRequest';
                    notif.action = {
                      requestId: this.localRequest.id,
                    };
                    notif.senderName = "economizei";
                    this.db.users.notifications.create(this.localRequest.clientId, notif)
                      .then(() => {
                        this.UiFeedBackCtrl.dismissLoader();
                        this.canLeaveCtrl = true;
                        this.navCtrl.pop();
                        this.UiFeedBackCtrl.presentAlert('Maravilha :)', 'Sua Proposta já Foi enviada. Agora é só esperar!', 'success');
                      })
                  })
              })
              .catch(e => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert('Erro ao salvar Proposta', e);
              });
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro ao registrar envio de proposta', e);
          })
      });
  }

}