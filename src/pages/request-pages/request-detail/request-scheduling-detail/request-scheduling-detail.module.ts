import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestSchedulingDetailPage } from './request-scheduling-detail';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    RequestSchedulingDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(RequestSchedulingDetailPage),
  ],
})
export class RequestSchedulingDetailPageModule {}
