import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { Schedule } from '../../../../models/Schedule';


@IonicPage()
@Component({
  selector: 'page-request-scheduling-detail',
  templateUrl: 'request-scheduling-detail.html',
})

export class RequestSchedulingDetailPage {

  localSchedule: Schedule = new Schedule();

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
    this.localSchedule = navParams.get('schedule');
  }


  close() {
    this.viewCtrl.dismiss();
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  convertMiliSecondsToHMmSs(miliseconds: number): string {
    var seconds = Math.abs(miliseconds / 1000);
    var s = Math.trunc(seconds % 60);
    var m = Math.trunc((seconds / 60) % 60);
    var h = Math.trunc((seconds / (60 * 60)) % 24);
    return `${this.numberPadding(h, 2)}:${this.numberPadding(m, 2)}:${this.numberPadding(s, 2)}`;
  }


  numberPadding(number: number, size: number): string {
    var s = number.toString();
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }

}
