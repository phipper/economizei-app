import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';
import { RequestDetailPage } from './request-detail';

@NgModule({
  declarations: [
    RequestDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(RequestDetailPage),
  ],
})
export class RequestDetailPageModule {}
