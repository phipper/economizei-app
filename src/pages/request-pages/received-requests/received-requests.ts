import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Content, ActionSheetButton } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';
import { LocalizationProvider } from '../../../providers/Localization/Localization';

import { UserDataProvider } from '../../../providers/UserData';
import { ServiceTypeSub } from '../../../models/ServiceTypeSub';
import { Request } from '../../../models/Request';
import { DocumentSnapshot } from '@firebase/firestore-types';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { LocalDbProvider } from '../../../providers/LocalDbProvider';
import { AppConfig } from '../../../models/AppConfig';

@IonicPage()
@Component({
  selector: 'page-received-requests',
  templateUrl: 'received-requests.html',
})
export class ReceivedRequestsPage {

  @ViewChild(Content) content: Content;

  localUserId = '';

  localServiceTypeSub: ServiceTypeSub[] = [];
  selectedServiceTypeSub: ServiceTypeSub = new ServiceTypeSub();

  lastRequestDoc: DocumentSnapshot = null;
  localRequests: Request[] = [];
  queryLimit = 4;
  requestsIsLoaded = false;

  showSplash = true;

  isExecutingQuery = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  localAppConfig: AppConfig = new AppConfig();

  constructor(
    public platform: Platform,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private localizationCtrl: LocalizationProvider,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    private localDbCtrl: LocalDbProvider
  ) {
  }


  loadRequests() {
    console.log(`Executando loadRequests query`);
    this.isExecutingQuery = true;
    this.lastRequestDoc = null;
    this.showSplash = true;
    this.localRequests = [];
    this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
      .where('status', '==', 1)
      .where('serviceTypeId', '==', this.selectedServiceTypeSub.serviceTypeId)
      .where('address.state', '==', this.localAppConfig.state)
      .where('address.city', '==', this.localAppConfig.city)
      .limit(this.queryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              this.localRequests.push(doc.data() as Request);
            }
            if (querySnapshot.empty) {
              this.lastRequestDoc = null;
            } else {
              this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
            }
            this.isExecutingQuery = false;
            console.log(`query Executed`);
            this.showSplash = false;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }

  doRefresh(refresher) {
    this.isExecutingQuery = true;
    console.log(`Executando doRefresh query`);
    this.lastRequestDoc = null;
    this.localRequests = [];
    this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
      .where('status', '==', 1)
      .where('serviceTypeId', '==', this.selectedServiceTypeSub.serviceTypeId)
      .where('address.state', '==', this.localAppConfig.state)
      .where('address.city', '==', this.localAppConfig.city)
      .limit(this.queryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              this.localRequests.push(doc.data() as Request);
            }
            if (querySnapshot.empty) {
              this.lastRequestDoc = null;
            } else {
              this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
            }
            console.log(`query Executed`);
            refresher.complete();
            this.isExecutingQuery = false;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }

  doInfinite(infiniteScroll) {
    if (this.lastRequestDoc != null) {
      this.isExecutingQuery = true;
      console.log(`Executando doInfinite query`);
      this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
        .where('status', '==', 1)
        .where('serviceTypeId', '==', this.selectedServiceTypeSub.serviceTypeId)
        .where('address.state', '==', this.localAppConfig.state)
        .where('address.city', '==', this.localAppConfig.city)
        .startAfter(this.lastRequestDoc)
        .limit(this.queryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localRequests.push(doc.data() as Request);
              }
              if (querySnapshot.empty) {
                this.lastRequestDoc = null;
              } else {
                this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              infiniteScroll.complete();
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });

    } else {
      console.log(`no need to execute doInfinite query`);
      infiniteScroll.complete();
    }
  }

  serviceTypeSubTapped(serviceTypeSub: ServiceTypeSub) {
    this.selectedServiceTypeSub = serviceTypeSub;

    console.log(`Executando serviceTypeSubTapped query`);
    this.isExecutingQuery = true;
    this.lastRequestDoc = null;
    this.localRequests = [];
    this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
      .where('status', '==', 1)
      .where('serviceTypeId', '==', this.selectedServiceTypeSub.serviceTypeId)
      .where('address.state', '==', this.localAppConfig.state)
      .where('address.city', '==', this.localAppConfig.city)
      .limit(this.queryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              this.localRequests.push(doc.data() as Request);
            }
            if (querySnapshot.empty) {
              this.lastRequestDoc = null;
            } else {
              this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
            }
            console.log(`query Executed`);
            this.isExecutingQuery = false;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }


  loadlocalServiceTypeSub() {
    this.localServiceTypeSub = [];
    this.db.afs.collection(this.db.COLLECTIONS.SERVICE_TYPES_SUBS_COLLECTION).ref
      .where('providerId', '==', this.localUserId)
      .orderBy('createdOn')
      .get()
      .then(ServicesTypesQuery => {
        for (let ServicesTypeSubDoc of ServicesTypesQuery.docs) {
          this.localServiceTypeSub.push(ServicesTypeSubDoc.data() as ServiceTypeSub);
        }
        if (this.localServiceTypeSub.length > 0 && !this.requestsIsLoaded) {
          this.selectedServiceTypeSub = this.localServiceTypeSub[0];
          this.loadRequests();
        } else {
          this.showSplash = false;
        }
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar suas inscrições em serviço', e);
      });
  }

  ionViewDidLoad() {

    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;

        if (this.platform.is('cordova')) {

          console.log('HomePage says: É cordova');

          this.localDbCtrl.getAppConfig()
            .then(appConfig => {
              if (appConfig.isConfigured) {

                this.localAppConfig = appConfig;
                this.UiFeedBackCtrl.presentToast(`Mostrando solicitações para ${this.localAppConfig.city}, ${this.localAppConfig.state}`, '');
                this.loadlocalServiceTypeSub();
              } else {
                this.localizationCtrl.getLastAddress()
                  .then(address => {
                    this.localAppConfig.state = address.state;
                    this.localAppConfig.city = address.city;
                    this.UiFeedBackCtrl.presentToast(`Mostrando solicitações para ${this.localAppConfig.city}, ${this.localAppConfig.state}`, '');
                    this.loadlocalServiceTypeSub();
                  })
                  .catch(e => {
                    this.localAppConfig.state = 'Ceará';
                    this.localAppConfig.city = 'Fortaleza';
                    this.UiFeedBackCtrl.presentAlert('GPS INDISPONIVEL 😭', `Mostrando solicitações para minha casa: ${this.localAppConfig.city}, ${this.localAppConfig.state}`, '');
                    this.loadlocalServiceTypeSub();
                  });
              }
            })
        } else {
          console.log('HomePage says: não é cordova 😭');
          this.localAppConfig.state = 'Ceará';
          this.localAppConfig.city = 'Fortaleza';
          this.UiFeedBackCtrl.presentToast(`não é cordova 😭, Mostrando solicitações para minha casa: ${this.localAppConfig.city}, ${this.localAppConfig.state}`, 'error');
          this.loadlocalServiceTypeSub();
        }

      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }


  requestTapped(request) {
    this.navCtrl.push('RequestDetailPage', {
      request: request
    });
  }

  requestPressed(request: Request) {

    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share(`Olha essa Solicitação que achei pra você no economizei, ${request.serviceTypeTitle}`, request.serviceTypeTitle, null, request.dynamicLink);
      }
    });

    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy(request.dynamicLink).then(() => {
          this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', request.dynamicLink, 'success');
        });
      }
    });


    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: request.serviceTypeTitle,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

  doSubscription() {
    this.navCtrl.push('AddServiceSubPage');
  }

}
