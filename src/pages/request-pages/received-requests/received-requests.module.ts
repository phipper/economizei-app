import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceivedRequestsPage } from './received-requests';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ReceivedRequestsPage,
  ],
  imports: [
    IonicPageModule.forChild(ReceivedRequestsPage),
    LottieAnimationViewModule
  ],
})
export class ReceivedRequestsPageModule {}
