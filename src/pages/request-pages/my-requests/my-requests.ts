import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Platform } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { UserDataProvider } from '../../../providers/UserData';

import { Request } from '../../../models/Request';
import { UtilProvider } from '../../../providers/Util';
import { DocumentSnapshot, WhereFilterOp } from '@firebase/firestore-types';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';


@IonicPage()
@Component({
  selector: 'page-my-requests',
  templateUrl: 'my-requests.html',
})
export class MyRequestsPage {
  @ViewChild(Content) content: Content;

  localUserId = '';

  segment: string = "Abertas";
  showSplash = true;
  isExecutingQuery = false;
  localRequests: Request[] = [];
  requestQueryLimit = 5;
  lastRequestDoc: DocumentSnapshot = null;

  openRequestsWhere = {
    id: 0,
    name: 'Open',
    fieldPath: 'status',
    opStr: '==' as WhereFilterOp,
    value: 1
  };

  pausedRequestsWhere = {
    id: 1,
    name: 'PausedRequests',
    fieldPath: 'status',
    opStr: '==' as WhereFilterOp,
    value: 2
  };

  otherRequestsWhere = {
    id: 2,
    name: 'OtherRequests',
    fieldPath: 'status',
    opStr: '>=' as WhereFilterOp,
    value: 5
  };

  requestsWhere = this.openRequestsWhere;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider
  ) {
  }

  scrollToTop() {
    if (this.navCtrl.getActive().name == 'MyRequestsPage') {
      this.content.scrollToTop();
    }
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(userId => {
        this.localUserId = userId;
        this.reLoadActiveSegment();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }

  reLoadActiveSegment() {
    switch (this.segment) {
      case "Abertas":
        this.requestsWhere = this.openRequestsWhere;
        this.loadRequests();
        break;

      case "Pausadas":
        this.requestsWhere = this.pausedRequestsWhere;
        this.loadRequests();
        break;

      case "Outras":
        this.requestsWhere = this.otherRequestsWhere;
        this.loadRequests();
        break;
    }
  }

  loadRequests() {
    this.isExecutingQuery = true;
    console.log(`Executando loadRequest query: ${JSON.stringify(this.requestsWhere)}`);
    this.showSplash = true;
    this.localRequests = [];
    this.lastRequestDoc = null;
    if (this.requestsWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
        .where('clientId', '==', this.localUserId)
        .orderBy(this.requestsWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.requestsWhere.fieldPath, this.requestsWhere.opStr, this.requestsWhere.value)
        .limit(this.requestQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let requestDoc of querySnapshot.docs) {
                this.localRequests.push(requestDoc.data() as Request);
              }
              if (querySnapshot.empty) {
                this.lastRequestDoc = null;
              } else {
                this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
        .where('clientId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.requestsWhere.fieldPath, this.requestsWhere.opStr, this.requestsWhere.value)
        .limit(this.requestQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let requestDoc of querySnapshot.docs) {
                this.localRequests.push(requestDoc.data() as Request);
              }
              if (querySnapshot.empty) {
                this.lastRequestDoc = null;
              } else {
                this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              this.scrollToTop();
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert("Erro ao ler solicitações abertas", e);
        });
    }
  }

  doRefresh(refresher) {
    this.isExecutingQuery = true;
    console.log(`Executando loadRequest query: ${JSON.stringify(this.requestsWhere)}`);
    this.lastRequestDoc = null;
    this.localRequests = [];
    if (this.requestsWhere.opStr !== '==') {
      this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
        .where('clientId', '==', this.localUserId)
        .orderBy(this.requestsWhere.fieldPath, 'asc')
        .orderBy('createdOn', 'desc')
        .where(this.requestsWhere.fieldPath, this.requestsWhere.opStr, this.requestsWhere.value)
        .limit(this.requestQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localRequests.push(doc.data() as Request);
              }
              if (querySnapshot.empty) {
                this.lastRequestDoc = null;
              } else {
                this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              refresher.complete();
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
        .where('clientId', '==', this.localUserId)
        .orderBy('createdOn', 'desc')
        .where(this.requestsWhere.fieldPath, this.requestsWhere.opStr, this.requestsWhere.value)
        .limit(this.requestQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localRequests.push(doc.data() as Request);
              }
              if (querySnapshot.empty) {
                this.lastRequestDoc = null;
              } else {
                this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.isExecutingQuery = false;
              console.log(`query Executed`);
              refresher.complete();
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doInfinite(infiniteScroll) {
    if (this.lastRequestDoc != null) {
      this.isExecutingQuery = true;
      console.log(`Executando loadRequest query: ${JSON.stringify(this.requestsWhere)}`);
      if (this.requestsWhere.opStr !== '==') {
        this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
          .where('clientId', '==', this.localUserId)
          .orderBy(this.requestsWhere.fieldPath, 'asc')
          .orderBy('createdOn', 'desc')
          .where(this.requestsWhere.fieldPath, this.requestsWhere.opStr, this.requestsWhere.value)
          .startAfter(this.lastRequestDoc)
          .limit(this.requestQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localRequests.push(doc.data() as Request);
                }
                if (querySnapshot.empty) {
                  this.lastRequestDoc = null;
                } else {
                  this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.REQUESTS_COLLECTION).ref
          .where('clientId', '==', this.localUserId)
          .orderBy('createdOn', 'desc')
          .where(this.requestsWhere.fieldPath, this.requestsWhere.opStr, this.requestsWhere.value)
          .startAfter(this.lastRequestDoc)
          .limit(this.requestQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localRequests.push(doc.data() as Request);
                }
                if (querySnapshot.empty) {
                  this.lastRequestDoc = null;
                } else {
                  this.lastRequestDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.isExecutingQuery = false;
                console.log(`query Executed`);
                infiniteScroll.complete();
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      console.log(`no need to execute doInfinite query for: ${JSON.stringify(this.requestsWhere)}`);
      infiniteScroll.complete();
    }
  }

  cardTapped(request) {
    this.navCtrl.push('MyRequestDetailPage', {
      request: request,
    });
  }

  createRequestPage() {
    this.navCtrl.push('CreateRequestPage');
  }

  getColor(segment) {
    switch(segment) {
      case 'Abertas':
        return '#004c90';
      case 'Pausadas':
        return '#e9a032';
      case 'Atrasado':
        return '#e9a032';
      case 'Cancelada':
        return '#ce5854'
      case 'Finalizado':
        return '#2dcc6f'
    }
  }

}
