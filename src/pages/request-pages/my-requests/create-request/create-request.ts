import { Component, ViewChild } from "@angular/core";
import { Platform, IonicPage, Content, NavController, NavParams } from "ionic-angular";

import { UiFeedBackProvider } from "../../../../providers/UifeedBack/UifeedBack";
import { UtilProvider } from "../../../../providers/Util";
import { PicProvider } from "../../../../providers/Pic";
import { DynamicLinksApiProvider } from "../../../../providers/DynamicLinksApi";

import { UserDataProvider } from "../../../../providers/UserData";

import { Category } from "../../../../models/Category";
import { ServiceType } from "../../../../models/ServiceType";
import { Characteristic } from "../../../../models/Characteristic";
import { Address } from "../../../../models/Address";
import { Request } from "../../../../models/Request";
import { Pic } from "../../../../models/Pic";
import { Schedule } from "../../../../models/Schedule";
import { LocalDbProvider } from "../../../../providers/LocalDbProvider";
import { EcoFirestore } from "../../../../providers/EcoFirestore/EcoFirestore";
import { User } from "../../../../models/User";

@IonicPage()
@Component({
  selector: "page-create-request",
  templateUrl: "create-request.html"
})
export class CreateRequestPage {
  @ViewChild(Content) content: Content;

  localUser: User = new User();

  localRequest = new Request();
  tempPics: Pic[] = [];
  uploadedPicsNum = 0;
  maxNumOfPics = 3; //parametro

  localCategories: Category[] = [];
  selectedCategory: Category = null;
  localServiceTypes: ServiceType[] = [];
  serviceTypesToSelection: ServiceType[] = [];
  selectedServiceType: ServiceType = null;

  canSelectServiceType = false;

  descriptionCaracCountMin = 0;

  segment: string = "Dados";

  selectedScheduleType: string = "";

  localSchedule: Schedule = new Schedule();

  localDate = "";
  localDateValid = false;
  localHourValues: string[] = [];
  localTime = "";
  scheduleReady = false;
  minDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMinDate();
  maxDate = this.utilCtrl.ionDateTimePandH.generateISODateStringToMaxDate(60);

  addresses: Address[] = [];
  lastRadons: string[] = [];
  localCharacteristics: Characteristic[] = [];
  localCharacteristicsReady = false;
  localCharacteristicsModel: Characteristic[] = [];
  uploadedCaracNum = 0;

  canLeaveCtrl = true;
  showSplash = true;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: "assets/animations/servishero_loading.json"
  };

  constructor(
    public utilCtrl: UtilProvider,
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private dynamicLinksApi: DynamicLinksApiProvider,
    private PicCtrl: PicProvider,
    private localDbCtrl: LocalDbProvider,
  ) { }

  scrollToTop() {
    if (this.navCtrl.getActive().name == "CreateRequestPge") {
      this.content.scrollToTop();
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll) this.content.scrollToBottom();
    }, 10);
  }

  loadServiceTypesAndCategories() {
    if (this.platform.is("cordova")) {
      console.log("CreateRequestPage says: É cordova");
      this.localDbCtrl
        .getServiceTypesAndCategories()
        .then(data => {
          console.log(
            "CreateRequestPage says: ServiceTypes And Categories Data received from local DB"
          );
          this.localCategories = data.categories;
          this.localServiceTypes = data.serviceTypes;
          this.showSplash = false;
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert(`Error`, e);
        });
    } else {
      console.log("CreateRequestPage says: Não É cordova");
      this.db.categories
        .getAll()
        .then(CategoriesData => {
          console.log(
            "CreateRequestPage says: categories Data received from Firebase"
          );
          this.localCategories = CategoriesData;
          this.db.serviceTypes.getAll().then(ServicesTypesData => {
            this.localServiceTypes = ServicesTypesData;
            this.showSplash = false;
          });
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert(`Error`, e);
        });
    }
  }

  ionViewDidEnter() {
    this.UserDataCtrl.getUser()
      .then(UserDoc => {
        this.localUser = UserDoc;

        if (this.UserDataCtrl.localUser.status == "active") {
          this.db.addresses
            .getUserAddresses(this.localUser.id)
            .then(addresses => {
              if (addresses.length == 0) {
                const alert = this.UiFeedBackCtrl.alertCtrl.create({
                  title: "Não possuimos nenhum endereço seu !",
                  subTitle:
                    "Você precisa cadastrar um endereço para poder criar uma solicitação. Deseja cadastrar agora?",
                  buttons: [
                    {
                      text: "Sim",
                      handler: () => {
                        this.navCtrl.push("AddAddressPage");
                      }
                    },
                    "Cancelar"
                  ]
                });
                this.navCtrl.pop();
                this.showSplash = false;
                this.UiFeedBackCtrl.hapticCtrl.notification("error");
                alert.present();
              } else {
                this.localRequest.id = this.db.createId();
                this.localRequest.addressId = this.UserDataCtrl.localUser.homeAdressId;

                this.addresses = addresses;

                this.loadServiceTypesAndCategories();
              }
            });
        } else {
          this.navCtrl.pop();
          this.UiFeedBackCtrl.presentAlert(
            "Seu usuário não esta ativo!",
            "Favor entrar em contato com o nosso suporte para mais esclarecimentos!",
            "error",
            true
          );
        }
      })
      .catch(e => {
        this.navCtrl.pop();
        this.UiFeedBackCtrl.presentErrorAlert("erro", e);
      });
  }

  ionViewCanLeave(): boolean {
    if (!this.canLeaveCtrl) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: "Tem certeza?",
        subTitle:
          "Você quer descartar todos os dados e cancelar a criação da sua solicitação?",
        buttons: [
          {
            text: "SIM",
            handler: () => {
              this.canLeaveCtrl = true;
              this.navCtrl.pop();
            }
          },
          "Cancelar"
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification("warning");
      alert.present();
    } else {
      return true;
    }
    return false;
  }

  selectCategory() {
    if (this.selectedCategory != null) {
      this.canLeaveCtrl = false;
      this.selectedServiceType = null;
      this.serviceTypesToSelection = [];
      this.localCharacteristicsReady = false;

      let tempSelectServices = this.localServiceTypes.filter(servicesType => {
        return (
          this.selectedCategory.serviceTypesIds.indexOf(servicesType.id) != -1
        );
      });

      this.serviceTypesToSelection = tempSelectServices;
      this.canSelectServiceType = true;

      this.scrollToBottom();
    }
  }

  selectServiceType() {
    if (this.selectedServiceType != null) {
      this.localCharacteristicsReady = false;

      this.localRequest.serviceTypeId = this.selectedServiceType.id;
      this.localRequest.serviceTypeTitle = this.selectedServiceType.title;
      this.localRequest.serviceTypeSymbol = this.selectedServiceType.symbol;

      // Importanta!!!!
      this.localRequest.acceptHomeDelivery = this.selectedServiceType.acceptHomeDelivery;
      this.localRequest.requiredHomeDelivery = this.selectedServiceType.requiredHomeDelivery;

      this.localSchedule.homeDelivery = this.localRequest.acceptHomeDelivery;

      this.scrollToBottom();
    }
  }

  randomStr() {
    var s = "";
    var r = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    do {
      for (var i = 0; i < 5; i++) {
        s += r.charAt(Math.floor(Math.random() * r.length));
      }
    } while (this.lastRadons.indexOf(s, 0) != -1);

    this.lastRadons.push(s);
    return s;
  }

  descriptionOnInput() {
    this.descriptionCaracCountMin = 20 - this.localRequest.description.length;
  }

  goForward() {
    switch (this.segment) {
      case "Dados":
        this.segment = "Serviço";
        if (!this.localCharacteristicsReady) {
          this.loadServiceCaracTab();
        }
        break;

      case "Serviço":
        this.segment = "Endereco";
        break;

      case "Endereco":
        this.segment = "Agendamento";
        break;

      case "Agendamento":
        this.segment = "Fotos";
        break;
    }
  }

  goBack() {
    switch (this.segment) {
      case "Serviço":
        this.segment = "Dados";
        break;

      case "Endereco":
        this.segment = "Serviço";
        break;

      case "Agendamento":
        this.segment = "Endereco";
        break;

      case "Fotos":
        this.segment = "Agendamento";
        break;
    }
  }

  loadServiceCaracTab() {
    this.showSplash = true;
    this.localCharacteristicsModel = [];
    this.localCharacteristics = [];
    this.localCharacteristicsReady = false;

    this.db.serviceTypes
      .getCharacteristics(this.selectedServiceType.id)
      .then(characteristics => {
        this.localCharacteristicsModel = characteristics;

        let tempCarac = new Characteristic();

        tempCarac.id = this.db.createId();
        tempCarac.title = this.localCharacteristicsModel[0].title;
        tempCarac.bcTitle = this.localCharacteristicsModel[0].bcTitle;
        tempCarac.orderNumber = this.localCharacteristicsModel[0].orderNumber;
        tempCarac.IsDependent = this.localCharacteristicsModel[0].IsDependent;
        tempCarac.rcDataSelection = this.localCharacteristicsModel[0].rcDataSelection;
        tempCarac.dependencyCON = this.localCharacteristicsModel[0].dependencyCON;
        tempCarac.dependencyD = this.localCharacteristicsModel[0].dependencyD;

        tempCarac.data = [];

        this.localCharacteristics.push(tempCarac);
        this.scrollToTop();
        this.showSplash = false;
      });
  }

  checkDate() {
    this.localDateValid = true;
    this.scrollToBottom();
  }

  checkTime() {
    if (this.localTime != "") {
      const currentTime = new Date().getTime();
      let tempDuedate = new Date(
        `${this.localDate} ${
        this.localTime
        }:00 GMT-0300 (Hora oficial do Brasil)`
      );
      if (tempDuedate.getTime() < currentTime) {
        this.scheduleReady = false;
        this.localTime = "";
        this.UiFeedBackCtrl.presentAlert(
          "O horario selecionado já passou!",
          "Favor selecionar um hórario valido!",
          "error",
          true
        );
      } else {
        this.scheduleReady = true;
        this.scrollToBottom();
      }
    }
  }

  toggleHomeDelivery() {
    this.localSchedule.homeDelivery = !this.localSchedule.homeDelivery;
    if (this.localSchedule.homeDelivery) {
      this.localSchedule.geoCheckin = true;
      this.UiFeedBackCtrl.presenLottietAlert(
        "Atendimento em domicílio adicionada ao seu agendamento",
        "O prestador terá de se deslocar até o endereço do seu pedido no horario agendado",
        "assets/animations/location.json",
        "warning"
      );
    } else {
      this.UiFeedBackCtrl.presentAlert(
        "Atendimento em domicílio removida do seu agendamento",
        "Isto significa que você irá se deslocar até o prestador ou deverá receber seu serviço por outros meios",
        "warning"
      );
    }
  }

  toggleCheckIn() {
    this.localSchedule.geoCheckin = !this.localSchedule.geoCheckin;
    if (this.localSchedule.geoCheckin) {
      this.UiFeedBackCtrl.presenLottietAlert(
        "Check-in via GPS adicionado ao seu agendamento",
        "O prestador terá de fazer Check-in ao chegar no local marcado",
        "assets/animations/location_map_pin.json",
        "warning"
      );
    } else {
      this.UiFeedBackCtrl.presentAlert(
        "Check-in via GPS removido do seu agendamento",
        "",
        "warning"
      );
    }
  }

  toggleRequiredHomeDelivery() {
    this.localRequest.requiredHomeDelivery = !this.localRequest
      .requiredHomeDelivery;
  }

  addPicFromCamera() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('camera')
      .then(newPic => {
        let tempPic = newPic;
        tempPic.info = this.localRequest.id + "_" + this.randomStr() + ".jpeg";
        this.tempPics.push(tempPic);
        this.UiFeedBackCtrl.dismissLoader();
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não tirou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  addPicFromGallery() {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('galery')
      .then(newPic => {
        let tempPic = newPic;
        tempPic.info = this.localRequest.id + "_" + this.randomStr() + ".jpeg";
        this.tempPics.push(tempPic);
        this.UiFeedBackCtrl.dismissLoader();
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não selecionou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  deletePic(pic: Pic) {
    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: "Tem certeza?",
      subTitle: "",
      buttons: [
        {
          text: "Sim",
          handler: () => {
            let loader = this.UiFeedBackCtrl.loadingCtrl.create({
              content: "Excluindo..."
            });
            loader.present().then(() => {
              loader.setContent("Atualizando dados da solicitação...");
              let tempPics: Pic[] = [];
              this.tempPics.forEach(picObject => {
                if (picObject.info != pic.info) {
                  tempPics.push(picObject);
                }
              });
              this.tempPics = tempPics;
              loader.dismiss();
            });
          }
        },
        "Cancelar"
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification("warning");
    alert.present();
  }

  pics_findIndexByInfo(picInfo: string): number {
    let index = 0;
    let foundIt = false;
    this.tempPics.forEach(picObject => {
      if (picObject.info == picInfo) {
        foundIt = true;
      } else if (!foundIt) {
        index++;
      }
    });
    if (foundIt) {
      return index;
    } else {
      return -1;
    }
  }

  pics_IndexUp(pic: Pic) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: "Alterando..."
    });

    loader.present().then(() => {
      let originalIndex = this.pics_findIndexByInfo(pic.info);
      let tempPic = this.tempPics[originalIndex];
      let tempPics: Pic[] = [];
      this.tempPics.forEach(picObject => {
        if (picObject.info != pic.info) {
          tempPics.push(picObject);
        }
      });
      tempPics.splice(originalIndex - 1, 0, tempPic);
      this.tempPics = tempPics;
      loader.dismiss();
    });
  }

  pics_IndexDown(pic: Pic) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: "Alterando..."
    });

    loader.present().then(() => {
      let originalIndex = this.pics_findIndexByInfo(pic.info);
      let tempPic = this.tempPics[originalIndex];
      let tempPics: Pic[] = [];
      this.tempPics.forEach(picObject => {
        if (picObject.info != pic.info) {
          tempPics.push(picObject);
        }
      });
      tempPics.splice(originalIndex + 1, 0, tempPic);
      this.tempPics = tempPics;
      loader.dismiss();
    });
  }

  Pic_Options(pic: Pic) {
    let buttonsArray = [
      {
        text: "Deletar",
        role: "destructive",
        icon: !this.platform.is("ios") ? "trash" : null,
        handler: () => {
          this.deletePic(pic);
        }
      }
    ];

    if (this.pics_findIndexByInfo(pic.info) != 0) {
      buttonsArray.push({
        text: "Mover para o " + this.pics_findIndexByInfo(pic.info) + "ª lugar",
        role: null,
        icon: !this.platform.is("ios") ? "arrow-dropup" : null,
        handler: () => {
          this.pics_IndexUp(pic);
        }
      });
    }

    if (this.pics_findIndexByInfo(pic.info) != this.tempPics.length - 1) {
      buttonsArray.push({
        text:
          "Mover para o " +
          (this.pics_findIndexByInfo(pic.info) + 2) +
          "ª lugar",
        role: null,
        icon: !this.platform.is("ios") ? "arrow-dropdown" : null,
        handler: () => {
          this.pics_IndexDown(pic);
        }
      });
    }

    buttonsArray.push({
      text: "Cancelar",
      icon: null,
      role: "cancel",
      handler: null
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: "Opções da foto",
      buttons: buttonsArray
    });
    actionSheet.present();
  }

  findIndexByOrderNumber(
    CaracVector: Characteristic[],
    orderNumber: number
  ): number {
    let count = 0;
    let foundIt = false;
    CaracVector.forEach(carac => {
      if (carac.orderNumber == orderNumber) {
        foundIt = true;
      } else if (!foundIt) {
        count++;
      }
    });
    if (foundIt) {
      return count;
    } else {
      return -1;
    }
  }

  answerCharac(orderNumber: number) {
    let characModelIndex = this.findIndexByOrderNumber(
      this.localCharacteristicsModel,
      orderNumber
    );

    let alert = this.UiFeedBackCtrl.alertCtrl.create({
      subTitle: this.localCharacteristicsModel[characModelIndex].title
    });

    this.localCharacteristicsModel[characModelIndex].data.forEach(data => {
      alert.addInput({
        type: this.localCharacteristicsModel[characModelIndex].rcDataSelection,
        label: data,
        value: data
      });
    });

    alert.addButton({
      text: "Cancelar",
      role: "Cancel"
    });

    alert.addButton({
      text: "Ok",
      handler: data => {
        if (
          this.localCharacteristicsModel[characModelIndex].rcDataSelection ==
          "checkbox"
        ) {
          if (data.length > 0) {
            if (this.canLeaveCtrl) this.canLeaveCtrl = false;
            let localCharacIndex = this.findIndexByOrderNumber(
              this.localCharacteristics,
              orderNumber
            );
            this.localCharacteristics[localCharacIndex].data = data;
            if (this.localCharacteristicsModel.length == characModelIndex + 1) {
              this.localCharacteristicsReady = true;
              this.scrollToBottom();
              this.UiFeedBackCtrl.presentToastTop(
                "Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!",
                "success"
              );
            } else {
              this.picCharacModel(characModelIndex + 1, orderNumber);
              this.scrollToBottom();
            }
          } else {
            this.answerCharac(orderNumber);
            this.UiFeedBackCtrl.presentAlert(
              "Erro !",
              "Favor selecionar alguma Opção!",
              "error"
            );
          }
        } else {
          if (data) {
            if (this.canLeaveCtrl) this.canLeaveCtrl = false;
            let localCharacIndex = this.findIndexByOrderNumber(
              this.localCharacteristics,
              orderNumber
            );
            this.localCharacteristics[localCharacIndex].data = data;
            if (this.localCharacteristicsModel.length == characModelIndex + 1) {
              this.localCharacteristicsReady = true;
              this.scrollToBottom();
              this.UiFeedBackCtrl.presentToastTop(
                "Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!",
                "success"
              );
            } else {
              this.picCharacModel(characModelIndex + 1, orderNumber);
              this.scrollToBottom();
            }
          } else {
            this.answerCharac(orderNumber);
            this.UiFeedBackCtrl.presentAlert(
              "Erro !",
              "Favor selecionar alguma Opção!",
              "error"
            );
          }
        }
      }
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    alert.present();
  }

  picCharacModel(characModelIndex: number, orderNumber: number) {
    if (!this.localCharacteristicsModel[characModelIndex].IsDependent) {
      if (this.localCharacteristicsModel.length >= characModelIndex + 1) {
        let tempCarac = new Characteristic();

        tempCarac.id = this.db.createId();
        tempCarac.title = this.localCharacteristicsModel[
          characModelIndex
        ].title;
        tempCarac.bcTitle = this.localCharacteristicsModel[
          characModelIndex
        ].bcTitle;
        tempCarac.orderNumber = this.localCharacteristicsModel[
          characModelIndex
        ].orderNumber;
        tempCarac.IsDependent = this.localCharacteristicsModel[
          characModelIndex
        ].IsDependent;
        tempCarac.rcDataSelection = this.localCharacteristicsModel[
          characModelIndex
        ].rcDataSelection;
        tempCarac.dependencyCON = this.localCharacteristicsModel[
          characModelIndex
        ].dependencyCON;
        tempCarac.dependencyD = this.localCharacteristicsModel[
          characModelIndex
        ].dependencyD;

        tempCarac.data = [];

        let localCharacIndex = this.findIndexByOrderNumber(
          this.localCharacteristics,
          orderNumber
        );

        if (this.localCharacteristics.length == localCharacIndex + 1) {
          this.localCharacteristics[localCharacIndex + 1] = tempCarac;
        } else {
          this.localCharacteristicsReady = false;
          let timesToPop =
            this.localCharacteristics.length - (localCharacIndex + 1);
          for (let i = 0; i < timesToPop; i++) {
            this.localCharacteristics.pop();
          }
          this.localCharacteristics[localCharacIndex + 1] = tempCarac;
        }
      } else {
        this.localCharacteristicsReady = true;
        this.UiFeedBackCtrl.presentToastTop(
          "Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!",
          "success"
        );
      }
    } else {
      if (
        this.localCharacteristics[
          this.localCharacteristicsModel[characModelIndex].dependencyCON - 1
        ].data.indexOf(
          this.localCharacteristicsModel[characModelIndex].dependencyD,
          0
        ) != -1
      ) {
        if (this.localCharacteristicsModel.length >= characModelIndex + 1) {
          let tempCarac = new Characteristic();

          tempCarac.id = this.db.createId();
          tempCarac.title = this.localCharacteristicsModel[
            characModelIndex
          ].title;
          tempCarac.bcTitle = this.localCharacteristicsModel[
            characModelIndex
          ].bcTitle;
          tempCarac.orderNumber = this.localCharacteristicsModel[
            characModelIndex
          ].orderNumber;
          tempCarac.IsDependent = this.localCharacteristicsModel[
            characModelIndex
          ].IsDependent;
          tempCarac.rcDataSelection = this.localCharacteristicsModel[
            characModelIndex
          ].rcDataSelection;
          tempCarac.dependencyCON = this.localCharacteristicsModel[
            characModelIndex
          ].dependencyCON;
          tempCarac.dependencyD = this.localCharacteristicsModel[
            characModelIndex
          ].dependencyD;

          tempCarac.data = [];

          let localCharacIndex = this.findIndexByOrderNumber(
            this.localCharacteristics,
            orderNumber
          );

          if (this.localCharacteristics.length == localCharacIndex + 1) {
            this.localCharacteristics[localCharacIndex + 1] = tempCarac;
          } else {
            this.localCharacteristicsReady = false;
            let timesToPop =
              this.localCharacteristics.length - (localCharacIndex + 1);
            for (let i = 0; i < timesToPop; i++) {
              this.localCharacteristics.pop();
            }
            this.localCharacteristics[localCharacIndex + 1] = tempCarac;
          }
        } else {
          this.localCharacteristicsReady = true;
          this.UiFeedBackCtrl.presentToastTop(
            "Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!",
            "success"
          );
        }
      } else {
        if (characModelIndex + 1 < this.localCharacteristicsModel.length) {
          this.picCharacModel(characModelIndex + 1, orderNumber);
        } else if (
          characModelIndex + 1 ==
          this.localCharacteristicsModel.length
        ) {
          this.localCharacteristicsReady = true;
          this.UiFeedBackCtrl.presentToastTop(
            "Bom trabalho! Caracteriticas do serviço preenchidas com sucesso!",
            "success"
          );
        }
      }
    }
  }

  private uploadPics(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (var i = 0; i < this.tempPics.length; i++) {
        let tempPic = this.tempPics[i];
        tempPic.index = i;
        this.PicCtrl.uploadRequestImg(tempPic.url, tempPic.info)
          .then(url => {
            tempPic.url = url;
            if (tempPic.index == 0) {
              this.tempPics[0].url = tempPic.url;
            }
            this.db.afs
              .collection(this.db.COLLECTIONS.REQUESTS_COLLECTION)
              .doc(this.localRequest.id)
              .collection(this.db.COLLECTIONS.PICS_COLLECTION)
              .add(Object.assign({}, tempPic))
              .then(() => {
                this.uploadedPicsNum++;
                if (this.tempPics.length == this.uploadedPicsNum) {
                  resolve();
                }
              });
          })
          .catch(e => {
            reject({
              log: "erro ao fazer upload de uma imagem",
              error: JSON.stringify(e)
            });
          });
      }
      if (this.tempPics.length == 0) {
        resolve();
      }
    });
  }

  private uploadCaracs(): Promise<any> {
    return new Promise((resolve, reject) => {
      for (let carac of this.localCharacteristics) {
        this.db.afs
          .collection(this.db.COLLECTIONS.REQUESTS_COLLECTION)
          .doc(this.localRequest.id)
          .collection(this.db.COLLECTIONS.CHARACTERISTICS_COLLECTION)
          .doc(carac.id)
          .set(Object.assign({}, carac))
          .then(() => {
            this.uploadedCaracNum++;
            if (this.localCharacteristics.length == this.uploadedCaracNum) {
              resolve();
            }
          })
          .catch(e => {
            reject({
              log: "erro ao fazer upload de uma caracteristica do serviço",
              error: JSON.stringify(e)
            });
          });
      }
    });
  }

  finish() {
    this.UiFeedBackCtrl.presenLoader("Salvando...").then(() => {
      this.localRequest.status = 1;
      this.localRequest.scheduleType = this.selectedScheduleType;
      this.localRequest.clientId = this.UserDataCtrl.localUser.id;
      this.localRequest.clientName = this.UserDataCtrl.localUser.fullName;
      this.localRequest.clientJob = this.UserDataCtrl.localUser.job;
      this.localRequest.clientThumbnailUrl = this.UserDataCtrl.localUser.pic.thumbnailUrl;
      this.localRequest.requiredHomeDelivery = this.selectedServiceType.requiredHomeDelivery;
      this.localRequest.clientThumbnailUrl = this.UserDataCtrl.localUser.pic.thumbnailUrl;
      let tempAddres = this.addresses.find(a => {
        return a.id == this.localRequest.addressId;
      });
      this.localRequest.address = Object.assign({}, tempAddres);

      if (this.tempPics.length > 0) {
        this.localRequest.thumbnail = this.tempPics[0].thumbnailUrl;
      }

      if (this.localRequest.scheduleType == "close") {
        this.localSchedule.id = this.db.createId();
        this.localSchedule.clientId = this.UserDataCtrl.localUser.id;
        this.localSchedule.createdBy = this.UserDataCtrl.localUser.id;
        this.localSchedule.requestId = this.localRequest.id;

        this.localSchedule.dueDate = new Date(
          `${this.localDate} ${
          this.localTime
          }:00 GMT-0300 (Hora oficial do Brasil)`
        );

        this.localRequest.schedule = Object.assign({}, this.localSchedule);
      }

      this.UiFeedBackCtrl.updateLoaderMessage("Salvando Solicitação");
      this.db.requests
        .create(this.localRequest)
        .then(() => {
          this.UiFeedBackCtrl.updateLoaderMessage(
            "Salvando as caracteristicas do Serviço Solicitado"
          );
          this.uploadCaracs().then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage("Salvando Fotos");
            this.uploadPics().then(() => {
              this.UiFeedBackCtrl.updateLoaderMessage(
                "Gerando link para compartilhamento"
              );
              var picUrl = "";
              if (this.tempPics.length > 0) {
                picUrl = this.tempPics[0].url;
              }
              this.dynamicLinksApi
                .createRequestDynamicLink(
                  this.localRequest.id,
                  this.localRequest.serviceTypeTitle,
                  this.localRequest.description,
                  picUrl
                )
                .then(dynamicLink => {
                  this.UiFeedBackCtrl.updateLoaderMessage(
                    "Salvando link para compartilhamento"
                  );
                  this.localRequest.dynamicLink = dynamicLink.shortLink;
                  this.db.requests
                    .update(this.localRequest.id, {
                      dynamicLink: this.localRequest.dynamicLink
                    })
                    .then(() => {
                      this.UiFeedBackCtrl.dismissLoader();
                      this.canLeaveCtrl = true;
                      this.navCtrl.pop();
                      this.UiFeedBackCtrl.presentAlert(
                        "Maravilha :)",
                        "Sua Solicitação foi realizado com sucesso. Agora é só esperar!",
                        "success",
                        true
                      );
                    });
                });
            });
          });
        })
        .catch(e => {
          this.UiFeedBackCtrl.dismissLoader();
          console.log("Erro :( " + JSON.stringify(e));
          this.UiFeedBackCtrl.presentAlert(
            "Erro :(",
            JSON.stringify(e),
            "error"
          );
        });
    });
  }

  isString(val: any) {
    return typeof val === "string";
  }

  stepToNumber() {
    switch (this.segment) {
      case "Dados":
        return 1;
      case "Serviço":
        return 2;
      case "Endereco":
        return 3;
      case "Agendamento":
        return 4;
      case "Fotos":
        return 5;
    }
  }
}
