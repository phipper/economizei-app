import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateRequestPage } from './create-request';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    CreateRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateRequestPage),
    LottieAnimationViewModule
  ],
})
export class CreateRequestPageModule {}
