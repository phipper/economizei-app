import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyRequestsPage } from './my-requests';
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    MyRequestsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyRequestsPage),
    LottieAnimationViewModule
  ],
})
export class MyRequestsPageModule {}
