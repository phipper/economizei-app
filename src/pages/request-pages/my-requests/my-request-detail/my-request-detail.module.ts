import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyRequestDetailPage } from './my-request-detail';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    MyRequestDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(MyRequestDetailPage),
  ],
})
export class MyRequestDetailPageModule {}
