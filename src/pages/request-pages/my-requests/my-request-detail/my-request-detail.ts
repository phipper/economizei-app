import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Slides, ModalController } from 'ionic-angular';

import { UserDataProvider } from '../../../../providers/UserData';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { LocalizationProvider } from '../../../../providers/Localization/Localization';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';
import { Request } from '../../../../models/Request';
import { Proposal } from '../../../../models/Proposal';
import { Pic } from '../../../../models/Pic';
import { UtilProvider } from '../../../../providers/Util';
import { EcoFirestore } from '../../../../providers/EcoFirestore/EcoFirestore';
import { Notification } from '../../../../models/Notification';

@IonicPage()
@Component({
  selector: 'page-my-request-detail',
  templateUrl: 'my-request-detail.html',
})
export class MyRequestDetailPage {
  @ViewChild(Slides) SlidesCtrl: Slides;

  showSplash = true;
  localRequest = new Request();
  localPics: Pic[] = [];
  showPicSplash = true;
  receivedProposalsNumber = '⌛';

  segment = 'Detalhes';

  localProposals: Proposal[] = [];
  limitOfProposals = 10;
  requestSub: any;
  descTrucated = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public modalCtrl: ModalController,
    public LocalizationCtrl: LocalizationProvider,
    public utilCtrl: UtilProvider
  ) {
    this.localRequest = navParams.get("request");
    if(this.localRequest.description.length > 250) {
      this.descTrucated = true;
    }
  }

  toggleTruncateDesc() {
    if(this.localRequest.description.length > 250) {
      this.descTrucated = !this.descTrucated;
    }
  }

  // loadPics() {
  //   this.db.requests.pics.getAll(this.localRequest.id)
  //     .then(pics => {
  //       this.localPics = pics;
  //       this.SlidesCtrl.lockSwipes(this.localPics.length == 1);
  //       this.showPicSplash = false;
  //     })
  //     .catch(e => {
  //       this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar fotos', e);
  //     });
  // }

  loadProposals() {
    this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
      .where('requestId', '==', this.localRequest.id)
      .orderBy('createdOn', 'asc')
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            this.receivedProposalsNumber = querySnapshot.size.toString();
            let tempProposalsVet: Proposal[] = [];
            for (let tempPropDoc of querySnapshot.docs) {
              tempProposalsVet.push(tempPropDoc.data() as Proposal);
            }
            this.localProposals = tempProposalsVet;
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      })
  }


  ionViewDidEnter() {
    this.UserDataCtrl.getUid()
      .then(localUserUid => {
        if (this.localRequest.clientId == localUserUid) {
          // this.loadPics();
          this.loadProposals();
          this.showSplash = false;
        } else {
          this.navCtrl.pop()
            .then(() => {
              this.UiFeedBackCtrl.presentAlert('Acesso NEGADO!', 'Somente o Cliente desta solicitação tem acesso à esta pagina!', 'error');
            });
        }
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar seus dados', e);
      });
  }

  ionViewDidLeave() {
    if (this.requestSub != null) {
      this.requestSub.unsubscribe();
    }
  }

  numberPadding(number: number, size: number): string {
    var s = number.toString();
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }

  share() {
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Compartilhar',
          icon: !this.platform.is('ios') ? 'md-share' : null,
          handler: () => {
            this.socialSharing.share(`Olha essa Soliciatação que achei pra você no economizei, ${this.localRequest.serviceTypeTitle}`, this.localRequest.serviceTypeTitle, null, this.localRequest.dynamicLink);
          }
        },
        {
          text: 'Copiar link',
          icon: !this.platform.is('ios') ? 'link' : null,
          handler: () => {
            this.clipboard.copy(this.localRequest.dynamicLink)
              .then(() => {
                this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', this.localRequest.dynamicLink, 'success');
              });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  proposalDetails(proposal: Proposal) {
    this.navCtrl.push('ProposalDetailPage', {
      request: this.localRequest,
      proposal: proposal
    });
  }

  resetProposalReview(proposal: Proposal) {
    proposal.clientReview = 0;
  }

  likeProposal(proposal: Proposal) {
    this.db.proposals.get(proposal.id)
      .then(prop => {
        if (prop.clientReview != 1) {
          proposal.clientReview = 1;
          this.db.proposals.update(proposal.id, { clientReview: proposal.clientReview })
            .then(() => {
              console.log(`Proposta ${proposal.id}, likeada com sucesso!`);
              let notif: Notification = {
                id: this.db.createId(),
                title: `${this.UserDataCtrl.localUser.firstName} Gostou da sua Proposta!`,
                text: `${this.UserDataCtrl.localUser.firstName} acabou de enviar um like para sua proposta na solicitação para "${this.localRequest.serviceTypeTitle}"`,
                type: 'showRequest',
                action: {
                  requestId: this.localRequest.id,
                },
                senderName: "economizei",
                createdOn: new Date(),
                readed: false
              };
              this.db.users.notifications.create(proposal.providerId, notif)
                .then(() => {
                  console.log(`Notificação envidada com sucesso!`);
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro ao dislikear proposta', e);
                })

            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro ao likear proposta', e);
            })
        } else {
          proposal.clientReview = 1;
          this.UiFeedBackCtrl.presentToast('A proposta ja esta likada!', 'warning');
        }
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao dislikear proposta', e);
      })
  }

  dislikeProposal(proposal: Proposal) {
    this.db.proposals.get(proposal.id)
      .then(prop => {
        if (prop.clientReview != -1) {
          proposal.clientReview = -1;
          this.db.proposals.update(proposal.id, { clientReview: proposal.clientReview })
            .then(() => {
              console.log(`Proposta ${proposal.id}, dislikeada com sucesso!`);

              let notif: Notification = {
                id: this.db.createId(),
                title: `${this.UserDataCtrl.localUser.firstName} Nào Gostou da sua Proposta!`,
                text: `${this.UserDataCtrl.localUser.firstName} acabou de enviar um DesLike para sua proposta na solicitação para "${this.localRequest.serviceTypeTitle}"`,
                type: 'showRequest',
                action: {
                  requestId: this.localRequest.id,
                },
                senderName: "economizei",
                createdOn: new Date(),
                readed: false
              };

              this.db.users.notifications.create(proposal.providerId, notif)
                .then(() => {
                  console.log(`Notificação envidada com sucesso!`);
                })
                .catch(e => {
                  this.UiFeedBackCtrl.presentErrorAlert('Erro ao dislikear proposta', e);
                })

            })
            .catch(e => {
              this.UiFeedBackCtrl.presentErrorAlert('Erro ao dislikear proposta', e);
            })
        } else {
          proposal.clientReview = -1;
          this.UiFeedBackCtrl.presentToast('A proposta ja esta dislikada!', 'warning');
        }
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao dislikear proposta', e);
      })
  }

  pauseConfirmation() {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Tem certeza?',
      subTitle: 'Ao pausar a Solicitação a mesma não poderá receber mais propostas!',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.pauseRequest();
          }
        },
        'Cancelar'
      ]
    });
    alert.present();
  }

  pauseRequest() {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Pausando...',
    });

    loader.present()
      .then(() => {
        this.localRequest.status = 2;
        this.db.requests.update(this.localRequest.id, { status: this.localRequest.status })
          .then(() => {
            loader.dismiss();
            this.navCtrl.pop();
            this.UiFeedBackCtrl.presentToast('Solicitação Pausada!', 'success');
          }).catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao Pausar Solicitação', e);
          });
      });
  }

  resumeRequest() {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Re Abrindo a solicitação...',
    });

    loader.present()
      .then(() => {
        this.localRequest.status = 1;
        this.db.requests.update(this.localRequest.id, { status: this.localRequest.status })
          .then(() => {
            loader.dismiss();
            this.navCtrl.pop();
            this.UiFeedBackCtrl.presentToast('Solicitação Pausada!', 'success');
          }).catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao Pausar Solicitação', e);
          });
      });
  }

  cancelRequest() {
    let alert = this.UiFeedBackCtrl.alertCtrl.create();
    alert.setTitle('Motivo');

    alert.addInput({
      type: 'radio',
      label: 'Indisponibilidade de horário',
      value: 'Indisponibilidade de horário',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Valor das propostas',
      value: 'Valor das propostas'
    });

    alert.addInput({
      type: 'radio',
      label: 'Falta de propostas',
      value: 'Falta de propostas'
    });

    alert.addInput({
      type: 'radio',
      label: 'Resolvi por outro meio',
      value: 'Resolvi por outro meio'
    });

    alert.addInput({
      type: 'radio',
      label: 'Outros',
      value: 'Outros',
      handler: () => {
        this.cancelOrderOtherMotive();
        alert.dismiss();
      }
    });

    alert.addButton({
      text: 'Confimar',
      handler: data => {
        this.cancelRequestStep2(data);
      }
    });
    alert.addButton('Cancelar');
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    alert.present();
  }

  cancelOrderOtherMotive() {
    let prompt = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Outro motivo',
      message: "Digite o Motivo para cancelar o pedido",
      inputs: [
        {
          name: 'motive',
          placeholder: 'Motivo'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.cancelRequestStep2(data.motive)
          }
        }
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('warning');
    prompt.present();
  }

  cancelRequestStep2(reason) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Salvando...',
    });
    loader.present().then(() => {
      this.localRequest.status = 1303;
      this.localRequest.canceledReason = reason;
      this.db.requests.update(this.localRequest.id, { status: this.localRequest.status, canceledReason: reason })
        .then(() => {
          loader.dismiss();
          this.UiFeedBackCtrl.presentToast('Solicitação CANCELADA com sucesso!', 'success');
        }).catch(e => {
          loader.dismiss();
          this.UiFeedBackCtrl.presentAlert("Erro :(", JSON.stringify(e), 'error');
        });
    });
  }


  showServiceDetail() {
    this.navCtrl.push('RequestServiceDetailPage', {
      request: this.localRequest
    });
  }

  showSchedulingDetail() {
    let modal = this.modalCtrl.create('RequestSchedulingDetailPage', {
      schedule: Object.assign({}, this.localRequest.schedule)
    });
    modal.present();
  }

  getDistance(lat: number, long: number): number {
    return this.LocalizationCtrl.getDistanceFromCoordinatesInKm(this.localRequest.address.latitude, this.localRequest.address.longitude, lat, long);
  }

}
