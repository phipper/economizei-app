import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, Content, NavController, NavParams } from 'ionic-angular';

import { UserDataProvider } from '../../../../../providers/UserData';
import { UiFeedBackProvider } from '../../../../../providers/UifeedBack/UifeedBack';

import { Request } from '../../../../../models/Request';
import { Proposal } from '../../../../../models/Proposal';
import { Order } from '../../../../../models/Order';
import { Characteristic } from '../../../../../models/Characteristic';
import { Notification } from '../../../../../models/Notification';
import { EcoFirestore } from '../../../../../providers/EcoFirestore/EcoFirestore';

@IonicPage()
@Component({
  selector: 'page-proposal-detail',
  templateUrl: 'proposal-detail.html',
})
export class ProposalDetailPage {
  @ViewChild(Content) content: Content;

  localRequest: Request = new Request();
  localProposal: Proposal = new Proposal();

  localOrder: Order = new Order();


  localCharacteristics: Characteristic[] = [];
  rejectOtherProposals_count = 0;


  constructor(
    public UserDataCtrl: UserDataProvider,
    public db: EcoFirestore,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams
  ) {
    this.localRequest = navParams.get("request");
    this.localProposal = navParams.get("proposal");
  }

  ionViewDidEnter() {
  }

  convertMiliSecondsToHMmSs(miliseconds: number): string {
    var seconds = Math.abs(miliseconds / 1000);
    var s = Math.trunc(seconds % 60);
    var m = Math.trunc((seconds / 60) % 60);
    var h = Math.trunc((seconds / (60 * 60)) % 24);
    return `${this.numberPadding(h, 2)}:${this.numberPadding(m, 2)}:${this.numberPadding(s, 2)}`;
  }


  numberPadding(number: number, size: number): string {
    var s = number.toString();
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }

  sendProviderNotif(): Promise<any> {
    return new Promise((resolve, reject) => {
      let notif = new Notification();
      notif.id = this.db.createId();
      notif.title = 'Proposta Aceita!';
      notif.text = `A sua proposta para a solicitação de ${this.localRequest.clientName} foi aceita!`;
      notif.type = 'generalAlerts';
      notif.action = {
        type: 'showReceivedOrder',
        orderId: this.localOrder.id,
      };
      notif.senderName = "economizei";
      this.db.users.notifications.create(this.localProposal.providerId, notif)
        .then(() => {
          resolve();
        })
        .catch(e => {
          reject({ log: 'Erro ao enviar notificação para o profissional', error: JSON.stringify(e) });
        });
    });
  }

  updateRequestAndProposal(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.localRequest.status = 1300;
      this.db.requests.update(this.localRequest.id, { status: this.localRequest.status })
        .then(() => {
          this.localProposal.status = 11;
          this.db.proposals.update(this.localProposal.id, { status: this.localProposal.status })
            .then(() => {
              resolve();
            });
        })
        .catch(e => {
          reject({ log: 'Erro ao atualizar dados da solicitação ou proposta', error: JSON.stringify(e) });
        });
    });
  }


  rejectOtherProposals(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.afs.collection(this.db.COLLECTIONS.PROPOSALS_COLLECTION).ref
        .where('requestId', '==', this.localRequest.id)
        .orderBy('createdOn', 'asc')
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {

              let tempProposalsVet: Proposal[] = [];
              for (let tempPropDoc of querySnapshot.docs) {
                tempProposalsVet.push(tempPropDoc.data() as Proposal);
              }
              this.rejectOtherProposals_count = 0;
              for (let proposal of tempProposalsVet) {
                if (proposal.id != this.localProposal.id) {
                  this.db.proposals.update(this.localProposal.id, { status: this.localProposal.status })
                    .then(() => {
                      this.rejectOtherProposals_count++;
                      if (this.rejectOtherProposals_count == tempProposalsVet.length - 1) {
                        resolve();
                      }
                    })
                    .catch(e => {
                      reject({ log: 'Erro ao atualizar dados da proposta', error: JSON.stringify(e) });
                    });
                }
              }
            })
        })
        .catch(e => {
          reject({ log: 'Erro ao rejeitar outras propostas!', error: JSON.stringify(e) });
        })
    });
  }

  readCharacs(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.requests.characteristics.getAll(this.localRequest.id)
        .then(characs => {
          this.localCharacteristics = characs;
          resolve();
        })
        .catch(e => {
          reject({ log: 'Erro ao ler caracteristicas do pedido', error: JSON.stringify(e) });
        });
    });
  }

  uploadCharacs(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.orders.characteristics.add(this.localOrder.id, this.localCharacteristics)
        .then(() => {
          resolve();
        })
        .catch(e => {
          reject({ log: 'erro ao fazer upload de uma caracteristica do serviço', error: JSON.stringify(e) });
        });
    });
  }

  createOrder(): Promise<any> {
    return new Promise((resolve, reject) => {

      this.localOrder.id = this.db.createId();
      this.localOrder.title = this.localRequest.serviceTypeTitle;

      this.localOrder.request = Object.assign({}, this.localRequest);

      this.localOrder.addressId = this.localRequest.addressId;
      this.localOrder.address = Object.assign({}, this.localRequest.address);

      this.localOrder.clientId = this.UserDataCtrl.localUser.id;
      this.localOrder.clientName = this.UserDataCtrl.localUser.fullName;
      this.localOrder.clientThumbnailUrl = this.UserDataCtrl.localUser.pic.thumbnailUrl;

      this.localOrder.providerId = this.localProposal.providerId;
      this.localOrder.providerName = this.localProposal.providerName;
      this.localOrder.providerThumbnailUrl = this.localProposal.providerThumbnailUrl;

      this.localOrder.serviceTypeId = this.localRequest.serviceTypeId;
      this.localOrder.serviceTypeTitle = this.localRequest.serviceTypeTitle;
      this.localOrder.serviceTypeSymbol = this.localRequest.serviceTypeSymbol;

      this.localOrder.acceptHomeDelivery = this.localRequest.acceptHomeDelivery;
      this.localOrder.requiredHomeDelivery = this.localRequest.requiredHomeDelivery;

      let tempBudget = this.localProposal.budget;

      tempBudget.status = 'approved';

      this.localOrder.acceptedBudget = Object.assign({}, tempBudget);

      if (this.localRequest.scheduleType == 'close' && this.localProposal.acceptedSchedule) {
        let tempSchedule = this.localRequest.schedule;
        tempSchedule.providerId = this.localProposal.providerId;
        tempSchedule.orderId = this.localOrder.id;
        tempSchedule.status = 'approved';
        this.localOrder.acceptedSchedule = Object.assign({}, tempSchedule);
      }

      if (this.localOrder.acceptedSchedule != null) {
        this.localOrder.step = 3;
        this.localOrder.status = 4;
      } else {
        this.localOrder.step = 2;
        this.localOrder.status = 2;
      }

      this.db.orders.create(this.localOrder)
        .then(() => {
          this.readCharacs()
            .then(() => {
              this.uploadCharacs()
                .then(() => {
                  this.db.orders.budgets.create(this.localOrder.id, this.localOrder.acceptedBudget.id, this.localOrder.acceptedBudget)
                    .then(() => {

                      if (this.localRequest.scheduleType == 'open' && this.localProposal.schedule == null) {
                        resolve();
                      }

                      if (this.localRequest.scheduleType == 'open' && this.localProposal.schedule != null) {
                        this.db.orders.schedules.create(this.localOrder.id, this.localProposal.schedule.id, this.localProposal.schedule)
                          .then(() => {
                            resolve();
                          })
                          .catch(e => {
                            reject({ log: 'erro ao fazer upload do agendamento proposto', error: JSON.stringify(e) });
                          });
                      }

                      if (this.localRequest.scheduleType == 'close' && this.localProposal.acceptedSchedule) {
                        this.db.orders.schedules.create(this.localOrder.id, this.localOrder.acceptedSchedule.id, this.localOrder.acceptedSchedule)
                          .then(() => {
                            resolve();
                          })
                          .catch(e => {
                            reject({ log: 'erro ao fazer upload do agendamento aceito', error: JSON.stringify(e) });
                          });
                      }

                      if (this.localRequest.scheduleType == 'close' && !this.localProposal.acceptedSchedule) {
                        let tempClientSchedule = this.localRequest.schedule;
                        tempClientSchedule.providerId = this.localProposal.providerId;
                        tempClientSchedule.orderId = this.localOrder.id;
                        tempClientSchedule.rejectReason = 'Rejeitado durante a criação da proposta';
                        tempClientSchedule.status = 'rejected_by_provider';
                        this.db.orders.schedules.create(this.localOrder.id, tempClientSchedule.id, tempClientSchedule)
                          .then(() => {
                            this.db.orders.schedules.create(this.localOrder.id, this.localProposal.schedule.id, this.localProposal.schedule)
                              .then(() => {
                                resolve();
                              })
                          })
                          .catch(e => {
                            reject({ log: 'erro ao fazer upload do agendamento aceito', error: JSON.stringify(e) });
                          });
                      }

                    })
                })
            })
        })
        .catch(e => {
          reject({ log: 'erro ao fazer upload do pedido', error: JSON.stringify(e) });
        });
    });
  }



  continueAcceptProposal() {
    this.UiFeedBackCtrl.presenLoader('Salvando...', 'assets/animations/newspaper.json')
      .then(() => {
        this.UiFeedBackCtrl.updateLoaderMessage('Atualizando propostas...');
        this.updateRequestAndProposal()
          .then(() => {
            this.UiFeedBackCtrl.updateLoaderMessage('Atualizando Solicitação e proposta');
            this.updateRequestAndProposal()
              .then(() => {
                this.UiFeedBackCtrl.updateLoaderMessage('Criando pedido');
                this.createOrder()
                  .then(() => {
                    this.UiFeedBackCtrl.updateLoaderMessage('Avisando o profissional');
                    this.sendProviderNotif()
                      .then(() => {
                        this.navCtrl.push('MyOrderDetailPage', {
                          orderId: this.localOrder.id,
                        })
                          .then(() => {
                            this.UiFeedBackCtrl.dismissLoader();
                            if (this.localOrder.step == 2) {
                              this.UiFeedBackCtrl.presenLottietAlert('Pedido em agendamento!', 'Atenção você ainda deverá combianar um agendamento com o prestador', 'assets/animations/calendar.json', '');
                            }
                            this.UiFeedBackCtrl.presentAlert('Feito!', 'Proposta aceita com sucesso, continue o atendimento atravez do seu novo pedido', 'success', true);
                          })
                      })
                  })
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          })
      });

  }

  acceptProposal() {
    if (this.localRequest.clientId == this.UserDataCtrl.localUser.id) {
      const alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Atenção !',
        subTitle: 'Ao aceitar a proposta você sua solicitação será encerrada e será aberto um pedido para que você possa proseguir com o seu atendimento, deseja continuar?',
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.continueAcceptProposal();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('error');
      alert.present();
    } else {
      this.UiFeedBackCtrl.presentAlert('Computer says NO!', 'Somente o cliente desta solicitação pode efetuar esta atividade', 'error', true);
    }

  }

}