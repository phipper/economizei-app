import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProposalDetailPage } from './proposal-detail';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    ProposalDetailPage,
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(ProposalDetailPage),
  ],
})
export class ProposalDetailPageModule {}
