import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Platform, Slides } from 'ionic-angular';
import { CoreProvider } from '../../providers/CoreProvider';
import { AppConfig } from '../../models/AppConfig';
import { UiFeedBackProvider } from '../../providers/UifeedBack/UifeedBack';
import { LocalizationProvider } from '../../providers/Localization/Localization';

@IonicPage()
@Component({
  selector: 'page-config-assistant',
  templateUrl: 'config-assistant.html',
})
export class ConfigAssistantPage {
  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;

  SwiperLockerServ: any;

  showSplash = true;
  localAppConfig: AppConfig = new AppConfig();

  selectedState = null;
  selectedCity = null;
  canSelectCity = false;
  citiesToSelection: string[] = [];

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/gears.json'
  }

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public coreCtrl: CoreProvider,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private LocalizationCtrl: LocalizationProvider
  ) {
  }

  scrollToTop() {
    this.content.scrollToTop();
  }

  lockSwiperServ() {
    this.SwiperLockerServ = setTimeout(() => {
      if (this.slides) {
        this.slides.lockSwipes(true);
        clearTimeout(this.SwiperLockerServ);
      } else {
        this.lockSwiperServ();
      }
    }, 100)
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  ionViewDidLoad() {
    if (this.coreCtrl.platform.is(`cordova`)) {
      this.coreCtrl.hideStatusBar();
    }
  }

  ionViewWillLeave() {
    if (this.coreCtrl.platform.is(`cordova`)) {
      this.coreCtrl.showStatusBar();
    }
  }

  ionViewDidEnter() {
    if (this.platform.is('cordova')) {
      this.coreCtrl.localDbCtrl.getAppConfig()
        .then(appConfig => {
          this.localAppConfig = appConfig;
          this.showSplash = false;
          this.lockSwiperServ();
        })
        .catch(e => {
          this.showSplash = false;
          this.lockSwiperServ();
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      setTimeout(() => {
        this.showSplash = false;
        this.lockSwiperServ();
      }, 2000)
    }
  }

  next() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  prev() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  selectState() {
    if (this.selectedState != null) {
      this.selectedCity = null
      this.citiesToSelection = this.LocalizationCtrl.getCities(this.selectedState);
      this.canSelectCity = true;
    }
  }

  save() {
    this.UiFeedBackCtrl.presenLoader(`Salvando...`, `assets/animations/gears.json`)
      .then(() => {
        this.localAppConfig.isConfigured = true;
        this.localAppConfig.state = this.selectedState;
        this.localAppConfig.city = this.selectedCity;
        this.coreCtrl.localDbCtrl.updateAppConfig(this.localAppConfig)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert(`Configurações Salvas!`, ``, `success`, true);
            this.coreCtrl.navSetRoot('HomePage');
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      })
  }

}
