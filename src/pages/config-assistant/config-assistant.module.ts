import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigAssistantPage } from './config-assistant';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ConfigAssistantPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfigAssistantPage),
    LottieAnimationViewModule
  ],
})
export class ConfigAssistantPageModule {}
