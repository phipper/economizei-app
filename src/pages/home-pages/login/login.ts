import { Component } from '@angular/core';
import { IonicPage, Platform, NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook'
import firebase from 'firebase';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilProvider } from '../../../providers/Util';

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage {


	public credentialsForm: FormGroup;

	error = "";

	constructor(
		public platform: Platform,
		public fire: AngularFireAuth,
		public afs: AngularFirestore,
		private googlePlus: GooglePlus,
		private fb: Facebook,
		public navCtrl: NavController,
		public UiFeedBackCtrl: UiFeedBackProvider,
		public utilCtrl: UtilProvider,
		private formBuilder: FormBuilder
	) {
		this.credentialsForm = this.formBuilder.group({
			email: [
				'',
				Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.email), Validators.required])
			],
			password: [
				'',
				Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(30)])
			]
		});
	}

	ionViewDidLoad() {
	}

	loginErrorHandler(e) {
		if (e.code === 'auth/invalid-email') {
			this.UiFeedBackCtrl.presentAlert("Erro ", "Email invalido !", 'warning', true);
			this.error = "Email invalido !";
		} else if (e.code === 'auth/user-not-found') {
			this.UiFeedBackCtrl.presentAlert("Erro ", "Não existe nehuma conta com este email !", 'warning', true);
			this.error = "Não existe nehuma conta com este email !";
		}
		else {
			this.UiFeedBackCtrl.presentErrorAlert("Erro ", e);
		}
	}

	loginWithEmail() {
		this.UiFeedBackCtrl.presenLoader('Entrando com Email...')
			.then(() => {
				this.fire.auth.signInWithEmailAndPassword(this.credentialsForm.controls['email'].value, this.credentialsForm.controls['password'].value)
					.then(data => {
						this.UiFeedBackCtrl.dismissLoader();
						this.navCtrl.popToRoot();
						this.UiFeedBackCtrl.presentToast("Login efetuado! Bem vindo de volta", 'success');
					})
					.catch(e => {
						this.UiFeedBackCtrl.dismissLoader();
						this.loginErrorHandler(e);
					});
			});
	}

	loginWithGoogle() {
		this.UiFeedBackCtrl.presenLoader('Entrando com Google...')
			.then(() => {
				if (this.platform.is('cordova')) {
					this.googlePlus.login({
						'webClientId': '668476234421-vlj24b3di0bjf01q9lqend69s3csgtgs.apps.googleusercontent.com',
						'offline': true,
						'scopes': 'profile email'
					})
						.then(googlePlusRes => {
							this.fire.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(googlePlusRes.idToken))
								.then(result => {
									this.UiFeedBackCtrl.dismissLoader();
									this.navCtrl.popToRoot();
								})
						})
						.catch(e => {
							this.UiFeedBackCtrl.dismissLoader();
							this.loginErrorHandler(e);
						});
				} else {
					return this.fire.auth
						.signInWithPopup(new firebase.auth.GoogleAuthProvider())
						.then(res => {
							this.UiFeedBackCtrl.dismissLoader();
							this.navCtrl.popToRoot();
						})
						.catch(e => {
							this.UiFeedBackCtrl.dismissLoader();
							this.loginErrorHandler(e);
						});
				}
			});
	}

	loginWtihFacebook() {

		this.UiFeedBackCtrl.presenLoader('Entrando com Facebook...')
			.then(() => {
				if (this.platform.is('cordova')) {

					this.fb.login(['public_profile', 'email'])
						.then((res: FacebookLoginResponse) => {
							console.log('Logged into Facebook!', res);

							this.fire.auth.signInWithCredential(firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken))
								.then(result => {
									this.UiFeedBackCtrl.dismissLoader();
									this.navCtrl.popToRoot();
								})

						})
						.catch(e => {
							this.UiFeedBackCtrl.dismissLoader();
							this.loginErrorHandler(e);
						});

				}
				else {
					return this.fire.auth
						.signInWithPopup(new firebase.auth.FacebookAuthProvider())
						.then(res => {
							this.UiFeedBackCtrl.dismissLoader();
							this.navCtrl.popToRoot();
						})
						.catch(e => {
							this.UiFeedBackCtrl.dismissLoader();
							this.loginErrorHandler(e);
						});
				}
			});
	}

	login(provider) {
		switch (provider) {

			case 'email':
				this.loginWithEmail();
				break;

			case 'google':
				this.loginWithGoogle();
				break;

			case 'facebook':
				this.loginWtihFacebook();
				break;
		}
	}

	resetPassword(email: string) {
		if (email == '' || email == null) {
			this.UiFeedBackCtrl.presentAlert("Erro", "Favor inserir um email !", 'warning', true);
			this.resetPasswordPopUp();
			return;
		}
		this.UiFeedBackCtrl.presenLoader(`Enviando email para Reset de senha...`)
			.then(() => {
				this.fire.auth.sendPasswordResetEmail(email)
					.then(() => {
						this.UiFeedBackCtrl.dismissLoader();
						this.UiFeedBackCtrl.presentAlert("Reset Efetuado!", "Link de reset de senha enviado para: " + email, 'success', true);
					})
					.catch(e => {
						if (e.code === 'auth/invalid-email') {
							this.UiFeedBackCtrl.dismissLoader();
							this.UiFeedBackCtrl.presentAlert("Erro ", "Email invalido !", 'warning', true);
							this.resetPasswordPopUp();
						} else if (e.code === 'auth/user-not-found') {
							this.UiFeedBackCtrl.dismissLoader();
							this.UiFeedBackCtrl.presentAlert("Erro ", "Não existe nehuma conta com este email !", 'warning', true);
						}
						else {
							this.UiFeedBackCtrl.dismissLoader();
							this.UiFeedBackCtrl.presentErrorAlert("Erro ", e);
						}
					})
			})
	}

	resetPasswordPopUp() {
		let prompt = this.UiFeedBackCtrl.alertCtrl.create({
			title: 'Reiniciar Senha',
			message: "Digite o email da conta que deseja reiniciar a senha:",
			inputs: [
				{
					name: 'email',
					placeholder: 'Email'
				},
			],
			buttons: [
				{
					text: 'Cancelar',
					handler: data => {
					}
				},
				{
					text: 'Confirmar',
					handler: data => {
						this.resetPassword(data.email)
					}
				}
			]
		});
		this.UiFeedBackCtrl.hapticCtrl.notification('warning');
		prompt.present();
	}

	signUpPage() {
		this.navCtrl.push('RegisterPage');
	}
}
