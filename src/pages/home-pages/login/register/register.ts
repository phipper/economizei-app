import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, TextInput, Alert } from 'ionic-angular';

import { CoreProvider } from '../../../../providers/CoreProvider';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilProvider } from '../../../../providers/Util';
import { User } from '../../../../models/User';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';

@IonicPage()
@Component({
	selector: 'page-register',
	templateUrl: 'register.html',
})
export class RegisterPage {
	@ViewChild('name_input') nameInput: TextInput;
	@ViewChild('cpf_input') cpfInput: TextInput;
	@ViewChild('email_input') emailInput: TextInput;
	@ViewChild('password_input') passwordInput: TextInput;
	@ViewChild('password2_input') password2Input: TextInput;

	focusServ = false;
	focusServExecInter = 500; // Intervalo em que o serviço de Setar o focus ira funcionar

	public credentialsForm: FormGroup;
	public passwordForm: FormGroup;

	alertIsActive = false;

	loaderIsActive = false;

	formNumb = 1;
	formValidatorServ = false;
	formValidatorServExecInter = 125; // Intervalo em que o serviço de validar o formulario ira funcionar
	formValid = false;

	name = '';
	cpf = '';
	email = '';
	password = '';
	password2 = '';

	mascaredPassword: string = ``;

	error = "";

	constructor(
		private formBuilder: FormBuilder,
		public coreCtrl: CoreProvider,
		public navCtrl: NavController,
		public navParams: NavParams,
		public utilCtrl: UtilProvider,
		public UiFeedBackCtrl: UiFeedBackProvider
	) {
		this.credentialsForm = this.formBuilder.group({
			name: [
				'',
				Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.name), Validators.required, Validators.minLength(3), Validators.maxLength(90)])
			],
			cpf: [
				'',
				Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.cpf), Validators.required, Validators.minLength(11), Validators.maxLength(14)])
			],
			email: [
				'',
				Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.email), Validators.required])
			]
		});

		this.passwordForm = this.formBuilder.group({
			password: [
				'',
				Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(30)])
			],
			passwordConfirm: [
				'',
				Validators.compose([Validators.required])
			]
		});
	}


	startFormValidatorServ() {
		this.formValidatorServ = true;
		this.formValidatorCore();
	}

	stopFormValidatorServ() {
		this.formValidatorServ = false;
	}

	formValidatorCore() {
		if (this.formValidatorServ) {
			this.validateForm();

			setTimeout(() => {
				this.formValidatorCore();
			}, this.formValidatorServExecInter)
		}
	}


	startFocusServ() {
		this.focusServ = true;
		this.focusServCore();
	}

	stopFocusServ() {
		this.focusServ = false;
	}

	focusServCore() {
		if (!this.loaderIsActive) {

			if (!this.alertIsActive) {

				if (this.focusServ) {
					console.log(`seting focus`)


					switch (this.formNumb) {
						case 1:
							this.nameInput.setFocus()
							break;
						case 2:
							this.cpfInput.setFocus()
							break;
						case 3:
							this.emailInput.setFocus()
							break;
						case 4:
							this.passwordInput.setFocus()
							break;
						case 5:
							this.password2Input.setFocus()
							break;
					}
				}

			} else {
				console.log(`Canot set focus because a alert is active, waiting ${this.focusServExecInter} ms to try again...`)
			}

		} else {
			console.log(`Canot set focus because a loader is active, waiting ${this.focusServExecInter} ms to try again...`)
		}

		setTimeout(() => {
			this.focusServCore();
		}, this.focusServExecInter)
	}


	ionViewDidEnter() {
		this.startFormValidatorServ();
		this.startFocusServ();
	}

	ionViewDidLeave() {
		this.stopFormValidatorServ();
		this.stopFocusServ();
	}

	tryToAdvance() {
		switch (this.formNumb) {
			case 1:
				if (this.name.match(this.utilCtrl.regexValidators.name)) {
					this.next();
				}
				break;
			case 2:
				if (this.cpf.length <= 14) {
					if (this.cpf.match(this.utilCtrl.regexValidators.cpf)) {
						this.next();
					}
				}
				break;
			case 3:
				if (this.email.match(this.utilCtrl.regexValidators.email)) {
					this.next();
				}
				break;
			case 4:
				if (this.password.length >= 8 && this.password.length <= 30) {
					this.next();
				}
				break;
			case 5:
				if (this.password == this.password2) {
					this.next();
				}
				break;
		}
	}

	next() {
		switch (this.formNumb) {
			case 1:
				this.formNumb = this.formNumb + 1;
				break;
			case 2:
				this.checkCpf();
				break;
			case 3:
				this.checkEmail();
				break;
			case 4:
				this.updateMascaredPassword();
				this.formNumb = this.formNumb + 1;
				break;
			case 5:
				this.signUpUser();
				break;
		}
	}

	prev() {
		if (this.formNumb > 1) {
			this.formNumb = this.formNumb - 1;
		}
	}

	updateMascaredPassword() {
		this.mascaredPassword = ``;
		for (var i = 0; i < this.password.length; i++) {
			this.mascaredPassword = `${this.mascaredPassword}•`;
		}
	}

	validateForm() {
		switch (this.formNumb) {
			case 1:
				this.validateNameForm();
				break;
			case 2:
				this.validateCPFForm();
				break;
			case 3:
				this.validateEmailForm();
				break;
			case 4:
				this.validatePasswordForm();
				break;
			case 5:
				this.validatePassword2Form();
				break;
		}
	}

	validateNameForm() {
		// console.log(`this.name: ${this.name}: ${this.name.match(this.utilCtrl.regexValidators.name) ? 'OK' : 'Not OK'}`);

		if (this.name.match(this.utilCtrl.regexValidators.name)) {
			this.formValid = true;
		} else {
			this.formValid = false;
		}

	}

	validateCPFForm() {
		if (this.cpf.length <= 14) {

			// console.log(`this.cpf: ${thisthis.password..cpf}: ${this.cpf.match(this.utilCtrl.regexValidators.cpf) ? 'OK' : 'Not OK'}`);

			if (this.cpf.match(this.utilCtrl.regexValidators.cpf)) {
				this.formValid = true;
			} else {
				this.formValid = false;
			}
		}
	}

	validateEmailForm() {
		// console.log(`this.email: ${this.email}: ${this.email.match(this.utilCtrl.regexValidators.email) ? 'OK' : 'Not OK'}`);

		if (this.email.match(this.utilCtrl.regexValidators.email)) {
			this.formValid = true;
		} else {
			this.formValid = false;
		}

	}

	validatePasswordForm() {
		if (this.password.length >= 8 && this.password.length <= 30) {
			this.formValid = true;
		} else {
			this.formValid = false;
		}
	}

	validatePassword2Form() {
		if (this.checkpasswordConfirm()) {
			this.formValid = true;
		} else {
			this.formValid = false;
		}
	}


	checkpasswordConfirm(): boolean {
		return this.password2 != '' && this.password == this.password2;
	}

	/**
	 * Apresenta o alerta e chaveia as variaveis de controle interno da pagina
	 * @param {Alert} alert Mensagem do alerta
	 */
	private custonAlertPreseter(alert: Alert): Promise<any> {

		alert.onWillDismiss((data, role) => {

			this.alertIsActive = false;

		});

		this.alertIsActive = true;
		return alert.present();
	}

	/**
	 * Apresenta loader e chaveia as variaveis de controle interno da pagina
	 * @param {string} message Mensagem do alerta
	 * @param {string} path `opcional` Caminho do Lottie que deverá ser utilizado ex: `assets/animations/check_animation.json` se não for passado o lottie padrão sera utilizado.
	 */
	private custonPresenLoader(message: string, path?: string): Promise<void> {
		this.loaderIsActive = true;
		return this.coreCtrl.UiFeedBackCtrl.presenLoader(message, path)
	}

	/**
	 * Remove o lottie loader ativo
	 */
	public custonDismissLoader(): Promise<void> {
		this.loaderIsActive = false;
		return this.coreCtrl.UiFeedBackCtrl.dismissLoader()
	}

	checkCpf() {

		this.custonPresenLoader('Verificando CPF...')
			.then(() => {

				console.log(`inserted CPF: ${this.cpf}`);

				this.coreCtrl.db.afs.collection('Users').ref
					.where('cpf', '==', this.cpf)
					.get()
					.then(cpfQuerry => {
						this.coreCtrl.db.validators.validateQuerySnapshot(cpfQuerry)
							.then(cpfQuerry => {
								if (cpfQuerry.empty) {

									this.custonDismissLoader();

									const alert = this.coreCtrl.UiFeedBackCtrl.alertCtrl.create({
										title: 'Confira seu CPF!',
										subTitle: `O CPF inserido ${this.cpf} sera registrado e não poderá ser utilizado para cadastrar outras contas, deseja continuar?`,
										buttons: [
											'Cancelar',
											{
												text: 'Sim',
												handler: () => {
													this.formNumb = this.formNumb + 1;
												}
											}
										]
									});
									this.coreCtrl.UiFeedBackCtrl.hapticCtrl.notification('error');

									this.custonAlertPreseter(alert);

								} else {
									this.custonDismissLoader();
									let tempUser = cpfQuerry.docs[0].data() as User;
									if (tempUser.loginMethod == 'password') {
										const alert = this.coreCtrl.UiFeedBackCtrl.alertCtrl.create({
											title: 'CPF já utilizado!',
											subTitle: `O CPF inserido já esta utilizado pelo usuário de nome ${tempUser.firstName}, e email: ${tempUser.email}! Deseja reperar a senha agora?`,
											buttons: [
												'Cancelar',
												{
													text: 'Sim',
													handler: () => {
														this.resetPasswordPopUp();
													}
												}
											]
										});
										this.coreCtrl.UiFeedBackCtrl.hapticCtrl.notification('error');
										this.custonAlertPreseter(alert);

									} else {
										this.coreCtrl.UiFeedBackCtrl.presentAlert(`CPF já utilizado!`, `O CPF inserido já esta utilizado pelo usuário de nome ${tempUser.firstName}, e email: ${tempUser.email}! Favor entrar via ${tempUser.loginMethod}!`, `warning`, true);
									}
								}
							})
					})
					.catch(e => {
						this.custonDismissLoader();
						this.coreCtrl.UiFeedBackCtrl.presentErrorAlert('Erro', e);
					})
			})
	}

	checkEmail() {
		this.custonPresenLoader('Verificando email...')
			.then(() => {
				console.log(`Inserted cpf: ${this.email}`);
				this.coreCtrl.db.afs.collection('Users').ref
					.where('email', '==', this.email)
					.get()
					.then(emailQuerry => {
						this.coreCtrl.db.validators.validateQuerySnapshot(emailQuerry)
							.then(emailQuerry => {
								if (emailQuerry.empty) {

									this.custonDismissLoader();
									this.formNumb = this.formNumb + 1;
									setTimeout(() => {
										this.passwordInput.setFocus()
									}, 500);

								} else {
									this.custonDismissLoader();
									let tempUser = emailQuerry.docs[0].data() as User;
									if (tempUser.loginMethod == 'password') {
										const alert = this.coreCtrl.UiFeedBackCtrl.alertCtrl.create({
											title: 'Email já utilizado!',
											subTitle: `O Email inserido já esta utilizado pelo usuário de nome ${tempUser.firstName}! Deseja repecuperar a senha agora?`,
											buttons: [
												'Cancelar',
												{
													text: 'Sim',
													handler: () => {
														this.resetPassword(this.email);
													}
												}
											]
										});
										this.coreCtrl.UiFeedBackCtrl.hapticCtrl.notification('error');
										this.custonAlertPreseter(alert);

									} else {
										this.coreCtrl.UiFeedBackCtrl.presentAlert(`Email já utilizado!`, `O CPF inserido já esta utilizado pelo usuário de nome ${tempUser.firstName}! Favor entrar via ${tempUser.loginMethod}!`, `warning`, true);
									}
								}
							})
					})
					.catch(e => {
						this.custonDismissLoader();
						this.coreCtrl.UiFeedBackCtrl.presentErrorAlert('Erro', e);
					})
			})
	}

	resetPasswordPopUp() {
		let prompt = this.UiFeedBackCtrl.alertCtrl.create({
			title: 'Reiniciar Senha',
			message: "Digite o email da conta que deseja reiniciar a senha:",
			inputs: [
				{
					name: 'email',
					placeholder: 'Email'
				},
			],
			buttons: [
				{
					text: 'Cancelar',
					handler: data => {
					}
				},
				{
					text: 'Confirmar',
					handler: data => {
						this.resetPassword(data.email)
					}
				}
			]
		});
		this.UiFeedBackCtrl.hapticCtrl.notification('warning');
		prompt.present();
	}

	resetPassword(email: string) {
		this.custonPresenLoader(`Enviando email para Reset de senha...`)
			.then(() => {
				this.coreCtrl.fire.auth.sendPasswordResetEmail(email)
					.then(() => {
						this.coreCtrl.navSetRoot('HomePage');
						this.custonDismissLoader();
						this.coreCtrl.UiFeedBackCtrl.presentAlert("Reset Efetuado!", "Link de reset de senha enviado para: " + email, 'success', true);
					})
					.catch(e => {
						this.custonDismissLoader();
						if (e.code === 'auth/invalid-email') {
							this.coreCtrl.UiFeedBackCtrl.presentAlert("Erro ", "Email invalido !", 'warning', true);
						} else if (e.code === 'auth/user-not-found') {
							this.coreCtrl.UiFeedBackCtrl.presentAlert("Erro ", "Não existe nehuma conta com este email !", 'warning', true);
						}
						else {
							this.coreCtrl.UiFeedBackCtrl.presentErrorAlert("Erro ", e);
						}
					})
			})
	}

	signUpUserErrorHandler(e) {
		if (e.code === 'auth/invalid-email') {
			this.coreCtrl.UiFeedBackCtrl.presentAlert("Erro ", "Email invalido !", 'warning', true);
			this.error = "Email invalido !";
		} else if (e.code === 'auth/email-already-in-use') {
			this.coreCtrl.UiFeedBackCtrl.presentAlert("Erro Email já em uso", "Já existe uma conta com este email !", 'warning', true);
			this.error = "Já existe uma conta com este email !";
		}
		else {
			this.coreCtrl.UiFeedBackCtrl.presentErrorAlert("Erro ", e);
		}
	}

	signUpUser() {

		this.custonPresenLoader('Salvando...')
			.then(() => {
				this.coreCtrl.fire.auth.createUserWithEmailAndPassword(this.email, this.password)
					.then(() => {
						this.coreCtrl.fire.auth.signInWithEmailAndPassword(this.email, this.password)
							.then(() => {
								var user = this.coreCtrl.fire.auth.currentUser;
								user.updateProfile({ displayName: this.name, photoURL: null })
									.then(() => {
										this.coreCtrl.db.usersRegisters.create({
											id: user.uid,
											userId: user.uid,
											userName: this.name,
											userCpf: this.cpf,
											userEmail: this.email,
											createdOn: new Date()
										})
											.then(() => {
												this.custonDismissLoader();
												this.coreCtrl.UiFeedBackCtrl.presentAlert("Sucesso!", "Complete seu cadastro na próxima pagina!", 'success');
												this.coreCtrl.navSetRoot('RegisterContinuePage');
											})
											.catch(e => {
												this.custonDismissLoader();
												this.coreCtrl.UiFeedBackCtrl.presentErrorAlert("Erro ao criar registro do usuário", e);
											});

									})
									.catch(e => {
										this.custonDismissLoader();
										this.coreCtrl.UiFeedBackCtrl.presentErrorAlert("Erro ao atualizar usuário", e);
									});
							})
							.catch(e => {
								this.custonDismissLoader();
								this.coreCtrl.UiFeedBackCtrl.presentErrorAlert("Erro ao entrar", e);
							});
					})
					.catch(e => {
						this.custonDismissLoader();
						this.signUpUserErrorHandler(e);
					});
			});

	}
}