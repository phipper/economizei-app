import { Component, ViewChild } from '@angular/core';
import { trigger, transition, style, state, animate, keyframes } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Content, ActionSheetButton, Slides, Searchbar } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';
import { Events } from 'ionic-angular';

import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { LocalizationProvider } from '../../../providers/Localization/Localization';

import { Banner } from '../../../models/Banner';
import { HomePageBanner } from '../../../models/HomePageBanner';
import { UserDataProvider } from '../../../providers/UserData';
import { LocalDbProvider } from '../../../providers/LocalDbProvider';
import { OrderByDirection, DocumentSnapshot } from '@firebase/firestore-types';
import { ServiceType } from '../../../models/ServiceType';
import { Category } from '../../../models/Category';
import { AppConfig } from '../../../models/AppConfig';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    trigger('swipe', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(-361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({ transform: 'translateX(0)', offset: 0 }),
        style({ transform: 'translateX(361px)', offset: 0.3 }),
        style({ transform: 'translateX(0)', offset: 1.0 })
      ])))
    ])
  ]
})

export class HomePage {

  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;
  @ViewChild('searchbar_input') searchbar_Input: Searchbar;
  swipeIndex: number = 0;
  state: string = 'x';

  localHomePageBanners: HomePageBanner[] = [];


  lastBannerDoc: DocumentSnapshot = null;
  localBanners: Banner[] = [];
  arrangedBanners: Banner[] = [];
  bannerPointsOrderbyOption = {
    id: 0,
    name: 'Pontos',
    fieldPath: 'points',
    directionStr: 'desc' as OrderByDirection
  };
  bannersOrderBy = this.bannerPointsOrderbyOption;
  bannerQueryLimit = 4;

  showSplash = true;
  showQuickActions = false;
  isRefreshing = false;

  searchBarQuery: string = '';
  showSearchBar = false;

  localAppConfig: AppConfig = new AppConfig();

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }



  constructor(
    public platform: Platform,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public events: Events,
    private localizationCtrl: LocalizationProvider,
    private localDbCtrl: LocalDbProvider
  ) {
  }

  slideMoved() {


    if (this.swipeIndex != 0 && this.slides.getActiveIndex() >= this.slides.getPreviousIndex()) {
      this.state = 'rightSwipe';
      this.swipeIndex -= 361;
      this.slides.setElementStyle("transform", `translate3d(${this.swipeIndex}, 0px, 0px)`);
    }
    else {
      this.state = 'leftSwipe';
      this.swipeIndex += 361;
      this.slides.setElementStyle("transform", `translate3d(${this.swipeIndex}, 0px, 0px)`);
    }
  }

  animationDone() {
    this.state = 'x';
  }

  arrangeBanners(): Promise<void> {
    return new Promise((resolve) => {
      let bannersSize = this.localBanners.length;
      let tempArrangedBanners: Banner[] = [];
      tempArrangedBanners[0] = this.localBanners[0];
      for (var i = 0; i < bannersSize; i++) {
        if (i != 0) {
          if (i % 2 == 0)
            tempArrangedBanners[i / 2] = this.localBanners[i];
          else
            tempArrangedBanners[bannersSize / 2 + Math.floor(i / 2)] = this.localBanners[i];
        }
      }
      this.arrangedBanners = tempArrangedBanners;
      resolve();
    });
  }

  loadBanners() {
    this.lastBannerDoc = null;
    this.showSplash = true;
    this.localBanners = [];
    this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
      .where('status', '==', 11)
      .where('address.state', '==', this.localAppConfig.state)
      // .where('address.city', '==', this.localAppConfig.city)
      .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
      .limit(this.bannerQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              this.localBanners.push(doc.data() as Banner);
            }
            if (querySnapshot.empty) {
              this.lastBannerDoc = null;
              this.showSplash = false;
            } else {
              this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.arrangeBanners()
                .then(() => {
                  this.showSplash = false;
                });
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }

  doBannersRefresh(refresher) {
    this.isRefreshing = true;
    this.lastBannerDoc = null;
    this.localBanners = [];
    this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
      .where('status', '==', 11)
      .where('address.state', '==', this.localAppConfig.state)
      // .where('address.city', '==', this.localAppConfig.city)
      .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
      .limit(this.bannerQueryLimit)
      .get()
      .then(querySnapshot => {
        this.db.validators.validateQuerySnapshot(querySnapshot)
          .then(querySnapshot => {
            for (let doc of querySnapshot.docs) {
              this.localBanners.push(doc.data() as Banner);
            }
            if (querySnapshot.empty) {
              this.lastBannerDoc = null;
              refresher.complete();
              this.isRefreshing = false;
            } else {
              this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              this.arrangeBanners()
                .then(() => {
                  refresher.complete();
                  this.isRefreshing = false;
                });
            }
          })
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
      });
  }

  doBannersInfinite(infiniteScroll) {
    if (this.lastBannerDoc != null) {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('status', '==', 11)
        .where('address.state', '==', this.localAppConfig.state)
        // .where('address.city', '==', this.localAppConfig.city)
        .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
        .startAfter(this.lastBannerDoc)
        .limit(this.bannerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastBannerDoc = null;
                infiniteScroll.complete();
              } else {
                this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.arrangeBanners()
                  .then(() => {
                    infiniteScroll.complete();
                  });
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });

    } else {
      infiniteScroll.complete();
    }
  }

  loadHomePageBanners() {
    this.showSplash = true
    this.localHomePageBanners = [];
    this.db.homePageBanners.getActives()
      .then(homePageBanners => {
        this.localHomePageBanners = homePageBanners;
        this.loadBanners();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar HomePageBanners', e);
      });
  }

  ionViewDidLoad() {
    this.events.publish('homePage:didEnter');
    console.log('HomePage says: publishing homePage:didEnter');

    if (this.platform.is('cordova')) {

      console.log('HomePage says: É cordova');

      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          if (appConfig.isConfigured) {
            this.localAppConfig = appConfig;
            this.UiFeedBackCtrl.presentToast(`Mostrando anuncios para ${this.localAppConfig.city}, ${this.localAppConfig.state}`, '');
            this.localDbCtrl.getHomePageBanners()
              .then(HPB => {
                console.log('HomePage says: HomePageData received from local DB');
                this.localHomePageBanners = HPB;
                this.loadBanners();
              });
          } else {
            this.localizationCtrl.getLastAddress()
              .then(address => {
                this.localAppConfig.state = address.state;
                this.localAppConfig.city = address.city;
                this.UiFeedBackCtrl.presentToast(`Mostrando anuncios para ${this.localAppConfig.city}, ${this.localAppConfig.state}`, '');

                this.localDbCtrl.getHomePageBanners()
                  .then(HPB => {
                    console.log('HomePage says: HomePageData received from local DB');
                    this.localHomePageBanners = HPB;
                    this.loadBanners();
                  });

              })
              .catch(e => {
                this.localAppConfig.state = 'Ceará';
                this.localAppConfig.city = 'Fortaleza';

                this.UiFeedBackCtrl.presentAlert('GPS INDISPONIVEL 😭', `Mostrando anuncios para minha casa: ${this.localAppConfig.city}, ${this.localAppConfig.state}`, '');
                this.localDbCtrl.getHomePageBanners()
                  .then(HPB => {
                    console.log('HomePage says: HomePageBannersData received from local DB');
                    this.localHomePageBanners = HPB;
                    this.loadBanners();
                  });
              });
          }
        })
    } else {
      console.log('HomePage says: não é cordova 😭');
      this.localAppConfig.state = 'Ceará';
      this.localAppConfig.city = 'Fortaleza';
      this.UiFeedBackCtrl.presentToast(`não é cordova 😭, Mostrando anuncios para minha casa: ${this.localAppConfig.city}, ${this.localAppConfig.state}`, 'error');
      this.loadHomePageBanners();
    }
  }

  changeLocalizationConfig() {
    this.toggleQuickActions();
    this.localizationCtrl.requestManualInput()
      .then(data => {
        this.UiFeedBackCtrl.presenLoader('Alterando...', 'assets/animations/toggle_switch.json')
          .then(() => {
            this.localAppConfig.state = data.selectedState;
            this.localAppConfig.city = data.selectedCity;
            this.localDbCtrl.updateAppConfig(this.localAppConfig)
              .then(() => {
                this.UiFeedBackCtrl.dismissLoader();
                this.loadBanners();
              })
              .catch(e => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
              });
          })
      })
      .catch(e => {
        if (e.message !== "Ação cancelada!") {
          this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
        } else {
          this.UiFeedBackCtrl.presentToast("Ação cancelada!", `warning`);
        }
      });
  }

  toggleQuickActions() {
    this.showQuickActions = !this.showQuickActions;
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  actionTapped(page: string) {
    this.showQuickActions = false;
    this.navCtrl.push(page);
  }

  search(event) {
    if (this.searchBarQuery.length > 0) {
      this.navCtrl.push('SearchPage', {
        query: this.searchBarQuery
      });
      this.searchBarQuery = '';
      this.showSearchBar = false;
    }
  }

  onCancel(event) {
    this.searchBarQuery = '';
    this.toggleShowSearchBar();
  }

  toggleShowSearchBar() {
    this.showSearchBar = !this.showSearchBar;
    if (this.showSearchBar) {
      setTimeout(() => {
        this.searchbar_Input.setFocus();
      }, 400);
    }
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  notificationsPage() {
    this.navCtrl.push('NotificationsPage');
  }

  bannerTapped(banner) {
    this.navCtrl.push('BannerDetailPage', {
      banner: banner
    });
  }

  saveBanner(bannerId: string) {
    this.UiFeedBackCtrl.presenLoader('Adiconando aos favoritos...')
      .then(() => {
        this.UserDataCtrl.saveBanner(bannerId)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast('Anuncio adicionado aos favoritos', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  unsaveBanner(bannerId: string) {
    this.UiFeedBackCtrl.presenLoader('Removendo dos favoritos...')
      .then(() => {
        this.UserDataCtrl.unsaveBanner(bannerId)
          .then(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast('Anuncio removido dos favoritos', 'success');
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentAlert('Erro', JSON.stringify(e), 'error');
          });
      });
  }

  bannerPressed(banner: Banner) {

    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share(`Olha esse anuncio que achei pra você no economizei, ${banner.title}`, banner.title, null, banner.dynamicLink);
      }
    });

    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy(banner.dynamicLink).then(() => {
          this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', banner.dynamicLink, 'success');
        });
      }
    });

    if (this.UserDataCtrl.hasUser && !this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Adicionar aos favoritos',
        icon: !this.platform.is('ios') ? 'heart-outline' : null,
        handler: () => {
          this.saveBanner(banner.id);
        }
      });
    }

    if (this.UserDataCtrl.hasUser != null && this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Remover dos favoritos',
        icon: !this.platform.is('ios') ? 'heart' : null,
        handler: () => {
          this.unsaveBanner(banner.id);
        }
      });
    }

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: banner.title,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

  categoryPage() {
    this.navCtrl.push('CategoriesPage', {
      actualState: this.localAppConfig.state,
      actualCity: this.localAppConfig.city
    });
  }

  homePageBannerTapped(homePageBanner: HomePageBanner) {
    switch (homePageBanner.action) {
      case 'openCategory':
        this.UiFeedBackCtrl.presenLoader('Carregando...')
          .then(() => {
            let tempCat = homePageBanner.actionData.category as Category;
            let tempServicesTypes = [];
            let count = 0;
            for (let STid of tempCat.serviceTypesIds) {
              this.db.serviceTypes.get(STid)
                .then(St => {
                  tempServicesTypes.push(St);
                  count++;
                  if (count == tempCat.serviceTypesIds.length) {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.navCtrl.push('SelectCategoryFromHomePage', {
                      selectedCategory: tempCat,
                      servicesTypes: tempServicesTypes,
                      actualState: this.localAppConfig.state,
                      actualCity: this.localAppConfig.city
                    });
                  }
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar Tipos de Serviço', e);
                });
            }
          })
        break;
      case 'openServiceType':
        let tempSt = homePageBanner.actionData.serviceType as ServiceType;
        this.navCtrl.push('SelectServiceTypeFromCategoryPage', {
          selectedServiceType: tempSt,
          actualState: this.localAppConfig.state,
          actualCity: this.localAppConfig.city
        });
        break;
    }
  }

  goToPage(page) {
    this.navCtrl.push(page, {
      localUser: this.UserDataCtrl.localUser
    });
  }

  openCreateRequestPage() {
    if (this.UserDataCtrl.hasUser) {
      this.navCtrl.push('CreateRequestPage');
    } else {
      this.UiFeedBackCtrl.presentToast('Faça Login para poder abrir uma solicitação de Serviço!', 'warning');
      this.navCtrl.push('LoginPage');
    }
  }

}
