import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriesPage } from './categories';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    CategoriesPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriesPage),
    LottieAnimationViewModule
  ],
})
export class CategoriesPageModule {}
