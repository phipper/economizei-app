import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';

import { AngularFirestore } from 'angularfire2/firestore';

import { Category } from '../../../../models/Category';
import { ServiceType } from '../../../../models/ServiceType';
import { UserDataProvider } from '../../../../providers/UserData';
import { LocalDbProvider } from '../../../../providers/LocalDbProvider';

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})

export class CategoriesPage {

  localCategories: Category[] = [];
  localServicesTypes: ServiceType[] = [];
  showSplash = true;

  actualCity = '';
  actualState = '';

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public afs: AngularFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private localDbCtrl: LocalDbProvider
  ) {
    this.actualState = navParams.get("actualState");
    this.actualCity = navParams.get("actualCity");
  }

  loadCategories() {
    this.localCategories = [];
    this.afs.collection('Categories').ref.orderBy('index', 'asc').get()
      .then(CategoriesQuery => {
        for (let CategoriesDoc of CategoriesQuery.docs) {
          this.localCategories.push(CategoriesDoc.data() as Category);
        }
        this.loadServicesTypes();
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar Categorias', e);
      });
  }

  loadServicesTypes() {
    this.localServicesTypes = [];
    this.afs.collection('ServiceTypes').ref.get()
      .then(ServicesTypesQuery => {
        for (let ServicesTypeDoc of ServicesTypesQuery.docs) {
          this.localServicesTypes.push(ServicesTypeDoc.data() as ServiceType);
        }
        this.showSplash = false;
      })
      .catch(e => {
        this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar Tipos de Serviço', e);
      });
  }


  ionViewDidEnter() {
    if (this.platform.is('cordova')) {

      console.log('CategoriesPage says: É cordova');

      this.localDbCtrl.getServiceTypesAndCategories()
        .then(data => {
          console.log('CategoriesPage says: categories and Services received from local DB');

          this.localCategories = data.categories;
          this.localServicesTypes = data.serviceTypes;

          this.showSplash = false;

        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        })

    } else {
      console.log('CategoriesPage says: Não é cordova');

      this.loadCategories();

    }
  }

  categoryTapped(category: Category) {
    this.navCtrl.push('SelectCategoryFromHomePage', {
      selectedCategory: category,
      servicesTypes: this.localServicesTypes,
      actualState: this.actualState,
      actualCity: this.actualCity
    });
  }

}
