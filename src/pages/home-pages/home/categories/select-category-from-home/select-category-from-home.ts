import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Content } from 'ionic-angular';

import { AngularFirestore } from 'angularfire2/firestore';

import { User } from '../../../../../models/User';
import { Category } from '../../../../../models/Category';
import { ServiceType } from '../../../../../models/ServiceType';


@IonicPage()
@Component({
  selector: 'page-select-category-from-home',
  templateUrl: 'select-category-from-home.html',
})
export class SelectCategoryFromHomePage {
  @ViewChild(Content) content: Content;

  localUser = new User();
  showSplash = true;
  localCategory = new Category();
  localServicesTypes: ServiceType[] = [];

  actualCity = '';
  actualState = '';

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.localCategory = navParams.get("selectedCategory");
    this.localServicesTypes = navParams.get("servicesTypes");
    this.actualState = navParams.get("actualState");
    this.actualCity = navParams.get("actualCity");
  }

  ionViewDidEnter() {
    this.showSplash = true;

    let tempSelectServices = this.localServicesTypes.filter((servicesType) => {
      return (this.localCategory.serviceTypesIds.indexOf(servicesType.id) != -1);
    });
    this.localServicesTypes = tempSelectServices;

    this.showSplash = false;
  }

  itemTapped(serviceType: ServiceType) {
    this.navCtrl.push('SelectServiceTypeFromCategoryPage', {
      selectedServiceType: serviceType,
      actualState: this.actualState,
      actualCity: this.actualCity
    });
  }

}
