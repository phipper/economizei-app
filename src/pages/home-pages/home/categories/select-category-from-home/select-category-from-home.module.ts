import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectCategoryFromHomePage } from './select-category-from-home';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    SelectCategoryFromHomePage,
  ],
  imports: [
    IonicPageModule.forChild(SelectCategoryFromHomePage),
    LottieAnimationViewModule
  ],
})
export class SelectCategoryFromHomePageModule {}
