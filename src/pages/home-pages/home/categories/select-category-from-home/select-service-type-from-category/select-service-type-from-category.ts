import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Content, ActionSheetButton } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../../../../providers/UifeedBack/UifeedBack';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { UserDataProvider } from '../../../../../../providers/UserData';

import { Banner } from '../../../../../../models/Banner';
import { ServiceType } from '../../../../../../models/ServiceType';
import { EcoFirestore } from '../../../../../../providers/EcoFirestore/EcoFirestore';
import { OrderByDirection, DocumentSnapshot } from '@firebase/firestore-types';
import { UtilProvider } from '../../../../../../providers/Util';
import { LocalizationProvider } from '../../../../../../providers/Localization/Localization';

@IonicPage()
@Component({
  selector: 'page-select-service-type-from-category',
  templateUrl: 'select-service-type-from-category.html',
})
export class SelectServiceTypeFromCategoryPage {

  @ViewChild(Content) content: Content;

  originalCity = '';
  originalState = '';

  cityToSearch = '';
  stateToSearch = '';

  lastBannerDoc: DocumentSnapshot = null;
  localBanners: Banner[] = [];
  arrangedBanners: Banner[] = [];

  bannerPointsOrderbyOption = {
    id: 0,
    name: 'Pontos',
    fieldPath: 'points',
    directionStr: 'desc' as OrderByDirection
  };
  bannersOrderBy = this.bannerPointsOrderbyOption;
  bannerQueryLimit = 4;

  selectedServiceType = new ServiceType();

  showFilters = false;
  selectedState = '';
  selectedCity = '';
  canSelectCity = true;
  citiesToSelection: string[] = [];
  bannersSelectedOrderBy = this.bannersOrderBy;

  bannerOrderbyFiltersOptions = [
    this.bannerPointsOrderbyOption,
    {
      id: 1,
      name: 'Preço (menor para o Maior)',
      fieldPath: 'price',
      directionStr: 'asc' as OrderByDirection
    },
    {
      id: 2,
      name: 'Preço (Maior para o menor)',
      fieldPath: 'price',
      directionStr: 'desc' as OrderByDirection
    },
    {
      id: 3,
      name: 'Data (mais recentes para os mais antigos)',
      fieldPath: 'createdOn',
      directionStr: 'asc' as OrderByDirection
    },
    {
      id: 4,
      name: 'Data (mais antigos para os mais novos)',
      fieldPath: 'createdOn',
      directionStr: 'desc' as OrderByDirection
    }
  ];

  showSplash = true;
  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider,
    private LocalizationCtrl: LocalizationProvider
  ) {
    this.selectedServiceType = navParams.get('selectedServiceType');
    this.originalState = navParams.get('actualState');
    this.originalCity = navParams.get('actualCity');
    this.stateToSearch = `${this.originalState}`;
    this.cityToSearch = `${this.originalCity}`;
  }

  arrangeBanners(): Promise<void> {
    return new Promise((resolve) => {
      let bannersSize = this.localBanners.length;
      let tempArrangedBanners: Banner[] = [];
      tempArrangedBanners[0] = this.localBanners[0];
      for (var i = 0; i < bannersSize; i++) {
        if (i != 0) {
          if (i % 2 == 0)
            tempArrangedBanners[i / 2] = this.localBanners[i];
          else
            tempArrangedBanners[bannersSize / 2 + Math.floor(i / 2)] = this.localBanners[i];
        }
      }
      this.arrangedBanners = tempArrangedBanners;
      resolve();
    });
  }

  loadBanners() {
    this.lastBannerDoc = null;
    this.showSplash = true;
    this.localBanners = [];
    if (this.cityToSearch != 'ALL') {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('status', '==', 11)
        .where('serviceTypeId', '==', this.selectedServiceType.id)
        .where('address.state', '==', this.stateToSearch)
        .where('address.city', '==', this.cityToSearch)
        .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
        .limit(this.bannerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastBannerDoc = null;
              } else {
                this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.arrangeBanners()
                .then(() => {
                  this.showSplash = false;
                });
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('status', '==', 11)
        .where('serviceTypeId', '==', this.selectedServiceType.id)
        .where('address.state', '==', this.stateToSearch)
        .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
        .limit(this.bannerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                this.localBanners.push(doc.data() as Banner);
              }
              if (querySnapshot.empty) {
                this.lastBannerDoc = null;
              } else {
                this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
              }
              this.arrangeBanners()
                .then(() => {
                  this.showSplash = false;
                });
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doBannersInfinite(infiniteScroll) {
    if (this.lastBannerDoc != null) {
      if (this.cityToSearch != 'ALL') {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('status', '==', 11)
          .where('serviceTypeId', '==', this.selectedServiceType.id)
          .where('address.state', '==', this.stateToSearch)
          .where('address.city', '==', this.cityToSearch)
          .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
          .startAfter(this.lastBannerDoc)
          .limit(this.bannerQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localBanners.push(doc.data() as Banner);
                }
                if (querySnapshot.empty) {
                  this.lastBannerDoc = null;
                } else {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.arrangeBanners()
                  .then(() => {
                    infiniteScroll.complete();
                  });
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('status', '==', 11)
          .where('serviceTypeId', '==', this.selectedServiceType.id)
          .where('address.state', '==', this.stateToSearch)
          .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
          .startAfter(this.lastBannerDoc)
          .limit(this.bannerQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  this.localBanners.push(doc.data() as Banner);
                }
                if (querySnapshot.empty) {
                  this.lastBannerDoc = null;
                } else {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                }
                this.arrangeBanners()
                  .then(() => {
                    infiniteScroll.complete();
                  });
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      infiniteScroll.complete();
    }
  }

  ionViewDidLoad() {
    this.initFiltersParams();
    this.loadBanners();
  }

  toggleFilters() {
    this.showFilters = !this.showFilters;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    if (!this.showFilters) {
      this.initFiltersParams();
    }
  }

  initFiltersParams() {
    this.selectedState = `${this.stateToSearch}`;
    this.selectedCity = `${this.cityToSearch}`;
    this.bannersSelectedOrderBy = this.bannersOrderBy;
    this.citiesToSelection = this.LocalizationCtrl.getCities(this.selectedState);
  }

  cleanFilters() {
    this.showFilters = false;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.stateToSearch = this.originalState;
    this.cityToSearch = this.originalCity;
    this.bannersOrderBy = this.bannerPointsOrderbyOption;
    this.loadBanners();
    this.initFiltersParams();
  }

  canFilter(): boolean {
    if (this.selectedCity == null) {
      return false;
    }
    if (this.selectedCity != this.cityToSearch) {
      return true;
    }
    if (this.selectedState != this.stateToSearch) {
      return true;
    }
    if (this.bannersSelectedOrderBy != this.bannersOrderBy) {
      return true;
    }
    return false;
  }

  filter() {
    this.showFilters = false;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.stateToSearch = `${this.selectedState}`;
    this.cityToSearch = `${this.selectedCity}`;
    this.bannersOrderBy = this.bannersSelectedOrderBy;
    this.loadBanners();
  }

  selectState() {
    if (this.selectedState != null && this.selectedState != this.stateToSearch) {
      this.canSelectCity = false;
      this.selectedCity = null;
      this.citiesToSelection = this.LocalizationCtrl.getCities(this.selectedState);
      this.canSelectCity = true;
    }
  }

  bannerTapped(banner) {
    this.navCtrl.push('BannerDetailPage', {
      banner: banner
    });
  }

  saveBanner(bannerId: string) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Adiconando aos favoritos...',
      dismissOnPageChange: true
    });

    loader.present()
      .then(() => {
        this.UserDataCtrl.saveBanner(bannerId)
          .then(() => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentToast('Anuncio adicionado aos favoritos', 'success');
          }).catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  unsaveBanner(bannerId: string) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Removendo dos favoritos...',
      dismissOnPageChange: true
    });
    loader.present()
      .then(() => {
        this.UserDataCtrl.unsaveBanner(bannerId)
          .then(() => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentToast('Anuncio removido dos favoritos', 'success');
          }).catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentAlert('Erro', JSON.stringify(e), 'error');
          });
      });
  }

  bannerPressed(banner: Banner) {

    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share(`Olha esse anuncio que achei pra você no economizei, ${banner.title}`, banner.title, null, banner.dynamicLink);
      }
    });

    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy(banner.dynamicLink).then(() => {
          this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', banner.dynamicLink, 'success');
        });
      }
    });

    if (this.UserDataCtrl.hasUser && !this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Adicionar aos favoritos',
        icon: !this.platform.is('ios') ? 'heart-outline' : null,
        handler: () => {
          this.saveBanner(banner.id);
        }
      });
    }

    if (this.UserDataCtrl.hasUser != null && this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Remover dos favoritos',
        icon: !this.platform.is('ios') ? 'heart' : null,
        handler: () => {
          this.unsaveBanner(banner.id);
        }
      });
    }

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: banner.title,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

}
