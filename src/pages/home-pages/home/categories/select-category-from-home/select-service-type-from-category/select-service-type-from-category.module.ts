import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectServiceTypeFromCategoryPage } from './select-service-type-from-category';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    SelectServiceTypeFromCategoryPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(SelectServiceTypeFromCategoryPage),
  ],
})
export class SelectServiceTypeFromCategoryPageModule {}
