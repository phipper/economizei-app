import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, Content, ActionSheetButton, Searchbar } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { UserDataProvider } from '../../../providers/UserData';

import { User } from '../../../models/User';
import { Banner } from '../../../models/Banner';

import { LocalDbProvider } from '../../../providers/LocalDbProvider';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { LocalizationProvider } from '../../../providers/Localization/Localization';
import { DocumentSnapshot, OrderByDirection } from '@firebase/firestore-types';
import { UtilProvider } from '../../../providers/Util';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  @ViewChild(Content) content: Content;
  @ViewChild('searchbar_input') searchbar_Input: Searchbar;

  originalCity = '';
  originalState = '';

  oldSearchBarQuery: string = '';
  searchBarQuery: string = '';
  filteredQueryWords: string[] = [];

  showSearchBar = false;
  cityToSearch = '';
  stateToSearch = '';


  lastBannerDoc: DocumentSnapshot = null;
  localBanners: Banner[] = [];
  arrangedBanners: Banner[] = [];

  bannerPointsOrderbyOption = {
    id: 0,
    name: 'Pontos',
    fieldPath: 'points',
    directionStr: 'desc' as OrderByDirection
  };
  bannersOrderBy = this.bannerPointsOrderbyOption;
  bannerQueryLimit = 4;
  bannersIsLoaded = false;

  lastProviderDoc: DocumentSnapshot = null;
  localProviders: User[] = [];
  providerPointsOrderbyOption = {
    id: 0,
    name: 'Pontos',
    fieldPath: 'providerPoints',
    directionStr: 'desc' as OrderByDirection
  };
  providersOrderBy = this.providerPointsOrderbyOption;
  providerQueryLimit = 10;
  providersIsLoaded = false;

  showFilters = false;
  selectedState = '';
  selectedCity = '';
  canSelectCity = true;
  citiesToSelection: string[] = [];
  bannersSelectedOrderBy = this.bannersOrderBy;
  bannerOrderbyFiltersOptions = [
    this.bannerPointsOrderbyOption,
    {
      id: 1,
      name: 'Preço (menor para o Maior)',
      fieldPath: 'price',
      directionStr: 'asc' as OrderByDirection
    },
    {
      id: 2,
      name: 'Preço (Maior para o menor)',
      fieldPath: 'price',
      directionStr: 'desc' as OrderByDirection
    },
    {
      id: 3,
      name: 'Data (mais recentes para os mais antigos)',
      fieldPath: 'createdOn',
      directionStr: 'asc' as OrderByDirection
    },
    {
      id: 4,
      name: 'Data (mais antigos para os mais novos)',
      fieldPath: 'createdOn',
      directionStr: 'desc' as OrderByDirection
    }
  ];


  providersSelectedOrderBy = this.providersOrderBy;
  providersOrderbyFiltersOptions = [
    this.providerPointsOrderbyOption,
    {
      id: 1,
      name: 'Reputação (menor para o Maior)',
      fieldPath: 'providerReputation',
      directionStr: 'asc' as OrderByDirection
    },
    {
      id: 2,
      name: 'Reputação (Maior para o menor)',
      fieldPath: 'providerReputation',
      directionStr: 'desc' as OrderByDirection
    },
    {
      id: 3,
      name: 'Data (mais recentes para os mais antigos)',
      fieldPath: 'registerDate',
      directionStr: 'asc' as OrderByDirection
    },
    {
      id: 4,
      name: 'Data (mais antigos para os mais novos)',
      fieldPath: 'registerDate',
      directionStr: 'desc' as OrderByDirection
    }
  ];

  showSplash = true;
  segment = 'Anúncios';
  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public platform: Platform,
    public db: EcoFirestore,
    public UserDataCtrl: UserDataProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private LocalizationCtrl: LocalizationProvider,
    private socialSharing: SocialSharing,
    private clipboard: Clipboard,
    private UiFeedBackCtrl: UiFeedBackProvider,
    public utilCtrl: UtilProvider,
    private localDbCtrl: LocalDbProvider
  ) {
    let tempQuery = navParams.get('query');
    if (tempQuery !== undefined) {
      this.searchBarQuery = tempQuery;
      this.oldSearchBarQuery = `${this.searchBarQuery}`;
    }
  }

  private filterQueryWords(searchBarQuery: string): Promise<string[]> {
    return new Promise((resolve) => {
      let tempQueryWords = searchBarQuery.split(' ');
      let tempFilteredQueryWords = [];
      for (let queryword of tempQueryWords) {
        let tempWord = queryword.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '');
        if (tempWord.length > 2) {
          tempFilteredQueryWords.push(tempWord);
        }
      }
      resolve(tempFilteredQueryWords);
    })
  }

  private checkProviderMatch(provider: User): boolean {

    let tempFullNameWords = provider.fullName.split(' ');
    let tempJobWords = provider.job.split(' ');
    let tempJobKeyWords = provider.jobKeyWords.split(' ');


    for (let validQueryword of this.filteredQueryWords) {
      for (let word of tempFullNameWords) {
        if (word.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '') == validQueryword) {
          return true;
        }
      }
      for (let word of tempJobWords) {
        if (word.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '') == validQueryword) {
          return true;
        }
      }
      for (let word of tempJobKeyWords) {
        if (word.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '') == validQueryword) {
          return true;
        }
      }
    }
    return false;
  }

  private checkBannerMatch(banner: Banner): boolean {
    let tempTitleWords = banner.title.split(' ');
    let tempDescriptionWords = banner.description.split(' ');
    let tempKeyWords = banner.keyWords.split(' ');

    for (let validQueryword of this.filteredQueryWords) {
      for (let word of tempTitleWords) {
        if (word.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '') == validQueryword) {
          return true;
        }
      }
      for (let word of tempDescriptionWords) {
        if (word.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '') == validQueryword) {
          return true;
        }
      }
      for (let word of tempKeyWords) {
        if (word.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '') == validQueryword) {
          return true;
        }
      }
    }
    return false;
  }

  arrangeBanners(): Promise<void> {
    return new Promise((resolve) => {
      let bannersSize = this.localBanners.length;
      let tempArrangedBanners: Banner[] = [];
      tempArrangedBanners[0] = this.localBanners[0];
      for (var i = 0; i < bannersSize; i++) {
        if (i != 0) {
          if (i % 2 == 0)
            tempArrangedBanners[i / 2] = this.localBanners[i];
          else
            tempArrangedBanners[bannersSize / 2 + Math.floor(i / 2)] = this.localBanners[i];
        }
      }
      this.arrangedBanners = tempArrangedBanners;
      resolve();
    });
  }

  loadBanners() {
    if (this.searchBarQuery.length > 0) {
      this.lastBannerDoc = null;
      this.showSplash = true;
      this.localBanners = [];
      if (this.cityToSearch != 'ALL') {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('status', '==', 11)
          .where('address.state', '==', this.stateToSearch)
          .where('address.city', '==', this.cityToSearch)
          .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
          .limit(this.bannerQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  let tempBanner = doc.data() as Banner;
                  if (this.checkBannerMatch(tempBanner)) {
                    this.localBanners.push(tempBanner);
                  }
                }
                if (querySnapshot.empty) {
                  this.showSplash = false;
                  this.lastBannerDoc = null;
                } else if (this.localBanners.length > this.bannerQueryLimit - 1) {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.arrangeBanners()
                    .then(() => {
                      this.showSplash = false;
                    });
                } else {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreBanners();
                }
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
          .where('status', '==', 11)
          .where('address.state', '==', this.stateToSearch)
          .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
          .limit(this.bannerQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  let tempBanner = doc.data() as Banner;
                  if (this.checkBannerMatch(tempBanner)) {
                    this.localBanners.push(tempBanner);
                  }
                }
                if (querySnapshot.empty) {
                  this.showSplash = false;
                  this.lastBannerDoc = null;
                } else if (this.localBanners.length > this.bannerQueryLimit - 1) {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.arrangeBanners()
                    .then(() => {
                      this.showSplash = false;
                    });
                } else {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreBanners();
                }
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      this.showSplash = false;
    }
  }

  loadMoreBanners() {
    if (this.cityToSearch != 'ALL') {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('status', '==', 11)
        .where('address.state', '==', this.stateToSearch)
        .where('address.city', '==', this.cityToSearch)
        .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
        .startAfter(this.lastBannerDoc)
        .limit(this.bannerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempBanner = doc.data() as Banner;
                if (this.checkBannerMatch(tempBanner)) {
                  this.localBanners.push(tempBanner);
                }
              }
              if (this.localBanners.length > this.bannerQueryLimit - 1) {
                this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.arrangeBanners()
                  .then(() => {
                    this.showSplash = false;
                  });
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreBanners();
                } else {
                  this.arrangeBanners()
                    .then(() => {
                      this.showSplash = false;
                    });
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('status', '==', 11)
        .where('address.state', '==', this.stateToSearch)
        .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
        .startAfter(this.lastBannerDoc)
        .limit(this.bannerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempBanner = doc.data() as Banner;
                if (this.checkBannerMatch(tempBanner)) {
                  this.localBanners.push(tempBanner);
                }
              }
              if (this.localBanners.length > this.bannerQueryLimit - 1) {
                this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.arrangeBanners()
                  .then(() => {
                    this.showSplash = false;
                  });
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreBanners();
                } else {
                  this.arrangeBanners()
                    .then(() => {
                      this.showSplash = false;
                    });
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doBannersInfinite(infiniteScroll) {
    if (this.cityToSearch != 'ALL') {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('status', '==', 11)
        .where('address.state', '==', this.stateToSearch)
        .where('address.city', '==', this.cityToSearch)
        .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
        .startAfter(this.lastBannerDoc)
        .limit(this.bannerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempBanner = doc.data() as Banner;
                if (this.checkBannerMatch(tempBanner)) {
                  this.localBanners.push(tempBanner);
                }
              }
              if (this.localBanners.length > this.bannerQueryLimit - 1) {
                this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.arrangeBanners()
                  .then(() => {
                    infiniteScroll.complete();
                  });
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.doBannersInfinite(infiniteScroll);
                } else {
                  infiniteScroll.complete();
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.BANNERS_COLLECTION).ref
        .where('status', '==', 11)
        .where('address.state', '==', this.stateToSearch)
        .orderBy(this.bannersOrderBy.fieldPath, this.bannersOrderBy.directionStr)
        .startAfter(this.lastBannerDoc)
        .limit(this.bannerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempBanner = doc.data() as Banner;
                if (this.checkBannerMatch(tempBanner)) {
                  this.localBanners.push(tempBanner);
                }
              }
              if (this.localBanners.length > this.bannerQueryLimit - 1) {
                this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.arrangeBanners()
                  .then(() => {
                    infiniteScroll.complete();
                  });
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastBannerDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.doBannersInfinite(infiniteScroll);
                } else {
                  infiniteScroll.complete();
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }


  loadProviders() {
    if (this.searchBarQuery.length > 0) {
      this.lastProviderDoc = null;
      this.showSplash = true;
      this.localProviders = [];
      if (this.cityToSearch != 'ALL') {
        this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).ref
          .where('type', '==', 10)
          .where('status', '==', 'active')
          .where('verifiedProvider', '==', true)
          .where('workState', '==', this.stateToSearch)
          .where('workCity', '==', this.cityToSearch)
          .orderBy(this.providersOrderBy.fieldPath, this.providersOrderBy.directionStr)
          .limit(this.providerQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  let tempProvider = doc.data() as User;
                  if (this.checkProviderMatch(tempProvider)) {
                    this.localProviders.push(tempProvider);
                  }
                }
                if (querySnapshot.empty) {
                  this.showSplash = false;
                  this.lastProviderDoc = null;
                } else if (this.localProviders.length > this.providerQueryLimit - 1) {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.showSplash = false;
                } else {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreProviders();
                }
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      } else {
        this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).ref
          .where('type', '==', 10)
          .where('status', '==', 'active')
          .where('verifiedProvider', '==', true)
          .where('workState', '==', this.stateToSearch)
          .orderBy(this.providersOrderBy.fieldPath, this.providersOrderBy.directionStr)
          .limit(this.providerQueryLimit)
          .get()
          .then(querySnapshot => {
            this.db.validators.validateQuerySnapshot(querySnapshot)
              .then(querySnapshot => {
                for (let doc of querySnapshot.docs) {
                  let tempProvider = doc.data() as User;
                  if (this.checkProviderMatch(tempProvider)) {
                    this.localProviders.push(tempProvider);
                  }
                }
                if (querySnapshot.empty) {
                  this.showSplash = false;
                  this.lastProviderDoc = null;
                } else if (this.localProviders.length > this.providerQueryLimit - 1) {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.showSplash = false;
                } else {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreProviders();
                }
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      }
    } else {
      this.showSplash = false;
    }
  }

  loadMoreProviders() {
    if (this.cityToSearch != 'ALL') {
      this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).ref
        .where('type', '==', 10)
        .where('status', '==', 'active')
        .where('verifiedProvider', '==', true)
        .where('workState', '==', this.stateToSearch)
        .where('workCity', '==', this.cityToSearch)
        .orderBy(this.providersOrderBy.fieldPath, this.providersOrderBy.directionStr)
        .startAfter(this.lastProviderDoc)
        .limit(this.providerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempProvider = doc.data() as User;
                if (this.checkProviderMatch(tempProvider)) {
                  this.localProviders.push(tempProvider);
                }
              }
              if (this.localProviders.length > this.providerQueryLimit - 1) {
                this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.showSplash = false;
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreProviders();
                } else {
                  this.showSplash = false;
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).ref
        .where('type', '==', 10)
        .where('status', '==', 'active')
        .where('verifiedProvider', '==', true)
        .where('workState', '==', this.stateToSearch)
        .orderBy(this.providersOrderBy.fieldPath, this.providersOrderBy.directionStr)
        .startAfter(this.lastProviderDoc)
        .limit(this.providerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempProvider = doc.data() as User;
                if (this.checkProviderMatch(tempProvider)) {
                  this.localProviders.push(tempProvider);
                }
              }
              if (this.localProviders.length > this.providerQueryLimit - 1) {
                this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                this.showSplash = false;
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.loadMoreProviders();
                } else {
                  this.showSplash = false;
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  doProvidersInfinite(infiniteScroll) {
    if (this.cityToSearch != 'ALL') {
      this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).ref
        .where('type', '==', 10)
        .where('status', '==', 'active')
        .where('verifiedProvider', '==', true)
        .where('workState', '==', this.stateToSearch)
        .where('workCity', '==', this.cityToSearch)
        .orderBy(this.providersOrderBy.fieldPath, this.providersOrderBy.directionStr)
        .startAfter(this.lastProviderDoc)
        .limit(this.providerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempProvider = doc.data() as User;
                if (this.checkProviderMatch(tempProvider)) {
                  this.localProviders.push(tempProvider);
                }
              }
              if (this.localProviders.length > this.providerQueryLimit - 1) {
                this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                infiniteScroll.complete();
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.doProvidersInfinite(infiniteScroll);
                } else {
                  infiniteScroll.complete();
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    } else {
      this.db.afs.collection(this.db.COLLECTIONS.USERS_COLLECTION).ref
        .where('type', '==', 10)
        .where('status', '==', 'active')
        .where('verifiedProvider', '==', true)
        .where('workState', '==', this.stateToSearch)
        .orderBy(this.providersOrderBy.fieldPath, this.providersOrderBy.directionStr)
        .startAfter(this.lastProviderDoc)
        .limit(this.providerQueryLimit)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              for (let doc of querySnapshot.docs) {
                let tempProvider = doc.data() as User;
                if (this.checkProviderMatch(tempProvider)) {
                  this.localProviders.push(tempProvider);
                }
              }
              if (this.localProviders.length > this.providerQueryLimit - 1) {
                this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                infiniteScroll.complete();
              } else {
                if (querySnapshot.docs.length > 0) {
                  this.lastProviderDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
                  this.doProvidersInfinite(infiniteScroll);
                } else {
                  infiniteScroll.complete();
                }
              }
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        });
    }
  }

  ionViewDidLoad() {
    if (this.platform.is('cordova')) {
      console.log('SearchPage says: É cordova');
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          if (appConfig.isConfigured) {
            this.originalState = appConfig.state;
            this.originalCity = appConfig.city;
            this.stateToSearch = `${this.originalState}`;
            this.cityToSearch = `${this.originalCity}`;
            this.initFiltersParams();

            this.filterQueryWords(this.searchBarQuery)
              .then(queryWords => {
                if (queryWords.length > 0) {
                  this.filteredQueryWords = queryWords;
                  this.loadBanners();
                } else {
                  this.showSplash = false;
                  this.UiFeedBackCtrl.presentAlert(`Termos de busca invalidos!`, `Favor verificar os termos de inseridos!`, `warning`, true);
                }
              })

          } else {
            this.LocalizationCtrl.getLastAddress()
              .then(address => {
                this.originalState = address.state;
                this.originalCity = address.city;
                this.stateToSearch = `${this.originalState}`;
                this.cityToSearch = `${this.originalCity}`;
                this.initFiltersParams();

                this.filterQueryWords(this.searchBarQuery)
                  .then(queryWords => {
                    if (queryWords.length > 0) {
                      this.filteredQueryWords = queryWords;
                      this.loadBanners();
                    } else {
                      this.showSplash = false;
                      this.UiFeedBackCtrl.presentAlert(`Termos de busca invalidos!`, `Favor verificar os termos de inseridos!`, `warning`, true);
                    }
                  })

              })
              .catch(e => {
                this.originalState = 'Ceará';
                this.originalCity = 'Fortaleza';
                this.stateToSearch = `${this.originalState}`;
                this.cityToSearch = `${this.originalCity}`;
                this.initFiltersParams();
                this.UiFeedBackCtrl.presentAlert('GPS INDISPONIVEL 😭', `Mostrando anuncios para minha casa: ${this.cityToSearch}, ${this.stateToSearch}`, '');


                this.filterQueryWords(this.searchBarQuery)
                  .then(queryWords => {
                    if (queryWords.length > 0) {
                      this.filteredQueryWords = queryWords;
                      this.loadBanners();
                    } else {
                      this.showSplash = false;
                      this.UiFeedBackCtrl.presentAlert(`Termos de busca invalidos!`, `Favor verificar os termos de inseridos!`, `warning`, true);
                    }
                  })

              });
          }
        })
    } else {
      console.log('SearchPage says: não é cordova 😭');
      this.originalState = 'Ceará';
      this.originalCity = 'Fortaleza';
      this.stateToSearch = this.originalState;
      this.cityToSearch = this.originalCity;
      this.initFiltersParams();
      this.UiFeedBackCtrl.presentToast(`não é cordova 😭, Mostrando anuncios para minha casa: ${this.cityToSearch}, ${this.stateToSearch}`, 'error');

      this.filterQueryWords(this.searchBarQuery)
        .then(queryWords => {
          if (queryWords.length > 0) {
            this.filteredQueryWords = queryWords;
            this.loadBanners();
          } else {
            this.showSplash = false;
            this.UiFeedBackCtrl.presentAlert(`Termos de busca invalidos!`, `Favor verificar os termos de inseridos!`, `warning`, true);
          }
        })

    }
  }

  search(event) {
    if (this.searchBarQuery.length > 0 && this.searchBarQuery.toLowerCase() != this.oldSearchBarQuery.toLowerCase()) {
      this.filterQueryWords(this.searchBarQuery)
        .then(queryWords => {
          if (queryWords.length > 0) {
            this.oldSearchBarQuery = this.searchBarQuery;
            this.filteredQueryWords = queryWords;
            if (this.segment == 'Anúncios') {
              this.loadBanners();
            } else {
              this.loadProviders();
            }
          } else {
            this.searchBarQuery = this.oldSearchBarQuery;
            this.UiFeedBackCtrl.presentAlert(`Termos de busca invalidos!`, `Favor verificar os termos de inseridos!`, `warning`, true);
          }
          this.toggleShowSearchBar();
        })
    } else {
      this.searchBarQuery = this.oldSearchBarQuery;
    }
  }

  onCancel(event) {
    this.toggleShowSearchBar();
  }

  toggleShowSearchBar() {
    this.showSearchBar = !this.showSearchBar;
    if (this.showSearchBar) {
      setTimeout(() => {
        this.searchbar_Input.setFocus();
      }, 400);
    } else {
      this.searchBarQuery = this.oldSearchBarQuery;
    }
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  toggleFilters() {
    this.showFilters = !this.showFilters;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    if (!this.showFilters) {
      this.initFiltersParams();
    }
  }

  initFiltersParams() {
    this.selectedState = `${this.stateToSearch}`;
    this.selectedCity = `${this.cityToSearch}`;
    this.bannersSelectedOrderBy = this.bannersOrderBy;
    this.providersSelectedOrderBy = this.providersOrderBy;
    this.citiesToSelection = this.LocalizationCtrl.getCities(this.selectedState);
  }

  cleanFilters() {
    this.showFilters = false;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.stateToSearch = this.originalState;
    this.cityToSearch = this.originalCity;
    if (this.segment == 'Anúncios') {
      this.bannersOrderBy = this.bannerPointsOrderbyOption;
      this.loadBanners();
    } else {
      this.providersOrderBy = this.providerPointsOrderbyOption;
      this.loadProviders();
    }
    this.initFiltersParams();
  }

  canFilter(): boolean {
    if (this.selectedCity == null) {
      return false;
    }
    if (this.selectedCity != this.cityToSearch) {
      return true;
    }
    if (this.selectedState != this.stateToSearch) {
      return true;
    }
    if (this.segment == 'Anúncios') {
      if (this.bannersSelectedOrderBy != this.bannersOrderBy) {
        return true;
      }
    } else {
      if (this.providersSelectedOrderBy != this.providersOrderBy) {
        return true;
      }
    }
    return false;
  }

  filter() {
    this.showFilters = false;
    this.UiFeedBackCtrl.hapticCtrl.selection();
    this.stateToSearch = `${this.selectedState}`;
    this.cityToSearch = `${this.selectedCity}`;
    if (this.segment == 'Anúncios') {
      this.bannersOrderBy = this.bannersSelectedOrderBy;
      this.loadBanners();
    } else {
      this.providersOrderBy = this.providersSelectedOrderBy;
      this.loadProviders();
    }
  }

  selectState() {
    if (this.selectedState != null && this.selectedState != this.stateToSearch) {
      this.canSelectCity = false;
      this.selectedCity = null;
      this.citiesToSelection = this.LocalizationCtrl.getCities(this.selectedState);
      this.canSelectCity = true;
    }
  }

  bannerTapped(banner) {
    this.navCtrl.push('BannerDetailPage', {
      banner: banner
    });
  }

  saveBanner(bannerId: string) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Adiconando aos favoritos...',
      dismissOnPageChange: true
    });

    loader.present()
      .then(() => {
        this.UserDataCtrl.saveBanner(bannerId)
          .then(() => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentToast('Anuncio adicionado aos favoritos', 'success');
          })
          .catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      });
  }

  unsaveBanner(bannerId: string) {
    let loader = this.UiFeedBackCtrl.loadingCtrl.create({
      content: 'Removendo dos favoritos...',
      dismissOnPageChange: true
    });
    loader.present()
      .then(() => {
        this.UserDataCtrl.unsaveBanner(bannerId)
          .then(() => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentToast('Anuncio removido dos favoritos', 'success');
          })
          .catch(e => {
            loader.dismiss();
            this.UiFeedBackCtrl.presentAlert('Erro', JSON.stringify(e), 'error');
          });
      });
  }

  bannerPressed(banner: Banner) {

    let buttonsArray: ActionSheetButton[] = [];

    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share(`Olha esse anuncio que achei pra você no economizei, ${banner.title}`, banner.title, null, banner.dynamicLink);
      }
    });

    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy(banner.dynamicLink).then(() => {
          this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', banner.dynamicLink, 'success');
        });
      }
    });

    if (this.UserDataCtrl.hasUser && !this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Adicionar aos favoritos',
        icon: !this.platform.is('ios') ? 'heart-outline' : null,
        handler: () => {
          this.saveBanner(banner.id);
        }
      });
    }

    if (this.UserDataCtrl.hasUser != null && this.UserDataCtrl.isBannerSaved(banner.id)) {
      buttonsArray.push({
        text: 'Remover dos favoritos',
        icon: !this.platform.is('ios') ? 'heart' : null,
        handler: () => {
          this.unsaveBanner(banner.id);
        }
      });
    }

    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });

    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl.create({
      title: banner.title,
      buttons: buttonsArray,
    });
    this.UiFeedBackCtrl.hapticCtrl.selection();
    actionSheet.present();
  }

  showprovider(provider) {
    this.navCtrl.push('ProviderDetailPage', {
      provider: provider
    });
  }

}
