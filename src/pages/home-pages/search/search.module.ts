import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';
import { SearchPage } from './search';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    SearchPage,
  ],
  imports: [
    Ionic2RatingModule,
    LottieAnimationViewModule,
    IonicPageModule.forChild(SearchPage),
  ],
})
export class SearchPageModule {}
