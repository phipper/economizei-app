import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { CoreProvider } from '../../providers/CoreProvider';
import { UiFeedBackProvider } from '../../providers/UifeedBack/UifeedBack';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
  @ViewChild(Slides) slides: Slides;

  showSplash = true;

  tutorialDone = false;
  needConfig = false;

  tutorialPics = [
    'assets/imgs/tutorial/1.png',
    'assets/imgs/tutorial/2.png',
    'assets/imgs/tutorial/3.png',
    'assets/imgs/tutorial/4.png',
    'assets/imgs/tutorial/5.png',
    'assets/imgs/tutorial/6.png',
    'assets/imgs/tutorial/7.png',
  ];

  actualPicIndex = 0;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    private coreCtrl: CoreProvider,
    private UiFeedBackCtrl: UiFeedBackProvider
  ) {
  }

  ionViewDidLoad() {
    if (this.coreCtrl.platform.is(`cordova`)) {
      this.coreCtrl.hideStatusBar();
    }
  }

  ionViewWillLeave() {
    if (this.coreCtrl.platform.is(`cordova`)) {
      this.coreCtrl.showStatusBar();
    }
  }

  ionViewDidEnter() {
    if (this.platform.is('cordova')) {
      this.coreCtrl.getTutorialStatus()
        .then(tutorialStatus => {
          this.tutorialDone = tutorialStatus;
          this.coreCtrl.getConfigStatus()
            .then(configStatus => {
              this.needConfig = !configStatus;
              this.showSplash = false;
            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
        })
    } else {
      setTimeout(() => {
        this.showSplash = false;
      }, 2000)
    }
  }

  next() {
    if (this.actualPicIndex < this.tutorialPics.length - 1) {
      this.slides.lockSwipes(false);
      this.slides.slideNext();
      this.slides.lockSwipes(true);
      this.actualPicIndex++;
    }
  }

  prev() {
    if (this.actualPicIndex > 0) {
      this.slides.lockSwipes(false);
      this.slides.slidePrev();
      this.slides.lockSwipes(true);
      this.actualPicIndex--;
    }
  }

  finishedTutorial() {
    if (!this.tutorialDone) {
      this.coreCtrl.setTutorialDone()
        .then(() => {
          if (this.needConfig) {
            this.coreCtrl.navPush(`ConfigAssistantPage`)
          }
          else {
            this.coreCtrl.navPop();
          }
        })
    } else {
      this.coreCtrl.navPop();
    }
  }

}
