import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorialPage } from './tutorial';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    TutorialPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorialPage),
    LottieAnimationViewModule
  ],
})
export class TutorialPageModule {}
