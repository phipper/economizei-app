import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoInternetPage } from './no-internet';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    NoInternetPage,
  ],
  imports: [
    IonicPageModule.forChild(NoInternetPage),
    LottieAnimationViewModule
  ],
})
export class NoInternetPageModule {}
