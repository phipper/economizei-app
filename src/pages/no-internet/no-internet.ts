import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { CoreProvider } from '../../providers/CoreProvider';

@IonicPage()
@Component({
  selector: 'page-no-internet',
  templateUrl: 'no-internet.html',
})
export class NoInternetPage {

  lottie = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/network_lost.json'
  }

  constructor(
    public navCtrl: NavController,
    private coreCtrl: CoreProvider
  ) {
  }

  retry() {
    this.coreCtrl.navSetRoot('HomePage');
  }

}
