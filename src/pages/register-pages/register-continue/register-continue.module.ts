import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterContinuePage } from './register-continue';
import { LottieAnimationViewModule } from 'ng-lottie';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    RegisterContinuePage,
  ],
  imports: [
    LottieAnimationViewModule,
    BrMaskerModule,
    IonicPageModule.forChild(RegisterContinuePage),
  ],
})
export class RegisterContinuePageModule {}
