import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController, TextInput, Alert, DateTime } from 'ionic-angular';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';

import { AngularFireAuth } from 'angularfire2/auth';
import { Firebase } from '@ionic-native/firebase';

import { User } from '../../../models/User';
import { UserDataProvider } from '../../../providers/UserData';
import { LocalDbProvider } from '../../../providers/LocalDbProvider';
import { ProviderInviteUtilization } from '../../../models/ProviderInviteUtilization';
import { LocalDBInviteData } from '../../../models/LocalDBInviteData';
import { ProviderInvite } from '../../../models/ProviderInvite';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilProvider } from '../../../providers/Util';
import { Subscription } from 'rxjs/Subscription';
import { EcoFirestore } from '../../../providers/EcoFirestore/EcoFirestore';
import { CoreProvider } from '../../../providers/CoreProvider';
import { UserRegister } from '../../../models/UserRegister';

@IonicPage()
@Component({
  selector: 'page-register-continue',
  templateUrl: 'register-continue.html',
})

export class RegisterContinuePage {
  @ViewChild('cpf_input') cpfInput: TextInput;
  @ViewChild('cellPhone_input') cellPhoneInput: TextInput;
  @ViewChild('job_input') jobInput: TextInput;
  @ViewChild('workPhone_input') workPhoneInput: TextInput;

  @ViewChild('birthDate_input') birthDate_input: DateTime;



  focusServ = false;
  focusServExecInter = 500; // Intervalo em que o serviço de Setar o focus ira funcionar

  formNumb = 1;
  formValidatorServ = false;
  formValidatorServExecInter = 125; // Intervalo em que o serviço de validar o formulario ira funcionar
  formValid = false;

  localUser: User = new User();

  birthDateString: string = '';

  showCancelRegisterButton = false;

  hasUserRegister = false;
  localUserRegister: UserRegister = new UserRegister;

  localDBProviderInviteData: LocalDBInviteData = new LocalDBInviteData();
  localProviderInvite: ProviderInvite = new ProviderInvite();


  private authStateSub: Subscription = null;

  public userForm: FormGroup;

  public termsOfUseAccepted = false;

  alertIsActive = false;

  loaderIsActive = false;

  addWorkPhoneNow: boolean = null;

  showSplash = true;
  authStateFired = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/auth.json'
  }

  constructor(
    private fire: AngularFireAuth,
    public db: EcoFirestore,
    public firebaseNative: Firebase,
    private platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public UiFeedBackCtrl: UiFeedBackProvider,
    public menuCtrl: MenuController,
    public UserDataCtrl: UserDataProvider,
    public localDbCtrl: LocalDbProvider,
    private formBuilder: FormBuilder,
    public utilCtrl: UtilProvider,
    public coreCtrl: CoreProvider
  ) {
    this.userForm = this.formBuilder.group({
      cpf: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.cpf), Validators.required, Validators.minLength(11), Validators.maxLength(14)])
      ],
      job: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.text), Validators.required, Validators.minLength(3), Validators.maxLength(50)])
      ],
      workPhone: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.phone), Validators.required, Validators.minLength(10), Validators.maxLength(16)])
      ],
      cellPhone: [
        '',
        Validators.compose([Validators.pattern(this.utilCtrl.regexValidators.phone), Validators.required, Validators.minLength(10), Validators.maxLength(16)])
      ]
    });

    //Não descomentar para testes only

    // this.localDBProviderInviteData = {
    //   hasProviderInvite: true,
    //   inviteId: 'vWPDF79G29pYG1ndg6kF'
    // };

    // this.localProviderInvite = {
    //   createdOn: new Date(),
    //   dynamicLink: 'https://economizei.page.link/yRStsMKyNCEW3AKi9',
    //   id: 'vWPDF79G29pYG1ndg6kF',
    //   providerId: 'rOwLdPa3aRfFMCHvl7xqkcG5GUh1',
    //   providerName: 'Mateus Lopes',
    //   providerThumbnailUrl: 'https://lh5.googleusercontent.com/-XYMvQrZ1E1M/AAAAAAAAAAI/AAAAAAAAAuM/-gMLqpWQND4/photo.jpg',
    //   rewardUsed: false
    // };

    //
  }

  startFormValidatorServ() {
    this.formValidatorServ = true;
    this.formValidatorCore();
  }

  stopFormValidatorServ() {
    this.formValidatorServ = false;
  }

  formValidatorCore() {
    if (this.formValidatorServ) {
      this.validateForm();

      setTimeout(() => {
        this.formValidatorCore();
      }, this.formValidatorServExecInter)
    }
  }


  startFocusServ() {
    this.focusServ = true;
    this.focusServCore();
  }

  stopFocusServ() {
    this.focusServ = false;
  }

  focusServCore() {

    if (!this.loaderIsActive) {

      if (!this.alertIsActive) {

        if (this.focusServ) {
          console.log(`seting focus`)

          switch (this.formNumb) {
            case 2:
              if (!this.hasUserRegister) {
                this.cpfInput.setFocus()
              }
              break;

            case 4:
              if (this.birthDateString == ``) {

                this.birthDate_input.open();

              }
              break;

            case 5:
              this.cellPhoneInput.setFocus()
              break;
            case 7:
              this.jobInput.setFocus()
              break;
            case 8:
              if (this.addWorkPhoneNow) {
                this.workPhoneInput.setFocus()
              }
              break;
          }
        }

      } else {
        console.log(`Canot set focus because a alert is active, waiting ${this.focusServExecInter} ms to try again...`)
      }

    } else {
      console.log(`Canot set focus because a loader is active, waiting ${this.focusServExecInter} ms to try again...`)
    }

    setTimeout(() => {
      this.focusServCore();
    }, this.focusServExecInter)
  }


  tryToAdvance() {
    switch (this.formNumb) {
      case 2:
        if (this.localUser.cpf.length <= 14) {
          if (this.localUser.cpf.match(this.utilCtrl.regexValidators.cpf)) {
            this.next();
          }
        }
        break;

      case 5:
        if (this.localUser.cellPhone.length <= 16) {
          if (this.localUser.cellPhone.match(this.utilCtrl.regexValidators.phone)) {
            this.next();
          }
        }
        break;

      case 7:
        if (this.localUser.job.match(this.utilCtrl.regexValidators.text)) {
          this.next();
        }
        break;

      case 8:
        if (this.localUser.workPhone.length <= 16) {
          if (this.localUser.workPhone.match(this.utilCtrl.regexValidators.phone)) {
            this.next();
          }
        }
        break;

    }
  }

  formNumbUp() {
    this.formNumb = this.formNumb + 1;
  }

  next() {
    switch (this.formNumb) {
      case 1:
        this.formNumbUp();
        break;
      case 2:
        this.checkCpf();
        break;
      case 3:
        this.formNumbUp();
        break;
      case 4:
        this.formNumbUp();
        break;
      case 5:
        this.formNumbUp();
        break;
      case 6:
        if (this.localUser.type == 1) {
          this.formNumb = 9999;
        } else {
          this.formNumbUp();
        }
        break;
      case 7:
        this.formNumbUp();
        break;
      case 8:
        this.formNumb = 9999;
        break;
    }
  }

  prev() {
    if (this.formNumb > 1) {
      if (this.formNumb == 9999) {
        if (this.localUser.type == 1) {
          this.formNumb = 6;
        } else {
          this.formNumb = 8;
        }
      } else {
        this.formNumb = this.formNumb - 1;
      }
    }
  }

  validateForm() {
    switch (this.formNumb) {
      case 2:
        this.validateCPFForm();
        break;
      case 3:
        this.validateGenderForm();
        break;
      case 4:
        this.validateBirthDateForm();
        break;
      case 5:
        this.validateCellPhoneForm();
        break;
      case 6:
        this.validateTypeForm();
        break;
      case 7:
        this.validateJobForm();
        break;
      case 8:
        this.validateWorkPhoneForm();
        break;
      case 9999:
        this.validateTermsOfUseForm();
        break;
    }
  }

  validateCPFForm() {
    if (this.localUser.cpf.length <= 14) {

      // console.log(`this.localUser.cpf: ${thisthis.password..cpf}: ${this.localUser.cpf.match(this.utilCtrl.regexValidators.cpf) ? 'OK' : 'Not OK'}`);

      if (this.localUser.cpf.match(this.utilCtrl.regexValidators.cpf)) {
        this.formValid = true;
      } else {
        this.formValid = false;
      }
    }
  }

  validateGenderForm() {
    if (this.localUser.gender != "") {
      this.formValid = true;
    } else {
      this.formValid = false;
    }
  }

  validateBirthDateForm() {
    if (this.birthDateString != "") {
      this.formValid = true;
    } else {
      this.formValid = false;
    }
  }

  validateTypeForm() {
    if (this.localUser.type != 0) {
      this.formValid = true;
    } else {
      this.formValid = false;
    }
  }

  validateCellPhoneForm() {
    if (this.localUser.cellPhone.length <= 16) {

      // console.log(`this.localUser.cellPhone: ${this.localUser.cellPhone}: ${this.localUser.cellPhone.match(this.utilCtrl.regexValidators.phone) ? 'OK' : 'Not OK'}`);

      if (this.localUser.cellPhone.match(this.utilCtrl.regexValidators.phone)) {
        this.formValid = true;
      } else {
        this.formValid = false;
      }
    }
  }

  validateWorkPhoneForm() {
    if (this.addWorkPhoneNow != null) {
      if (this.addWorkPhoneNow) {
        if (this.localUser.workPhone.length <= 16) {

          // console.log(`this.localUser.workPhone: ${this.localUser.workPhone}: ${this.localUser.workPhone.match(regexValidators.phone) ? 'OK' : 'Not OK'}`);

          if (this.localUser.workPhone.match(this.utilCtrl.regexValidators.phone)) {
            this.formValid = true;
          } else {
            this.formValid = false;
          }
        }
      } else {
        this.formValid = true;
      }
    } else {
      this.formValid = false;
    }
  }

  validateJobForm() {
    if (this.localUser.job.length >= 3) {

      // console.log(`this.localUser.job: ${this.localUser.job}: ${this.localUser.job.match(this.utilCtrl.regexValidators.text) ? 'OK' : 'Not OK'}`);

      if (this.localUser.job.match(this.utilCtrl.regexValidators.text)) {
        this.formValid = true;
      } else {
        this.formValid = false;
      }
    } else {
      this.formValid = false;
    }
  }

  validateTermsOfUseForm() {
    this.formValid = this.termsOfUseAccepted;
  }

  ionViewDidLoad() {

    this.authStateSub = this.fire.authState
      .subscribe(user => {
        if (user != null && user.displayName != null) {
          if (!this.authStateFired) {
            this.authStateFired = true;

            this.localUser.loginMethod = user.providerData[0].providerId;
            this.localUser.email = user.email;
            this.localUser.emailVerified = user.emailVerified;

            this.db.usersRegisters.get(user.uid)
              .then(userRegister => {

                if (userRegister != null) {
                  console.log('hasUserRegister');
                  console.log(`userRegister: ${JSON.stringify(userRegister)}`);
                  this.hasUserRegister = true;
                  this.localUserRegister = userRegister;
                  this.localUser.cpf = userRegister.userCpf
                }


                if (this.fire.auth.currentUser.photoURL != null) {
                  console.log('this.fire.auth.currentUser.photoURL != null');
                  this.localUser.pic.url = this.fire.auth.currentUser.photoURL;
                  this.localUser.pic.thumbnailUrl = this.fire.auth.currentUser.photoURL;
                  this.localUser.pic.info = this.fire.auth.currentUser.providerData[0].providerId;
                  this.localUser.pic.createdOn = new Date();
                }

                this.localUser.id = this.fire.auth.currentUser.uid;

                let tempNameVet = user.displayName.split(' ');
                this.localUser.fullName = user.displayName;
                this.localUser.firstName = tempNameVet[0];
                for (let i = 1; i < tempNameVet.length; i++) {
                  this.localUser.lastName = `${this.localUser.lastName} ${tempNameVet[i]}`
                }

                console.log(`this.localUser ${JSON.stringify(this.localUser)}`)

                this.showSplash = false;

                this.startFormValidatorServ();
                this.startFocusServ();

              })
              .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('erro ao buscar o registro do usuário', e);
              })
          }

        }
      },
        error => {
          this.UiFeedBackCtrl.presentErrorAlert('Error at authState subscriber', error);
        });

    if (this.platform.is('cordova')) {
      this.loadProviderInviteData();
    }

  }

  ionViewDidLeave() {

    if (this.authStateSub != null) {
      this.authStateSub.unsubscribe();
    }

    this.stopFormValidatorServ();
    this.stopFocusServ();
  }

  goToTermsOfUsePage() {
    this.navCtrl.push('TermsOfUsePage')
      .then(() => {
        this.startFormValidatorServ();
        this.startFocusServ();
      })
  }

  loadProviderInviteData(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getProviderInviteData()
        .then(providerInviteData => {
          this.localDBProviderInviteData = providerInviteData;
          if (this.localDBProviderInviteData.hasProviderInvite) {
            this.db.afs.collection('ProviderInvites').doc(this.localDBProviderInviteData.inviteId).ref.get()
              .then(ProviderInviteDoc => {
                this.localProviderInvite = ProviderInviteDoc.data() as ProviderInvite;
                resolve();
              })
              .catch(e => {
                reject(e);
              })
          } else {
            resolve();
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  saveProviderInviteUtilization(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.UserDataCtrl.localUserIsProvider) {
        if (this.localDBProviderInviteData.hasProviderInvite) {

          var tempProviderInviteUtilization = new ProviderInviteUtilization();
          tempProviderInviteUtilization.id = this.db.createId();
          tempProviderInviteUtilization.providerId = this.UserDataCtrl.localUser.id;
          tempProviderInviteUtilization.providerName = this.UserDataCtrl.localUser.fullName;
          tempProviderInviteUtilization.providerThumbnailUrl = this.UserDataCtrl.localUser.pic.thumbnailUrl;

          this.db.afs.collection('ProviderInvites').doc(this.localProviderInvite.id)
            .collection('InviteUtilizations').doc(tempProviderInviteUtilization.id)
            .set(Object.assign({}, tempProviderInviteUtilization))
            .then(() => {
              resolve();
            })
            .catch(e => {
              reject(e);
            })
        } else {
          resolve();
        }
      } else {
        resolve();
      }
    })
  }

  cancelConfirmation() {
    const alert = this.UiFeedBackCtrl.alertCtrl.create({
      title: 'Tem certeza?',
      subTitle: `Tem certeza que deseja cancelar o cadastro de seus dados?`,
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.cancel();
          }
        },
        'Não'
      ]
    });
    this.UiFeedBackCtrl.hapticCtrl.notification('error');
    this.custonAlertPreseter(alert);
  }

  cancel() {
    this.custonPresenLoader('Cancelando...', 'assets/animations/logout.json')
      .then(() => {
        this.fire.auth.signOut()
          .then(() => {
            this.coreCtrl.navSetRoot('HomePage');
            this.custonDismissLoader();
            this.UiFeedBackCtrl.presentAlert(`Registro cancelado!`, `Para voltar pro registro faça login novamente!`, `warning`, false);
          })
          .catch(e => {
            this.custonDismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      })
  }

  /**
 * Apresenta o alerta e chaveia as variaveis de controle interno da pagina
 * @param {Alert} alert Mensagem do alerta
 */
  private custonAlertPreseter(alert: Alert): Promise<any> {

    alert.onWillDismiss((data, role) => {

      this.alertIsActive = false;

    });

    this.alertIsActive = true;
    return alert.present();
  }

	/**
	 * Apresenta loader e chaveia as variaveis de controle interno da pagina
	 * @param {string} message Mensagem do alerta
	 * @param {string} path `opcional` Caminho do Lottie que deverá ser utilizado ex: `assets/animations/check_animation.json` se não for passado o lottie padrão sera utilizado.
	 */
  private custonPresenLoader(message: string, path?: string): Promise<void> {
    this.loaderIsActive = true;
    return this.UiFeedBackCtrl.presenLoader(message, path)
  }

	/**
	 * Remove o lottie loader ativo
	 */
  public custonDismissLoader(): Promise<void> {
    this.loaderIsActive = false;
    return this.UiFeedBackCtrl.dismissLoader()
  }


  checkCpf() {
    this.custonPresenLoader('Verificando CPF...')
      .then(() => {

        console.log(`inserted CPF: ${this.localUser.cpf}`);

        this.coreCtrl.db.afs.collection('Users').ref
          .where('cpf', '==', this.localUser.cpf)
          .get()
          .then(cpfQuerry => {
            this.coreCtrl.db.validators.validateQuerySnapshot(cpfQuerry)
              .then(cpfQuerry => {
                if (cpfQuerry.empty) {

                  this.custonDismissLoader();

                  const alert = this.coreCtrl.UiFeedBackCtrl.alertCtrl.create({
                    title: 'Confira seu CPF!',
                    subTitle: `O CPF inserido ${this.localUser.cpf} sera registrado e não poderá ser utilizado para cadastrar outras contas, deseja continuar?`,
                    buttons: [
                      'Cancelar',
                      {
                        text: 'Sim',
                        handler: () => {
                          this.formNumbUp();
                        }
                      }
                    ]
                  });
                  this.coreCtrl.UiFeedBackCtrl.hapticCtrl.notification('error');
                  this.custonAlertPreseter(alert);

                } else {
                  this.custonDismissLoader();
                  let tempUser = cpfQuerry.docs[0].data() as User;
                  let alertSubTitle = '';

                  if (tempUser.loginMethod == 'password') {
                    alertSubTitle = `O CPF inserido ${this.localUser.cpf} já esta utilizado pelo usuário de nome ${tempUser.firstName}, e email: ${tempUser.email}. Registrado via Email! Verifique o CPF inserido e caso o mesmo esteja coreto cancele o Registro e faça o login com sua conta original!`;
                  } else {
                    alertSubTitle = `O CPF inserido ${this.localUser.cpf} já esta utilizado pelo usuário de nome ${tempUser.firstName}, e email: ${tempUser.email}. Registrado via ${tempUser.loginMethod}! Verifique o CPF inserido e caso o mesmo esteja coreto cancele o Registro e faça o login com sua conta original!`;
                  }

                  const alert = this.coreCtrl.UiFeedBackCtrl.alertCtrl.create({
                    title: 'CPF já utilizado!',
                    subTitle: alertSubTitle,
                    buttons: [
                      'Cancelar',
                      {
                        text: 'Cancelar Registro',
                        handler: () => {

                          this.cancel();

                        }
                      }
                    ]
                  });
                  this.coreCtrl.UiFeedBackCtrl.hapticCtrl.notification('error');
                  this.custonAlertPreseter(alert);

                }
              })
              .catch(e => {
                this.custonDismissLoader();
                this.coreCtrl.UiFeedBackCtrl.presentErrorAlert('Erro', e);
              })
          })
      })
  }


  finish() {
    this.custonPresenLoader('Salvando...')
      .then(() => {

        if (this.termsOfUseAccepted == false) {
          this.UiFeedBackCtrl.presentAlert('Erro :(', 'Você precisa aceitar os termos de uso !', 'error');
          this.custonDismissLoader();
          return;
        } else {
          this.localUser.termsOfUseAcceptedOn = new Date();
          this.localUser.termsOfUseAcceptedVersion = '1.0';
        }

        this.localUser.birthDate = new Date(`${this.birthDateString} 00:00:00 GMT-0300 (Hora oficial do Brasil)`);

        const userRef = this.db.afs.doc<User>('Users/' + this.fire.auth.currentUser.uid);

        if (this.platform.is('cordova')) {
          if (this.platform.is('android')) {
            return this.firebaseNative.getToken()
              .then(token => {
                this.localUser.token = token;
                return userRef.set(Object.assign({}, this.localUser))
                  .then(() => {
                    this.UserDataCtrl.init()
                      .then(() => {
                        this.saveProviderInviteUtilization()
                          .then(() => {
                            this.menuCtrl.enable(true, 'menu');
                            this.navCtrl.setRoot('HomePage');

                            if (this.localUser.emailVerified == false) {
                              this.fire.auth.currentUser.sendEmailVerification();
                              this.UiFeedBackCtrl.presentPersistentToast('Verifique seu email! Email de verificação enviado para ' + this.fire.auth.currentUser.email, '');
                            }

                            this.UiFeedBackCtrl.presentAlert('Cadastro concluído', 'Bem-vindo ao Economizei!', 'success', true);
                            this.custonDismissLoader();
                          })

                      })
                  })
              })
              .catch(e => {
                this.custonDismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
              })
          }

          if (this.platform.is('ios')) {
            return this.firebaseNative.getToken()
              .then(token => {
                this.localUser.token = token;
                this.firebaseNative.grantPermission();// ONLY FOR IOS DEVICES !!!!
                return userRef.set(Object.assign({}, this.localUser))
                  .then(() => {
                    this.UserDataCtrl.init()
                      .then(() => {
                        this.saveProviderInviteUtilization()
                          .then(() => {

                            this.menuCtrl.enable(true, 'menu');
                            this.navCtrl.setRoot('HomePage');

                            if (this.localUser.emailVerified == false) {
                              this.fire.auth.currentUser.sendEmailVerification();
                              this.UiFeedBackCtrl.presentPersistentToast('Verifique seu email! Email de verificação enviado para ' + this.fire.auth.currentUser.email, '');
                            }

                            this.UiFeedBackCtrl.presentAlert('Cadastro concluído', 'Bem-vindo ao Economizei!', 'success', true);
                            this.custonDismissLoader();
                          })
                      })
                  })
              })
              .catch(e => {
                this.custonDismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
              })
          }
        } else {
          return userRef.set(Object.assign({}, this.localUser))
            .then(() => {
              this.menuCtrl.enable(true, 'menu');
              this.navCtrl.setRoot('HomePage');

              this.UserDataCtrl.init()
                .then(() => {
                  if (this.localUser.emailVerified == false) {
                    this.fire.auth.currentUser.sendEmailVerification();
                    this.UiFeedBackCtrl.presentPersistentToast('Verifique seu email! Email de verificação enviado para ' + this.fire.auth.currentUser.email, '');
                  }

                  this.UiFeedBackCtrl.presentAlert('Cadastro concluído', 'Bem-vindo ao Economizei!', 'success', true);
                  this.custonDismissLoader();
                })
            })
            .catch(e => {
              this.custonDismissLoader();
              this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
            })
        }

      })
  }
}