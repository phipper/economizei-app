import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Events, FabContainer } from 'ionic-angular';

import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions } from '@ionic-native/camera-preview';

import { UiFeedBackProvider } from '../../../../providers/UifeedBack/UifeedBack';



@IonicPage()
@Component({
  selector: 'page-provider-verification-take-picture',
  templateUrl: 'provider-verification-take-picture.html',
})

export class ProviderVerificationTakePicturePage {

  @ViewChild(Content) content: Content;

  picIndex: number = 0;
  flash = false;
  torch = false;
  cameraGrid = false;
  needToUploadPicture = false;
  picture: string;

  cameraOpts: CameraPreviewOptions = {
    x: 0,
    y: 0,
    width: window.innerWidth,
    height: window.innerHeight,
    toBack: true,
    tapToFocus: true
  };

  cameraPictureOpts: CameraPreviewPictureOptions = {
    width: 1600,
    height: 1600,
    quality: 100
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private cameraPreview: CameraPreview,
    public events: Events
  ) {
    this.picIndex = this.navParams.get('picIndex');
    switch (this.picIndex) {
      case 0:
        this.cameraOpts.camera = this.cameraPreview.CAMERA_DIRECTION.FRONT;
        break;
      case 1:
        this.cameraOpts.camera = this.cameraPreview.CAMERA_DIRECTION.BACK;
        break;
      case 2:
        this.cameraOpts.camera = this.cameraPreview.CAMERA_DIRECTION.BACK;
        break;

      default:
        break;
    }
  }

  showCameraGrid() {
    setTimeout(() => {
      this.cameraGrid = true;
    }, 1000);
  }

  ionViewDidLoad() {
    this.startCamera();
    if (this.picIndex == 1 || this.picIndex == 2) {
      this.showCameraGrid();
    }
  }

  ionViewDidLeave() {
    if (this.needToUploadPicture) {
      this.events.publish('ProviderVerificationTakePicturePage:didLeave',
        {
          needToUploadPicture: true,
          picIndex: this.picIndex,
          picDataUrl: this.picture
        });
    } else {
      this.events.publish('ProviderVerificationTakePicturePage:didLeave', { needToUploadPicture: false });
    }
    this.cameraPreview.stopCamera();
  }

  cancelPic() {
    this.navCtrl.pop();
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  async startCamera() {
    this.picture = null;
    const result = await this.cameraPreview.startCamera(this.cameraOpts);
    this.torch = false;
    this.flash = false;
    this.cameraPreview.setFlashMode(this.cameraPreview.FLASH_MODE.OFF);
    console.log(result);
  }

  switchCamera(fab: FabContainer) {
    this.cameraPreview.switchCamera()
      .then(() => {
        this.torch = false;
        this.flash = false;
        this.cameraPreview.setFlashMode(this.cameraPreview.FLASH_MODE.OFF);
      })
    fab.close();
  }

  toggleFlash(fab: FabContainer) {
    if (this.flash) {
      this.flash = false;
      this.cameraPreview.setFlashMode(this.cameraPreview.FLASH_MODE.OFF);
      this.UiFeedBackCtrl.presentToastTop('Flash desativado', 'success');
    } else {
      this.flash = true
      this.cameraPreview.setFlashMode(this.cameraPreview.FLASH_MODE.ON);
      this.UiFeedBackCtrl.presentToastTop('Flash ativado', 'success');
    }
    fab.close();
  }

  toggleTorch(fab: FabContainer) {
    if (this.torch) {
      this.torch = false;
      this.flash = false;
      this.cameraPreview.setFlashMode(this.cameraPreview.FLASH_MODE.OFF);
      this.UiFeedBackCtrl.presentToastTop('Lanterna desativada', 'success');
    } else {
      this.torch = true;
      this.flash = false;
      this.cameraPreview.setFlashMode(this.cameraPreview.FLASH_MODE.TORCH);
      this.UiFeedBackCtrl.presentToastTop('Lanterna ativada', 'success');
    }
    fab.close();
  }

  async takePicture() {
    const result = await this.cameraPreview.takePicture(this.cameraPictureOpts);
    await this.cameraPreview.stopCamera();
    this.picture = `data:image/jpeg;base64,${result}`;
  }

  confirmPicture() {
    this.needToUploadPicture = true;
    this.navCtrl.pop();
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

}