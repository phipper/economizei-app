import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderVerificationTakePicturePage } from './provider-verification-take-picture';

import { MatSelectModule, MatFormFieldModule, MatStepperModule, MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';

@NgModule({
  declarations: [
    ProviderVerificationTakePicturePage,
  ],
  imports: [
    MatButtonModule,
    MatStepperModule,
    MatIconModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    IonicPageModule.forChild(ProviderVerificationTakePicturePage),
  ],
})
export class ProviderVerificationTakePicturePageModule {}
