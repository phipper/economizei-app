import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderVerificationPage } from './provider-verification';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    ProviderVerificationPage,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(ProviderVerificationPage),
  ],
})
export class ProviderVerificationPageModule {}
