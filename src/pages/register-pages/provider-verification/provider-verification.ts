import { Component, ViewChild } from '@angular/core';
import { IonicPage, Content, NavController, NavParams, Events } from 'ionic-angular';

import { AngularFirestore } from 'angularfire2/firestore';

import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { PicProvider } from '../../../providers/Pic';
import { SAC } from '../../../models/SAC';
import { ProviderVerfication } from '../../../models/ProviderVerfication';
import { SacProvider } from '../../../providers/SAC';
import { LocalDbProvider } from '../../../providers/LocalDbProvider';
import { UserDataProvider } from '../../../providers/UserData';



@IonicPage()
@Component({
  selector: 'page-provider-verification',
  templateUrl: 'provider-verification.html',
})

export class ProviderVerificationPage {

  @ViewChild(Content) content: Content;

  segment: string = "Selfie";

  localSac: SAC = new SAC();
  localProvideVerification: ProviderVerfication = new ProviderVerfication();
  localCharacteristicsReady = false;
  showSplash = true;
  canLeaveCtrl = true;
  dummySelfieUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2FProviderVerificationPage%20pics%2FdummySelfie.jpg?alt=media&token=92cab9b6-ca2e-4427-b5fc-a23a6c7484b2';
  dummyDocFrontUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2FProviderVerificationPage%20pics%2FdummyDocFrontUrl.PNG?alt=media&token=2811e804-5728-4c13-9aa3-7a9f9402a24d';
  dummyDocBackUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2FProviderVerificationPage%20pics%2FdummyDocBackUrl.PNG?alt=media&token=145f4a61-346c-4d08-9af0-2b06de76c4fb';
  selfieReady = false;
  docFrontReady = false;

  lottieSplash = {
    loop: true,
    autoplay: true,
    path: 'assets/animations/servishero_loading.json'
  }

  constructor(
    public UserDataCtrl: UserDataProvider,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private sacCtrl: SacProvider,
    private PicCtrl: PicProvider,
    private localDbCtrl: LocalDbProvider,
    public events: Events
  ) {
  }

  scrollToTop() {
    this.content.scrollToTop();
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll)
        this.content.scrollToBottom();
    }, 10);
  }

  ionViewDidLoad() {
    if (this.UserDataCtrl.localUser.providerVerificationSacId == "") {
      this.afs.collection('ProviderVerifications').ref.where("providerId", "==", this.UserDataCtrl.localUser.id).get()
        .then(querySnapshot => {
          if (querySnapshot.empty) {
            this.localProvideVerification.id = this.afs.createId();
            this.localProvideVerification.providerId = this.UserDataCtrl.localUser.id;
            this.localProvideVerification.providerName = this.UserDataCtrl.localUser.fullName;
            this.localProvideVerification.status = -2;
            this.localProvideVerification.pics = [{ info: `${this.UserDataCtrl.localUser.id}_selfie`, thumbnailUrl: null, url: '', createdOn: null, index: 0 },
            { info: `${this.UserDataCtrl.localUser.id}_docFront`, thumbnailUrl: null, url: '', createdOn: null, index: 1 },
            { info: `${this.UserDataCtrl.localUser.id}_docBack`, thumbnailUrl: null, url: '', createdOn: null, index: 2 }];

            this.afs.collection('ProviderVerifications').doc(this.localProvideVerification.id).set(Object.assign({}, this.localProvideVerification))
              .then(() => {
                this.showSplash = false;
              })
              .catch(e => {
                this.canLeaveCtrl = true;
                this.navCtrl.pop();
                console.log("Erro" + JSON.stringify(e));
                this.UiFeedBackCtrl.presentAlert("Erro", JSON.stringify(e), 'error');
              });
          } else {
            this.localProvideVerification = querySnapshot.docs[0].data() as ProviderVerfication;
            switch (this.localProvideVerification.status) {
              case -2:
                this.segment = "Selfie"
                this.canLeaveCtrl = false;
                this.selfieReady = true;
                break;
              case -1:
                this.segment = "RG Frente"
                this.canLeaveCtrl = false;
                this.selfieReady = true;
                this.docFrontReady = true;
                break;
              case 0:
                this.segment = "RG Verso"
                this.canLeaveCtrl = false;
                this.selfieReady = true;
                this.docFrontReady = true;
                break;
            }
            this.showSplash = false;
          }
        });
    }
  }

  ionViewCanLeave(): boolean {
    if (!this.canLeaveCtrl) {
      let alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: "Tem certeza?",
        subTitle: "Os dados ficarão salvos para que você possa concluir a sua Solicitação de verificação mais tarde! Deseja sair?",
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              this.canLeaveCtrl = true;
              this.navCtrl.pop();
            }
          },
          'Cancelar'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('warning');
      alert.present();
    } else {
      return true;
    }
    return false;
  }

  goBack() {
    switch (this.segment) {
      case "RG Frente":
        this.segment = "Selfie"
        break;
      case "RG Verso":
        this.segment = "RG Frente"
        break;
    }
  }

  goForward() {
    switch (this.segment) {
      case "Selfie":
        this.segment = "RG Frente"
        break;
      case "RG Frente":
        this.segment = "RG Verso"
        break;
    }
  }

  docBackReady(): boolean {
    return this.localProvideVerification.pics[0].url != '' &&
      this.localProvideVerification.pics[1].url != '' &&
      this.localProvideVerification.pics[2].url != '';
  }

  startCameraPrev(picIndex) {
    this.canLeaveCtrl = true;
    this.navCtrl.push('ProviderVerificationTakePicturePage', {
      picIndex: picIndex
    })
      .then(() => {
        this.events.subscribe('ProviderVerificationTakePicturePage:didLeave', data => {
          this.canLeaveCtrl = false;
          this.events.unsubscribe('ProviderVerificationTakePicturePage:didLeave');
          if (data != null && data.needToUploadPicture) {

            let loader = this.UiFeedBackCtrl.loadingCtrl.create({
              content: 'Fazendo Upload...',
            });

            loader.present()
              .then(() => {
                this.localProvideVerification.pics[data.picIndex].createdOn = new Date();
                this.PicCtrl.uploadProVeriftImg(data.picDataUrl, `${this.localProvideVerification.pics[data.picIndex].info}.jpeg`)
                  .then(url => {
                    loader.setContent("Atualizando dados...");
                    this.localProvideVerification.pics[data.picIndex].url = url;
                    this.localProvideVerification.status = (data.picIndex - 2);
                    this.afs.collection('ProviderVerifications').doc<ProviderVerfication>(this.localProvideVerification.id).update({ pics: this.localProvideVerification.pics, status: this.localProvideVerification.status })
                      .then(() => {
                        switch (data.picIndex) {
                          case 0:
                            this.segment = "Selfie";
                            this.selfieReady = true;
                            break;
                          case 1:
                            this.segment = "RG Frente";
                            this.docFrontReady = true;
                            break;
                          case 2:
                            this.segment = "RG Verso"
                            break;
                        }
                        loader.dismiss();
                        this.scrollToBottom();
                        this.UiFeedBackCtrl.presentToastTop("Foto adicionada com sucesso!", 'success');
                      });
                  })
                  .catch(e => {
                    loader.dismiss();
                    this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                  });
              });
          }
        });
      });
  }


  ionViewDidLeave() {
  }


  addPhotoFromGallery(picIndex: number) {
    this.UiFeedBackCtrl.presenLoader('Carregando..', 'assets/animations/pic.json')
    this.PicCtrl.newPicture('galery')
      .then(newPic => {

        let tempPic = newPic;

        this.PicCtrl.uploadProVeriftImg(tempPic.url, `${this.localProvideVerification.pics[picIndex].info}.jpeg`)
          .then(url => {
            this.localProvideVerification.pics[picIndex].url = url;
            this.localProvideVerification.status = (picIndex - 2);
            this.afs.collection('ProviderVerifications').doc<ProviderVerfication>(this.localProvideVerification.id)
              .update({ pics: this.localProvideVerification.pics, status: this.localProvideVerification.status })
              .then(() => {
                this.canLeaveCtrl = false;
                switch (picIndex) {
                  case 0:
                    this.selfieReady = true;
                    break;
                  case 1:
                    this.docFrontReady = true;
                    break;
                  case 2:

                    break;
                }
                this.UiFeedBackCtrl.dismissLoader();
                this.scrollToBottom();
                this.UiFeedBackCtrl.presentToastTop("Foto adicionada com sucesso!", 'success');
              })
          })
          .catch(e => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
          });
      })
      .catch(e => {
        if (e == "No Image Selected") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Você não selecionou nehuma foto!", "warning");
          }, 100);
        } else if (e == "userCancelled") {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentToast("Ação cancelada!", "warning");
          }, 100);
        } else {
          setTimeout(() => {
            this.UiFeedBackCtrl.dismissLoader();
            this.UiFeedBackCtrl.presentErrorAlert("Erro", e);
          }, 100);
        }
      })
  }

  createSac(): Promise<string> {
    return new Promise((resolve, reject) => {
      let tempSac = new SAC();
      this.localDbCtrl.getPVSModelData()
        .then(SacModelData => {

          tempSac.type = SacModelData.type;
          tempSac.priority = SacModelData.priority;
          tempSac.businessRole = SacModelData.businessRole;
          tempSac.category = SacModelData.category;
          tempSac.subCategory = SacModelData.subCategory;
          tempSac.assignedTeam = SacModelData.assignedTeam;

          tempSac.userId = this.UserDataCtrl.localUser.id;
          tempSac.userName = this.UserDataCtrl.localUser.fullName;
          tempSac.providerVerificationId = this.localProvideVerification.id;
          tempSac.title = `Verificação de Anuncio para: ${this.UserDataCtrl.localUser.firstName}`;
          tempSac.description = `Verificação de Anuncio para: ${this.UserDataCtrl.localUser.fullName},
                                      name: ${this.UserDataCtrl.localUser.fullName}, 
                                      cpf: ${this.UserDataCtrl.localUser.cpf}, 
                                      email: ${this.UserDataCtrl.localUser.email}, 
                                      cellPhone: ${this.UserDataCtrl.localUser.cellPhone}, 
                                      workPhone: ${this.UserDataCtrl.localUser.workPhone}, 
                                      birthDate: ${this.UserDataCtrl.localUser.birthDate}, 
                                      loginMethod: ${this.UserDataCtrl.localUser.loginMethod}, 
                                      job: ${this.UserDataCtrl.localUser.job}`;


          return this.sacCtrl.saveSac(tempSac, 'RDS')
            .then(SacId => {
              resolve(SacId);
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  finish() {
    this.UiFeedBackCtrl.presenLoader('Salvando Nova Solicitação de SAC...', 'assets/animations/newspaper.json')
      .then(() => {
        this.createSac()
          .then(sacId => {
            this.UiFeedBackCtrl.updateLoaderMessage('Salvando seus Documentos...');
            this.localProvideVerification.status = 1;
            this.afs.collection('ProviderVerifications').doc<ProviderVerfication>(this.localProvideVerification.id).update({ status: this.localProvideVerification.status })
              .then(() => {
                this.UserDataCtrl.localUser.providerVerificationSacId = sacId;
                this.UserDataCtrl.update({ providerVerificationSacId: this.UserDataCtrl.localUser.providerVerificationSacId })
                  .then(() => {
                    this.UiFeedBackCtrl.dismissLoader();
                    this.canLeaveCtrl = true;
                    this.navCtrl.pop();
                    this.UiFeedBackCtrl.presentAlert("Feito !", "Seus dados foram enviados, agora é só aguardar! ", 'success', true);
                  });
              });
          });
      });
  }

}