import { Injectable } from '@angular/core';
import { ActionSheetButton, Platform, Events } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links';
import { FCM } from '@ionic-native/fcm';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

import { UiFeedBackProvider } from './UifeedBack/UifeedBack';

import { AngularFireAuth } from 'angularfire2/auth';
import { EcoFirestore } from './EcoFirestore/EcoFirestore';
import { LocalDbProvider } from './LocalDbProvider';

//Phipper LLC 09.09.2018

var TUTORIAL_VERSION = 1;

@Injectable()
export class CoreProvider {

  private backButtonPressedOnce = false;
  private backButtonIsEnabled = true;

  public statusBarModes: string[] = ['default', 'light', 'hide'];

  constructor(
    public db: EcoFirestore,
    public localDbCtrl: LocalDbProvider,
    public UiFeedBackCtrl: UiFeedBackProvider,
    public platform: Platform,
    public fire: AngularFireAuth,
    private events: Events,
    private firebaseDynamicLinks: FirebaseDynamicLinks,
    private firebaseCloudMessaging: FCM,
    public socialSharing: SocialSharing,
    public clipboard: Clipboard,
    public statusBar: StatusBar
  ) {
  }


  /**
 * Retorna se o tutorial mais recente foi realizado ou não
 * @return pageName `Promise<boolean>`
 */
  public getTutorialStatus(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          if (!appConfig.isTutorialDone) {
            resolve(false);
          } else if (appConfig.tutorialVersion < TUTORIAL_VERSION) {
            resolve(false);
          } else {
            resolve(true);
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
  * Retorna se o app foi configurado ou não
  * @return pageName `Promise<boolean>`
  */
  public getConfigStatus(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          resolve(appConfig.isConfigured);
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  public getHapticFeedbackConfig(): boolean {
    return this.UiFeedBackCtrl.hapticCtrl.getHapticFeedbackConfig();
  }

  /**
  * seta a configuração de Modo da Status Bar
  * @param {string} newMode
  * @return `Promise<void>`
  */
  public setStatusBarMode(newMode: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          let tempAppConfig = appConfig;
          tempAppConfig.statusBarMode = newMode;
          this.localDbCtrl.updateAppConfig(tempAppConfig)
            .then(() => {
              switch (tempAppConfig.statusBarMode) {
                case 'default':
                  this.statusBar.show();
                  break;
                case 'light':
                  this.statusBar.show();
                  this.statusBar.styleDefault();
                  if (this.platform.is(`ios`)) {
                    this.statusBar.overlaysWebView(false)
                  }
                  this.statusBar.backgroundColorByHexString('#fff');
                  break;
                case 'hide':
                  this.statusBar.hide();
                  break;
              }
              resolve();
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
  * Set the status bar to a specific hex color (CSS shorthand supported!).
  * @param {string} hexString  The hex value of the color.
  * @return `Promise<void>`
  */
  public setStatusBarColor(hexString: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          if (appConfig.statusBarMode != 'hide') {
            this.statusBar.show();
            this.statusBar.styleDefault();
            if (this.platform.is(`ios`)) {
              this.statusBar.overlaysWebView(false);
            }
            this.statusBar.backgroundColorByHexString(hexString);
          }
          resolve();
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
  * Hide the StatusBar
  */
  public hideStatusBar(): void {
    this.statusBar.hide();
  }

  /**
  * Show the StatusBar
  */
  public showStatusBar(): void {
    this.statusBar.show();
  }

  /**
  * seta a configuração de Modo do app na Status Bar
  * @return `Promise<void>`
  */
  public setAppConfigOnStatusBar(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          switch (appConfig.statusBarMode) {
            case 'default':
              this.statusBar.show();
              break;
            case 'light':
              this.statusBar.show();
              this.statusBar.styleDefault();
              if (this.platform.is(`ios`)) {
                this.statusBar.overlaysWebView(false);
              }
              this.statusBar.backgroundColorByHexString('#fff');
              break;
            case 'hide':
              this.statusBar.hide();
              break;
          }
          resolve();
        })
        .catch(e => {
          reject(e);
        })
    })
  }


  /**
   * seta a configuração de vibração
   * @param {boolean} newConfig
   * @return pageName `Promise<void>`
   */
  public setHapticFeedbackConfig(newConfig: boolean): Promise<void> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          let tempAppConfig = appConfig;
          tempAppConfig.hapticFeedback = newConfig;
          this.localDbCtrl.updateAppConfig(tempAppConfig)
            .then(() => {
              this.UiFeedBackCtrl.hapticCtrl.setHapticFeedbackConfig(newConfig);
              resolve();
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * Salva que o tutorial foi feito e salva a versão do tutorial que foi feito
   * @return pageName `Promise<void>`
   */
  public setTutorialDone(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.localDbCtrl.getAppConfig()
        .then(appConfig => {
          let tempAppConfig = appConfig;
          tempAppConfig.isTutorialDone = true;
          tempAppConfig.tutorialVersion = TUTORIAL_VERSION;
          this.localDbCtrl.updateAppConfig(tempAppConfig)
            .then(() => {
              resolve();
            })
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * Chama o metodo SetRoot para o NavControler do app.component
   * @param {string} page
   */
  public navSetRoot(page: string) {
    this.events.publish('NavProvider:setRoot', {
      page: page
    });
  }

  /**
   * Chama o metodo Push para o NavControler do app.component
   * @param {string} page
   * @param {string} params
   */
  public navPush(page: string, params?: any) {
    this.events.publish('NavProvider:push', {
      page: page,
      params: params
    });
  }

  /**
   * Chama o metodo Pop para o NavControler do app.component
   */
  public navPop() {
    this.events.publish('NavProvider:pop');
  }

  /**
   * Retorna o nome da pagina ativa
   * @return pageName `Promise<string>`
   */
  public navGetActiveName(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.events.subscribe('NavProvider:getActiveName:response', data => {
        this.events.unsubscribe('NavProvider:getActiveName:response');
        resolve(data.pageName);
      });
      this.events.publish('NavProvider:getActiveName');
    });
  }

  /**
   * Retorna o nome da rootPage
   * @return pageName `Promise<string>`
   */
  public navGetRootName(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.events.subscribe('NavProvider:getRootName:response', data => {
        this.events.unsubscribe('NavProvider:getRootName:response');
        resolve(data.rootName);
      });
      this.events.publish('NavProvider:getRootName');
    });
  }


  /**
   * desabilita o botao de voltar
   */
  public disableBackbutton() {
    this.backButtonIsEnabled = false;
  }

  /**
 * habilita o botao de voltar
 */
  public enableBackbutton() {
    this.backButtonIsEnabled = true;
  }

  /**
   * Registra a ação que deverá ser executada pelo botao de voltar
   */
  private registerBackbuttonAction() {
    console.log('CoreProvider says: registering Back button Action');
    this.platform.registerBackButtonAction(() => {
      if (this.backButtonIsEnabled) {
        this.navGetActiveName()
          .then(pageName => {
            this.navGetRootName()
              .then(rootName => {
                if (rootName == pageName) {
                  if (!this.backButtonPressedOnce) {
                    this.backButtonPressedOnce = true;
                    this.UiFeedBackCtrl.presentToast('Pressione novamente para sair...', 'warning');
                    setTimeout(() => {
                      this.backButtonPressedOnce = false;
                    }, 2000);
                  } else {
                    this.platform.exitApp();
                  }
                } else {
                  this.navPop();
                }
              })
          })
      } else {
        console.log(`CoreProvider says: ignoring Back button press`);
      }
    });
  }

  /**
   * Inicia o Provider 
   */
  public init() {

    if (this.platform.is('cordova')) {
      console.log('CoreProvider says: É cordova');
      console.log('CoreProvider says: Initiating Subscribes');

      //subscribe to DynamicLinks clicks
      this.firebaseDynamicLinks.onDynamicLink().subscribe(dynamicLink => {
        console.log(`DynamicLink open: ${JSON.stringify(dynamicLink)}`);
        this.openDynamicLink(dynamicLink);
      });

      //subscribe to notifications
      this.firebaseCloudMessaging.onNotification().subscribe(data => {
        console.log(`Notification received: ${JSON.stringify(data)}`);
        if (data.wasTapped) {
          console.log('Notification Received in background');
          this.openNotificationFromBackground(data);
        } else {
          console.log('Notification Received in foreground');
          this.openNotificationInForeground(data);
        }
      });

      this.registerBackbuttonAction();

      console.log('CoreProvider says: initiating local DB');
      this.localDbCtrl.init()
        .then(() => {
          console.log('CoreProvider says: LocalDB initiated');
          this.localDbCtrl.getAppConfig()
            .then(appConfig => {
              this.UiFeedBackCtrl.hapticCtrl.setHapticFeedbackConfig(appConfig.hapticFeedback);

              if (!appConfig.isTutorialDone) {
                this.navPush(`TutorialPage`);
              } else if (appConfig.tutorialVersion < TUTORIAL_VERSION) {
                this.navPush(`TutorialPage`);
              } else if (!appConfig.isConfigured) {
                this.navPush(`ConfigAssistantPage`)
              }

              switch (appConfig.statusBarMode) {
                case 'default':
                  break;
                case 'light':
                  this.statusBar.styleDefault();
                  this.statusBar.backgroundColorByHexString('#fff');
                  break;
                case 'hide':
                  this.statusBar.hide();
                  break;
              }

            })
        })
        .catch(e => {
          this.UiFeedBackCtrl.presentErrorAlert('Erro ao iniciar o Banco local', e);
        })

    } else {
      console.log('CoreProvider says: não é cordova');
    }

  }

  private openDynamicLink(dynamicLink) {
    if (dynamicLink.deepLink.split('/').indexOf('auth') == -1) {
      this.UiFeedBackCtrl.presenLoader('Carregando SmartLink...')
        .then(() => {
          //Handle the logic here after opening the app with the Dynamic link
          var page = dynamicLink.deepLink.split('/')[3]
          var data = dynamicLink.deepLink.split('/')[4]
          switch (page) {
            case 'showbanner':
              this.UiFeedBackCtrl.updateLoaderMessage('Carregando Anuncio...');
              this.db.banners.get(data)
                .then(banner => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.navPush('BannerDetailPage', {
                    banner: banner
                  });
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar anuncio', e);
                });
              break;
            case 'showrequest':
              this.UiFeedBackCtrl.updateLoaderMessage('Carregando Solicitação...');
              this.db.requests.get(data)
                .then(request => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.navPush('RequestDetailPage', {
                    request: request
                  });
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar solicitação', e);
                });
              break;

            case 'openProviderInvite':
              this.UiFeedBackCtrl.updateLoaderMessage('Carregando Convite...');
              this.localDbCtrl.saveProviderInviteId(data)
                .then(() => {
                  this.db.providerInvites.get(data)
                    .then(providerInvite => {
                      var tempProviderInvite = providerInvite;
                      this.UiFeedBackCtrl.dismissLoader();
                      this.UiFeedBackCtrl.presentAlert('Convite Salvo!', `O convite de(a) ${tempProviderInvite.providerName} foi salvo, Porfavor faça cadastro como prestador!`, 'success', true);
                    })
                })
                .catch(e => {
                  this.UiFeedBackCtrl.dismissLoader();
                  this.UiFeedBackCtrl.presentErrorAlert('Erro', e);
                });
              break;

            default:
              this.UiFeedBackCtrl.dismissLoader();
              this.UiFeedBackCtrl.presentToast(`SmartLink não reconhecido!, deepLink: ${dynamicLink.deepLink}`, 'error');
              console.log('DynamicLink não reconhecido!', `deepLink: ${dynamicLink.deepLink}`);
              break;
          }
        });
    }
  }

  private openNotificationInForeground(data) {
    if (data.action) {
      var tempAction = JSON.parse(data.action);
      console.log(tempAction);
    }
    switch (data.type) {
      case 'msgNotif':
        this.navGetActiveName()
          .then(pageName => {
            if (pageName != 'ChatPage') {
              let toastOptions = {
                message: `Nova mensagem de ${data.senderFullName.split(' ')[0]}!`,
                duration: 4000,
                position: 'top',
              };
              this.UiFeedBackCtrl.toast.showWithOptions(toastOptions);
            }
          })
        break;
      case 'termsOfUseViolation':
        let toastOptions = {
          message: `${data.title}`,
          duration: 4000,
          position: 'top'
        };
        this.UiFeedBackCtrl.toast.showWithOptions(toastOptions);
        break;
      case 'generalAlerts':
        let toastOptions1 = {
          message: `${data.title}`,
          duration: 4000,
          position: 'top'
        };
        this.UiFeedBackCtrl.toast.showWithOptions(toastOptions1);
        break;
      case 'newOrder':
        let toastOptions2 = {
          message: `${data.title}`,
          duration: 4000,
          position: 'top',
        };
        this.UiFeedBackCtrl.toast.showWithOptions(toastOptions2);
        break;
      case 'showRequest':
        let toastOptions3 = {
          message: `${data.title}`,
          duration: 4000,
          position: 'top'
        };
        this.UiFeedBackCtrl.toast.showWithOptions(toastOptions3);
        break;

      case 'showMyRequest':
        let toastOptions4 = {
          message: `${data.title}`,
          duration: 4000,
          position: 'top'
        };
        this.UiFeedBackCtrl.toast.showWithOptions(toastOptions4);
        break;

      default:
        this.UiFeedBackCtrl.presentToast(`Notificação não reconhecida!`, 'error');
        console.log('Notificação não reconhecida!');
        break;
    }
  }

  private openNotificationFromBackground(data) {
    if (data.action) {
      var tempAction = JSON.parse(data.action);
    }
    switch (data.type) {
      case 'msgNotif':
        this.navGetActiveName()
          .then(pageName => {
            if (pageName != 'ChatPage') {
              this.navPush('ChatPage', {
                chatId: data.chatId
              });
            }
          });
        break;
      case 'termsOfUseViolation':
        this.navPush('TermsOfUsePage');
        this.UiFeedBackCtrl.presentAlert(data.title, data.text, 'error');
        this.db.users.notifications.update(this.fire.auth.currentUser.uid, data.notificationId, { readed: true })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao registar a leitura da notifcação', e);
          });
        break;
      case 'generalAlerts':
        switch (tempAction.type) {
          case 'gotoPage':
            this.navPush(tempAction.page);
            break;
          case 'showMyOrder':
            this.navPush('MyOrderDetailPage', {
              orderId: tempAction.orderId
            });
            break;
          case 'showReceivedOrder':
            this.navPush('ReceivedOrderDetailPage', {
              orderId: tempAction.orderId
            });
            break;
          case 'showMySac':
            this.navPush('MySacDetailPage', {
              sacId: tempAction.sacId
            });
            break;

          default:
            break;
        }
        this.db.users.notifications.update(this.fire.auth.currentUser.uid, data.notificationId, { readed: true })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao registar a leitura da notifcação', e);
          });
        break;
      case 'newOrder':
        this.navPush('ReceivedOrderDetailPage', {
          orderId: tempAction.orderId
        });
        this.db.users.notifications.update(this.fire.auth.currentUser.uid, data.notificationId, { readed: true })
          .catch(e => {
            this.UiFeedBackCtrl.presentErrorAlert('erro ao registar a leitura da notifcação', e);
          });
        break;
      case 'showRequest':
        this.UiFeedBackCtrl.presenLoader('Carregando Solicitação...')
          .then(() => {
            this.db.requests.get(tempAction.requestId)
              .then(request => {
                this.UiFeedBackCtrl.dismissLoader();
                this.navPush('RequestDetailPage', {
                  request: request
                });
              })
              .catch(e => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar solicitação', e);
              });
            this.db.users.notifications.update(this.fire.auth.currentUser.uid, data.notificationId, { readed: true })
              .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('erro ao registar a leitura da notifcação', e);
              });
          });
        break;

      case 'showMyRequest':
        this.UiFeedBackCtrl.presenLoader('Carregando Solicitação...')
          .then(() => {
            this.db.requests.get(tempAction.requestId)
              .then(request => {
                this.UiFeedBackCtrl.dismissLoader();
                this.navPush('MyRequestDetailPage', {
                  request: request
                });
              })
              .catch(e => {
                this.UiFeedBackCtrl.dismissLoader();
                this.UiFeedBackCtrl.presentErrorAlert('Erro ao carregar solicitação', e);
              });
            this.db.users.notifications.update(this.fire.auth.currentUser.uid, data.notificationId, { readed: true })
              .catch(e => {
                this.UiFeedBackCtrl.presentErrorAlert('erro ao registar a leitura da notifcação', e);
              });
          });
        break;

      default:
        this.UiFeedBackCtrl.presentToast(`Notificação não reconhecida!`, 'error');
        console.log('Notificação não reconhecida!');
        break;
    }
  }

  public shareApp() {
    let buttonsArray: ActionSheetButton[] = [];
    buttonsArray.push({
      text: 'Compartilhar',
      icon: !this.platform.is('ios') ? 'md-share' : null,
      handler: () => {
        this.socialSharing.share('Venha para o economizei!', null, null, 'https://economizei.page.link/economizeiapp');
      }
    });
    buttonsArray.push({
      text: 'Copiar link',
      icon: !this.platform.is('ios') ? 'link' : null,
      handler: () => {
        this.clipboard.copy('https://economizei.page.link/economizeiapp')
          .then(() => {
            this.UiFeedBackCtrl.presentAlert('Link copiado para a area de transferencia', '', 'success');
          });
      }
    });
    buttonsArray.push({
      text: 'Cancelar',
      icon: null,
      role: 'cancel',
      handler: null,
    });
    let actionSheet = this.UiFeedBackCtrl.actionSheetCtrl
      .create({
        title: 'Compartilhar o economizei',
        buttons: buttonsArray,
      });
    actionSheet.present();
  }

}