import { Injectable } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';
import { SAC } from '../models/SAC';
// import { User } from '../models/User';

export type SacType = 'RDS' | 'INC';


@Injectable()
export class SacProvider {

  constructor(
    private afs: AngularFirestore
  ) {
  }

  private numberPadding(number: number, size: number): string {
    var s = number.toString();
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }


  /**
   * Verifica se o Usuário passado pode criar pedidos 
   * @param {type} string Tipo do SAC `RDS` ou `INC`
   * @return `Promise<string>`, Id para ser utilizado na nova Ordem
   */
  private generateSACId(type: SacType): Promise<string> {
    return new Promise((resolve, reject) => {
      if (type == 'RDS' || type == 'INC') {
        return this.afs.collection('SACs').ref.get()
          .then(querySnapshot => {
            let tempId = `${type}${this.numberPadding(querySnapshot.size, 12)}`;
            return this.afs.collection('SACs').doc(tempId).ref.get()
              .then(SacDoc => {
                if (SacDoc.exists) {
                  return this.generateSACId(type);
                } else {
                  resolve(tempId);
                }
              });
          }).catch(e => {
            console.log(e.message);
            reject(e.message);
          });
      } else {
        console.log(`Tipo de SAC Invalido`);
        reject(`Tipo de SAC Invalido`);
      }
    });
  }

  /**
   * Salva o SAC
   * @param {sac} SAC SAC a ser salvo
   * @param {type} string Tipo do SAC `RDS` ou `INC`
   * @return `Promise<string>` - sacId
   */
  public saveSac(sac: SAC, type: SacType): Promise<string> {
    return new Promise((resolve, reject) => {
      return this.generateSACId(type)
        .then(newSacId => {
          sac.id = newSacId;
          return this.afs.collection('SACs').doc(sac.id).set(Object.assign({}, sac))
            .then(() => {
              resolve(sac.id)
            })
            .catch(e => {
              reject({ log: 'Error on reverseGeocode location', error: JSON.stringify(e) });
            });
        })
        .catch(e => {
          reject({ log: 'Error on reverseGeocode location', error: JSON.stringify(e) });
        });
    });
  }


  // public genarateSacForError( sacTitle: string, error: any, errorLevel: string, user: User ){
  //   let tempErrorSAC = new SAC;
  //   tempErrorSAC.status = 1;
  //   tempErrorSAC.userId = user.id;
  //   tempErrorSAC.userName = user.fullName;
  //   tempErrorSAC.title = sacTitle;
  //   tempErrorSAC.description = `Ocorreu um erro nivel: ${errorLevel}, com o usuário ${user.fullName} 

  //                                 +++++++++++++++++++++++
  //                                 | Detalhes do usuário |
  //                                 +++++++++++++++++++++++

  //                                 name: ${user.fullName}, 
  //                                 cpf: ${user.cpf}, 
  //                                 email: ${user.email}, 
  //                                 cellPhone: ${user.cellPhone}, 
  //                                 workPhone: ${user.workPhone}, 
  //                                 birthDate: ${user.birthDate}, 
  //                                 loginMethod: ${user.loginMethod}, 
  //                                 job: ${user.job}

  //                                 ++++++++++++++++++++++++++++
  //                                 | Detalhes do erro em JSON |
  //                                 ++++++++++++++++++++++++++++

  //                                 ${JSON.stringify(error)}`;
  //   tempErrorSAC.businessRoleName = 'error';
  //   tempErrorSAC.categoryName = 'error';
  //   tempErrorSAC.subCategoryName = '';
  //   tempErrorSAC.assignedTeamId = 'error';
  //   tempErrorSAC.assignedTeamName = 'Equipe de error';

  //   this.generateSACId( 'INC' )
  //   .then( newSacId => {
  //     tempErrorSAC.id = newSacId;
  //     return this.afs.collection('SACs').doc(tempErrorSAC.id).set(Object.assign({}, tempErrorSAC));
  //   })
  // }

}