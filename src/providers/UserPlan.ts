import { Injectable } from '@angular/core';

import { User } from '../models/User';
import { OrderAcceptance } from '../models/OrderAcceptance';
import { ProposalSubmission } from '../models/ProposalSubmission';
import { BannerPromotion } from '../models/BannerPromotion';
import { TimeProvider } from './Time';
import { AngularFireAuth } from 'angularfire2/auth';
import { PlanAssignment } from '../models/PlanAssignment';
import { Plan } from '../models/Plan';
import { EcoFirestore } from './EcoFirestore/EcoFirestore';

@Injectable()
export class UserPlanProvider {

  private initialized = false;

  public localPlanAssingment: PlanAssignment = new PlanAssignment();
  public localPlan: Plan = new Plan();

  private planAssigmentsSub: any = null;

  constructor(
    private db: EcoFirestore,
    private timeCtrl: TimeProvider,
    private fire: AngularFireAuth
  ) {
  }

  /**
 * Inicia o Provider 
 * @return Retorna uma `Promise<void>`
 */
  public init(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!this.initialized) {
        this.planAssigmentsSub = this.db.afs.collection('PlanAssignments').ref
          .where('providerId', '==', this.fire.auth.currentUser.uid)
          .onSnapshot(UpaDocQuery => {
            if (!UpaDocQuery.empty) {
              return this.timeCtrl.getDate()
                .then(ecoDate => {
                  for (let UpaDoc of UpaDocQuery.docs) {
                    var tempUpa = UpaDoc.data() as PlanAssignment;
                    const expiringDate = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * tempUpa.plan.renovationPeriod);
                    if (expiringDate < tempUpa.createdOn.getTime()) {
                      this.localPlanAssingment = tempUpa;
                      this.localPlan = tempUpa.plan;
                      this.initialized = true;
                      resolve();
                      break;
                    }
                  }
                  if (this.localPlanAssingment.id == '') {
                    console.log('Não existe Atribuição de Plano valida para o usuário!');
                    this.getFreePlan()
                      .then(freePlan => {
                        this.localPlanAssingment.providerId = this.fire.auth.currentUser.uid;
                        this.localPlan = freePlan;
                        this.initialized = true;
                        resolve();
                      })
                      .catch(e => {
                        reject(e);
                      })
                  }
                })
                .catch(e => {
                  reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
                });
            } else {
              console.log('Não existe Atribuição de Plano valida para o usuário!');
              this.getFreePlan()
                .then(freePlan => {
                  this.localPlanAssingment.providerId = this.fire.auth.currentUser.uid;
                  this.localPlan = freePlan;
                  this.initialized = true;
                  resolve();
                })
                .catch(e => {
                  reject(e);
                })
            }
          },
            error => {
              this.initialized = false;
              console.log(`UserPlanProvider says: Error at PlanAssignments subscriber, message: ${error.message} error: ${JSON.stringify(error)}`);
              reject(JSON.stringify({ log: 'Error at PlanAssignments subscriber', message: error.message, error: error }));
            });

      } else {

        if (this.localPlan.id == '') {
          console.log(`UserPlanProvider says: Already initialized, waiting for subscribers to update`);
          setTimeout(() => {
            resolve(this.init());
          }, 500);
        } else {
          resolve();
          console.log(`UserPlanProvider says: Already initialized`);
        }
      }
    });
  }

  /**
   * Agpaga os dados do Provider 
   * @return Retorna uma `Promise<void>`
   */
  unInit(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.initialized) {
        if (this.planAssigmentsSub != null) {
          this.planAssigmentsSub();
          this.planAssigmentsSub = null;
        }
        this.localPlan = new Plan();
        this.initialized = false;
        resolve();
      } else {
        if (this.localPlan.id == '') {
          console.log(`UserPlanProvider says: Already UnInitialized`);
          setTimeout(() => {
            resolve();
          }, 500);
        } else {
          this.initialized = true;
          setTimeout(() => {
            resolve(this.unInit());
          }, 500);
        }
      }
    });
  }

  getFreePlan(): Promise<Plan> {
    return new Promise((resolve, reject) => {
      this.db.afs.collection('Plans').doc('free').ref.get()
        .then(freePlanDoc => {
          resolve(freePlanDoc.data() as Plan);
        })
        .catch(e => {
          console.log('Erro ao ler o plano free!');
          reject(e);
        })
    });
  }


  private failSafe(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.initialized) {
        if (this.localPlanAssingment.providerId == this.fire.auth.currentUser.uid) {
          resolve();
        } else {
          console.log('Usuário local mudou reiniciando Provider.')
          this.unInit()
            .then(() => {
              resolve(this.failSafe());
            })
            .catch(e => {
              reject(e)
            });
        }
      } else {
        this.init()
          .then(() => {
            resolve(this.failSafe());
          })
          .catch(e => {
            reject(e)
          });
      }
    })
  }


  getUserPlan(): Promise<Plan> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          resolve(this.localPlan);
        })
        .catch(e => {
          reject(e)
        });
    });
  }

  //Banners

  /**
   * Verifica se o Usuário passado pode criar pedidos 
   * @param {User} user Usuário
   * @return `boolean`, true se o usuário puder criar anuncios
   */
  canCreateBanner(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxNumOfBanners != null) {
            return this.availableBannersNumber(user)
              .then(num => {
                resolve(num > 0);
              })
              .catch(e => {
                reject(e);
              });
          } else {
            resolve(true);
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * Retorna o numero de anuncios ativos para o usaurio passado como paramentro
   * @param {User} user Usuário
   * @return  Promise<number>, Numero de anuncios ativos
   */
  activedBannersNumber(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.db.afs.collection('Banners').ref
        .where('providerId', '==', user.id)
        .where('status', '<', 13)
        .get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              resolve(querySnapshot.size);
            })
            .catch(e => {
              reject(e);
            });
        })
        .catch(e => {
          reject(e);
        });
    });
  }

  /**
   * Retorna o numero de anuncios que o usaurio pode criar
   * @param {User} user Usuário
   * @return Promise<number>
   */
  availableBannersNumber(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxNumOfBanners != null) {
            return this.db.afs.collection('Banners').ref
              .where('providerId', '==', user.id)
              .where('status', '<', 13)
              .get()
              .then(querySnapshot => {
                this.db.validators.validateQuerySnapshot(querySnapshot)
                  .then(querySnapshot => {
                    resolve(plan.maxNumOfBanners - querySnapshot.size);
                  })
                  .catch(e => {
                    reject(e);
                  });
              })
              .catch(e => {
                reject(JSON.stringify({ log: 'Error on availableBannersNumber query', message: e.message, error: e }));
              });
          } else {
            let e = { message: `Plano ${plan.title}, não possui limite de Anuncios!` };
            reject(JSON.stringify({ log: e.message, message: e.message, error: e }));
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
 * Retorna o a string do numero de anuncios que o usaurio pode criar
 * @param {User} user Usuário
 * @return Promise<string>
 */
  availableBannersStr(user: User): Promise<string> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxNumOfBanners != null) {
            return this.db.afs.collection('Banners').ref
              .where('providerId', '==', user.id)
              .where('status', '<', 13)
              .get()
              .then(querySnapshot => {
                this.db.validators.validateQuerySnapshot(querySnapshot)
                  .then(querySnapshot => {
                    resolve(`${plan.maxNumOfBanners - querySnapshot.size} de ${plan.maxNumOfBanners}`);
                  })
                  .catch(e => {
                    reject(e);
                  });
              })
              .catch(e => {
                reject(JSON.stringify({ log: 'Error on availableBannersNumber query', message: e.message, error: e }));
              });
          } else {
            resolve(`Sem limites`);
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  //Banner Promotion

  /**
   * Verifica se o Usuário passado pode Promover un anuncio 
   * @param {User} user Usuário
   * @return `boolean`, true se o usuário poder promover anuncios
   */
  canPromoteBanner(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.freePromotions != null) {
            return this.availableBannerPromotion(user)
              .then(num => {
                resolve(num > 0);
              })
              .catch(e => {
                reject(e);
              });
          } else {
            resolve(true);
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * Retorna o numero de Anuncios promovidos no ultimo mês
   * @param {User} user Usuário
   * @return  Promise<number>, Anuncios promovidos no ultimo mês
   */
  lastMonthPromotedNumberBanner(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.timeCtrl.getDate()
        .then(ecoDate => {
          const last30DayTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 30);
          return this.db.afs.collection('Users').doc(user.id).collection('BannerPromotionHistory').ref
            .orderBy('timeInMs', 'desc')
            .where('timeInMs', '>', last30DayTime).get()
            .then(querySnapshot => {
              this.db.validators.validateQuerySnapshot(querySnapshot)
                .then(querySnapshot => {
                  resolve(querySnapshot.size);
                })
                .catch(e => {
                  reject(e);
                });
            }).catch(e => {
              reject(JSON.stringify({ log: 'Error on lastMonthPromotedNumberBanner query', message: e.message, error: e }));
            });
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
        });
    });
  }

  /**
   * Retorna o numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
   * @param {User} user Usuário
   * @return Promise<number>, Numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
   */
  availableBannerPromotion(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          return this.timeCtrl.getDate()
            .then(ecoDate => {
              const last30DayTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 30);
              return this.db.afs.collection('Users').doc(user.id).collection('BannerPromotionHistory').ref
                .orderBy('timeInMs', 'desc')
                .where('timeInMs', '>', last30DayTime).get()
                .then(querySnapshot => {
                  this.db.validators.validateQuerySnapshot(querySnapshot)
                    .then(querySnapshot => {
                      resolve(plan.freePromotions - querySnapshot.size);
                    })
                    .catch(e => {
                      reject(e);
                    });
                })
                .catch(e => {
                  reject(JSON.stringify({ log: 'Error on availableBannerPromotion query', message: e.message, error: e }));
                });
            })
            .catch(e => {
              reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
            });
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
   * Registra o envio do Budget
   * @param {string} userId Id do Usuário
   * @param {string} bannerId Id do anuncio
   * @return Promise<any>, Resolvese quando a aceitação for registrada com sucesso!
   */
  registrateBannerPromotion(userId: string, bannerId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.timeCtrl.getDate()
        .then(ecoDate => {
          var tempBannerPromotion: BannerPromotion = new BannerPromotion();
          tempBannerPromotion.bannerId = bannerId;
          tempBannerPromotion.time = ecoDate;
          tempBannerPromotion.timeInMs = ecoDate.getTime();
          resolve(this.db.afs.collection('Users').doc(userId).collection('BannerPromotionHistory').add(Object.assign({}, tempBannerPromotion)))
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
        });
    });
  }

  //Orders

  /**
   * Verifica se o Usuário passado pode Aceitar pedidos 
   * @param {User} user Usuário
   * @return `boolean`, true se o usuário puder aceitar pedidos
   */
  public canAcceptOrder(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxAcceptedOrdersPerDay != null) {
            return this.next24AvailableOrdersAcceptance(user)
              .then(num => {
                resolve(num > 0);
              })
              .catch(e => {
                reject(e);
              });
          } else {
            resolve(true);
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
   * Retorna o numero de pedidos aceitos nas ultimas 24 h
   * @param {User} user Usuário
   * @return  Promise<number>, Numero de pedidos aceitos nas ultimas 24 h
   */
  public last24hAcceptedOrderNumber(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.timeCtrl.getDate()
        .then(ecoDate => {
          const last24hTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1);
          return this.db.afs.collection('Users').doc(user.id).collection('OrderAcceptanceHistory').ref
            .orderBy('timeInMs', 'desc')
            .where('timeInMs', '>', last24hTime).get()
            .then(querySnapshot => {
              this.db.validators.validateQuerySnapshot(querySnapshot)
                .then(querySnapshot => {
                  resolve(querySnapshot.size);
                })
                .catch(e => {
                  reject(e);
                });
            })
            .catch(e => {
              reject(JSON.stringify({ log: 'Error on last24hAcceptedOrderNumber query', message: e.message, error: e }));
            });
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
        });
    });
  }

  /**
   * Retorna o numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
   * @param {User} user Usuário
   * @return Promise<number>, Numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
   */
  public next24AvailableOrdersAcceptance(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxAcceptedOrdersPerDay != null) {
            return this.timeCtrl.getDate()
              .then(ecoDate => {
                const last24hTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1);
                return this.db.afs.collection('Users').doc(user.id).collection('OrderAcceptanceHistory').ref
                  .orderBy('timeInMs', 'desc')
                  .where('timeInMs', '>', last24hTime).get()
                  .then(querySnapshot => {
                    this.db.validators.validateQuerySnapshot(querySnapshot)
                      .then(querySnapshot => {
                        resolve(plan.maxAcceptedOrdersPerDay - querySnapshot.size);
                      })
                      .catch(e => {
                        reject(e);
                      });
                  })
                  .catch(e => {
                    reject(JSON.stringify({ log: 'Error on next24AvailableOrdersAcceptance query', message: e.message, error: e }));
                  });
              })
              .catch(e => {
                reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
              });
          } else {
            let e = { message: `Plano ${plan.title}, não possui limite de aceitação de pedidos!` };
            reject(JSON.stringify({ log: e.message, message: e.message, error: e }));
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }


  /**
 * Retorna o numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
 * @param {User} user Usuário
 * @return Promise<number>, Numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
 */
  public next24AvailableOrdersAcceptanceStr(user: User): Promise<string> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxAcceptedOrdersPerDay != null) {
            return this.timeCtrl.getDate()
              .then(ecoDate => {
                const last24hTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1);
                return this.db.afs.collection('Users').doc(user.id).collection('OrderAcceptanceHistory').ref
                  .orderBy('timeInMs', 'desc')
                  .where('timeInMs', '>', last24hTime).get()
                  .then(querySnapshot => {
                    this.db.validators.validateQuerySnapshot(querySnapshot)
                      .then(querySnapshot => {
                        resolve(`${plan.maxAcceptedOrdersPerDay - querySnapshot.size} de ${plan.maxAcceptedOrdersPerDay}`);
                      })
                      .catch(e => {
                        reject(e);
                      });
                  })
                  .catch(e => {
                    reject(JSON.stringify({ log: 'Error on next24AvailableOrdersAcceptance query', message: e.message, error: e }));
                  });
              })
              .catch(e => {
                reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
              });
          } else {
            resolve(`Sem limites`);
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
   * Registra a aceitação do pedido
   * @param {string} userId Id do Usuário
   * @param {string} orderId Id do pedido
   * @return Promise<any>, Resolvese quando a aceitação for registrada com sucesso!
   */
  registrateOrderAcceptance(userId: string, orderId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.timeCtrl.getDate()
        .then(ecoDate => {
          var tempOrderAcceptance: OrderAcceptance = new OrderAcceptance();
          tempOrderAcceptance.orderId = orderId;
          tempOrderAcceptance.time = ecoDate;
          tempOrderAcceptance.timeInMs = ecoDate.getTime();
          resolve(this.db.afs.collection('Users').doc(userId).collection('OrderAcceptanceHistory').add(Object.assign({}, tempOrderAcceptance)));
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
        });
    });
  }

  //Proposals

  /**
   * Verifica se o Usuário passado pode evniar uma proposta para uma solicitação 
   * @param {User} user Usuário
   * @return `boolean`, true se o usuário puder aceitar pedidos
   */
  canSubmitProposal(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxSentProposalsPerDay != null) {
            return this.next24AvailableProposalSubmission(user)
              .then(num => {
                resolve(num > 0);
              })
              .catch(e => {
                reject(e);
              });
          } else {
            resolve(true);
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * Retorna o numero de pedidos aceitos nas ultimas 24 h
   * @param {User} user Usuário
   * @return  Promise<number>, Numero de pedidos aceitos nas ultimas 24 h
   */
  last24SubmittedProposalNumber(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.timeCtrl.getDate()
        .then(ecoDate => {
          const last24hTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1);
          return this.db.afs.collection('Users').doc(user.id).collection('ProposalSubmissionHistory').ref
            .orderBy('timeInMs', 'desc')
            .where('timeInMs', '>', last24hTime).get()
            .then(querySnapshot => {
              resolve(querySnapshot.size);
            })
            .catch(e => {
              reject(JSON.stringify({ log: 'Error on last24SubmittedProposalNumber query', message: e.message, error: e }));
            });
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
        });
    });
  }

  /**
   * Retorna o numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
   * @param {User} user Usuário
   * @return Promise<number>, Numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
   */
  next24AvailableProposalSubmission(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxSentProposalsPerDay != null) {
            return this.timeCtrl.getDate()
              .then(ecoDate => {
                const last24hTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1);
                return this.db.afs.collection('Users').doc(user.id).collection('ProposalSubmissionHistory').ref
                  .orderBy('timeInMs', 'desc')
                  .where('timeInMs', '>', last24hTime).get()
                  .then(querySnapshot => {
                    this.db.validators.validateQuerySnapshot(querySnapshot)
                      .then(querySnapshot => {
                        resolve(plan.maxSentProposalsPerDay - querySnapshot.size);
                      })
                      .catch(e => {
                        reject(e);
                      });
                  })
                  .catch(e => {
                    reject(JSON.stringify({ log: 'Error on next24AvailableProposalSubmission query', message: e.message, error: e }));
                  });
              })
              .catch(e => {
                reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
              });
          } else {
            let e = { message: `Plano ${plan.title}, não possui limite de aceitação de envio de propostas!` };
            reject(JSON.stringify({ log: e.message, message: e.message, error: e }));
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
 * Retorna o numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
 * @param {User} user Usuário
 * @return Promise<number>, Numero de pedidos que o usaurio ainda pode aceitar nas proximas 24 h
 */
  next24AvailableProposalSubmissionStr(user: User): Promise<string> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxSentProposalsPerDay != null) {
            return this.timeCtrl.getDate()
              .then(ecoDate => {
                const last24hTime = ecoDate.getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1);
                return this.db.afs.collection('Users').doc(user.id).collection('ProposalSubmissionHistory').ref
                  .orderBy('timeInMs', 'desc')
                  .where('timeInMs', '>', last24hTime).get()
                  .then(querySnapshot => {
                    this.db.validators.validateQuerySnapshot(querySnapshot)
                      .then(querySnapshot => {
                        resolve(`${plan.maxSentProposalsPerDay - querySnapshot.size} de ${plan.maxSentProposalsPerDay}`);
                      })
                      .catch(e => {
                        reject(e);
                      });
                  })
                  .catch(e => {
                    reject(JSON.stringify({ log: 'Error on next24AvailableProposalSubmission query', message: e.message, error: e }));
                  });
              })
              .catch(e => {
                reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
              });
          } else {
            resolve(`Sem limites`);
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
   * Registra o envio da proposta
   * @param {string} providerId Id do Usuário
   * @param {string} requestId Id da solicitação
   * @param {string} proposalId Id da proposta
   * @return Promise<any>, Resolvese quando a aceitação for registrada com sucesso!
   */
  registrateProposalSubmission(providerId: string, requestId: string, proposalId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.timeCtrl.getDate()
        .then(ecoDate => {
          var tempProposalSubmission: ProposalSubmission = new ProposalSubmission();
          tempProposalSubmission.time = ecoDate;
          tempProposalSubmission.timeInMs = ecoDate.getTime();
          tempProposalSubmission.requestId = requestId;
          tempProposalSubmission.proposalId = proposalId;
          resolve(this.db.afs.collection('Users').doc(providerId).collection('ProposalSubmissionHistory').add(Object.assign({}, tempProposalSubmission)))
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error on getDate', message: e.message, error: e }));
        });
    });
  }

  //Proposals

  /**
   * Verifica se o Usuário passado pode se inscrever em um tipo de serviço 
   * @param {User} user Usuário
   * @return `boolean`, true se o usuário puder aceitar pedidos
   */
  canSubcribeToServiceType(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxServiceTypesSub != null) {
            return this.subcribedServiceTypeNumber(user.id)
              .then(num => {
                resolve(num < plan.maxServiceTypesSub);
              })
              .catch(e => {
                reject(e);
              });
          } else {
            resolve(true);
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * Retorna o numero de inscrições para o userId passado
   * @param {string} providerId provider Id
   * @return  Promise<number>, Retorna o numero de inscrições para o userId passado
   */
  subcribedServiceTypeNumber(providerId: string): Promise<number> {
    return new Promise((resolve, reject) => {
      return this.db.afs.collection('ServiceTypeSubs').ref
        .where('providerId', '==', providerId).get()
        .then(querySnapshot => {
          this.db.validators.validateQuerySnapshot(querySnapshot)
            .then(querySnapshot => {
              resolve(querySnapshot.size);
            })
            .catch(e => {
              reject(e);
            });
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error on last24SubmittedProposalNumber query', message: e.message, error: e }));
        });
    });
  }

  /**
 * Retorna a string relatova a quantidade de inscrições de tipos de serviços disponiveis 
 * @param {User} user Usuário
 * @return `string`, true se o usuário puder aceitar pedidos
 */
  availableServiceTypeSubsStr(user: User): Promise<string> {
    return new Promise((resolve, reject) => {
      return this.getUserPlan()
        .then(plan => {
          if (plan.maxServiceTypesSub != null) {
            return this.subcribedServiceTypeNumber(user.id)
              .then(num => {
                resolve(`${plan.maxServiceTypesSub - num} de ${plan.maxServiceTypesSub}`);
              })
              .catch(e => {
                reject(e);
              });
          } else {
            resolve('Sem limites');
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

}