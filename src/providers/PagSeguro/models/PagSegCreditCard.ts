import { PagSegCreditCardHolder } from "./PagSegCreditCardHolder";

export class PagSegCreditCard {
      
      token: string; //REQUIRED Token retornado no método Javascript PagSeguroDirectPayment.createCardToken().
      holder: PagSegCreditCardHolder; // Dados do portador do cartão.


      public constructor(init?: Partial<PagSegCreditCard>) {
            
            this.token = '';
            this.holder = new PagSegCreditCardHolder();

            
            if (init) {
            Object.assign(this, init);
            }
      }
}