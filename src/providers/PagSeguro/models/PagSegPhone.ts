export class PagSegPhone {
      
      areaCode: string; //REQUIRED DDD do comprador. Formato: Um número de 2 dígitos correspondente a um DDD 
      number: string; //REQUIRED Número do telefone do comprador. Formato: Um número entre 7 e 9 dígitos.      

      public constructor(init?: Partial<PagSegPhone>) {
            
            this.areaCode = '';
            this.number = '';
            
            if (init) {
            Object.assign(this, init);
            }
      }
}