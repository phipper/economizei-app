import { CreditCardBrandConfig } from "./CreditCardBrandConfig";

export class CreditCardBrandData {

      bin: number;
      config: CreditCardBrandConfig;
      cvvSize: number;
      name: string;
      expirable: boolean;
      international: boolean;
      validationAlgorithm: string;


      public constructor(init?: Partial<CreditCardBrandData>) {

            this.bin = 0;
            this.config = new CreditCardBrandConfig();
            this.cvvSize = 0;
            this.name = '';
            this.expirable = false;
            this.international = false;
            this.validationAlgorithm = '';


            if (init) {
                  Object.assign(this, init);
            }
      }
}