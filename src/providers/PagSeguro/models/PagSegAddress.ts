
//Dados do assinante.
export class PagSegAddress {

      street: string; //REQUIRED Nome da rua. Formato: Livre, com limite de 80 caracteres.
      number: string; //REQUIRED Número. Formato: Livre, com limite de 20 caracteres.
      complement: string; //REQUIRED Complemento (bloco, apartamento, etc.). Formato: Livre, com limite de 40 caracteres.
      district: string; //REQUIRED Bairro. Formato: Livre, com limite de 60 caracteres.
      city: string; //REQUIRED Cidade. Formato: Livre. Deve ser um nome válido de cidade do Brasil, com no mínimo 2 e no máximo 60 caracteres.
      state: string; //REQUIRED Estado. Formato: Duas letras, representando a sigla do estado brasileiro correspondente.
      country: string; //REQUIRED País.
      postalCode: string;// CEP. Formato: Um número de 8 dígitos.

      public constructor(init?: Partial<PagSegAddress>) {

            this.street = '';
            this.number = '';
            this.complement = '';
            this.district = '';
            this.city = '';
            this.state = '';
            this.country = 'BRA';
            this.postalCode = '';

            if (init) {
                  Object.assign(this, init);
            }
      }
}