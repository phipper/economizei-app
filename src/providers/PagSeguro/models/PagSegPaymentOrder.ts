import { PagSegPaymentTransaction } from "./PagSegPaymentTransaction";
import { PagSegPaymentDiscount } from "./PagSegPaymentDiscount";

export class PagSegPaymentOrder {

      code: string;
      status: number;
      amount: number;
      grossAmount: number;
      lastEventDate: Date;
      schedulingDate: Date;
      transactions: PagSegPaymentTransaction[];
      discount: PagSegPaymentDiscount;

      public constructor(init?: Partial<PagSegPaymentOrder>) {

            this.code = '';
            this.status = 0;
            this.amount = 0;
            this.grossAmount = 0;
            this.lastEventDate = new Date();
            this.schedulingDate = new Date();
            this.transactions = [];
            this.discount = new PagSegPaymentDiscount();


            if (init) {
                  Object.assign(this, init);
            }
      }
}