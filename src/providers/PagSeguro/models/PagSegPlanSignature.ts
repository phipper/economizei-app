import { PagSegSender } from "./PagSegSender";
import { PagSegPaymentMethod } from "./PagSegPaymentMethod";

export class PagSegPlanSignature {


      plan: string; //REQUIRED Código do plano ao qual a assinatura será vinculada.
      reference: string; // Código de referência da assinatura no seu sistema. 

      sender: PagSegSender;
      paymentMethod: PagSegPaymentMethod; // Dados do pagamento.

      public constructor(init?: Partial<PagSegPlanSignature>) {

            this.plan = '';
            this.reference = '';

            this.sender = new PagSegSender();
            this.paymentMethod = new PagSegPaymentMethod();

            if (init) {
                  Object.assign(this, init);
            }
      }
}