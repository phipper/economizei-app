export class CreditCardBrandConfig {

      acceptedLengths: number[];
      hasCvv: boolean;
      hasDueDate: boolean;
      hasPassword: boolean;
      securityFieldLength: number;


      public constructor(init?: Partial<CreditCardBrandConfig>) {

            this.acceptedLengths = [];
            this.hasCvv = false;
            this.hasDueDate = false;
            this.hasPassword = false;
            this.securityFieldLength = 0;

            if (init) {
                  Object.assign(this, init);
            }
      }
}