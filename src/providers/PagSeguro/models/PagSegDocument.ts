
//Dados do assinante.
export class PagSegDocument {
      
      type: string; //REQUIRED Tipo de documento 
      value: string; //REQUIRED CPF. Formato: Um número de 11 dígitos.      

      public constructor(init?: Partial<PagSegDocument>) {
            
            this.type = 'CPF';
            this.value = '';
            
            if (init) {
            Object.assign(this, init);
            }
      }
}