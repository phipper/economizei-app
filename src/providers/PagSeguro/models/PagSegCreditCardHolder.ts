import { PagSegPhone } from "./PagSegPhone";
import { PagSegAddress } from "./PagSegAddress";
import { PagSegDocument } from "./PagSegDocument";

//Dados do assinante.
export class PagSegCreditCardHolder {
      
      name: string; //REQUIRED Nome conforme impresso no cartão de crédito. Formato: No mínimo 1 e no máximo 50 caracteres.
      birthDate: string; //REQUIRED Data de nascimento do dono do cartão de crédito. Formato: dd/MM/yyyy.

      documents: PagSegDocument[];// Documentos do dono do cartão.
      phone: PagSegPhone; // Telefone do dono do cartão.
      billingAddress: PagSegAddress; // Endereço de Cobrança.

      public constructor(init?: Partial<PagSegCreditCardHolder>) {
            
            this.name = '';
            this.birthDate = '';

            this.documents = [];
            this.phone = new PagSegPhone();
            this.billingAddress = new PagSegAddress();
            

            if (init) {
            Object.assign(this, init);
            }
      }
}