export class PagSegChangePlanSignatureCCSender {

      hash: string;
      ip: string;

      public constructor(init?: Partial<PagSegChangePlanSignatureCCSender>) {

            this.hash = '';
            this.ip = '';

            if (init) {
                  Object.assign(this, init);
            }
      }
}