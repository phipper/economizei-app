import { PagSegChangePlanSignatureCCSender } from "./PagSegChangePlanSignatureCCSender";
import { PagSegCreditCard } from "./PagSegCreditCard";

export class PagSegChangePlanSignatureCC {


      type: string; //REQUIRED Tipo do meio de pagamento utilizado na assinatura.
      sender: PagSegChangePlanSignatureCCSender;
      creditCard: PagSegCreditCard; // Dados do Cartão.


      public constructor(init?: Partial<PagSegChangePlanSignatureCC>) {

            this.type = '';
            this.sender = new PagSegChangePlanSignatureCCSender();
            this.creditCard = new PagSegCreditCard();

            if (init) {
                  Object.assign(this, init);
            }
      }
}