import { PagSegPhone } from "./PagSegPhone";
import { PagSegAddress } from "./PagSegAddress";
import { PagSegDocument } from "./PagSegDocument";

//Dados do assinante.
export class PagSegSender {
      
      name: string; //REQUIRED Nome completo do consumidor
      email: string; //REQUIRED E-mail do consumidor
      ip: string; //REQUIRED Endereços de IP de origem do consumir. Obrigatório. Formato: 4 números, de 0 a 255, separados por ponto.
      hash: string; //REQUIRED Identificador (fingerprint) gerado pelo vendedor por meio do JavaScript do PagSeguro. Obrigatório. Formato: Obtido a partir do método Javscript PagseguroDirectPayment.getSenderHash().
      
      phone: PagSegPhone; //Telefone do consumidor.
      address: PagSegAddress; // Endereço do consumidor.
      documents: PagSegDocument[];// Documentos do consumidor.

      public constructor(init?: Partial<PagSegSender>) {
            
            this.name = '';
            this.email = '';
            this.ip = '';
            this.hash = '';
            this.phone = new PagSegPhone();
            this.address = new PagSegAddress();
            this.documents = [];
            

            if (init) {
            Object.assign(this, init);
            }
      }
}