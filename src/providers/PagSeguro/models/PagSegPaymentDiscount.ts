export class PagSegPaymentDiscount {


    type: string;
    value: number;

    public constructor(init?: Partial<PagSegPaymentDiscount>) {

        this.type = '';
        this.value = 0;

        if (init) {
            Object.assign(this, init);
        }
    }
}