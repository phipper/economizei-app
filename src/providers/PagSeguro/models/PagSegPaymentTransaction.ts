export class PagSegPaymentTransaction {


    code: string;
    date: Date;
    status: number;

    public constructor(init?: Partial<PagSegPaymentTransaction>) {

        this.code = '';
        this.date = new Date();
        this.status = 0;

        if (init) {
            Object.assign(this, init);
        }
    }
}