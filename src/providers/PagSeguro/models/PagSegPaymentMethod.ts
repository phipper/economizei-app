import { PagSegCreditCard } from "./PagSegCreditCard";

export class PagSegPaymentMethod {
      
      type: string; //REQUIRED Tipo do meio de pagamento utilizado na assinatura.
      creditCard: PagSegCreditCard; // Dados do Cartão.

      public constructor(init?: Partial<PagSegPaymentMethod>) {
            
            this.type = 'CREDITCARD';
            this.creditCard = new PagSegCreditCard();
            
            if (init) {
            Object.assign(this, init);
            }
      }
}