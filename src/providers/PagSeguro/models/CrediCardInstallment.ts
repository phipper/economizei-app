export class CrediCardInstallment {

      installmentAmount: number;
      interestFree: boolean;
      quantity: number;
      totalAmount: number;

      public constructor(init?: Partial<CrediCardInstallment>) {

            this.installmentAmount = 0;
            this.interestFree = true;
            this.quantity = 0;
            this.totalAmount = 0;

            if (init) {
                  Object.assign(this, init);
            }
      }
}