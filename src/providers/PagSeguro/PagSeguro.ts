import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { CrediCardInstallment } from './models/CrediCardInstallment';
import { CreditCardBrandData } from './models/CreditCardBrandData';
import { PagSegPlanSignature } from './models/PagSegPlanSignature';
import { PagSegDocument } from './models/PagSegDocument';

import { User } from '../../models/User';
import { CreditCard } from '../../models/CreditCard';
import { Plan } from '../../models/Plan';
import { PlanSignature } from '../../models/PlanSignature';
import { Address } from '../../models/Address';
import { PagSegPaymentOrder } from './models/PagSegPaymentOrder';
import { PagSegChangePlanSignatureCC } from './models/PagSegChangePlanSignatureCC';

declare var PagSeguroDirectPayment: any;

var IS_SAND_BOX = false;

// CLASSE PARA ARMAZENAR NOSSOS DADOS DE ACESSO A CONTA DO PAGSEGURO
export class Credencial {
  key: string;
  urlPagSeguroDirectPayment: string;
  idSession: string;
}

@Injectable()
export class PagseguroPgtoServiceProvider {

  private apiKey = "1f5fe452-fc35-46b5-a574-930de7bfd595";
  private credencial: Credencial = new Credencial();

  // public dados = new Dados();

  private initialized = false;
  private initiating = true;

  constructor(
    private http: Http,
    private datepipe: DatePipe
  ) {
  }

  /**
   * Carrega o JS do PagSeguro para a Variavel Local   
   * @return Retorna uma `Promise<void>`
   */
  private loadPagSeguroDirectPayment(): Promise<void> {
    return new Promise((resolve, reject) => {
      let script: HTMLScriptElement = document.createElement('script');
      script.addEventListener('load', r => resolve());
      script.src = this.credencial.urlPagSeguroDirectPayment;
      document.head.appendChild(script);
    });
  }

  /**
   * Retorna o id da sessão que será utilizada pela API
   * Este é um id gerado pela API DO PagSeguro para fazer o consumo para concretizar a transação 
   * @return {Promise<string>} `Promise<string>`
   */
  private getSessionId(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://us-central1-economizeiapp.cloudfunctions.net/PagSegService_SessionId?key=${this.apiKey}`).toPromise()
        .then(response => {
          resolve(response.json().sessionId)
        })
        .catch(errorResponse => {
          reject(errorResponse);
        });
    });
  }

  private removeCaracs(str: string): string {
    return str.replace(/\D/g, "");
  }

  private stateuToUf(state: string): string {
    switch (state) {
      case 'Acre':
        return 'AC';
      case 'Alagoas':
        return 'AL';
      case 'Amapá':
        return 'AP';
      case 'Amazonas':
        return 'AM';
      case 'Bahia':
        return 'BA';
      case 'Ceará':
        return 'CE';
      case 'Distrito Federal':
        return 'DF';
      case 'Espírito Santo':
        return 'ES';
      case 'Goiás':
        return 'GO';
      case 'Maranhão':
        return 'MA';
      case 'Mato Grosso':
        return 'MT';
      case 'Mato Grosso do Sul':
        return 'MS';
      case 'Minas Gerais':
        return 'MG';
      case 'Pará':
        return 'PA';
      case 'Paraíba':
        return 'PB';
      case 'Paraná':
        return 'PR';
      case 'Pernambuco':
        return 'PE';
      case 'Piauí':
        return 'PI';
      case 'Rio de Janeiro':
        return 'RJ';
      case 'Rio Grande do Norte':
        return 'RN';
      case 'Rio Grande do Sul':
        return 'RS';
      case 'Rondônia':
        return 'RO';
      case 'Roraima':
        return 'RR';
      case 'Santa Catarina':
        return 'SC';
      case 'São Paulo':
        return 'SP';
      case 'Sergipe':
        return 'SE';
      case 'Tocantins':
        return 'TO';
      default:
        return '';
    }
  }

  /**
   * Inicia o Provider   
   * @return Retorna uma `Promise<void>`
   */
  public init(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!this.initialized) {
        this.initiating = true;

        if (IS_SAND_BOX) {
          this.credencial.urlPagSeguroDirectPayment = "https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
        } else {
          this.credencial.urlPagSeguroDirectPayment = "https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
        }

        this.credencial.key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");

        this.getSessionId()
          .then(sessionId => {
            this.credencial.idSession = sessionId;
            this.loadPagSeguroDirectPayment()
              .then(() => {
                PagSeguroDirectPayment.setSessionId(this.credencial.idSession);
                console.log('credencial: ' + JSON.stringify(this.credencial));
                console.log(PagSeguroDirectPayment);
                this.initialized = true;
                this.initiating = false;
                console.log('PagseguroPgtoServiceProvider says: Provider initialized');
                resolve();
              });
          })
          .catch(e => {
            reject(e);
          })
      } else {
        console.log('PagseguroPgtoServiceProvider says: already initiated');
        resolve();
      }
    })
  }

  private failSafe(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.initialized) {
        resolve()
      } else {
        if (!this.initiating) {
          this.init()
            .then(() => {
              resolve(this.failSafe());
            })
            .catch(e => {
              reject(e);
            })
        } else {
          console.log('PagseguroPgtoServiceProvider says: already initiating waiting for initialization to complete');
          setTimeout(() => {
            resolve(this.failSafe());
          }, 500);
        }
      }
    })
  }

  /**
   * Retorna a Bandeira do Cartão de credito
   * @param {string} creditCardNum Numero do cartão
   * @return Retorna uma `Promise<CreditCardBrandData>`
   */
  public getCreditCardBrandData(creditCardNum: string): Promise<CreditCardBrandData> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          PagSeguroDirectPayment.getBrand({
            cardBin: creditCardNum,
            success: response => {
              resolve(response.brand as CreditCardBrandData);
            },
            error: response => {
              reject(response);
            }
          });
        })
        .catch(e => {
          reject(e);
        })
    });
  }


  /**
   * Retorna as parcelas que a Bandeira do cartão oferece
   * @param {number} amount valor total da compra
   * @param {string} cardBrand bandeira do cartão
   * @param {string} maxInstallmentNoInterest numero de parcelas sem juros
   * @return Retorna uma `Promise<CrediCardInstallment[]>`
   */
  public getPayInstallments(amount: number, cardBrand: string, maxInstallmentNoInterest: number): Promise<CrediCardInstallment[]> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          PagSeguroDirectPayment.getInstallments({
            amount: amount,
            brand: cardBrand,
            maxInstallmentNoInterest: maxInstallmentNoInterest,
            success: response => {
              resolve(response.installments[cardBrand] as CrediCardInstallment[]);
            },
            error: response => {
              reject(response);
            }
          });
        })
        .catch(e => {
          reject(e);
        })
    });
  }


  /**
   * Retorna as parcelas que a Bandeira do cartão oferece
   * @param {number} cardNumber numero do cartão
   * @param {number} cardCvv codigo de segurança do cartão
   * @param {number} expirationMonth Mês de expiração
   * @param {number} expirationYear Ano de expiração do cartão com 4 digitos ex: '2019' 
   * @param {string} cardBrand bandeira do cartão
   * @return Retorna uma `Promise<string>`
   */
  private getCreditCardToken(cardNumber: string, cardCvv: string, expirationMonth: string, expirationYear: string, CardBrand: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          PagSeguroDirectPayment.createCardToken({
            cardNumber: cardNumber,
            cvv: cardCvv,
            expirationMonth: expirationMonth,
            expirationYear: expirationYear,
            brand: CardBrand,
            success: response => {
              resolve(response.card.token);
            },
            error: response => {
              reject(response);
            }
          });
        })
        .catch(e => {
          reject(e);
        })
    });
  }


  /**
 * Retorna a hash code do comprador ativo
 * @return Retorna uma `Promise<string>`
 */
  private getSenderHash(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          PagSeguroDirectPayment.onSenderHashReady(function (response) {
            if (response.status == 'success') {
              resolve(response.senderHash);
            } else {
              reject(response.message);
              console.log(response.message);
            }
          });
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
  * Verifica se o numero do cartão é valido
  * @param {number} number numero do cartão
  * @param {string} brand bandeira do cartão
  * @return Retorna uma `Promise<boolean>`
  */
  public validateCreditCardNumber(number: string, brand: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          PagSeguroDirectPayment.createCardToken({
            cardNumber: number,
            cvv: '999',
            expirationMonth: '11',
            expirationYear: '2011',
            brand: brand,
            success: response => {
              resolve(true);
            },
            error: response => {
              resolve(false);
            }
          });
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  private generateReference(userId: string): string {
    var s = '';
    var r = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789';

    for (var i = 0; i < 11; i++) {
      s += r.charAt(Math.floor(Math.random() * r.length));
    }
    return `${s}${userId}`;
  }

  /**
  * Registra a assinatura do Plano
  * @param {Plan} plan Plano ao qual deve ser feita a adesão 
  * @param {User} user Usuário
  * @param {Address} userAdrees Endereço do usuário
  * @param {CreditCard} creditCard Cartão de Credito que deverá ser utilizado para assinatura do plano 
  * @param {string} creditCardCvv Codigo de segurança do cartão
  * @return Retorna uma `Promise<boolean>`
  */
  public registerPlanSignature(plan: Plan, user: User, userAdrees: Address, creditCard: CreditCard, creditCardCvv: string): Promise<void> {
    return new Promise((resolve, reject) => {

      var tempPlanSignature: PlanSignature = new PlanSignature();

      tempPlanSignature.userId = user.id;
      tempPlanSignature.status = 'PENDING_PROCESSING';
      tempPlanSignature.plan = Object.assign({}, plan);
      tempPlanSignature.ceditCard = Object.assign({}, creditCard);
      tempPlanSignature.reference = this.generateReference(user.id);

      var tempPagSegData: PagSegPlanSignature = new PagSegPlanSignature();

      tempPagSegData.plan = plan.pagSegCode;
      tempPagSegData.reference = tempPlanSignature.reference;

      tempPagSegData.sender.name = user.fullName;
      tempPagSegData.sender.email = user.email;

      var tempSenderDocument: PagSegDocument = new PagSegDocument();
      tempSenderDocument.type = 'CPF';
      tempSenderDocument.value = this.removeCaracs(user.cpf);
      tempPagSegData.sender.documents.push(tempSenderDocument);

      var tempCellPhone = this.removeCaracs(user.cellPhone);

      tempPagSegData.sender.phone.areaCode = tempCellPhone.substr(0, 2);
      tempPagSegData.sender.phone.number = tempCellPhone.substr(2, 9);

      tempPagSegData.sender.address.city = userAdrees.city;
      tempPagSegData.sender.address.complement = userAdrees.complemento;
      tempPagSegData.sender.address.country = 'BRA';
      tempPagSegData.sender.address.district = userAdrees.bairro;
      tempPagSegData.sender.address.number = userAdrees.numero;
      tempPagSegData.sender.address.postalCode = userAdrees.cep;
      tempPagSegData.sender.address.state = this.stateuToUf(userAdrees.state);
      tempPagSegData.sender.address.street = userAdrees.street;

      tempPagSegData.paymentMethod.type = 'CREDITCARD';

      tempPagSegData.paymentMethod.creditCard.holder.name = creditCard.ownerName;
      tempPagSegData.paymentMethod.creditCard.holder.birthDate = creditCard.ownerBirthDate;

      tempPagSegData.paymentMethod.creditCard.holder.phone.areaCode = tempPagSegData.sender.phone.areaCode;
      tempPagSegData.paymentMethod.creditCard.holder.phone.number = tempPagSegData.sender.phone.number;

      tempPagSegData.paymentMethod.creditCard.holder.billingAddress = tempPagSegData.sender.address;

      var tempCreditCardHolderDocument: PagSegDocument = new PagSegDocument();
      tempCreditCardHolderDocument.type = 'CPF';
      tempCreditCardHolderDocument.value = this.removeCaracs(creditCard.ownerCPF);
      tempPagSegData.paymentMethod.creditCard.holder.documents.push(tempCreditCardHolderDocument);

      this.getSenderHash()
        .then(hash => {
          tempPagSegData.sender.hash = hash;
          this.getCreditCardToken(creditCard.number, creditCardCvv, creditCard.expirationMonth, creditCard.expirationYear, creditCard.brand)
            .then(cardToken => {

              tempPagSegData.paymentMethod.creditCard.token = cardToken;

              var body = JSON.stringify({ planSignature: tempPlanSignature, pagSegData: tempPagSegData });
              this.http.post(`https://us-central1-economizeiapp.cloudfunctions.net/PagSegService_PlanSubscriber?key=${this.apiKey}`, body).toPromise()
                .then(response => {
                  resolve()
                })
                .catch(errorResponse => {

                  let error = JSON.parse(errorResponse._body);

                  if (error.errosCodes.indexOf('10024') != -1) {
                    let tempUser = Object.assign({}, user);
                    tempUser.email = `financas.economizei@gmail.com`;
                    resolve(this.registerPlanSignature(plan, tempUser, userAdrees, creditCard, creditCardCvv))
                  } else {
                    reject(error);
                  }

                });
            })
        })
        .catch(e => {
          reject(e);
        });
    })
  }

  /**
 * Consulta as orderns de pagamento para a assinatura
 * @param {string} planSignatureId id ad assinatura 
 * @return Retorna uma `Promise<PagSegPaymentOrder[]>`
 */
  public getPlanSignaturePaymentOrders(planSignatureId: string): Promise<PagSegPaymentOrder[]> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://us-central1-economizeiapp.cloudfunctions.net/PagSegService_GetPaymentOrders?key=${this.apiKey}&planSignatureId=${planSignatureId}`).toPromise()
        .then(response => {
          resolve(response.json());
        })
        .catch(errorResponse => {
          reject(JSON.parse(errorResponse._body));
        });
    })
  }

  /**
* Cancelar a assinatura
* @param {string} planSignatureId id ad assinatura 
* @return Retorna uma `Promise<void>`
*/
  public cancelPlanSignature(planSignatureId: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://us-central1-economizeiapp.cloudfunctions.net/PagSegService_PlanUnSubscriber?key=${this.apiKey}&planSignatureId=${planSignatureId}`).toPromise()
        .then(response => {
          resolve();
        })
        .catch(errorResponse => {
          reject(JSON.parse(errorResponse._body));
        });
    })
  }

  /**
* Altera o cartão da assinatura
* @param {string} planSignatureId Id da assinatura que deverá ser alterado o cartão 
* @param {User} user Usuário
* @param {Address} userAdrees Endereço do usuário
* @param {CreditCard} creditCard Cartão de Credito que deverá ser utilizado para assinatura do plano 
* @param {string} creditCardCvv Codigo de segurança do cartão
* @return Retorna uma `Promise<boolean>`
*/
  public changePlanSignatureCard(planSignatureId: string, user: User, userAdrees: Address, creditCard: CreditCard, creditCardCvv: string): Promise<void> {
    return new Promise((resolve, reject) => {

      var tempPagSegData: PagSegChangePlanSignatureCC = new PagSegChangePlanSignatureCC();
      tempPagSegData.type = 'CREDITCARD';

      tempPagSegData.creditCard.holder.name = creditCard.ownerName;
      tempPagSegData.creditCard.holder.birthDate = creditCard.ownerBirthDate;

      var tempCellPhone = this.removeCaracs(user.cellPhone);
      tempPagSegData.creditCard.holder.phone.areaCode = tempCellPhone.substr(0, 2);
      tempPagSegData.creditCard.holder.phone.number = tempCellPhone.substr(2, 9);

      tempPagSegData.creditCard.holder.billingAddress.city = userAdrees.city;
      tempPagSegData.creditCard.holder.billingAddress.complement = userAdrees.complemento;
      tempPagSegData.creditCard.holder.billingAddress.country = 'BRA';
      tempPagSegData.creditCard.holder.billingAddress.district = userAdrees.bairro;
      tempPagSegData.creditCard.holder.billingAddress.number = userAdrees.numero;
      tempPagSegData.creditCard.holder.billingAddress.postalCode = userAdrees.cep;
      tempPagSegData.creditCard.holder.billingAddress.state = this.stateuToUf(userAdrees.state);
      tempPagSegData.creditCard.holder.billingAddress.street = userAdrees.street;

      var tempCreditCardHolderDocument: PagSegDocument = new PagSegDocument();
      tempCreditCardHolderDocument.type = 'CPF';
      tempCreditCardHolderDocument.value = this.removeCaracs(creditCard.ownerCPF);
      tempPagSegData.creditCard.holder.documents.push(tempCreditCardHolderDocument);

      this.getSenderHash()
        .then(hash => {
          tempPagSegData.sender.hash = hash;
          this.getCreditCardToken(creditCard.number, creditCardCvv, creditCard.expirationMonth, creditCard.expirationYear, creditCard.brand)
            .then(cardToken => {

              tempPagSegData.creditCard.token = cardToken;

              var body = JSON.stringify({ planSignatureId: planSignatureId, newCreditCard: creditCard, pagSegData: tempPagSegData });
              this.http.post(`https://us-central1-economizeiapp.cloudfunctions.net/PagSegService_ChangePlanSignatureCC?key=${this.apiKey}`, body).toPromise()
                .then(response => {
                  resolve()
                })
                .catch(errorResponse => {
                  reject(JSON.parse(errorResponse._body));
                });
            })
        })
        .catch(e => {
          reject(e);
        });
    })
  }

}