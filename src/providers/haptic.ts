import { Injectable } from '@angular/core';
import { Vibration } from '@ionic-native/vibration';
import { Haptic } from 'ionic-angular';


@Injectable()
export class HapticProvider {

  private hapticAvailable: boolean = false;
  private hapticFeedbackConfig = true;


  constructor(
    private vibration: Vibration,
    private haptic: Haptic
  ) {
    this.hapticAvailable = haptic.available();
  }

  public getHapticFeedbackConfig(): boolean {
    return this.hapticFeedbackConfig;
  }


  public setHapticFeedbackConfig(newConfig: boolean) {
    this.hapticFeedbackConfig = newConfig;
  }

  /**
   * Impacto
   * @param {string} style Tipo de haptic feedback (`light`, `medium` e `heavy`)
   */
  impact(style: string): void {
    if (this.hapticFeedbackConfig) {
      if (style != '') {
        if (this.hapticAvailable) {
          this.haptic.impact({ style: style });
        } else {
          switch (style) {
            case 'light':
              this.vibration.vibrate(60);
              break;

            case 'medium':
              this.vibration.vibrate(80);
              break;

            case 'heavy':
              this.vibration.vibrate(100);
              break;
          }
        }
      }
    }
  }

  /**
   * Notificação
   * @param {string} type Tipo de haptic feedback (`success`, `warning` e `error`)
   */
  notification(type: string): void {
    if (this.hapticFeedbackConfig) {
      if (type != '') {
        if (this.hapticAvailable) {
          this.haptic.notification({ type: type });
        } else {
          switch (type) {
            case 'success':
              this.vibration.vibrate([80, 40, 80]);
              break;

            case 'warning':
              this.vibration.vibrate([100, 50, 100]);
              break;

            case 'error':
              this.vibration.vibrate(150);
              break;
          }
        }
      }
    }
  }

  /**
   * Seleção
   */
  selection(): void {
    if (this.hapticFeedbackConfig) {
      if (this.hapticAvailable) {
        this.haptic.selection();
      } else {
        this.vibration.vibrate(10);
      }
    }
  }

}