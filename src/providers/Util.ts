import { Injectable } from '@angular/core';
import { Schedule } from '../models/Schedule';
import { AngularFirestore } from 'angularfire2/firestore';
import { Notification } from '../models/Notification';
import { FormControl } from '@angular/forms';
import { Characteristic } from '../models/Characteristic';

export class regexValidators {
  readonly name = /^((\b[A-zÀ-ú']{2,40}\b)\s*){2,}$/;
  readonly email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  readonly text = /^[^0-9]*$/;
  readonly phone = /\(\d{2,}\) \d{4,}\-\d{4}/g;
  readonly password = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
  readonly cpf = /^[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}$/;
  readonly cnpj = /^[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2}$/;
  readonly cpf_cnpj = /^([0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}|[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2})$/;
};


export class ionDateTimePandH {

  readonly displayFormat = "DDDD, D MMM YYYY";
  readonly monthNames = "janeiro, fevereiro, março, abril, maio, junho, julho, agosto, setembro, outubro, novembro, dezembro";
  readonly monthShortNames = "jan, fev, mar, abr, mai, jun, jul, ago, set, out, nov, dez";
  readonly dayNames = "domingo, segunda-feira, terça-feira, quarta-feira, quinta-feira, sexta-feira, sábado";
  readonly dayShortNames = "dom, seg, ter, qua, quin, sex, sab";
  readonly cancelText = "Cancelar";
  readonly doneText = "Ok";

  readonly hourValues = "00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23";
  readonly minuteValues = "00,15,30,45";


  public generateISODateStringToMinDate(): string {
    let s = new Date().toLocaleDateString("pt-br");
    let isoDateString = `${s.split("/")[2]}-${s.split("/")[1]}-${s.split("/")[0]}`;
    console.log(`generated ISO Date String To Min Date: ${isoDateString}`)
    return isoDateString;
  }

  public generateISODateStringToMaxDate(days: number): string {
    let isoDateString = new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * days).toISOString().substring(0, 10).toString();
    console.log(`generated ISO Date String To Max Date: ${isoDateString}`)
    return isoDateString;
  }

};


@Injectable()
export class UtilProvider {

  public economizeiNotifThumbUrl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMkAAADJCAYAAACJxhYFAAAACXBIWXMAAAjbAAAI2wHNrPlDAAARi0lEQVR42u2dX2xb53nGnzRRlVg1JaejXEzKxEqwNkuORWKdzc3ZDo2gLBDbkYLoIrUxmBGQdr2ZjoHsYkNXHwVYbxYgx8M2FDUgyxd2ciE3Uq3eMAhEFnVGqwVIJZYcyGNML9TqSJ1NyaY3WQi0iyOmjq1/PPy+84/P78YXlo/Fw/Oc8z7v93zveWxlZSUBQAEhZC0GvsJzQMjGUCSEUCSEUCSEUCSEUCSEUCSEUCSEUCSEUCSEEIqEEDM8wVNgM/cXgdvTwM3Uo3/3jTCwM8xzRJFUKZ+lgOwwkL2w/s98eAqo2Q78URTYqwJfa+Z5o0iqgLt54DdvAJ++t7WfX75jCCl7AdjbD+zuA77q43mkJ/FoWTWpA2MvbF0gaz1Zfvac8QQiFImnyA4b4vjwlPFkqITlO8AHf2cc77MUzy3LLQ/4jkkd+Oyy+GPfvgrEvws8823gWz+iX6FIXFha/eaNjU25KD59z+iM7e6jX2G55RIm9VXfcMG6/3P5jlHKjb1Av0KROJhP44Y4RPgOsxRnDb8SfwW4Nc3vhCJxCHfzxkWZ+L5xkTrCC10GfnEI+OB1o/Qj9CS2+Y5JHfj4jHN/x+wF4L/ihlfpUvmd8UliIVcHjdLKyQJ52K/87DmjJCR8ksgtY1LApdedU1aV61cS3wd27gf+4k22jCkSCb6jnCiJCRJzzUjMGxduQ80SIo15BBvm5fiVd/8S+JNXjRKMLWOKpGLfcXXQKFckMTLbBjWt4Ma9Ry9WxZ+HHkrKEcvHZ4x2cZdqeBayLo9xzOl6pncY+PUb0tq5uaIPsYkokvOblz3HA9PQQ0k01CzJ+ax1TcCBNxnLX5sBimQt3/HrN4zYhwQKy7XQroRx6lqorH9XX7MEtT0NrVNiXosRF4pkU9/xoS51pVyfCUGbCmNhudb0MVq2LUIPJdHTlJV3LhjJp0jW9B1XB6WVVom5ZsQmomv6DrMo/jyG9sURqJO0WFizHfizHwFtvRRJVYskO2wsCEpq6eaKPqgZBaOzbdI+Qv+uNLQ9KXl+ZcduQyzV61eqVCS3po2WrowI+6rv0GdCGJiy5sKqr1mC1pmC2p6W95+0vVytW4irTCQWRNiHch1Q00pFvsMsXQ3z0INJRBrz8kqw6ovkV5FIJnXpvkObCm+ppSub7qYs9GBSnl+pazJKsGeiFIknkBwlyRV90KbCOJvrcNxHP7lagknzKzv3Gy3jpzsoEldyN29ExSX7Dn0mZEtpVY5f0UNJxAIS95e0vWyIxZslmAdFYkGEfaMoiVNR/HlonSn5fsV7kXyPieTqoCEQSb4jU/BDTSuO8B1mOR6YhtaZkutXvBVx8YhILIiSqGnFkb7DbAmmtqfl+xVvRPJdLhILIuzaVNjxvsMslkRc3B/Jd6lILIiwy4iSONmvSIvkl/yKeyP5LhSJgyLsXkN6JN+dERcXiUTmNESYj7B7DUby3SgSl0TYvehXhvbF5bWMAbdE8h0uEguiJGpGwWTBT1Vs4FeqPJLvUJF8Gjd8h4sj7F5DeiR/537D3DvPrzhMJB6LsHvRr1gScXFWJN8hIrEgSjKU64B2JVwVLV3ZWBbJd0bExQEiuTUNJL4nrbRyUoTda0iP5O/YDUTfsdvY2yySW9PGsGkJxtzJEXavITWSb79QbBTJ3bzxPg0JAvFylMSptGxbhLYnJcev2CuUAfsGZn/wunCBjMy2ITDWhwGueVjOjXs+vDoRRWS8F4k5waXt7auGZ7UJe0SSHRbawcoU/IiM9+KlS0dozG0mOd+Mg4lexCaiyBUFfhcfnzGqj6oRiaC7QmG5FrGJKELxYzTmDuNsrgPB+DFoItvtH+pVIpJb00I6WfpMCIGxPhpzB7OwXIuBqTACY30YEbFwa+V7KG0VSYUvkknMNSMw1ocTGYW+w0V+5aVLRxAZ70Wm0giQDe+ut14kt811PwrLtYiM9+Jgope+w8V+JRQ/BjWtoGD2BnezGkRi4kWXuaIPgbE++g6PcOpaCJHxXvNCsRhXvMSn59IRT5dWwYZ56KHkl5+c942mRMGjn3uy4Id2JfzI53ZiueV4kYzMtnk6yh5smEfi4DDq11itThwcdtUd18wTRW1Py4u1uLbcMiESL9PTlF1TIIARJAzumPf053fD92u5SHJz5d01hC5IEcdR7lMy/7u73hfJQnGJVwYxzf/c+V+WW4Q4Db6iWjANNUtl+YjNTGs5s7ByRR/LU4rE+QJJHBxGl8Ahb28Ft94iXVhdcM1wsAXLLaeitqeFCqRc6ldFSigSsolQCEVCCEVCCEVCCEVC7OLViShPgmDYAraYyYJfWl4pI/HYFAmxTCBeTvWy3CIVMzLbRoFQJIRQJIRQJIQQioQQioQQioQQioQQioQQioQQioQQioQQQpEQQpEQQpG4ip6mrJzXOBOpcD+JhXStTpAXsTFqZLaN87UoEu8KRcRsLrU9zUF0LLfIRtTXLJX/AhxCkRBCkRBCkRBCkRCBFO5zqARFQtZlsuBHjIPoLIEtYBsubjWjVHyczG0/xxNRJN4UCIfTsdwiG8DhdBQJIRQJIRQJIYQiIYQiIYQiIYQiIYQiIYQiIYQiIYQiIYRQJIRQJI6Dc7fcCaPyFiJy7tZaFJZroc+EeKIpEvcLRcTcrfUINsxzxyLLLbIRxwPTiDTmeSIoEkIoEkIoEkIoEkIoEvIw+kwIkzZPeT+b60BirplfhkDYAhZIYbkWkfFeBHdsvcUbC0zjeGB63b8/kVHKer0CBUKRuEIo5VyoEf/G7dpMwc8Ln+UWIRQJIRSJTAJ1i/yWPIwbAp+OF0lsA1PrBUZm27CwzujTyYIfmdvefidiT1OWxr1SIo15KP48kvPeNK+Z1SHaa10s+kzI07OD+3ely64Ufnv/aXRRJGvcbZ+7iMh4r+1rEDKFUm1v0e1qmDf1YtSbyzu8X2799v7TpurWxMHhDdcTiHs42ZlCJnrO1L+9+/lT3hdJ5p65DUcNNUsY2hfHeGRY6n4MIo/upiyuHxqE1pmy/PpxlUh+daezYo+SiZ7DmX1x1HMrrCto2baI8cgwRg5crKhbeeOeD//5f3/ofZHc/fwpnM11VHycWGAaucODOFnBXYnIpb5mCW8Fk8gdHhSyEWzoeoctn8OWFrA2FRZynIaaJWidKVw/NIhuF7QSq4n+XWnkDg9CbU8LOd6Cjfv3bRFJrujDgCChAMaC48iBixiPDKNlGxcf7UTx53H90CD0UFLoQmFsImpbO9y2xURtKix87SPSmEfu8CDeCibpV2zwHe8euIjEwWHhKYlT10LSJsw4WiQA0POrI1LWPtR241HfvyvNq9cC33GyM4Xc4UEpq+dncx1Q04qtn9FWkRSWaxGMH8Opa+JrzYaaJeihJK4fGoTi5/QQGRxfbZ5oEponC8u1OJFRHDEeyRHZLTWt4GCiV0r0JFC3iMTBYbx74CL9ikDfkY6ew9C+uJSA4uhsG4LxY2sa9ciz1seTHBNLScw1IzLXi1hgGnpIvKfoacqipykLbSoMfSa0bqiQbOw7tD0paaHTyYIfakZx3CYzy58km90JhnIdCIz1Ce1+falhsFo/M+JSvu/IfOecFIGUSqtg/NimAgm2Wp9xe2xlZSUBwDJnlPlkHqH+81sulfRgUtoaSGKuWUqXzUt0N2WhB5PS9vWcuhaCdiW85fbu7Xf+Bg11llYBA5aXW8FWP1oat+PG3J1NfzZX9KHn0hFEGvMY2hcX7ikijXkkGocxlOuAdiWMG/d8VMUqXQ3z0INJaSNTk/PNiE1EkStu/Zx372+1WiD2GXftaHmlVGKuGYGxPpzIKFK8RCwwjcx3zuFkZ6rq11fqa5ZwZl8cmeg5KQK5cc+Hly4dQWS8tyyBAIDabc+Ku+Xl1hdPlL89h8nrvyv73zXULEHbk5K2BpIr+qBmFIzauHhlFyc7U1Db01I6VqVYidlIUvf+Voz88Igdp2XANpFkPplH5B8uYKFo7gsJ1C1iaF9c2hpIYq4Zakbx7EavB1H8Rjkry3eUFgTNxkpaGrcj8y/HbCm1bBUJAAy9P41X9fcqOkZPUxZ6KCltDWRo9Qv2Ysu4ZZtxo5HpO9S0UtGuy/q6WiR+/LItXS1HiAQAEh/l0fNPY6afKF/4nNVSQYanKL1BSlZb2g7fUTpfMlhYroWaVjBU4ZaIrm/+AYbUqJ0CcYZIACA3t4jYW3Ekr8xWdJxSFEXWGkiu6ENsIurqlnH/rjS0PSmpo3wWlmvR8O4PKvs9XwxCOxq2q8RynkgefKrE9PiW2sMbNgVWhwzI9CuxiairWsaKPw89lETQoq3PofgxU2WWsqcJ+muK3U8P54qkhD6ahvb25YpLsFhgGtqelDS/UurWONmvtGxbhB5KWj7f6kRGKWuTVEvjduivKegJO66r6EyRAEChuATtfAqnfp6puART29NS/Yp2JSwlyVyp71Db01ISulthdLYNPZc2b9nW19VCXS2tHIpzRVIi88k81NPJiv1KoG4RWmdKml/JFPxQ04oj/Mrx1ZConSNEt+JLjj+/G/prihN8h7tFUmIklYV6OlmxX4k05qEHk9LGEo3MtkFNK7b4FcWfh9aZcszbd9fzJcqeJmhHw7bE3j0tkhLa+RT0n2eE+BUZkfwvfk8LI/myI+yifElL43ZoR8OIPd8BF+E+kZT8ino6ibPvXxXiV2SNJcoVfdCmwkJGKK2HzCiJKF9S8h1qd8jppZV3RFIi8VEe2vmUEL8iO+IiOpIvO8Iuypcc/+9/hv49BYFG1yas3S2SEkPvT0M7nxLiV2RE8r/4PQVE8mVH2IVz6BfA0x1uvry8IZJSCaaPpjHw9uWKj1VqnTop4lK/miZw3ftavvWPwO4+isRJ5OYWof40idHLn1TsV5wSybciSiKNZ74NRH5KkTjVr6ink6b2rDyIFRGX9fyKVRH24I55qWUm/vo6ReJ0v6Ke/mXFLWPZkfzSi3xyRR8CdYuI+PPSxJGcN4T58NAFaUnq6NvAzjBF4nS/IiLiIvVCsoAb93zQroQ3jLBLGb6xtx/oUikSt/gVN0TyRVPaOlvOOxiFdvp27gei71AkbvMrIiL5kUYjBuLkMaqjs21QM0rZQxeEPznd60uqUyQl3BLJN4PIaYhCSjD3+pLqFknJr7gh4lJOaSVi66zwEsy9voQiKSEyki9z6uRGlDsN0SymFlvd60sokodxSyT/QcxMQ6wUU80Ld/oSimS9EkwfTTs+kn/jnjGYws4p7GXdDNzpSwYe1zQtBiBAafyeJ7/6BCLPNuOVv2pHobhU0ap9puDHT7J78dQTnyP89ZvCfMfff/QcXvmPFyp+evS/GMSTNY+bfnLmij78JLsXC8u1+POv38STj3++/g/XNQPfcJ1IknySbAEnRfIrnYZYont/65ci7CKSCZuWYO70JSy3ykFUxMVMl0jENETAGPimv6asuXVWVDJhwxLMfb6EIjHrV0RE8ktv3+ppyq7pWW7c8yEx1wx9JlSxOOrraqF9d/+WJrOL6vSt2QVzny+hSMwiKpL/YKkS3PH7O69IM252GqKIzWyPlGDuWy+hSET4FRGRfBkoe5owdCJa0dZZUU/OYMM8/vVPf4muVj++9uIFiqQaERVxEUFL43YMqVGhI3tEhUN/8Mcz+Pc3/40iqWa/IiqSb9Z3yJ6GKGKxdeViP0VCvyLmrlsOVk5DrLQEo0jIl/yKiEj+Zr7DrinsZm8GFAl5BFFTJx/2HU6Zwl5uCUaRkHVLFBGRfKdOQyynBKNIyIZUslB3/Pnd0I6GHT0NcSvrRxQJ2XKJoo+mtySW7v2tULtDbpnCvqkfo0hI2XfexEd55D4z/gSAhrpaBFv9CLb6EXm22Y1Dpjf0YxQJIZuUYG4TyVf4FRLZBBp9GPnhEYz/+GW0NG533e/PJwkhfJIQUhkUCSEUCSEUCSEUCSEUCSEUCSEUCSEUCSEUCSGEIiHEDI+trKwEATTwVBCyJrn/BwCNmVqYDaq7AAAAAElFTkSuQmCC';

  public regexValidators: regexValidators = new regexValidators();

  public ionDateTimePandH: ionDateTimePandH = new ionDateTimePandH();

  // Utilizar somente palavras em minusculo no bannedKeyWords
  private bannedKeyWords = ['da', 'de', 'outra', 'outras', 'outro', 'outros', 'sim', 'não'];

  constructor(
    private afs: AngularFirestore
  ) { }

  public generateKeyWords(bannerCharacteristics: Characteristic[]): Promise<string> {
    return new Promise((resolve) => {
      let tempKeyWord = '';
      for (let carac of bannerCharacteristics) {
        for (let caracKeyWords of carac.data) {
          let tempCaracKeyWordsVect = caracKeyWords.split(' ');
          for (let keyWord of tempCaracKeyWordsVect) {
            if (this.bannedKeyWords.indexOf(keyWord.toLowerCase()) == -1) {
              if (tempKeyWord.length > 0) {
                tempKeyWord = `${tempKeyWord} ${keyWord}`;
              } else {
                tempKeyWord = keyWord;
              }
            }
          }
        }
      }
      resolve(tempKeyWord);
    });
  }


  public compareFn(o1, o2): boolean {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

  /**
   * Retorna se o campo esta valido ou não
   * @param {FormControl} fromControl
   * @return  {boolean} boolean
   */
  public checkFormControlAlert(formContrl: FormControl): boolean {
    return ((!formContrl.valid && formContrl.dirty) || (!formContrl.valid && formContrl.touched)) && (formContrl.value != '' && formContrl.value != null);
  }

  /**
  * Retorna a a string para o boolean
  * @param {boolean} boolean
  * @return  {string} Sim ou Não
  */
  public translateBoolean(boolean: boolean): string {
    if (boolean) {
      return `Sim`
    } else {
      return `Não`
    }
  }

  /**
   * Retorna a a string para o genero do usuário
   * @param {string} gender gender
   * @return  {string} gender txt
   */
  public translateGender(gender: string): string {
    switch (gender) {
      case 'female':
        return 'Feminino'
      case 'male':
        return 'Masculino'
      case 'ND':
        return 'Não declarar'
    }
  }

  /**
 * Retorna a a string para o creador
 * @param {number} userType
 * @return  {string} userType txt
 */
  public translateUserType(userType: number): string {
    if (userType == 10) {
      return 'Prestador de Serviço'
    } else {
      return 'Cliente'
    }
  }

  public unFormat(val: string): string {
    if (!val) {
      return '';
    }
    val = val.replace(/\D/g, '');
    return val.replace(/,/g, '');
  }

  /**
   * Retorna a a string para o status do banner
   * @param {number} status
   * @return  {string} status txt
   */
  public translateBannerStatus(status: number): string {
    switch (status) {
      case 7:
        return 'Pendente (Dados atualizados)'
      case 8:
        return 'Pendente (Endereço atualizado)'
      case 9:
        return 'Pendente (Fotos atualizadas)'
      case 10:
        return 'Pendente'
      case 11:
        return 'Ativo'
      case 12:
        return 'Pausado'
      case 13:
        return 'Rejeitado'
      case 14:
        return 'Finalizado'
      case 101:
        return 'Em criação, passo 2'
      case 102:
        return 'Em criação, passo 3'
      case 103:
        return 'Em criação, passo 4'
    }
  }

  /**
   * Retorna a a string para o status da Order
   * @param {number} status
   * @return  {string} status txt
   */
  public translateOrderStatus(status: number): string {
    switch (status) {
      case 1:
        return 'Aguardando aceitação'
      case 1301:
        return 'Rejeitado pelo prestador'
      case 1302:
        return 'Cancelado pelo prestador'
      case 1303:
        return 'Cancelado pelo cliente'
      case 2:
        return 'Em agendamento'
      case 3:
        return 'Em orçamento'
      case 4:
        return 'Agendado'
      case 5:
        return 'Atrasado'
      case 6:
        return 'Em andamento'
      case 7:
        return 'Pendente avaliação'
      case 1300:
        return 'Finalizado'
    }
  }

  /**
   * Retorna a a string para o status do agendamento
   * @param {string} schedulingStatus status do agendamento
   * @param {number} orderStatus status da Order
   * @return  {string} status txt
   */
  public translateSchedulingStatus(schedulingStatus: string, orderStatus: number): string {
    switch (schedulingStatus) {
      case 'new':
        return 'Novo'
      case 'pending_provider':
        if (orderStatus == 1) {
          return 'Aguadando aceitação do pedido'
        }
        else {
          return 'Aguadando aceite do prestador'
        }
      case 'pending_client':
        return 'Aguadando aceite do cliente'
      case 'approved':
        return 'Agendamento aprovado'
      case 'rejected_by_client':
        return 'Rejeitado pelo cliente'
      case 'rejected_by_provider':
        return 'Rejeitado pelo prestador'
      case 'canceled_by_client':
        return 'Cancelado pelo cliente'
      case 'canceled_by_provider':
        return 'Cancelado pelo prestador'
      case 'checkin_done':
        return 'Checkin Realizado'
      case 'realized':
        return 'Realizado'
      case 'expired':
        return 'Expirado'
    }
  }

  /**
   * Retorna a a string para o creador
   * @param {Schedule} schedule status do agendamento
   * @return  {string} creator txt
   */
  public translateSchedulingCreator(schedule: Schedule): string {
    if (schedule.createdBy == schedule.providerId) {
      return 'Prestador de Serviço'
    } else {
      return 'Cliente'
    }
  }

  /**
   * Retorna a a string para o status do budget
   * @param {string} status status do budget
   * @return  {string} status txt
   */
  public translateBudgetStatus(status: string): string {
    switch (status) {
      case 'pending':
        return 'Aguadando aceite do cliente'
      case 'approved':
        return 'Orçamento aprovado'
      case 'rejected_by_client':
        return 'Rejeitado pelo cliente'
      case 'canceled_by_client':
        return 'Cancelado pelo cliente'
      case 'canceled_by_provider':
        return 'Cancelado pelo prestador'
    }
  }

  public numberPadding(number: number, size: number): string {
    var s = number.toString();
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }

  public convertMiliSecondsToHMmSs(miliseconds: number): string {
    var seconds = Math.abs(miliseconds / 1000);
    var s = Math.trunc(seconds % 60);
    var m = Math.trunc((seconds / 60) % 60);
    var h = Math.trunc((seconds / (60 * 60)) % 24);
    return `${this.numberPadding(h, 2)}:${this.numberPadding(m, 2)}:${this.numberPadding(s, 2)}`;
  }

  public ISOdatetimeFormatToMMDDYYYY(dateString: string): string {
    return `${dateString.substr(8, 2)}/${dateString.substr(5, 2)}/${dateString.substr(0, 4)}`;
  }

  /**
   * Retorna a a string para o status da Sac
   * @param {number} status
   * @return  {string} status txt
   */
  public translateSacStatus(status: number): string {
    switch (status) {
      case 1:
        return 'Aguardando atendimento'
      case 2:
        return 'Em atendimento'
      case 3:
        return 'Pendente usúario'
      case 1300:
        return 'Finalizado'
      case 1301:
        return 'Rejeitado'
      case 1302:
        return 'Cancelado pelo economizei'
      case 1303:
        return 'Cancelado pelo usúario'
    }
  }

  /**
  * Retorna a a string para o status da Sac
  * @param {number} priority
  * @return  {string} status txt
  */
  public translateSacPriority(priority: string): string {
    switch (priority) {
      case 'P1':
        return '🔥 P1 - Crítico';
      case 'P2':
        return '⚠️ P2 - Alto';
      case 'P3':
        return 'P3 - Moderado';
      case 'P4':
        return 'P4 - Baixo';
      case 'P5':
        return 'P5 - Planejamento';
    }
  }

  /**
   * Retorna a a string para o tipo da Sac
   * @param {number} status
   * @return  {string} status txt
   */
  public translateSacType(status: string): string {
    switch (status) {
      case `pvs`:
        return 'Verificação de Prestador'
      case `bvs`:
        return 'Verificação de Banner'
      case `ebvs`:
        return 'Verificação de Edição Banner'
    }
  }

  /**
   * Retorna a a string para o status da Request
   * @param {number} status
   * @return  {string} status txt
   */
  public translateRequestStatus(status: number): string {
    switch (status) {
      case 1:
        return 'Aberta'
      case 2:
        return 'Pausada'
      case 1303:
        return 'Cancelada'
      case 5:
        return 'Atrasado'
      case 1300:
        return 'Finalizado'
    }
  }

  /**	
   * Retorna a a string para o status da Proposal
   * @param {number} status
   * @return  {string} status txt
   */
  public translateProposalStatus(status: number): string {
    switch (status) {
      case 1:
        return 'Pendente'
      case 11:
        return 'Aceita'
      case 13:
        return 'Rejeitada'
      case 1301:
        return 'Cancelada'
      case 1302:
        return 'Expirada'
    }
  }


  /**
  * Envia Notificação para o usuário
  * @param {Notification} notification
  * @param {string} userId
  * @return  {Promise<void>}
  */
  sendNotification(notification: Notification, userId: string): Promise<void> {
    return new Promise((resolve, reject) => {
      return this.afs.collection('Users').doc(userId).collection('Notifications').doc(notification.id).set(Object.assign({}, notification))
        .then(() => {
          resolve();
        })
        .catch(e => {
          reject(e);
        });
    })
  }

  /**
   * Retorna a a string formatada para o numerdo do cartão
   * @param {number} number numero do cartão
   * @return  {string}
   */
  public translateCreditCardNumber(number: string): string {
    return `${number.substr(0, 4)} ${number.substr(4, 4)} ${number.substr(8, 4)} ${number.substr(12, 4)}`;
  }

  /**
 * Retorna a a string formatada para o numerdo do cartão
 * @param {number} number numero do cartão
 * @return  {string}
 */
  public maskCreditCardNumber(number: string): string {
    return `•••• •••• •••• ${number.substr(12, 4)}`;
  }


  /**
   * Converte o UF para o Nome do Estado Brasileiro
   * @param {state} uf Unidadte Federativa
   * @return {string} `string` Nome do estado Brasileiro
   */
  public ufToState(uf: string): string {
    uf = uf.toUpperCase();
    switch (uf) {
      case 'AC':
        return 'Acre';
      case 'AL':
        return 'Alagoas';
      case 'AP':
        return 'Amapá';
      case 'AM':
        return 'Amazonas';
      case 'BA':
        return 'Bahia';
      case 'CE':
        return 'Ceará';
      case 'DF':
        return 'Distrito Federal';
      case 'ES':
        return 'Espírito Santo';
      case 'GO':
        return 'Goiás';
      case 'MA':
        return 'Maranhão';
      case 'MT':
        return 'Mato Grosso';
      case 'MS':
        return 'Mato Grosso do Sul';
      case 'MG':
        return 'Minas Gerais';
      case 'PA':
        return 'Pará';
      case 'PB':
        return 'Paraíba';
      case 'PR':
        return 'Paraná';
      case 'PE':
        return 'Pernambuco';
      case 'PI':
        return 'Piauí';
      case 'RJ':
        return 'Rio de Janeiro';
      case 'RN':
        return 'Rio Grande do Norte';
      case 'RS':
        return 'Rio Grande do Sul';
      case 'RO':
        return 'Rondônia';
      case 'RR':
        return 'Roraima';
      case 'SC':
        return 'Santa Catarina';
      case 'SP':
        return 'São Paulo';
      case 'SE':
        return 'Sergipe';
      case 'TO':
        return 'Tocantins';
      default:
        return '';
    }
  }

  /**
   * Converte o Nome do estado para a uf
   * @param {string} state Nome do estado Brasileiro
   * @return {string} `string` Unidadte Federativa
   */
  public stateuToUf(state: string): string {
    switch (state) {
      case 'Acre':
        return 'AC';
      case 'Alagoas':
        return 'AL';
      case 'Amapá':
        return 'AP';
      case 'Amazonas':
        return 'AM';
      case 'Bahia':
        return 'BA';
      case 'Ceará':
        return 'CE';
      case 'Distrito Federal':
        return 'DF';
      case 'Espírito Santo':
        return 'ES';
      case 'Goiás':
        return 'GO';
      case 'Maranhão':
        return 'MA';
      case 'Mato Grosso':
        return 'MT';
      case 'Mato Grosso do Sul':
        return 'MS';
      case 'Minas Gerais':
        return 'MG';
      case 'Pará':
        return 'PA';
      case 'Paraíba':
        return 'PB';
      case 'Paraná':
        return 'PR';
      case 'Pernambuco':
        return 'PE';
      case 'Piauí':
        return 'PI';
      case 'Rio de Janeiro':
        return 'RJ';
      case 'Rio Grande do Norte':
        return 'RN';
      case 'Rio Grande do Sul':
        return 'RS';
      case 'Rondônia':
        return 'RO';
      case 'Roraima':
        return 'RR';
      case 'Santa Catarina':
        return 'SC';
      case 'São Paulo':
        return 'SP';
      case 'Sergipe':
        return 'SE';
      case 'Tocantins':
        return 'TO';
      default:
        return '';
    }
  }

  /**
   * Converte o Nome do estado para a uf
   * @param {string} str String para remover os caracteres
   * @return {string} `string` somente com numeros
   */
  public removeCaracs(str: string): string {
    return str.replace(/\D/g, "");
  }

  /**
 * Retorna a a string para o status da Assinatura
 * @param {string} status status da Assinatura
 * @return  {string} status txt
 */
  public translatePlanSignaturesStatus(status: string): string {
    switch (status) {
      case 'INITIATED':
        return 'Iniciada'
      case 'PENDING':
        return 'Pendente'
      case 'PENDING_PROCESSING':
        return 'Pendente Processamento'
      case 'ACTIVE':
        return 'Ativa'
      case 'PAYMENT_METHOD_CHANGE':
        return 'Metodo de Pagamento Pendente'
      case 'SUSPENDED':
        return 'Suspensa pelo vendedor'
      case 'CANCELLED':
        return 'Cancelada pelo PagSeguro'
      case 'CANCELLED_BY_RECEIVER':
        return 'Cancelada pelo Economizei'
      case 'CANCELLED_BY_SENDER':
        return 'Cancelada pelo cliente'
      case 'EXPIRED':
        return 'Expirada'
    }
  }

  /**
* Retorna a a string para o status da Assinatura
* @param {string} status status da Assinatura
* @return  {string} status txt
*/
  public planSignaturesStatusDetail(status: string): string {
    switch (status) {
      case 'INITIATED':
        return 'O comprador iniciou o processo de pagamento, mas abandonou o checkout e não concluiu a compra.'
      case 'PENDING':
        return 'O processo de pagamento foi concluído e transação está em análise ou aguardando a confirmação da operadora.'
      case 'PENDING_PROCESSING':
        return 'Pendente Processamento da API do PagSeguro'
      case 'ACTIVE':
        return 'A criação da recorrência, transação validadora ou transação recorrente foi aprovada.'
      case 'PAYMENT_METHOD_CHANGE':
        return 'Metodo de Pagamento Sob Confirmação. Você Solicitou uma Mudança de Cartão que será validada na proxima Renovação ou uma transação retornou como "Cartão Expirado, Cancelado ou Bloqueado" e o cartão da recorrência precisa ser substituído pelo comprador.'
      case 'SUSPENDED':
        return 'A recorrência foi suspensa pelo vendedor.'
      case 'CANCELLED':
        return 'A criação da recorrência foi cancelada pelo PagSeguro'
      case 'CANCELLED_BY_RECEIVER':
        return 'A recorrência foi cancelada a pedido do vendedor.'
      case 'CANCELLED_BY_SENDER':
        return 'A recorrência foi cancelada a pedido do comprador.'
      case 'EXPIRED':
        return 'A recorrência expirou por atingir a data limite da vigência ou por ter atingido o valor máximo de cobrança definido na cobrança do plano.'
    }
  }

  public getDate(miliseconds: number): string {
    return new Date(miliseconds).toLocaleString('pt-br');
  }

  public getW3CLocaleDateString(w3Cstring: string): string {
    return new Date(Date.parse(w3Cstring)).toLocaleString('pt-br');
  }

  /**
* Retorna a a string para o status da Ordem de pagamento
* @param {string} status status da Ordem de pagamento
* @return  {string} status txt
*/
  public translatePaymentOrderStatus(status: number): string {
    switch (status) {
      case 1:
        return 'Agendada'
      case 2:
        return 'Processando'
      case 3:
        return 'Não Processada'
      case 4:
        return 'Suspensa'
      case 5:
        return 'Paga'
      case 6:
        return 'Não Paga'
    }
  }

  /**
* Retorna a a string para o status da Ordem de pagamento
* @param {string} status status da Ordem de pagamento
* @return  {string} status txt
*/
  public paymentOrderStatusDetail(status: number): string {
    switch (status) {
      case 1:
        return 'A ordem de pagamento está aguardando a data agendada para processamento.'
      case 2:
        return 'A ordem de pagamento está sendo processada pelo sistema.'
      case 3:
        return 'A ordem de pagamento não pôde ser processada por alguma falha interna, a equipe do PagSeguro é notificada imediatamente assim que isso ocorre.'
      case 4:
        return 'A ordem de pagamento foi desconsiderada pois a recorrência estava suspensa na data agendada para processamento.'
      case 5:
        return 'A ordem de pagamento foi paga, ou seja, a última transação vinculada à ordem de pagamento foi paga.'
      case 6:
        return 'A ordem de pagamento não pôde ser paga, ou seja, nenhuma transação associada apresentou sucesso no pagamento.'
    }
  }

  /**
* Retorna a a string para o status da transação da Ordem de pagamento
* @param {string} status status da transação da Ordem de pagamento
* @return  {string} status txt
*/
  public translatePaymentOrderTransactionStatus(status: number): string {
    switch (status) {
      case 1:
        return 'Aguardando pagamento'
      case 2:
        return 'Em análise'
      case 3:
        return 'Paga'
      case 4:
        return 'Disponível'
      case 5:
        return 'Em disputa'
      case 6:
        return 'Devolvida'
      case 7:
        return 'Cancelada'
    }
  }

  /**
  * Retorna a a string para o status da transação da Ordem de pagamento
  * @param {string} status status da transação da Ordem de pagamento
  * @return  {string} status txt
  */
  public paymentOrderTransactionStatusDetail(status: number): string {
    switch (status) {
      case 1:
        return 'O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.'
      case 2:
        return 'O comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.'
      case 3:
        return 'A transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.'
      case 4:
        return 'A transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.'
      case 5:
        return 'O comprador, dentro do prazo de liberação da transação, abriu uma disputa.'
      case 6:
        return 'O valor da transação foi devolvido para o comprador.'
      case 7:
        return 'A transação foi cancelada sem ter sido finalizada.'
    }
  }

  /**
* Retorna a a string para o status da transação da Ordem de pagamento
* @param {string} status status da transação da Ordem de pagamento
* @return  {string} status txt
*/
  public translateProviderExperience(reviewNum: number): string {
    if (reviewNum < 50) {
      return "Iniciante"
    }
    else if (reviewNum >= 50 && reviewNum < 100) {
      return "Intermediário"
    }
    else if (reviewNum >= 100 && reviewNum < 500) {
      return "Intermediário-avançado"
    }
    else if (reviewNum >= 500 && reviewNum < 1000) {
      return "Avançado"
    }
    else if (reviewNum >= 1000 && reviewNum < 5000) {
      return "Experiente"
    }
    else if (reviewNum >= 5000) {
      return "Profissional"
    }
  }


}
