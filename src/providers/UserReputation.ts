import { Injectable } from '@angular/core';

import { User } from '../models/User';

@Injectable()
export class UserReputationProvider {

  constructor() {
  }

  /**
   * Retorna a Porcentagem de avaliações como pontual
   * @param {User} provider Usuário
   * @return  {number} Porcentagem
   */
  getAsProviderOnTimeOrdersPercentage(provider: User): number {
    return Number(((provider.asProviderOnTimeOrders * 100) / (provider.asProviderOnTimeOrders + provider.asProviderLateOrders)).toFixed(2));
  }

  /**
   * Retorna a Porcentagem de avaliações como Atrasado
   * @param {User} provider Usuário
   * @return  {number} Porcentagem
   */
  getAsProviderLateOrdersPercentage(provider: User): number {
    return Number(((provider.asProviderLateOrders * 100) / (provider.asProviderLateOrders + provider.asProviderOnTimeOrders)).toFixed(2));
  }

  /**
 * Retorna a Porcentagem de avaliações com data de entrega pontual
 * @param {User} provider Usuário
 * @return  {number} Porcentagem
 */
  getAsProviderOnTimeDeliveredOrdersPercentage(provider: User): number {
    return Number(((provider.asProviderOnTimeDeliveredOrders * 100) / (provider.asProviderOnTimeDeliveredOrders + provider.asProviderLateDeliveredOrders)).toFixed(2));
  }

  /**
   * Retorna a Porcentagem de avaliações com data de entrega atrazada
   * @param {User} provider Usuário
   * @return  {number} Porcentagem
   */
  getAsProviderLateDeliveredOrdersPercentage(provider: User): number {
    return Number(((provider.asProviderLateDeliveredOrders * 100) / (provider.asProviderLateDeliveredOrders + provider.asProviderOnTimeDeliveredOrders)).toFixed(2));
  }

  /**
   * Retorna a Porcentagem de budgets compridos
   * @param {User} provider Usuário
   * @return  {number} Porcentagem
   */
  getAsProviderMetBudgetOrdersPercentage(provider: User): number {
    return Number(((provider.metBudgetOrders * 100) / (provider.metBudgetOrders + provider.notMetBudgetOrders)).toFixed(2));
  }

  /**
   * Retorna a Porcentagem de budgets não cumpridos
   * @param {User} provider Usuário
   * @return  {number} Porcentagem
   */
  getAsProviderNotMetBudgetOrdersPercentage(provider: User): number {
    return Number(((provider.notMetBudgetOrders * 100) / (provider.notMetBudgetOrders + provider.metBudgetOrders)).toFixed(2));
  }

  /**
   * Retorna a Porcentagem de avaliações como pontual
   * @param {User} client Usuário
   * @return  {number} Porcentagem
   */
  getAsClientOnTimeOrdersPercentage(client: User): number {
    return Number(((client.asClientOnTimeOrders * 100) / (client.asClientOnTimeOrders + client.asClientLateOrders)).toFixed(2));
  }

  /**
   * Retorna a Porcentagem de avaliações como Atrasado
   * @param {User} client Usuário
   * @return  {number} Porcentagem
   */
  getAsClientLateOrdersPercentage(client: User): number {
    return Number(((client.asClientLateOrders * 100) / (client.asClientLateOrders + client.asClientOnTimeOrders)).toFixed(2));
  }



}