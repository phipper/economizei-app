import { Injectable } from "@angular/core";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { FieldPath, OrderByDirection } from "@firebase/firestore-types";

@Injectable()
export class ChildCollectionCRUD<T> {
    private validators = new Validators();
    private count = 0;
    /**
   * Coleção filha generica
   * @param afs referencia ao AngularFirestore
   * @param PARENT_COLLECTION Coleçao Pai
   * @param COLLECTION Coleção
   * @param orderByFieldPath Propriedade que sera utilizada para ordernar os objetos coleção
   * @param orderByDirectionStr Opcional direção para organizar ('asc' or 'desc'). If
   * not specified, order will be ascending.
   * @return The created Query.
   */
    constructor(private afs: AngularFirestore, private PARENT_COLLECTION: string, private COLLECTION: string, private orderByFieldPath: string | FieldPath, private orderByDirectionStr?: OrderByDirection) { }
    public getAll(parentId: string): Promise<T[]> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.PARENT_COLLECTION).doc(parentId).collection(this.COLLECTION).ref
                .orderBy(this.orderByFieldPath, this.orderByDirectionStr).get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            let tempData: T[] = [];
                            for (let doc of querySnapshot.docs) {
                                tempData.push(doc.data() as T);
                            }
                            resolve(tempData);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public create(parentId: string, id: string, data: T): Promise<void> {
        return new Promise((resolve, reject) => {
            if (id != '') {
                resolve(this.afs.collection(this.PARENT_COLLECTION).doc(parentId).collection(this.COLLECTION).doc(id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }

        })
    }
    public update(parentId: string, id: string, data: Partial<T>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.PARENT_COLLECTION).doc(parentId).collection(this.COLLECTION).doc(id).update(data));
        })
    }
    public add(parentId: string, vet: T[]): Promise<void> {
        return new Promise((resolve, reject) => {
            this.count = 0;
            for (let data of vet) {
                this.afs.collection(this.PARENT_COLLECTION).doc(parentId).collection(this.COLLECTION).add(Object.assign({}, data))
                    .then(() => {
                        this.count++;
                        if (vet.length == this.count) {
                            resolve();
                        }
                    })
                    .catch(e => {
                        reject(e);
                    });
            }
        });
    }
}