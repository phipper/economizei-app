import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { DocumentSnapshot } from "@firebase/firestore-types";
import { ProviderInvite } from "../../../../models/ProviderInvite";

@Injectable()
export class ProviderInvitesCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public get(id: string): Promise<ProviderInvite> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.PROVIDER_INVITES_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as ProviderInvite);
                            } else {
                                reject({ message: `there is no ProviderInvite with id: ${id}` });
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public getDoc(id: string): Promise<DocumentSnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.PROVIDER_INVITES_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    resolve(this.validators.validateDoc(doc));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
}