import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { ServiceType } from "../../../../models/ServiceType";
import { Characteristic } from "../../../../models/Characteristic";

@Injectable()
export class ServiceTypesCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public getAll(): Promise<ServiceType[]> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.SERVICE_TYPES_COLLECTION).ref.get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            let tempData: ServiceType[] = [];
                            for (let doc of querySnapshot.docs) {
                                tempData.push(doc.data() as ServiceType);
                            }
                            resolve(tempData);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                })
        })
    }
    public get(id: string): Promise<ServiceType> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.SERVICE_TYPES_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as ServiceType);
                            } else {
                                reject({ message: `there is no ServiceType with id: ${id}` })
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public getCharacteristics(serviceTypeId: string): Promise<Characteristic[]> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.SERVICE_TYPES_COLLECTION).doc(serviceTypeId).collection(this.COLLECTIONS.CHARACTERISTICS_COLLECTION).ref
                .orderBy('orderNumber').get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            let tempData: Characteristic[] = [];
                            for (let doc of querySnapshot.docs) {
                                tempData.push(doc.data() as Characteristic);
                            }
                            resolve(tempData);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                })
        })
    }
}