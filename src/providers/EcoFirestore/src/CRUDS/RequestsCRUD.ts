import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { ChildCollectionCRUD } from "../models/ChildCollectionCRUD";
import { Pic } from "../../../../models/Pic";
import { Characteristic } from "../../../../models/Characteristic";
import { DocumentSnapshot } from "@firebase/firestore-types";
import { Request } from "../../../../models/Request";


@Injectable()
export class RequestsCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    public pics: ChildCollectionCRUD<Pic>;
    public characteristics: ChildCollectionCRUD<Characteristic>;
    constructor(private afs: AngularFirestore) {
        this.pics = new ChildCollectionCRUD(afs, this.COLLECTIONS.REQUESTS_COLLECTION, this.COLLECTIONS.PICS_COLLECTION, 'index');
        this.characteristics = new ChildCollectionCRUD(afs, this.COLLECTIONS.REQUESTS_COLLECTION, this.COLLECTIONS.CHARACTERISTICS_COLLECTION, 'orderNumber');
    }
    public getDoc(id: string): Promise<DocumentSnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.REQUESTS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    resolve(this.validators.validateDoc(doc));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public create(data: Request): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.REQUESTS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }

        })
    }
    public get(id: string): Promise<Request> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.REQUESTS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Request);
                            } else {
                                reject({ message: `there is no Request with id: ${id}` });
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public update(id: string, data: Partial<Request>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.REQUESTS_COLLECTION).doc(id).update(data));
        })
    }
}