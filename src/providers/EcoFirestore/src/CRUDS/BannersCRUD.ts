import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";
import { DocumentSnapshot } from "@firebase/firestore-types";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { ChildCollectionCRUD } from "../models/ChildCollectionCRUD";
import { Pic } from "../../../../models/Pic";
import { Characteristic } from "../../../../models/Characteristic";
import { Banner } from "../../../../models/Banner";
import { BannerView } from "../../../../models/BannerView";


@Injectable()
export class BannersCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    public pics: ChildCollectionCRUD<Pic>;
    public characteristics: ChildCollectionCRUD<Characteristic>;
    constructor(private afs: AngularFirestore) {
        this.pics = new ChildCollectionCRUD(afs, this.COLLECTIONS.BANNERS_COLLECTION, this.COLLECTIONS.PICS_COLLECTION, 'index');
        this.characteristics = new ChildCollectionCRUD(afs, this.COLLECTIONS.BANNERS_COLLECTION, this.COLLECTIONS.CHARACTERISTICS_COLLECTION, 'orderNumber');
    }
    public getDoc(id: string): Promise<DocumentSnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    resolve(this.validators.validateDoc(doc));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public get(id: string): Promise<Banner> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Banner);
                            } else {
                                reject({ message: `there is no Banner with id: ${id}` });
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public create(data: Banner): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
    public update(id: string, data: Partial<Banner>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(id).update(data));
        })
    }
    public addView(bannerId: string, view: BannerView): Promise<void> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(bannerId).collection(this.COLLECTIONS.VIEWS_COLLECTION).add(Object.assign({}, view))
                .then(() => {
                    resolve();
                })
                .catch(e => {
                    reject(e);
                })
        })
    }
}