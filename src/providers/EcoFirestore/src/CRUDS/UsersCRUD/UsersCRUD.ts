import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../../COLLECTIONS";
import { Validators } from "../../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { DocumentSnapshot, FieldPath, WhereFilterOp, QuerySnapshot } from "@firebase/firestore-types";
import { User } from "../../../../../models/User";
import { NotificationsCRUD } from "./src/NotificationsCRUD";
import { UserHistory } from "./src/UserHistory";
import { AsProviderReviewsCRUD } from "./src/AsProviderReviewsCRUD";
import { AsClientReviewsCRUD } from "./src/AsClientReviewsCRUD";

@Injectable()
export class UsersCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    public notifications: NotificationsCRUD;
    public history: UserHistory;
    public asProviderReviews: AsProviderReviewsCRUD;
    public asClientReviews: AsClientReviewsCRUD;
    constructor(private afs: AngularFirestore) {
        this.notifications = new NotificationsCRUD(afs);
        this.history = new UserHistory(afs);
        this.asProviderReviews = new AsProviderReviewsCRUD(afs);
        this.asClientReviews = new AsClientReviewsCRUD(afs);
    }
    public get(id: string): Promise<User> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as User);
                            } else {
                                reject({ message: `there is no User with id: ${id}` })
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public getDoc(id: string): Promise<DocumentSnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    resolve(this.validators.validateDoc(doc));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    /**
     * Creates and returns a new Query with the additional filter that documents
     * must contain the specified field and the value should satisfy the
     * relation constraint provided.
     *
     * @param fieldPath The path to compare
     * @param opStr The operation string (e.g "<", "<=", "==", ">", ">=").
     * @param value The value for comparison
     * @return `Promise<QuerySnapshot>`
     */
    public where(fieldPath: string | FieldPath, opStr: WhereFilterOp, value: any): Promise<QuerySnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).ref.where(fieldPath, opStr, value).get()
                .then(querySnapshot => {
                    resolve(this.validators.validateQuerySnapshot(querySnapshot));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
}