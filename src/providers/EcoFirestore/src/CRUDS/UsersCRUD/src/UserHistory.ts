import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../../../COLLECTIONS";
import { Validators } from "../../../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { OrderCompletion } from "../../../../../../models/OrderCompletion";
import { Order } from "../../../../../../models/Order";
import { Notification } from "../../../../../../models/Notification";
import { Compliment } from "../../../../../../models/Compliment";
import { ComplimentsReport } from "../../../../../../models/ComplimentsReport";
import { Banner } from "../../../../../../models/Banner";

@Injectable()
export class UserHistory {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();

    public STANDARD_PROVIDER_COMPLIMENTS: Compliment[] = [
        {
            text: 'Excelente Atendimento',
            code: 'pc0'
        },
        {
            text: 'Respeitoso',
            code: 'pc1'
        },
        {
            text: 'Muito simpático',
            code: 'pc2'
        },
        {
            text: 'Profissional capacitado',
            code: 'pc3'
        }
    ];

    public STANDARD_CLIENT_COMPLIMENTS: Compliment[] = [
        {
            text: 'Gente Boa',
            code: 'cc0'
        },
        {
            text: 'Respeitoso',
            code: 'cc1'
        },
        {
            text: 'Muito simpático',
            code: 'cc2'
        },
        {
            text: 'Onesto',
            code: 'cc3'
        }
    ];

    constructor(private afs: AngularFirestore) { }

    /**
     * Registra a finalização do pedido e envia as notificações necessarias
     * @param {Order} order Pedido
     * @return  Promise<void>
     */
    registerOrderCompletion(order: Order): Promise<void> {
        return new Promise((resolve, reject) => {
            let tempOrderCompletion: OrderCompletion = new OrderCompletion();
            if (order.banner != null) {
                tempOrderCompletion.bannerId = order.banner.id;
            }
            tempOrderCompletion.orderId = order.id;
            tempOrderCompletion.clientId = order.clientId;
            tempOrderCompletion.providerId = order.providerId;
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(order.clientId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION)
                .add(Object.assign({}, tempOrderCompletion))
                .then(() => {
                    this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(order.providerId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION)
                        .add(Object.assign({}, tempOrderCompletion))
                        .then(() => {

                            if (order.banner != null) {
                                this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(order.banner.id).ref.get()
                                    .then(bannerDoc => {
                                        let tempBanner = bannerDoc.data() as Banner;
                                        tempBanner.performedServices = tempBanner.performedServices + 1;
                                        this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc<Banner>(order.banner.id).update({ performedServices: tempBanner.performedServices })
                                            .then(() => {
                                                resolve(this.sendfinishedOrderNotification(order));
                                            })
                                            .catch(e => {
                                                reject(e);
                                            })
                                    })
                                    .catch(e => {
                                        reject(e);
                                    })
                            } else {
                                resolve(this.sendfinishedOrderNotification(order));
                            }
                        })
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    private sendfinishedOrderNotification(order: Order): Promise<void> {
        return new Promise((resolve, reject) => {
            let clientNotif: Notification = {
                id: this.afs.createId(),
                title: 'Pedido Finalizado!',
                text: `Parabens seu pedido para ${order.title} foi completo com sucesso 🎉`,
                type: 'generalAlerts',
                action: {
                    type: 'showMyOrder',
                    orderId: order.id,
                },
                senderName: "economizei",
                createdOn: new Date(),
                readed: false
            };

            let providerNotif: Notification = {
                id: this.afs.createId(),
                title: 'Pedido Finalizado!',
                text: `Parabens seu pedido para ${order.clientName} foi completo com sucesso 🎉`,
                type: 'generalAlerts',
                action: {
                    type: 'showReceivedOrder',
                    orderId: order.id,
                },
                senderName: "economizei",
                createdOn: new Date(),
                readed: false
            };

            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(order.clientId).collection(this.COLLECTIONS.NOTIFICATIONS_COLLECTION).doc<Notification>(clientNotif.id).set(Object.assign({}, clientNotif))
                .then(() => {
                    resolve(this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(order.providerId).collection(this.COLLECTIONS.NOTIFICATIONS_COLLECTION).doc<Notification>(providerNotif.id).set(Object.assign({}, providerNotif)));
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
     * Retorna o numero de pedidos Prestados para o providerId e bannerId passado
     * @param {string} providerId Id do prestador do Banner
     * @return  Promise<number>, Numero de pedidos
     */
    public getCompletedOrdersTotalNumber(providerId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(providerId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION).ref
                .orderBy('time', 'desc').get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
     * Retorna o numero de pedidos Prestados para o providerId e bannerId passado
     * @param {string} providerId Id do prestador do Banner
     * @param {string} bannerId Id do banner
     * @return  Promise<number>, Numero de pedidos
     */
    public getCompletedOrdersNumber(providerId: string, bannerId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(providerId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION).ref
                .orderBy('time', 'desc').where('bannerId', '==', bannerId).get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
     * Retorna o numero de pedidos Prestados, para a quantidade de dias
     * @param {string} providerId Id do Usuário
     * @param {number} days Numero de dias 
     * @return  Promise<number>, Numero de pedidos
     */
    public getAsProviderCompletedOrderNumber(providerId: string, days: number): Promise<number> {
        const time = new Date().getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * days);
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(providerId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION).ref
                .where('providerId', '==', providerId).orderBy('timeInMs', 'desc')
                .where('timeInMs', '>', time).get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
     * Retorna o numero de pedidos contratados e finalizados, para a quantidade de dias
     * @param {string} clientId Id do Usuário
     * @param {number} days Numero de dias 
     * @return  Promise<number>, Numero de pedidos
     */
    public getAsClientCompletedOrderNumber(clientId: string, days: number): Promise<number> {
        const time = new Date().getTime() - (1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * days);
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(clientId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION).ref
                .where('clientId', '==', clientId).orderBy('timeInMs', 'desc')
                .where('timeInMs', '>', time).get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
     * Retorna um report de elogios como prestador 
     * @param {string} providerId Id do Usuário
     * @return  Promise<ComplimentsReport[]>
     */
    public getAsProviderReceivedReviewsCompliments(providerId: string): Promise<ComplimentsReport[]> {
        return new Promise((resolve, reject) => {
            let tempComp = this.STANDARD_PROVIDER_COMPLIMENTS;
            let tempCompReport: ComplimentsReport[] = [];
            let tempCompMap = new Map<string, Number>();

            for (let localComp of this.STANDARD_PROVIDER_COMPLIMENTS) {

                this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(providerId).collection(this.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref
                    .where('complimentCode', '==', localComp.code).get()
                    .then(querySnapshot => {
                        this.validators.validateQuerySnapshot(querySnapshot)
                            .then(querySnapshot => {
                                tempCompMap.set(localComp.code, querySnapshot.size);
                                if (tempCompMap.size == this.STANDARD_PROVIDER_COMPLIMENTS.length) {

                                    tempComp.sort(function (a, b) { return tempCompMap.get(b.code).valueOf() - tempCompMap.get(a.code).valueOf() });

                                    for (let comp of tempComp) {
                                        tempCompReport.push({
                                            text: comp.text,
                                            code: comp.code,
                                            quantity: tempCompMap.get(comp.code).valueOf()
                                        });
                                    }

                                    resolve(tempCompReport);
                                }
                            })
                            .catch(e => {
                                reject(e)
                            })
                    })
                    .catch(e => {
                        reject(e);
                    });

            }
        });
    }
    /**
     * Retorna um report dos elogios recebidas pelo cliente 
     * @param {string} clientId Id do Usuário
     * @return  Promise<ComplimentsReport[]>
     */
    public getAsClientReceivedReviewsCompliments(clientId: string): Promise<ComplimentsReport[]> {
        return new Promise((resolve, reject) => {
            let tempComp = this.STANDARD_CLIENT_COMPLIMENTS;
            let tempCompReport: ComplimentsReport[] = [];
            let tempCompMap = new Map<string, Number>();

            for (let localComp of this.STANDARD_CLIENT_COMPLIMENTS) {

                this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(clientId).collection(this.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).ref
                    .where('complimentCode', '==', localComp.code).get()
                    .then(querySnapshot => {
                        this.validators.validateQuerySnapshot(querySnapshot)
                            .then(querySnapshot => {
                                tempCompMap.set(localComp.code, querySnapshot.size);
                                if (tempCompMap.size == this.STANDARD_CLIENT_COMPLIMENTS.length) {

                                    tempComp.sort(function (a, b) { return tempCompMap.get(b.code).valueOf() - tempCompMap.get(a.code).valueOf() });

                                    for (let comp of tempComp) {
                                        tempCompReport.push({
                                            text: comp.text,
                                            code: comp.code,
                                            quantity: tempCompMap.get(comp.code).valueOf()
                                        });
                                    }

                                    resolve(tempCompReport);
                                }
                            })
                            .catch(e => {
                                reject(e)
                            })
                    })
                    .catch(e => {
                        reject(e);
                    });

            }
        });
    }
    /**
     * Retorna o numero de pedidos Prestados
     * @param {string} providerId Id do Usuário
     * @return  Promise<number>, Numero de pedidos
     */
    public getAsProviderCompletedOrderTotalNumber(providerId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(providerId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION).ref
                .where('providerId', '==', providerId).get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
     * Retorna o numero de pedidos Prestados
     * @param {string} clientId Id do Usuário
     * @return  Promise<number>, Numero de pedidos
     */
    public getAsClientCompletedOrderTotalNumber(clientId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(clientId).collection(this.COLLECTIONS.ORDER_COMPLETIONS_COLLECTION).ref
                .where('clientId', '==', clientId).get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
     * Retorna o numero de reviews recebidas como prestador 
     * @param {string} providerId Id do Usuário
     * @return  Promise<number>
     */
    public getAsProviderReceivedReviewsNumber(providerId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(providerId).collection(this.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref.get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
     * Retorna o numero de reviews recebidas como cliente 
     * @param {string} clientId Id do Usuário
     * @return  Promise<number>
     */
    public getAsClientReceivedReviewsNumber(clientId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            return this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(clientId).collection(this.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).ref.get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
    * retorna o numero de reviews que o prestador recebeu
    * @param {string} userId
    * @return Retorna uma `number`
    */
    public getUserAsProviderReviewsNumber(userId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(userId).collection(this.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).ref.get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
    /**
    * retorna o numero de reviews que o cliente recebeu
   * @param {string} userId
    * @return Retorna uma `number`
    */
    public getUserAsClientReviewsNumber(userId: string): Promise<number> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(userId).collection(this.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).ref.get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            resolve(querySnapshot.size);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        });
    }
}