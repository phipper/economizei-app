import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../../../COLLECTIONS";
import { Validators } from "../../../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { DocumentSnapshot } from "@firebase/firestore-types";
import { Notification } from "../../../../../../models/Notification";

@Injectable()
export class NotificationsCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public create(userId: string, data: Notification): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(userId).collection(this.COLLECTIONS.NOTIFICATIONS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }

        })
    }
    public getDoc(userId: string, notificationId: string): Promise<DocumentSnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(userId).collection(this.COLLECTIONS.NOTIFICATIONS_COLLECTION).doc(notificationId).ref.get()
                .then(doc => {
                    resolve(this.validators.validateDoc(doc));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public get(userId: string, notificationId: string): Promise<Notification> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(userId).collection(this.COLLECTIONS.NOTIFICATIONS_COLLECTION).doc(notificationId).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Notification);
                            } else {
                                reject({ message: `there is no Notification with id: ${notificationId}` });
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public update(userId: string, notificationId: string, data: Partial<Notification>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(userId).collection(this.COLLECTIONS.NOTIFICATIONS_COLLECTION).doc(notificationId).update(data));
        })
    }
    public delete(userId: string, notificationId: string): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(userId).collection(this.COLLECTIONS.NOTIFICATIONS_COLLECTION).doc(notificationId).delete());
        })
    }
}