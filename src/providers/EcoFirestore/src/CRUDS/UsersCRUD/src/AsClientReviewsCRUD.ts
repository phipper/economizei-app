import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../../../COLLECTIONS";
import { AngularFirestore } from "angularfire2/firestore";
import { AsClientReview } from "../../../../../../models/AsClientReview";
import { User } from "../../../../../../models/User";

@Injectable()
export class AsClientReviewsCRUD {
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }

    public getPoints(data: AsClientReview): number {

        let tempPoints = data.providerOpinion;

        if (data.punctuality == 1) {
            tempPoints = tempPoints + 1;
        } else {
            tempPoints = tempPoints - 1;
        }

        if (data.complimentCode != '') {
            tempPoints = tempPoints + 1;
        }

        return tempPoints;
    }

    private updateClientReputationForNewReview(data: AsClientReview): Promise<void> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.clientId).ref.get()
                .then(clientDoc => {
                    let tempClient = clientDoc.data() as User;

                    tempClient.clientReputationCount = tempClient.clientReputationCount + 1;
                    tempClient.clientReputationSum = tempClient.clientReputationSum + data.providerOpinion;
                    tempClient.clientReputation = tempClient.clientReputationSum / tempClient.clientReputationCount;
                    tempClient.clientPoints = tempClient.clientPoints + data.points;

                    if (data.punctuality == 1) {
                        tempClient.asClientOnTimeOrders = tempClient.asClientOnTimeOrders + 1;
                    } else {
                        tempClient.asClientLateOrders = tempClient.asClientLateOrders + 1;
                    }

                    clientDoc.ref.update({
                        clientReputationCount: tempClient.clientReputationCount,
                        clientReputationSum: tempClient.clientReputationSum,
                        clientPoints: tempClient.clientPoints,
                        clientReputation: tempClient.clientReputation,
                        asClientOnTimeOrders: tempClient.asClientOnTimeOrders,
                        asClientLateOrders: tempClient.asClientLateOrders
                    })
                        .then(() => {
                            resolve();
                        })
                        .catch(e => {
                            reject(e)
                        })

                })
                .catch(e => {
                    reject(e)
                })
        });
    }

    public create(data: AsClientReview): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                data.points = this.getPoints(data);
                this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.clientId).collection(this.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).doc(data.id).set(Object.assign({}, data))
                    .then(() => {
                        this.updateClientReputationForNewReview(data)
                            .then(() => {
                                resolve();
                            })
                            .catch(e => {
                                reject(e);
                            })
                    })
                    .catch(e => {
                        reject(e);
                    })
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }

    private updateClientReputationForReviewUpdate(oldData: AsClientReview, newData: AsClientReview): Promise<void> {
        return new Promise((resolve, reject) => {

            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(oldData.clientId).ref.get()
                .then(clientDoc => {
                    let tempClient = clientDoc.data() as User;

                    tempClient.clientReputationSum = tempClient.clientReputationSum + (newData.providerOpinion - oldData.providerOpinion);
                    tempClient.clientReputation = tempClient.clientReputationSum / tempClient.clientReputationCount;
                    tempClient.clientPoints = tempClient.clientPoints + (newData.points - oldData.points);

                    if (oldData.punctuality != newData.punctuality) {
                        if (newData.punctuality == 1) {
                            tempClient.asClientOnTimeOrders = tempClient.asClientOnTimeOrders + 1;
                            tempClient.asClientLateOrders = tempClient.asClientLateOrders - 1;
                        } else {
                            tempClient.asClientOnTimeOrders = tempClient.asClientOnTimeOrders - 1;
                            tempClient.asClientLateOrders = tempClient.asClientLateOrders + 1;
                        }
                    }

                    clientDoc.ref.update({
                        clientReputationCount: tempClient.clientReputationCount,
                        clientReputationSum: tempClient.clientReputationSum,
                        clientPoints: tempClient.clientPoints,
                        clientReputation: tempClient.clientReputation,
                        asClientOnTimeOrders: tempClient.asClientOnTimeOrders,
                        asClientLateOrders: tempClient.asClientLateOrders
                    })
                        .then(() => {
                            resolve();
                        })
                        .catch(e => {
                            reject(e)
                        })

                })
                .catch(e => {
                    reject(e)
                })
        });
    }

    public update(data: AsClientReview): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.clientId).collection(this.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).doc(data.id).ref.get()
                    .then(oldDataDoc => {
                        var oldData = oldDataDoc.data() as AsClientReview;
                        var newData = data;
                        newData.points = this.getPoints(newData);
                        this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.clientId).collection(this.COLLECTIONS.AS_CLIENT_REVIEWS_COLLECTION).doc(data.id).set(Object.assign({}, newData))
                            .then(() => {
                                this.updateClientReputationForReviewUpdate(oldData, newData)
                                    .then(() => {
                                        resolve();
                                    })
                                    .catch(e => {
                                        reject(e);
                                    })
                            })
                            .catch(e => {
                                reject(e);
                            })
                    })
                    .catch(e => {
                        reject(e);
                    })
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
}