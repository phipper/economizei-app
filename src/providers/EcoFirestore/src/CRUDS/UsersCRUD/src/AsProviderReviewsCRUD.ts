import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../../../COLLECTIONS";
import { AngularFirestore } from "angularfire2/firestore";
import { AsProviderReview } from "../../../../../../models/AsProviderReview";
import { Banner } from "../../../../../../models/Banner";
import { User } from "../../../../../../models/User";


@Injectable()
export class AsProviderReviewsCRUD {
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }

    public getPoints(data: AsProviderReview): number {

        let tempPoints = data.clientOpinion;

        if (data.punctuality == 1) {
            tempPoints = tempPoints + 1;
        } else {
            tempPoints = tempPoints - 1;
        }

        if (data.serviceDelivery == 1) {
            tempPoints = tempPoints + 1;
        } else {
            tempPoints = tempPoints - 1;
        }

        if (data.budgetAccuracy == 1) {
            tempPoints = tempPoints + 1;
        } else {
            tempPoints = tempPoints - 1;
        }

        if (data.complimentCode != '') {
            tempPoints = tempPoints + 1;
        }

        return tempPoints;
    }

    private updateBannerReputationForNewReview(data: AsProviderReview): Promise<void> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(data.bannerId).ref.get()
                .then(bannerDoc => {
                    let tempBanner = bannerDoc.data() as Banner;
                    tempBanner.bannerReputationCount = tempBanner.bannerReputationCount + 1;
                    tempBanner.bannerReputationSum = tempBanner.bannerReputationSum + data.clientOpinion;
                    tempBanner.bannerReputation = tempBanner.bannerReputationSum / tempBanner.bannerReputationCount;
                    tempBanner.points = tempBanner.points + data.points;
                    bannerDoc.ref.update({
                        bannerReputationCount: tempBanner.bannerReputationCount,
                        bannerReputationSum: tempBanner.bannerReputationSum,
                        bannerReputation: tempBanner.bannerReputation,
                        points: tempBanner.points
                    })
                        .then(() => {
                            resolve();
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        });
    }

    private updateProviderReputationForNewReview(data: AsProviderReview): Promise<void> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.providerId).ref.get()
                .then(providerDoc => {
                    let temProvider = providerDoc.data() as User;

                    temProvider.providerReputationCount = temProvider.providerReputationCount + 1;
                    temProvider.providerReputationSum = temProvider.providerReputationSum + data.clientOpinion;
                    temProvider.providerReputation = temProvider.providerReputationSum / temProvider.providerReputationCount;
                    temProvider.providerPoints = temProvider.providerPoints + data.points;


                    if (data.punctuality == 1) {
                        temProvider.asProviderOnTimeOrders = temProvider.asProviderOnTimeOrders + 1;
                    } else {
                        temProvider.asProviderLateOrders = temProvider.asProviderLateOrders + 1;
                    }

                    if (data.serviceDelivery == 1) {
                        temProvider.asProviderOnTimeDeliveredOrders = temProvider.asProviderOnTimeDeliveredOrders + 1;
                    } else {
                        temProvider.asProviderLateDeliveredOrders = temProvider.asProviderLateDeliveredOrders + 1;
                    }

                    if (data.budgetAccuracy == 1) {
                        temProvider.metBudgetOrders = temProvider.metBudgetOrders + 1;
                    } else {
                        temProvider.notMetBudgetOrders = temProvider.notMetBudgetOrders + 1;
                    }

                    providerDoc.ref.update({
                        providerPoints: temProvider.providerPoints,
                        providerReputation: temProvider.providerReputation,
                        providerReputationCount: temProvider.providerReputationCount,
                        providerReputationSum: temProvider.providerReputationSum,
                        asProviderOnTimeOrders: temProvider.asProviderOnTimeOrders,
                        asProviderLateOrders: temProvider.asProviderLateOrders,
                        asProviderOnTimeDeliveredOrders: temProvider.asProviderOnTimeDeliveredOrders,
                        asProviderLateDeliveredOrders: temProvider.asProviderLateDeliveredOrders,
                        metBudgetOrders: temProvider.metBudgetOrders,
                        notMetBudgetOrders: temProvider.notMetBudgetOrders
                    })
                        .then(() => {
                            resolve();
                        })
                        .catch(e => {
                            reject(e)
                        })

                })
                .catch(e => {
                    reject(e)
                })
        });
    }

    public create(data: AsProviderReview): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                data.points = this.getPoints(data);
                this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.providerId).collection(this.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).doc(data.id).set(Object.assign({}, data))
                    .then(() => {
                        this.updateProviderReputationForNewReview(data)
                            .then(() => {
                                if (data.bannerId != '') {
                                    this.updateBannerReputationForNewReview(data)
                                        .then(() => {
                                            resolve();
                                        })
                                        .catch(e => {
                                            reject(e);
                                        })
                                } else {
                                    resolve();
                                }
                            })
                            .catch(e => {
                                reject(e);
                            })
                    })
                    .catch(e => {
                        reject(e);
                    })
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }

    private updateBannerReputationForReviewUpdate(oldData: AsProviderReview, newData: AsProviderReview): Promise<void> {
        return new Promise((resolve, reject) => {

            this.afs.collection(this.COLLECTIONS.BANNERS_COLLECTION).doc(oldData.bannerId).ref.get()
                .then(bannerDoc => {
                    let tempBanner = bannerDoc.data() as Banner;

                    tempBanner.bannerReputationSum = tempBanner.bannerReputationSum + (newData.clientOpinion - oldData.clientOpinion);
                    tempBanner.bannerReputation = tempBanner.bannerReputationSum / tempBanner.bannerReputationCount;
                    tempBanner.points = tempBanner.points + (newData.points - oldData.points);

                    bannerDoc.ref.update({
                        bannerReputationCount: tempBanner.bannerReputationCount,
                        bannerReputationSum: tempBanner.bannerReputationSum,
                        bannerReputation: tempBanner.bannerReputation,
                        points: tempBanner.points
                    })
                        .then(() => {
                            resolve();
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })

        });
    }

    private updateProviderReputationForReviewUpdate(oldData: AsProviderReview, newData: AsProviderReview): Promise<void> {
        return new Promise((resolve, reject) => {

            this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(oldData.providerId).ref.get()
                .then(providerDoc => {
                    let temProvider = providerDoc.data() as User;

                    temProvider.providerReputationSum = temProvider.providerReputationSum + (newData.clientOpinion - oldData.clientOpinion);
                    temProvider.providerReputation = temProvider.providerReputationSum / temProvider.providerReputationCount;
                    temProvider.providerPoints = temProvider.providerPoints + (newData.points - oldData.points);

                    if (oldData.punctuality != newData.punctuality) {
                        if (newData.punctuality == 1) {
                            temProvider.asProviderOnTimeOrders = temProvider.asProviderOnTimeOrders + 1;
                            temProvider.asProviderLateOrders = temProvider.asProviderLateOrders - 1;
                        } else {
                            temProvider.asProviderOnTimeOrders = temProvider.asProviderOnTimeOrders - 1;
                            temProvider.asProviderLateOrders = temProvider.asProviderLateOrders + 1;
                        }
                    }

                    if (oldData.serviceDelivery != newData.serviceDelivery) {
                        if (newData.serviceDelivery == 1) {
                            temProvider.asProviderOnTimeDeliveredOrders = temProvider.asProviderOnTimeDeliveredOrders + 1;
                            temProvider.asProviderLateDeliveredOrders = temProvider.asProviderLateDeliveredOrders - 1;
                        } else {
                            temProvider.asProviderOnTimeDeliveredOrders = temProvider.asProviderOnTimeDeliveredOrders - 1;
                            temProvider.asProviderLateDeliveredOrders = temProvider.asProviderLateDeliveredOrders + 1;
                        }
                    }

                    if (oldData.budgetAccuracy != newData.budgetAccuracy) {
                        if (newData.budgetAccuracy == 1) {
                            temProvider.metBudgetOrders = temProvider.metBudgetOrders + 1;
                            temProvider.notMetBudgetOrders = temProvider.notMetBudgetOrders - 1;
                        } else {
                            temProvider.metBudgetOrders = temProvider.metBudgetOrders - 1;
                            temProvider.notMetBudgetOrders = temProvider.notMetBudgetOrders + 1;
                        }
                    }

                    providerDoc.ref.update({
                        providerPoints: temProvider.providerPoints,
                        providerReputation: temProvider.providerReputation,
                        providerReputationCount: temProvider.providerReputationCount,
                        providerReputationSum: temProvider.providerReputationSum,
                        asProviderOnTimeOrders: temProvider.asProviderOnTimeOrders,
                        asProviderLateOrders: temProvider.asProviderLateOrders,
                        asProviderOnTimeDeliveredOrders: temProvider.asProviderOnTimeDeliveredOrders,
                        asProviderLateDeliveredOrders: temProvider.asProviderLateDeliveredOrders,
                        metBudgetOrders: temProvider.metBudgetOrders,
                        notMetBudgetOrders: temProvider.notMetBudgetOrders
                    })
                        .then(() => {
                            resolve();
                        })
                        .catch(e => {
                            reject(e)
                        })

                })
                .catch(e => {
                    reject(e)
                })
        });
    }

    public update(data: AsProviderReview): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.providerId).collection(this.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).doc(data.id).ref.get()
                    .then(oldDataDoc => {
                        var oldData = oldDataDoc.data() as AsProviderReview;
                        var newData = data;
                        newData.points = this.getPoints(newData);
                        this.afs.collection(this.COLLECTIONS.USERS_COLLECTION).doc(data.providerId).collection(this.COLLECTIONS.AS_PROVIDER_REVIEWS_COLLECTION).doc(data.id).set(Object.assign({}, newData))
                            .then(() => {
                                this.updateProviderReputationForReviewUpdate(oldData, newData)
                                    .then(() => {
                                        if (data.bannerId != '') {
                                            this.updateBannerReputationForReviewUpdate(oldData, newData)
                                                .then(() => {
                                                    resolve();
                                                })
                                                .catch(e => {
                                                    reject(e);
                                                })
                                        } else {
                                            resolve();
                                        }
                                    })
                                    .catch(e => {
                                        reject(e);
                                    })
                            })
                            .catch(e => {
                                reject(e);
                            })
                    })
                    .catch(e => {
                        reject(e);
                    })
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
}