import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { OfficeHours } from "../../../../models/OfficeHours";

@Injectable()
export class OfficeHoursCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    /**
     * busca por horario de trabalho para o prestador passado como parametro
     * caso nao encotre retorna null
     * @param providerId The path to compare
     * @return `Promise<OfficeHours | null>`
     */
    public get(providerId: string): Promise<OfficeHours | null> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.OFFICE_HOURS_COLLECTION).ref
                .where('providerId', '==', providerId)
                .get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            if (!querySnapshot.empty) {
                                resolve(querySnapshot.docs[0].data() as OfficeHours);
                            } else {
                                resolve(null);
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        })
    }
    public create(data: OfficeHours): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.OFFICE_HOURS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
    public update(id: string, data: Partial<OfficeHours>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.OFFICE_HOURS_COLLECTION).doc(id).update(data));
        })
    }
}