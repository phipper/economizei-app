import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { HomePageBanner } from "../../../../models/HomePageBanner";

@Injectable()
export class HomePageBannersCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public getActives(): Promise<HomePageBanner[]> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.HOME_PAGE_BANNERS_COLLECTION).ref.where('status', '==', 'active').orderBy('position', 'asc').get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            let tempData: HomePageBanner[] = [];
                            for (let doc of querySnapshot.docs) {
                                tempData.push(doc.data() as HomePageBanner);
                            }
                            resolve(tempData);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                })
        })
    }
}