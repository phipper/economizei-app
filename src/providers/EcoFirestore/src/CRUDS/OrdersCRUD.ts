import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { ChildCollectionCRUD } from "../models/ChildCollectionCRUD";
import { Characteristic } from "../../../../models/Characteristic";
import { Schedule } from "../../../../models/Schedule";
import { Budget } from "../../../../models/Budget";
import { Order } from "../../../../models/Order";

@Injectable()
export class OrdersCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();

    public characteristics: ChildCollectionCRUD<Characteristic>;
    public schedules: ChildCollectionCRUD<Schedule>;
    public budgets: ChildCollectionCRUD<Budget>;

    constructor(private afs: AngularFirestore) {
        this.characteristics = new ChildCollectionCRUD(afs, this.COLLECTIONS.ORDERS_COLLECTION, this.COLLECTIONS.CHARACTERISTICS_COLLECTION, 'orderNumber');
        this.schedules = new ChildCollectionCRUD(afs, this.COLLECTIONS.ORDERS_COLLECTION, this.COLLECTIONS.SCHECDULES_COLLECTION, 'createdOn', 'desc');
        this.budgets = new ChildCollectionCRUD(afs, this.COLLECTIONS.ORDERS_COLLECTION, this.COLLECTIONS.BUDGETS_COLLECTION, 'createdOn', 'desc');
    }
    public create(data: Order): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.ORDERS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }

        })
    }
    public get(id: string): Promise<Order> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.ORDERS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Order);
                            } else {
                                reject({ message: `there is no Order with id: ${id}` });
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public update(id: string, data: Partial<Order>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.ORDERS_COLLECTION).doc(id).update(data));
        })
    }

}