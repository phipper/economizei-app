import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { Category } from "../../../../models/Category";

@Injectable()
export class CategoriesCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public getAll(): Promise<Category[]> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.CATEGORIES_COLLECTION).ref.orderBy('index', 'asc').get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            let tempData: Category[] = [];
                            for (let doc of querySnapshot.docs) {
                                tempData.push(doc.data() as Category);
                            }
                            resolve(tempData);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                })
        })
    }
    public get(id: string): Promise<Category> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.CATEGORIES_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Category);
                            } else {
                                reject({ message: `there is no Category with id: ${id}` })
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
}