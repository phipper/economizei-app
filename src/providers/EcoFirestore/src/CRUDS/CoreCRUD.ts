import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { Core } from "../../../../models/Core";

@Injectable()
export class CoreCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public getCore(): Promise<Core> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.CORE_COLLECTION).doc('EcoCore').ref.get()
                .then(coreDoc => {
                    this.validators.validateDoc(coreDoc)
                        .then(doc => {
                            resolve(doc.data() as Core);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                })
        })
    }
}