import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { UserRegister } from "../../../../models/UserRegister";

@Injectable()
export class UsersRegistersCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    /**
     * busca por um registro para o userId passado como parametro
     * caso não encotre retorna null
     * @param userId
     * @return `Promise<UserRegister | null>`
     */
    public get(userId: string): Promise<UserRegister | null> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.USERS_REGISTERS_COLLECTION).doc(userId).ref.get()
                .then(docRef => {
                    this.validators.validateDoc(docRef)
                        .then(docRef => {
                            if (docRef.exists) {
                                resolve(docRef.data() as UserRegister);
                            } else {
                                resolve(null);
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e);
                });
        })
    }
    public create(data: UserRegister): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.USERS_REGISTERS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
    public delete(id: string, data: Partial<UserRegister>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.USERS_REGISTERS_COLLECTION).doc(id).delete());
        })
    }
}