import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { Address } from "../../../../models/Address";

@Injectable()
export class AddressesCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public get(id: string): Promise<Address> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.ADDRESSES_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Address);
                            } else {
                                reject({ message: `there is no Address with id: ${id}` })
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public getUserAddresses(userId: string): Promise<Address[]> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.ADDRESSES_COLLECTION).ref.where('userId', '==', userId).get()
                .then(querySnapshot => {
                    this.validators.validateQuerySnapshot(querySnapshot)
                        .then(querySnapshot => {
                            let tempData: Address[] = [];
                            for (let doc of querySnapshot.docs) {
                                tempData.push(doc.data() as Address);
                            }
                            resolve(tempData);
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public create(data: Address): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.ADDRESSES_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
    public update(id: string, data: Partial<Address>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.ADDRESSES_COLLECTION).doc(id).update(data));
        })
    }
    public delete(id: string): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.ADDRESSES_COLLECTION).doc(id).delete());
        })
    }
}