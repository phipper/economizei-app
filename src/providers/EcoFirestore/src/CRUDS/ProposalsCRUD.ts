import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";
import { DocumentSnapshot } from "@firebase/firestore-types";
import { COLLECTIONS } from "../../COLLECTIONS";
import { Validators } from "../util/Validators";
import { Proposal } from "../../../../models/Proposal";


@Injectable()
export class ProposalsCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public getDoc(id: string): Promise<DocumentSnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.PROPOSALS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    resolve(this.validators.validateDoc(doc));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public get(id: string): Promise<Proposal> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.PROPOSALS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Proposal);
                            } else {
                                reject({ message: `there is no Banner with id: ${id}` });
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public create(data: Proposal): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.PROPOSALS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
    public update(id: string, data: Partial<Proposal>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.PROPOSALS_COLLECTION).doc(id).update(data));
        })
    }
}