import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../../../COLLECTIONS";
import { Validators } from "../../../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { Message } from "../../../../../../models/Message";
import { DocumentSnapshot } from "@firebase/firestore-types";

@Injectable()
export class MessagesCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    constructor(private afs: AngularFirestore) { }
    public create(chatId: string, data: Message): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.CHATS_COLLECTION).doc(chatId).collection(this.COLLECTIONS.MESSAGES_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }

        })
    }
    public getDoc(id: string, messageId: string): Promise<DocumentSnapshot> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.CHATS_COLLECTION).doc(id).collection(this.COLLECTIONS.MESSAGES_COLLECTION).doc(messageId).ref.get()
                .then(doc => {
                    resolve(this.validators.validateDoc(doc));
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public update(id: string, messageId: string, data: Partial<Notification>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.CHATS_COLLECTION).doc(id).collection(this.COLLECTIONS.MESSAGES_COLLECTION).doc(messageId).update(data));
        })
    }
}