import { Injectable } from "@angular/core";
import { COLLECTIONS } from "../../../COLLECTIONS";
import { Validators } from "../../util/Validators";
import { AngularFirestore } from "angularfire2/firestore";
import { MessagesCRUD } from "./src/MessagesCRUD";
import { Chat } from "../../../../../models/Chat";

@Injectable()
export class ChatsCRUD {
    private validators = new Validators();
    private COLLECTIONS = new COLLECTIONS();
    public messages: MessagesCRUD;
    constructor(private afs: AngularFirestore) {
        this.messages = new MessagesCRUD(afs);
    }
    public get(id: string): Promise<Chat> {
        return new Promise((resolve, reject) => {
            this.afs.collection(this.COLLECTIONS.CHATS_COLLECTION).doc(id).ref.get()
                .then(doc => {
                    this.validators.validateDoc(doc)
                        .then(doc => {
                            if (doc.exists) {
                                resolve(doc.data() as Chat);
                            } else {
                                reject({ message: `there is no Chat with id: ${id}` })
                            }
                        })
                        .catch(e => {
                            reject(e)
                        })
                })
                .catch(e => {
                    reject(e)
                })
        })
    }
    public create(data: Chat): Promise<void> {
        return new Promise((resolve, reject) => {
            if (data.id != '') {
                resolve(this.afs.collection(this.COLLECTIONS.CHATS_COLLECTION).doc(data.id).set(Object.assign({}, data)));
            } else {
                reject({ message: 'Id invalido!' });
            }
        })
    }
    public update(id: string, data: Partial<Chat>): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.CHATS_COLLECTION).doc(id).update(data));
        })
    }
    public delete(id: string): Promise<void> {
        return new Promise((resolve) => {
            resolve(this.afs.collection(this.COLLECTIONS.CHATS_COLLECTION).doc(id).delete());
        })
    }
}