import { Injectable } from '@angular/core';
import { OrderByDirection } from '@firebase/firestore-types';
import { AngularFirestore } from 'angularfire2/firestore';
import { Validators } from './src/util/Validators';
import { COLLECTIONS } from './COLLECTIONS';
import { CoreCRUD } from './src/CRUDS/CoreCRUD';
import { HomePageBannersCRUD } from './src/CRUDS/HomePageBannersCRUD';
import { CategoriesCRUD } from './src/CRUDS/CategoriesCRUD';
import { ServiceTypesCRUD } from './src/CRUDS/ServiceTypesCRUD';
import { UsersCRUD } from './src/CRUDS/UsersCRUD/UsersCRUD';
import { OfficeHoursCRUD } from './src/CRUDS/OfficeHoursCRUD';
import { AddressesCRUD } from './src/CRUDS/AddressesCRUD';
import { BannersCRUD } from './src/CRUDS/BannersCRUD';
import { RequestsCRUD } from './src/CRUDS/RequestsCRUD';
import { OrdersCRUD } from './src/CRUDS/OrdersCRUD';
import { ChatsCRUD } from './src/CRUDS/ChatsCRUD/ChatsCRUD';
import { ProviderInvitesCRUD } from './src/CRUDS/ProviderInvitesCRUD';
import { ProposalsCRUD } from './src/CRUDS/ProposalsCRUD';
import { UsersRegistersCRUD } from './src/CRUDS/UsersRegistersCRUD';


@Injectable()
export class EcoFirestore {

    public validators = new Validators();
    public COLLECTIONS = new COLLECTIONS();

    public core: CoreCRUD;
    public homePageBanners: HomePageBannersCRUD;
    public categories: CategoriesCRUD;
    public serviceTypes: ServiceTypesCRUD;
    public users: UsersCRUD;
    public usersRegisters: UsersRegistersCRUD;
    public officeHours: OfficeHoursCRUD;
    public addresses: AddressesCRUD;
    public banners: BannersCRUD;
    public requests: RequestsCRUD;
    public proposals: ProposalsCRUD;
    public orders: OrdersCRUD;
    public chats: ChatsCRUD;
    public providerInvites: ProviderInvitesCRUD;

    public createId(): string { return this.afs.createId() };

    /**
    * Organiza um array de objetos pelo campo createdOn
    * @param array array to sort 
    * @param orderByDirectionStr Opcional direção para organizar ('asc' or 'desc'). If
    * not specified, order will be ascending.
    * @return The sorted array
    */
    public sortByCreatedOn(array: any[], orderByDirectionStr?: OrderByDirection): any[] {
        if (orderByDirectionStr === 'desc') {
            return array.sort((a: any, b: any) => {
                return b.createdOn.getTime() - a.createdOn.getTime();
            });
        } else {
            return array.sort((a: any, b: any) => {
                return a.createdOn.getTime() - b.createdOn.getTime();
            });
        }
    }

    constructor(
        public afs: AngularFirestore
    ) {
        this.core = new CoreCRUD(afs);
        this.homePageBanners = new HomePageBannersCRUD(afs);
        this.categories = new CategoriesCRUD(afs);
        this.serviceTypes = new ServiceTypesCRUD(afs);
        this.users = new UsersCRUD(afs);
        this.usersRegisters = new UsersRegistersCRUD(afs);
        this.officeHours = new OfficeHoursCRUD(afs);
        this.addresses = new AddressesCRUD(afs);
        this.banners = new BannersCRUD(afs);
        this.requests = new RequestsCRUD(afs);
        this.proposals = new ProposalsCRUD(afs);
        this.orders = new OrdersCRUD(afs);
        this.chats = new ChatsCRUD(afs);
        this.providerInvites = new ProviderInvitesCRUD(afs);
    }
}