import { Injectable } from "@angular/core";

@Injectable()
export class COLLECTIONS {

    readonly CORE_COLLECTION = 'EcoCore';

    readonly HOME_PAGE_BANNERS_COLLECTION = 'HomePageBanners';

    readonly CATEGORIES_COLLECTION = 'Categories';

    readonly SERVICE_TYPES_COLLECTION = 'ServiceTypes';

    readonly SERVICE_TYPES_SUBS_COLLECTION = 'ServiceTypeSubs';

    readonly USERS_COLLECTION = 'Users';
    readonly USERS_REGISTERS_COLLECTION = 'UsersRegisters';

    readonly NOTIFICATIONS_COLLECTION = 'Notifications';
    readonly ORDER_COMPLETIONS_COLLECTION = 'OrderCompletions';
    readonly AS_PROVIDER_REVIEWS_COLLECTION = 'AsProviderReviews';
    readonly AS_CLIENT_REVIEWS_COLLECTION = 'AsClientReviews';

    readonly OFFICE_HOURS_COLLECTION = 'OfficeHours';

    readonly ADDRESSES_COLLECTION = 'Addresses';

    readonly BANNERS_COLLECTION = 'Banners';
    readonly PICS_COLLECTION = 'Pics';
    readonly VIEWS_COLLECTION = 'Views';
    readonly CHARACTERISTICS_COLLECTION = 'Characteristics';

    readonly ORDERS_COLLECTION = 'Orders';
    readonly SCHECDULES_COLLECTION = 'Schedules';
    readonly BUDGETS_COLLECTION = 'Budgets';

    readonly REQUESTS_COLLECTION = 'Requests';
    readonly PROPOSALS_COLLECTION = 'Proposals';

    readonly CHATS_COLLECTION = 'Chats';
    readonly MESSAGES_COLLECTION = 'Messages';

    readonly PROVIDER_INVITES_COLLECTION = 'ProviderInvites';

    readonly SAC_BUSINESS_ROLES_COLLECTION = 'SacBusinessRoles';

    readonly SAC_CATEGORIES_COLLECTION = 'SacCategories';

    readonly SAC_SUB_CATEGORIES_COLLECTION = 'SacSubCategories';

    readonly SAC_SOLVER_TEAMS_COLLECTION = 'SacSolverTeams';


}