import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { User } from '../models/User';

@Injectable()
export class FcmProvider {

  constructor(
    public firebaseNative: Firebase,
    public afs: AngularFirestore,
    private platform: Platform
  ) {
  }

  /**
   * Get permission, the token and update if is needed
   * @param {string} userId 
   * @param {string} oldToken
   */
  async updateToken(userId: string, oldToken: string) {
    let newToken;
    if (this.platform.is('android')) {
      newToken = await this.firebaseNative.getToken()
      if (oldToken != newToken) {
        return this.saveTokenToFirestore(userId, newToken);
      }
      return;
    }
    if (this.platform.is('ios')) {
      newToken = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
      if (oldToken != newToken) {
        return this.saveTokenToFirestore(userId, newToken);
      }
      return;
    }
  }

  /**
   * Save the token to firestore
   * @param {string} userId 
   * @param {string} newToken
   */
  private saveTokenToFirestore(userId: string, newToken: string) {
    if (!newToken) return;
    return this.afs.collection('Users').doc<User>(userId).update({ token: newToken });
  }

  // Listen to incoming FCM messages
  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen()
  }

}