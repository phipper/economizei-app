import { Injectable } from '@angular/core';
import { Http } from '@angular/http';



@Injectable()
export class DynamicLinksApiProvider {

  private firebaseAuthApiKey = "AIzaSyDDNN_49-8oTn9pgeBtucMQ_w3kVnXUTHo";

  private dynamicLinkDomain = "economizei.page.link";
  private androidPackageName = "com.phipper.economizei";
  private iosBundleId = "com.phipper.economizei.ios";
  private deskTopFallBackLink = "https://eueconomizei.com.br/";
  private standartRequestBody = {
    "longDynamicLink": '',
    "suffix": {
      "option": "UNGUESSABLE"
    }
  }

  constructor(
    private http: Http
  ) {
  }

  /**
 * Cria Dynamic Link para o ProviderInvite quais os dados são passados como parametro
 * @param {string} id Id do ProviderInvite
 * @param {string} providerName titulo do banner
 * @param {string} providerThumbnailUrl descrição do banner
 * @return Retorna uma `Promise` que é resolvida com o corpo da resposta http da API do firebase Dynamic Link
 * `res.shortLink: string` - The generated short Dynamic Link.
 *  `res.previewLink: string` - A link to a flowchart of the Dynamic Link's behavior.
 * }
 */
  createProviderInviteDynamicLink(id: string, providerName: string, providerThumbnailUrl: string): Promise<any> {
    let myDeepLink = `https://app.eueconomizei.com.br/openProviderInvite/${id}`;
    let body = this.standartRequestBody;
    body.longDynamicLink = `https://${this.dynamicLinkDomain}/?link=${myDeepLink}&apn=${this.androidPackageName}&ibi=${this.iosBundleId}&ofl=${this.deskTopFallBackLink}&st=Convite%20de%20${providerName}&sd=Convite%20de%20${providerName}&si=${providerThumbnailUrl}`;
    return new Promise((resolve, reject) => {
      return this.http.post(`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${this.firebaseAuthApiKey}`, body).toPromise()
        .then(response => {
          resolve(JSON.parse(response.text()));
        }).catch(errorResponse => {
          reject(JSON.parse(errorResponse.text()).error);
        });
    });
  }

  /**
   * Cria Dynamic Link para o Banner quais os dados são passados como parametro
   * @param {string} id Id do banner
   * @param {string} title titulo do banner
   * @param {string} description descrição do banner
   * @param {string} thumbnailUrl thumbnailUrl para ser  The image should be at least 300x200 px, and less than 300 KB.
   * @return Retorna uma `Promise` que é resolvida com o corpo da resposta http da API do firebase Dynamic Link
   * `res.shortLink: string` - The generated short Dynamic Link.
   *  `res.previewLink: string` - A link to a flowchart of the Dynamic Link's behavior.
   * }
   */
  createBannerDynamicLink(id: string, title: string, description: string, thumbnailUrl: string): Promise<any> {
    let myDeepLink = `https://app.eueconomizei.com.br/showbanner/${id}`;
    let body = this.standartRequestBody;
    body.longDynamicLink = `https://${this.dynamicLinkDomain}/?link=${myDeepLink}&apn=${this.androidPackageName}&ibi=${this.iosBundleId}&ofl=${this.deskTopFallBackLink}&st=${title}&sd=${description}&si=${thumbnailUrl}`;
    return new Promise((resolve, reject) => {
      return this.http.post(`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${this.firebaseAuthApiKey}`, body).toPromise()
        .then(response => {
          resolve(JSON.parse(response.text()));
        }).catch(errorResponse => {
          reject(JSON.parse(errorResponse.text()).error);
        });
    });
  }

  /**
   * Cria Dynamic Link para a Request quais os dados são passados como parametro
   * @param {string} id Id daRequest
   * @param {string} title titulo da Request
   * @param {string} description descrição da Request
   * @param {string} thumbnailUrl thumbnailUrl para ser  The image should be at least 300x200 px, and less than 300 KB.
   * @return Retorna uma `Promise` que é resolvida com o corpo da resposta http da API do firebase Dynamic Link
   * `res.shortLink: string` - The generated short Dynamic Link.
   *  `res.previewLink: string` - A link to a flowchart of the Dynamic Link's behavior.
   * }
   */
  createRequestDynamicLink(id: string, title: string, description: string, thumbnailUrl: string): Promise<any> {
    let myDeepLink = `https://app.eueconomizei.com.br/showrequest/${id}`;
    let body = this.standartRequestBody;
    body.longDynamicLink = `https://${this.dynamicLinkDomain}/?link=${myDeepLink}&apn=${this.androidPackageName}&ibi=${this.iosBundleId}&ofl=${this.deskTopFallBackLink}&st=${title}&sd=${description}&si=${thumbnailUrl}`;
    return new Promise((resolve, reject) => {
      return this.http.post(`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${this.firebaseAuthApiKey}`, body).toPromise()
        .then(response => {
          resolve(JSON.parse(response.text()));
        }).catch(errorResponse => {
          reject(JSON.parse(errorResponse.text()).error);
        });
    });
  }

}