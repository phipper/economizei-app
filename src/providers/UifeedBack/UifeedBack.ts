import { Injectable } from '@angular/core';
import { Platform, ModalController } from 'ionic-angular';
import { Events, LoadingController, ToastController, AlertController, ActionSheetController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

import { HapticProvider } from '../Haptic';
// import { SacProvider } from './SAC';
// import { User } from '../models/User';

//Phipper LLC 02.06.2018

const STANDART_LOADER_LOTTIE_PATH = 'assets/animations/servishero_loading.json';

@Injectable()
export class UiFeedBackProvider {

  private hasActiveLoader = false;

  constructor(
    private platform: Platform,
    public toast: Toast,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public hapticCtrl: HapticProvider,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private modalCtrl: ModalController,
    private events: Events
    // ,private SacControler: SacProvider
  ) {
  }

  public presentOfflineToast(): void {
    this.presentPersistentToast(`Você esta offline tente novamente mais tarde!`, `warning`);
  }

  public presentOfflineAlert(): void {
    this.presenLottietAlert(`Sem internet`, `Você esta offline tente novamente mais tarde!`, `assets/animations/network_lost.json`, `warning`, true);
  }

  public offlineRedirect(): void {
    this.events.publish('NavProvider:setRoot', {
      page: `NoInternetPage`
    });
  }

  /**
   * Apresenta Toast para o usuário
   * @param {string} text Texto da Toast na parte inferior da tela
   * @param {string} type Tipo de haptic feedback da Toast (`success`, `warning` ou `error`)
   */
  public presentToast(text: string, type: string): void {
    if (this.platform.is('cordova')) {
      this.hapticCtrl.notification(type); // success, warning and error
      this.toast.show(text, '2000', 'bottom').toPromise();
    } else {
      console.log('cordova not available using ionic-angular toast instead');
      let toast = this.toastCtrl.create({
        message: text,
        duration: 2000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  /**
   * Apresenta Toast para o usuário na parte superior da tela
   * @param {string} text Texto da Toast
   * @param {string} type Tipo de haptic feedback da Toast (`success`, `warning` ou `error`)
   */
  public presentToastTop(text: string, type: string): void {
    if (this.platform.is('cordova')) {
      this.hapticCtrl.notification(type); // success, warning and error
      this.toast.show(text, '2000', 'top').toPromise();
    } else {
      console.log('cordova not available using ionic-angular toast instead');
      let toast = this.toastCtrl.create({
        message: text,
        duration: 2000,
        position: 'top'
      });
      toast.present();
    }
  }

  /**
   * Apresenta Toast para o usuário
   * @param {string} text Texto da Toast no centro da tela
   * @param {string} type Tipo de haptic feedback da Toast (`success`, `warning` ou `error`)
   */
  public presentToastCenter(text: string, type: string): void {
    if (this.platform.is('cordova')) {
      this.hapticCtrl.notification(type); // success, warning and error
      this.toast.show(text, '2000', 'center').toPromise();
    } else {
      console.log('cordova not available using ionic-angular toast instead');
      let toast = this.toastCtrl.create({
        message: text,
        duration: 2000,
        position: 'center'
      });
      toast.present();
    }
  }

  /**
   * Apresenta Toast persistente para o usuário
   * @param {string} text Texto da Toast
   * @param {string} type Tipo de haptic feedback da Toast (`success`, `warning` ou `error`)
   */
  public presentPersistentToast(text: string, type: string): void {
    let toast = this.toastCtrl.create({
      message: text,
      showCloseButton: true,
      closeButtonText: 'Ok',
      position: 'bottom'
    });
    this.hapticCtrl.notification(type); // success, warning and error
    toast.present();
  }


  /**
   * Apresenta Toast persistente para o usuário na parte superior da tela
   * @param {string} text Texto da Toast
   * @param {string} type Tipo de haptic feedback da Toast (`success`, `warning` ou `error`)
   */
  public presentPersistentToastTop(text: string, type: string): void {
    let toast = this.toastCtrl.create({
      message: text,
      showCloseButton: true,
      closeButtonText: 'Ok',
      position: 'top'
    });
    this.hapticCtrl.notification(type); // success, warning and error
    toast.present();
  }

  /**
   * Apresenta aleta para o usuário
   * @param {string} title Título do alerta
   * @param {string} message Mensagem do alerta
   * @param {string} type Tipo de haptic feedback / lottie do alerta (`success`, `warning` e `error`)
   * @param {boolean} lottie `opcional` Se deverá ser exibido o lottie para o tipo de alerta 
   */
  public presentAlert(title: string, message: string, type: string, lottie?: boolean): void {
    let modal = this.modalCtrl.create('UiFeedBackAlertModal', {
      title: title,
      message: message,
      lottie: lottie,
      type: type
    });
    modal.present();
    this.dismissLoader();
  }

  /**
 * Apresenta aleta de informação para o usuário
 * @param {string} info Informação do alerta
 */
  public presentInfoAlert(info: string): void {
    this.presentAlert('Info', info, 'warning', false);
  }

  /**
   * Apresenta aleta com Lottie para o usuário
   * @param {string} title Título do alerta
   * @param {string} message Mensagem do alerta
   * @param {string} path Caminho do Lottie que será ser utilizado ex: `assets/animations/check_animation.json`
   * @param {string} type Tipo de haptic feedback (`success`, `warning` e `error`)
   * @param {boolean} loop `opcional` se o lottie ficara em loop (default: true)
   */
  public presenLottietAlert(title: string, message: string, path: string, type: string, loop?: boolean): void {
    let tempLoop = true;
    if (loop != null) {
      tempLoop = loop;
    }
    let modal = this.modalCtrl.create('UiFeedBackLottieAlertModal', {
      title: title,
      message: message,
      path: path,
      type: type,
      loop: tempLoop
    });
    modal.present();
    this.dismissLoader();
  }

  /**
   * Apresenta aleta de erro para o usuário
   * @param {string} title Título do erro
   * @param {string} error Erro
   */
  public presentErrorAlert(title: string, error: any): void {
    let errorMsg: string = error.message;
    let stringifiedError = JSON.stringify({ erro: error, msg: error.message });
    if (errorMsg != undefined) {
      const tempVet = errorMsg.split(' ');
      var isInternetError = false;
      for (let word of tempVet) {
        if (word.toLowerCase().replace(/[^a-zA-Z0-9_-]/g, '') == 'offline') {
          isInternetError = true;
          break;
        }
      }
      if (isInternetError) {
        this.offlineRedirect();
      } else {
        this.presentAlert(`${title} 😭`, error.message, 'error', true);
      }
    } else {
      this.presentAlert(`${title} 😭`, `Erro sem mensagem`, 'error', true);
    }
    console.log(`${title} 😭
    stringifiedError: ${stringifiedError}`);
  }


  /**
   * Apresenta loader com Lottie para o usuário
   * @param {string} message Mensagem do alerta
   * @param {string} path `opcional` Caminho do Lottie que deverá ser utilizado ex: `assets/animations/check_animation.json` se não for passado o lottie padrão sera utilizado.
   */
  public presenLoader(message: string, path?: string): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!this.hasActiveLoader) {
        let tempPath = STANDART_LOADER_LOTTIE_PATH;
        if (path != null) {
          tempPath = path
        }
        let modal = this.modalCtrl.create('UiFeedBackLottieLoaderModal', {
          message: message,
          path: tempPath
        });
        modal.present()
          .then(() => {
            this.hasActiveLoader = true;
            resolve();
          });
      } else {
        console.log(`App already has a active loader, updating message...`)
        this.updateLoaderMessage(message)
      }
    });
  }

  /**
   * Atualiza a mensagem do loader lottie ativo
   * @param {string} newMessage Nova mensagem
   */
  public updateLoaderMessage(newMessage: string) {
    if (this.hasActiveLoader) {
      this.events.publish('LottieLoaderCtrlChanel:updateMessage', {
        newMessage: newMessage
      });
    }
  }

  /**
   * Remove o lottie loader ativo
   */
  public dismissLoader(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.hasActiveLoader) {
        this.events.subscribe('LottieLoaderCtrlChanel:dismissCompleted', () => {
          this.events.unsubscribe('LottieLoaderCtrlChanel:dismissCompleted');
          this.hasActiveLoader = false;
          resolve();
        });
        this.events.publish('LottieLoaderCtrlChanel:dismiss')
      } else {
        resolve();
      }
    });
  }

}