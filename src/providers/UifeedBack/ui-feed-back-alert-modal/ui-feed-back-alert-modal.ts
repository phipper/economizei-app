import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HapticProvider } from '../../../providers/Haptic';
import { CoreProvider } from '../../CoreProvider';


@IonicPage()
@Component({
  selector: 'page-ui-feed-back-alert-modal',
  templateUrl: 'ui-feed-back-alert-modal.html',
})
export class UiFeedBackAlertModal {

  private SUCCESS_LOTTIE_PATH = 'assets/animations/check_animation.json';
  private WARNING_LOTTIE_PATH = 'assets/animations/warning.json';
  private ERROR_LOTTIE_PATH = 'assets/animations/uh_oh.json';

  title = '';
  message = '';
  type = '';
  lottie = false;

  lottieOptions = {
    loop: false,
    autoplay: true,
    path: ''
  }

  constructor(
    public viewCtrl: ViewController,
    private coreCtrl: CoreProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private hapticCtrl: HapticProvider
  ) {
    this.title = navParams.get('title');
    this.message = navParams.get('message');
    this.lottie = navParams.get('lottie');
    if (this.lottie == true) {
      this.type = navParams.get('type');
      switch (this.type) {
        case 'success':
          this.lottieOptions.path = this.SUCCESS_LOTTIE_PATH;
          break;
        case 'warning':
          this.lottieOptions.path = this.WARNING_LOTTIE_PATH;
          break;
        case 'error':
          this.lottieOptions.path = this.ERROR_LOTTIE_PATH;
          break;
      }
    }
    this.hapticCtrl.notification(this.type); // success, warning and error
    coreCtrl.disableBackbutton();
  }

  close() {
    this.viewCtrl.dismiss();
    this.hapticCtrl.selection();
  }

  ionViewWillLeave() {
    this.coreCtrl.enableBackbutton();
  }

}
