import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UiFeedBackAlertModal } from './ui-feed-back-Alert-modal';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    UiFeedBackAlertModal,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(UiFeedBackAlertModal)
  ],
})
export class UiFeedBackAlertModalPageModule {}
