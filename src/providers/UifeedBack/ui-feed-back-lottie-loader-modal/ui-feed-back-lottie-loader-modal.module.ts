import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UiFeedBackLottieLoaderModal } from './ui-feed-back-lottie-loader-modal';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    UiFeedBackLottieLoaderModal,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(UiFeedBackLottieLoaderModal)
  ],
})
export class UiFeedBackLottieLoaderModalPageModule {}
