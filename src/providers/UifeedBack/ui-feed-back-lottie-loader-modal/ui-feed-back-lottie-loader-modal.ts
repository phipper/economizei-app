import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { HapticProvider } from '../../../providers/Haptic';
import { CoreProvider } from '../../CoreProvider';


@IonicPage()
@Component({
  selector: 'page-ui-feed-back-lottie-loader-modal',
  templateUrl: 'ui-feed-back-lottie-loader-modal.html',
})

export class UiFeedBackLottieLoaderModal {


  message = '';

  lottie = {
    loop: true,
    autoplay: true,
    path: ''
  }

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private hapticCtrl: HapticProvider,
    private events: Events,
    private coreCtrl: CoreProvider
  ) {
    this.message = navParams.get('message');
    this.lottie.path = navParams.get('path');

    this.events.subscribe('LottieLoaderCtrlChanel:updateMessage', data => {
      this.message = data.newMessage;
    });

    this.events.subscribe('LottieLoaderCtrlChanel:dismiss', () => {
      this.events.unsubscribe('LottieLoaderCtrlChanel:dismiss');
      this.events.unsubscribe('LottieLoaderCtrlChanel:updateMessage');
      this.viewCtrl.dismiss();
      this.hapticCtrl.selection();
      this.events.publish('LottieLoaderCtrlChanel:dismissCompleted');
    });
    coreCtrl.disableBackbutton();
  }

  ionViewWillLeave() {
    this.coreCtrl.enableBackbutton();
  }

}
