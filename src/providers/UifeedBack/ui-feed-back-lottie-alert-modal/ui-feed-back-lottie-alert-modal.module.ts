import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UiFeedBackLottieAlertModal } from './ui-feed-back-lottie-alert-modal';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    UiFeedBackLottieAlertModal,
  ],
  imports: [
    LottieAnimationViewModule,
    IonicPageModule.forChild(UiFeedBackLottieAlertModal)
  ],
})
export class UiFeedBackLottieAlertModalPageModule {}
