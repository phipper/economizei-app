import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HapticProvider } from '../../../providers/Haptic';
import { CoreProvider } from '../../CoreProvider';


@IonicPage()
@Component({
  selector: 'page-ui-feed-back-lottie-alert-modal',
  templateUrl: 'ui-feed-back-lottie-alert-modal.html',
})

export class UiFeedBackLottieAlertModal {


  title = '';
  message = '';
  type = '';

  lottie = {
    loop: true,
    autoplay: true,
    path: ''
  }

  constructor(
    public viewCtrl: ViewController,
    private coreCtrl: CoreProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private hapticCtrl: HapticProvider
  ) {
    this.title = navParams.get('title');
    this.message = navParams.get('message');
    this.type = navParams.get('type');
    this.lottie.path = navParams.get('path');
    this.lottie.loop = navParams.get('loop');
    this.hapticCtrl.notification(this.type); // success, warning and error
    coreCtrl.disableBackbutton();
  }


  close() {
    this.viewCtrl.dismiss();
    this.hapticCtrl.selection();
  }

  ionViewWillLeave() {
    this.coreCtrl.enableBackbutton();
  }

}
