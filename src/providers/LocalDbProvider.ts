
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { EcoFirestore } from './EcoFirestore/EcoFirestore';

import { Category } from '../models/Category';
import { ServiceType } from '../models/ServiceType';
import { HomePageBanner } from '../models/HomePageBanner';
import { SacModelData } from '../models/SacModelData';
import { SacBusinessRole } from '../models/SacBusinessRole';
import { SacCategory } from '../models/SacCategory';
import { SacSubCategory } from '../models/SacSubCategory';
import { SacSolverTeam } from '../models/SacSolverTeam';
import { LocalDBInviteData } from '../models/LocalDBInviteData';
import { AppConfig } from '../models/AppConfig';

declare var require: any;
const loki = require('lokijs');
const DB_NAME = 'ecoDBLocal.db';

const CORE_COLLECTION = 'EcoCore';
const APP_CONFIG_COLLECTION = 'Config';
const PROVIDER_INVITE_COLLECTION = 'ProviderInvite';
const HOME_PAGE_BANNERS_COLLECTION = 'HomePageBanners';
const SERVICE_TYPES_COLLECTION = 'ServiceTypes';
const CATEGORIES_COLLECTION = 'Categories';



@Injectable()
export class LocalDbProvider {

    private db: any;

    private core: any;
    private appConfig: any;
    private providerInvite: any;
    private homePageBanners: any;
    private serviceTypes: any;
    private categories: any;

    private numOfReq = 0;
    private numOfResp = 0;

    private initialized = false;
    private initiating = true;

    constructor(
        private storage: NativeStorage,
        private onlineDb: EcoFirestore
    ) {
        this.db = new loki(DB_NAME);
    }

    public enablePersistence() {
        setInterval(() => {
            this.persist();
        }, 5000);
    }

    private persist(): Promise<void> {
        return this.storage.setItem(DB_NAME, JSON.stringify(this.db));
    }

    /**
     * Inicia o Provider 
     * @return Retorna uma `Promise<void>`
     */
    public init(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.initialized) {
                this.initiating = true;
                this.importOrCreate()
                    .then(res => {
                        if (res == 'created') {
                            console.log('LocalDbProvider says: Populating New DB');

                            console.log('LocalDbProvider says: getting ecoCore');
                            this.onlineDb.core.getCore()
                                .then(core => {
                                    this.core.insert(core);
                                    var tempAppConfig = new AppConfig();
                                    this.appConfig.insert(tempAppConfig);

                                    console.log('LocalDbProvider says: getting HomePageBanners');
                                    this.onlineDb.homePageBanners.getActives()
                                        .then(HomePageBanners => {
                                            for (let HomePageBanner of HomePageBanners) {
                                                this.homePageBanners.insert(HomePageBanner);
                                            }
                                            console.log('LocalDbProvider says: getting Categories');
                                            this.onlineDb.categories.getAll()
                                                .then(Categories => {
                                                    for (let Category of Categories) {
                                                        this.categories.insert(Category);
                                                    }

                                                    console.log('LocalDbProvider says: getting ServiceTypes');
                                                    this.onlineDb.serviceTypes.getAll()
                                                        .then(ServicesTypes => {
                                                            for (let ServicesType of ServicesTypes) {
                                                                this.serviceTypes.insert(ServicesType);
                                                            }

                                                            console.log('LocalDbProvider says: New DB Populated');
                                                            this.initialized = true;
                                                            this.initiating = false;
                                                            console.log('LocalDbProvider says: Local DB initialized');
                                                            resolve(this.persist());
                                                        })
                                                })
                                        })

                                })
                                .catch(e => {
                                    console.log('Error ' + JSON.stringify(e));
                                    reject(e);
                                });
                        } else {

                            console.log('LocalDbProvider says: Checking for Updates');

                            console.log('LocalDbProvider says: getting ecoCore');
                            this.onlineDb.core.getCore()
                                .then(core => {
                                    var fireBaseCore = core;
                                    console.log(`fireBaseCore: ${JSON.stringify(fireBaseCore)}`);
                                    var localCore = this.core.get(1);
                                    console.log(`localCore: ${JSON.stringify(localCore)}`);

                                    if (fireBaseCore.homepageBannersV > localCore.homepageBannersV) {
                                        this.numOfReq++;
                                        console.log('LocalDbProvider says: updating HomePageBanners');

                                        this.onlineDb.homePageBanners.getActives()
                                            .then(HomePageBanners => {
                                                this.homePageBanners.clear();
                                                for (let HomePageBanner of HomePageBanners) {
                                                    this.homePageBanners.insert(HomePageBanner);
                                                }
                                                this.numOfResp++;
                                                localCore.homepageBannersV = fireBaseCore.homepageBannersV;
                                                this.core.update(localCore);
                                                if (this.numOfReq == this.numOfResp) {
                                                    this.initialized = true;
                                                    this.initiating = false;
                                                    console.log('LocalDbProvider says: Local DB initialized');
                                                    resolve(this.persist());
                                                }
                                            })
                                            .catch(e => {
                                                console.log('Error ' + JSON.stringify(e));
                                                reject(e);
                                            })
                                    }

                                    if (fireBaseCore.categoriesV > localCore.categoriesV) {
                                        this.numOfReq++;
                                        console.log('LocalDbProvider says: updating Categories');
                                        this.onlineDb.categories.getAll()
                                            .then(Categories => {
                                                this.categories.clear();
                                                for (let Category of Categories) {
                                                    this.categories.insert(Category);
                                                }

                                                this.numOfResp++;
                                                localCore.categoriesV = fireBaseCore.categoriesV;
                                                this.core.update(localCore);
                                                if (this.numOfReq == this.numOfResp) {
                                                    this.initialized = true;
                                                    this.initiating = false;
                                                    console.log('LocalDbProvider says: Local DB initialized');
                                                    resolve(this.persist());
                                                }
                                            })
                                            .catch(e => {
                                                console.log('Error ' + JSON.stringify(e));
                                                this.initiating = false;
                                                reject(e);
                                            })
                                    }

                                    if (fireBaseCore.serviceTypeV > localCore.serviceTypeV) {
                                        this.numOfReq++;
                                        console.log('LocalDbProvider says: updating ServiceTypes');
                                        this.onlineDb.serviceTypes.getAll()
                                            .then(ServicesTypes => {
                                                this.serviceTypes.clear();
                                                for (let ServicesType of ServicesTypes) {
                                                    this.serviceTypes.insert(ServicesType);
                                                }

                                                this.numOfResp++;
                                                localCore.serviceTypeV = fireBaseCore.serviceTypeV;
                                                this.core.update(localCore);
                                                if (this.numOfReq == this.numOfResp) {
                                                    this.initialized = true;
                                                    this.initiating = false;
                                                    console.log('LocalDbProvider says: Local DB initialized');
                                                    resolve(this.persist());
                                                }
                                            })
                                            .catch(e => {
                                                console.log('Error ' + JSON.stringify(e));
                                                this.initiating = false;
                                                reject(e);
                                            })
                                    }

                                    if (this.numOfReq == 0) {
                                        this.initialized = true;
                                        this.initiating = false;
                                        console.log('LocalDbProvider says: Local DB initialized');
                                        resolve(this.persist());
                                    }

                                })
                                .catch(e => {
                                    console.log('Error ' + JSON.stringify(e));
                                    this.initiating = false;
                                    reject(e);
                                });

                        }
                    })
                    .catch(e => {
                        this.initiating = false;
                        reject(e);
                    })
            } else {
                console.log('LocalDbProvider says: already initiated');
                resolve();
            }
        });
    }

    private importOrCreate(): Promise<string> {
        return new Promise((resolve, reject) => {
            this.storage.getItem(DB_NAME)
                .then(
                    data => {

                        console.log('LocalDbProvider says: LocalDb found, Importing from Storage')

                        this.db.loadJSON(data);

                        this.core = this.db.addCollection(CORE_COLLECTION);
                        this.appConfig = this.db.addCollection(APP_CONFIG_COLLECTION);
                        this.providerInvite = this.db.addCollection(PROVIDER_INVITE_COLLECTION);
                        this.homePageBanners = this.db.addCollection(HOME_PAGE_BANNERS_COLLECTION);
                        this.serviceTypes = this.db.addCollection(SERVICE_TYPES_COLLECTION);
                        this.categories = this.db.addCollection(CATEGORIES_COLLECTION);

                        this.persist();

                        console.log('LocalDbProvider says: DB imported');
                        resolve('imported')

                    },
                    error => {

                        if (error.code == 2) {
                            console.log('LocalDbProvider says: LocalDb not found, criating new DB');

                            this.core = this.db.addCollection(CORE_COLLECTION);
                            this.appConfig = this.db.addCollection(APP_CONFIG_COLLECTION);
                            this.providerInvite = this.db.addCollection(PROVIDER_INVITE_COLLECTION);
                            this.homePageBanners = this.db.addCollection(HOME_PAGE_BANNERS_COLLECTION);
                            this.serviceTypes = this.db.addCollection(SERVICE_TYPES_COLLECTION);
                            this.categories = this.db.addCollection(CATEGORIES_COLLECTION);

                            this.persist();
                            console.log('LocalDbProvider says: New DB Created');
                            resolve('created');
                        } else {
                            console.error(error)
                            reject(error);
                        }
                    }
                );
        });
    }

    private cleanHomePageBanners(dirtyHomePageBanners): HomePageBanner[] {
        let tempHomePageBanners: HomePageBanner[] = [];
        for (let dirtyHomePageBanner of dirtyHomePageBanners) {
            let tempHomePageBanner: HomePageBanner =
            {
                id: dirtyHomePageBanner.id,
                title: dirtyHomePageBanner.title,
                description: dirtyHomePageBanner.description,
                picUrl: dirtyHomePageBanner.picUrl,
                position: dirtyHomePageBanner.position,
                status: dirtyHomePageBanner.status,
                action: dirtyHomePageBanner.action,
                actionData: dirtyHomePageBanner.actionData,
                createdOn: dirtyHomePageBanner.createdOn,
                isActive: null
            };
            tempHomePageBanners.push(Object.assign({}, tempHomePageBanner))
        }
        return tempHomePageBanners;
    }

    private cleanCategories(dirtyCategories): Category[] {
        let tempCategories: Category[] = [];
        for (let dirtyCategory of dirtyCategories) {
            let tempCategory: Category =
            {
                id: dirtyCategory.id,
                title: dirtyCategory.title,
                description: dirtyCategory.description,
                cssColor: dirtyCategory.cssColor,
                textColor: dirtyCategory.textColor,
                index: dirtyCategory.index,
                serviceTypesIds: dirtyCategory.serviceTypesIds
            };
            tempCategories.push(Object.assign({}, tempCategory))
        }
        return tempCategories;
    }

    private cleanServiceTypes(dirtyServiceTypes): ServiceType[] {
        let tempServiceTypes: ServiceType[] = [];
        for (let dirtyServiceType of dirtyServiceTypes) {
            let tempServiceType: ServiceType =
            {
                id: dirtyServiceType.id,
                title: dirtyServiceType.title,
                symbol: dirtyServiceType.symbol,
                acceptHomeDelivery: dirtyServiceType.acceptHomeDelivery,
                requiredHomeDelivery: dirtyServiceType.requiredHomeDelivery
            };
            tempServiceTypes.push(Object.assign({}, tempServiceType))
        }
        return tempServiceTypes;
    }

    private failSafe(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.initialized) {
                resolve()
            } else {
                if (!this.initiating) {
                    this.init()
                        .then(() => {
                            resolve(this.failSafe());
                        })
                        .catch(e => {
                            reject(e);
                        })
                } else {
                    console.log('LocalDbProvider says: already initiating waiting for initialization to complete');
                    setTimeout(() => {
                        resolve(this.failSafe());
                    }, 500);
                }
            }
        })
    }

    /**
     * retorna os dados para iniciar a pagina Home do economizei
     * @return Retorna uma `Promise<HomePageBanners[]>`
     */
    public getHomePageBanners(): Promise<HomePageBanner[]> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    let tempHomePageBanners = this.cleanHomePageBanners(this.homePageBanners.data);
                    resolve(tempHomePageBanners)
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
     * retorna os dados dos tipos de serviços e as categorias de serviço
     * @return Retorna uma `Promise<{ categories[], serviceTypes[]}>`
     */
    public getServiceTypesAndCategories(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    let tempCategories = this.cleanCategories(this.categories.data);
                    let tempServiceTypes = this.cleanServiceTypes(this.serviceTypes.data);
                    resolve({
                        categories: tempCategories,
                        serviceTypes: tempServiceTypes
                    })
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
     * retorna os dados das categorias de serviço
     * @return Retorna uma `Promise<Category[]>`
     */
    public getCategories(): Promise<Category[]> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    let tempCategories = this.cleanCategories(this.categories.data);
                    resolve(tempCategories)
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
     * retorna os dados dos tipos de serviço
     * @return Retorna uma `Promise<ServiceType[]>`
     */
    public getServiceTypes(): Promise<ServiceType[]> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    let tempServiceTypes = this.cleanServiceTypes(this.serviceTypes.data);
                    resolve(tempServiceTypes);
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
     * retorna os dados para a criação do SAC de verificação de Prestador
     * @return Retorna uma `Promise<SacModelData>`
     */
    public getPVSModelData(): Promise<SacModelData> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    var localCore = this.core.get(1);
                    var tempData = new SacModelData();
                    this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.providerVerficationSacModel.businessRoleId).ref.get()
                        .then(BRDoc => {
                            tempData.businessRole = BRDoc.data() as SacBusinessRole;
                            this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.providerVerficationSacModel.businessRoleId)
                                .collection("SacCategories").doc(localCore.providerVerficationSacModel.categoryId).ref.get()
                                .then(SCDoc => {
                                    tempData.category = SCDoc.data() as SacCategory;
                                    this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.providerVerficationSacModel.businessRoleId)
                                        .collection("SacCategories").doc(localCore.providerVerficationSacModel.categoryId)
                                        .collection("SacSubCategories").doc(localCore.providerVerficationSacModel.subCategoryId).ref.get()
                                        .then(SSCDoc => {
                                            tempData.subCategory = SSCDoc.data() as SacSubCategory;
                                            this.onlineDb.afs.collection('SacSolverTeams').doc(localCore.providerVerficationSacModel.assignedTeamId)
                                                .ref.get()
                                                .then(SSTDoc => {
                                                    tempData.type = localCore.providerVerficationSacModel.type;
                                                    tempData.priority = localCore.providerVerficationSacModel.priority;
                                                    tempData.assignedTeam = SSTDoc.data() as SacSolverTeam;
                                                    resolve(tempData);
                                                })
                                        })
                                })
                        })
                        .catch(e => {
                            reject(e);
                        })
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
   * retorna os dados para a criação do SAC de verificação de Novo banner
   * @return Retorna uma `Promise<SacModelData>`
   */
    public getNBVSModelData(): Promise<SacModelData> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    var localCore = this.core.get(1);
                    var tempData = new SacModelData();
                    this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.newBannerSacModel.businessRoleId).ref.get()
                        .then(BRDoc => {
                            tempData.businessRole = BRDoc.data() as SacBusinessRole;
                            this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.newBannerSacModel.businessRoleId)
                                .collection("SacCategories").doc(localCore.newBannerSacModel.categoryId).ref.get()
                                .then(SCDoc => {
                                    tempData.category = SCDoc.data() as SacCategory;
                                    this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.newBannerSacModel.businessRoleId)
                                        .collection("SacCategories").doc(localCore.newBannerSacModel.categoryId)
                                        .collection("SacSubCategories").doc(localCore.newBannerSacModel.subCategoryId).ref.get()
                                        .then(SSCDoc => {
                                            tempData.subCategory = SSCDoc.data() as SacSubCategory;
                                            this.onlineDb.afs.collection('SacSolverTeams').doc(localCore.newBannerSacModel.assignedTeamId)
                                                .ref.get()
                                                .then(SSTDoc => {
                                                    tempData.type = localCore.editBannerSacModel.type;
                                                    tempData.priority = localCore.editBannerSacModel.priority;
                                                    tempData.assignedTeam = SSTDoc.data() as SacSolverTeam;
                                                    resolve(tempData);
                                                })
                                        })
                                })
                        })
                        .catch(e => {
                            reject(e);
                        })
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
  * retorna os dados para a criação do SAC de verificação de edição em banner
  * @return Retorna uma `Promise<SacModelData>`
  */
    public getEBVSModelData(): Promise<SacModelData> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    var localCore = this.core.get(1);
                    var tempData = new SacModelData();
                    this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.editBannerSacModel.businessRoleId).ref.get()
                        .then(BRDoc => {
                            tempData.businessRole = BRDoc.data() as SacBusinessRole;
                            this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.editBannerSacModel.businessRoleId)
                                .collection("SacCategories").doc(localCore.editBannerSacModel.categoryId).ref.get()
                                .then(SCDoc => {
                                    tempData.category = SCDoc.data() as SacCategory;
                                    this.onlineDb.afs.collection('SacBusinessRoles').doc(localCore.editBannerSacModel.businessRoleId)
                                        .collection("SacCategories").doc(localCore.editBannerSacModel.categoryId)
                                        .collection("SacSubCategories").doc(localCore.editBannerSacModel.subCategoryId).ref.get()
                                        .then(SSCDoc => {
                                            tempData.subCategory = SSCDoc.data() as SacSubCategory;
                                            this.onlineDb.afs.collection('SacSolverTeams').doc(localCore.editBannerSacModel.assignedTeamId)
                                                .ref.get()
                                                .then(SSTDoc => {
                                                    tempData.type = localCore.editBannerSacModel.type;
                                                    tempData.priority = localCore.editBannerSacModel.priority;
                                                    tempData.assignedTeam = SSTDoc.data() as SacSolverTeam;
                                                    resolve(tempData);
                                                })
                                        })
                                })
                        })
                        .catch(e => {
                            reject(e);
                        })
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
      * Salva o Id de um convite Recebido
      * @param {string} providerInviteId - Id a ser Salvo
      * @return Retorna uma `Promise<void>`
      */
    public saveProviderInviteId(providerInviteId: string): Promise<void> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    this.providerInvite.insert({ id: providerInviteId });
                    resolve(this.persist())
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
    * retorna os dados para a criação do SAC de verificação de edição em banner
    * @return Retorna uma `Promise<LocalDBInviteData>`
    */
    public getProviderInviteData(): Promise<LocalDBInviteData> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    var tempData: LocalDBInviteData = new LocalDBInviteData();
                    tempData.hasProviderInvite = false;
                    if (this.providerInvite.data.length > 0) {
                        var tempProviderInviteData = this.providerInvite.get(1);
                        tempData.inviteId = tempProviderInviteData.id;
                        tempData.hasProviderInvite = true;
                        resolve(tempData)
                    } else {
                        resolve(tempData)
                    }
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
      * Salva a config passada como parametro
      * @param {AppConfig} appConfig nova configuração a ser Salvo
      * @return Retorna uma `Promise<void>`
      */
    public updateAppConfig(newAppConfig: AppConfig): Promise<void> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    this.appConfig.update(newAppConfig);
                    resolve(this.persist())
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

    /**
    * retorna os dados para a criação do SAC de verificação de edição em banner
    * @return Retorna uma `Promise<AppConfig>`
    */
    public getAppConfig(): Promise<AppConfig> {
        return new Promise((resolve, reject) => {
            this.failSafe()
                .then(() => {
                    let tempData = this.appConfig.get(1) as AppConfig;
                    resolve(tempData);
                })
                .catch(e => {
                    reject(e);
                })
        });
    }

}