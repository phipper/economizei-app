import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { AngularFirestore } from 'angularfire2/firestore';
import { UiFeedBackProvider } from '../../../providers/UifeedBack/UifeedBack';
import { LocalizationProvider } from '../../../providers/Localization/Localization';


@IonicPage()
@Component({
  selector: 'page-input-manual-location',
  templateUrl: 'input-manual-location.html',
})

export class InputManualLocation {

  hasChanges = false;

  selectedState = null;
  selectedCity = null;

  canSelectCity = false;
  citiesToSelection: string[] = [];

  constructor(
    public viewCtrl: ViewController,
    public afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private LocalizationCtrl: LocalizationProvider
  ) {
  }

  ionViewDidEnter() {
  }


  selectState() {
    if (this.selectedState != null) {
      this.selectedCity = null
      this.citiesToSelection = this.LocalizationCtrl.getCities(this.selectedState);
      this.canSelectCity = true;
    }
  }

  close() {
    this.viewCtrl.dismiss({ success: false });
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

  save() {
    this.viewCtrl.dismiss({ success: true, selectedState: this.selectedState, selectedCity: this.selectedCity });
    this.UiFeedBackCtrl.hapticCtrl.selection();
  }

}
