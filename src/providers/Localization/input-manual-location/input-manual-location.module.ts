import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InputManualLocation } from './input-manual-location';

@NgModule({
  declarations: [
    InputManualLocation,
  ],
  imports: [
    IonicPageModule.forChild(InputManualLocation),
  ],
})
export class InputManualLocationModule {}
