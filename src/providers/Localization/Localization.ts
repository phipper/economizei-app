import { Injectable } from '@angular/core';
import { Platform, ModalController } from 'ionic-angular';

import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation, Geoposition, GeolocationOptions } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Address } from '../../models/Address';
import { Observable } from 'rxjs/Observable';
import * as brStatesData from '../../assets/json/brStates.json';

@Injectable()
export class LocalizationProvider {

  public actualBairro = '';
  public actualCity = '';
  public actualState = '';
  public lastGeoLocAccuracy = 0;

  private lastAddress: Address = null;
  private initialized = false;

  private isRequestingManualInput = false;

  private brStates = brStatesData as any;

  private geoOptions: GeolocationOptions = {
    enableHighAccuracy: true,
    maximumAge: 30000,
    timeout: 15000
  }

  constructor(
    private diagnostic: Diagnostic,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private platform: Platform,
    private modalCtrl: ModalController
  ) {
  }

  /**
   * Pede para que o usuário faça a seleção do estado e cidade
   * @return Retorna uma `Promise<{ selectedState: string, selectedCity: string }>` 
   */
  public requestManualInput(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (!this.isRequestingManualInput) {
        let modal = this.modalCtrl.create('InputManualLocation');
        modal.onDidDismiss(data => {
          if (data.success) {
            resolve({ selectedState: data.selectedState, selectedCity: data.selectedCity })
          } else {
            reject({ message: `Ação cancelada!` });
          }
          this.isRequestingManualInput = false;
        });
        modal.present();
        this.isRequestingManualInput = true;
      } else {
        reject({ message: `already requesting manual input!` });
      }
    })
  }

  init(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (!this.initialized) {
        console.log('LocalizationProvider says: iniciando LocalizationProvider');
        if (this.platform.is('cordova')) {
          console.log('LocalizationProvider says: é cordova');
          this.diagnostic.isLocationAuthorized()
            .then(isAvailable => {
              console.log('LocalizationProvider says: checking Authorization');
              if (isAvailable) {
                console.log('LocalizationProvider says: Authorized');
                this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_BALANCED_POWER_ACCURACY)
                  .then(() => {

                    console.log('iniciando LocalizationProvider');
                    this.geolocation.getCurrentPosition(this.geoOptions)
                      .then(data => {

                        this.nativeGeocoder.reverseGeocode(data.coords.latitude, data.coords.longitude, { defaultLocale: 'pt-BR', maxResults: 1, useLocale: true })
                          .then(reverseResult => {

                            let tempAddress = new Address();

                            tempAddress.latitude = data.coords.latitude;
                            tempAddress.longitude = data.coords.longitude;
                            this.lastGeoLocAccuracy = Number(data.coords.accuracy.toFixed(2));

                            tempAddress.state = reverseResult[0].administrativeArea;
                            tempAddress.city = reverseResult[0].subAdministrativeArea;
                            tempAddress.bairro = reverseResult[0].subLocality;

                            this.actualState = reverseResult[0].administrativeArea;
                            this.actualCity = reverseResult[0].subAdministrativeArea;
                            this.actualBairro = reverseResult[0].subLocality;

                            this.lastAddress = tempAddress;
                            this.initialized = true;
                            resolve();
                          }, error => {
                            console.log(JSON.stringify({ log: 'Error on reverseGeocode location', error: JSON.stringify(error) }));
                            reject({ log: 'Error on reverseGeocode location', error: JSON.stringify(error) });
                          });
                      })
                      .catch(error => {
                        console.log(JSON.stringify({ log: 'Error on getCurrentPosition', error: JSON.stringify(error) }));
                        reject({ log: 'Error on getCurrentPosition', error: JSON.stringify(error) });
                      });
                  },
                    error => {

                      if (error.code == 4) {
                        console.log('User não ativou o GPS.');
                        this.requestManualInput()
                          .then(data => {
                            let tempAddress = new Address();
                            tempAddress.state = data.selectedState;
                            tempAddress.city = data.selectedCity;
                            this.lastAddress = tempAddress;
                            this.initialized = true;
                            resolve();
                          })
                          .catch(e => {
                            reject({ log: 'error on manualSelection' });
                          })
                      } else {
                        console.log(JSON.stringify({ log: 'Error requesting location', error: JSON.stringify(error) }));
                        reject({ log: 'Error requesting location', error: JSON.stringify(error) });
                      }
                    });
              } else {
                console.log('LocalizationProvider says: Un Authorized');
                console.log('LocalizationProvider says: Requesting Location Authorization');
                this.diagnostic.requestLocationAuthorization("when_in_use")
                  .then(result => {

                    if (result == this.diagnostic.permissionStatus.GRANTED || result == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
                      console.log('LocalizationProvider says: Permission Granted');
                      resolve(this.init());
                    } else {
                      console.log('LocalizationProvider says: Permission Not Granted');
                      console.log('LocalizationProvider says: Requesting Manual Input');
                      this.requestManualInput()
                        .then(data => {
                          let tempAddress = new Address();
                          tempAddress.state = data.selectedState;
                          tempAddress.city = data.selectedCity;
                          this.lastAddress = tempAddress;
                          this.initialized = true;
                          resolve();
                        })
                        .catch(e => {
                          reject({ log: 'error on manualSelection' });
                        })
                    }
                  })
                  .catch(e => {
                    reject({ log: 'error on init, at diagnostic.requestLocationAuthorization', msg: e.message, error: e });
                    console.log(JSON.stringify({ log: 'error on init, at diagnostic.requestLocationAuthorization', msg: e.message, error: e }));
                  })
              }
            })
            .catch(e => {
              reject({ log: 'error on init, at diagnostic.isLocationAuthorized', msg: e.message, error: e });
              console.log(JSON.stringify({ log: 'error on init, at diagnostic.isLocationAuthorized', msg: e.message, error: e }));
            });
        } else {
          this.requestManualInput()
            .then(data => {
              let tempAddress = new Address();
              tempAddress.state = data.selectedState;
              tempAddress.city = data.selectedCity;
              this.lastAddress = tempAddress;
              this.initialized = true;
              resolve();
            })
            .catch(e => {
              reject({ log: 'error on manualSelection' });
            });
        }
      } else {
        resolve({ log: 'Already initialized' });
        console.log(JSON.stringify({ log: 'Already initialized' }));
      }
    });
  }

  /**
   * Usa os sensores do dispositivo para retornar a Geoposition atual
   * @return Retorna um `Observable` com a Geoposition atual
   */
  watchPosition(): Observable<Geoposition> {
    return new Observable((observer) => {
      this.locationAccuracy.canRequest()
        .then((canRequest: boolean) => {
          if (canRequest) {
            // the accuracy option will be ignored by iOS
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
              .then(() => {
                const geoSubscription = this.geolocation.watchPosition(this.geoOptions)
                  .filter((p) => p.coords !== undefined) //Filter Out Errors
                  .subscribe(
                    (data) => {
                      observer.next(data);
                    },
                    (error) => {
                      observer.error(error);
                    }
                  );
                observer.add(() => {
                  geoSubscription.unsubscribe();
                })
              },
                error => {
                  observer.error({ log: 'Error requesting location permissions', error: JSON.stringify(error) });
                });
          } else {
            observer.error({ log: 'Error localization Provider cannot request location permissions' });
          }
        });
    });
  }

  /**
   * Usa os sensores do dispositivo para retornar a Geoposition atual
   * @return Retorna uma `Promise` que é resolvida com a Geoposition atual
   */
  getCurrentPosition(): Promise<Geoposition> {
    return new Promise((resolve, reject) => {
      this.locationAccuracy.canRequest()
        .then((canRequest: boolean) => {
          if (canRequest) {
            // the accuracy option will be ignored by iOS
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
              .then(() => {
                console.log('iniciando LocalizationProvider');
                this.geolocation.getCurrentPosition(this.geoOptions)
                  .then(data => {
                    resolve(data);
                  })
                  .catch(error => {
                    reject({ log: 'Error on getCurrentPosition', error: JSON.stringify(error) });
                  });
              },
                error => {
                  reject({ log: 'Error requesting location permissions', error: JSON.stringify(error) });
                });
          } else {
            reject({ log: 'Error localization Provider cannot request location permissions' });
          }
        });
    });
  }

  /**
   * Usa os sensores do dispositivo para retornar o Address atual
   * @return Retorna uma `Promise` que é resolvida com o Address atual
   */
  getCurrentAddress(): Promise<Address> {
    return new Promise((resolve, reject) => {
      this.locationAccuracy.canRequest()
        .then((canRequest: boolean) => {
          if (canRequest) {
            // the accuracy option will be ignored by iOS
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
              .then(() => {

                this.geolocation.getCurrentPosition(this.geoOptions)
                  .then(data => {
                    this.nativeGeocoder.reverseGeocode(data.coords.latitude, data.coords.longitude, { defaultLocale: 'pt-BR', maxResults: 1, useLocale: true })
                      .then(reverseResult => {

                        let tempAddress = new Address();

                        tempAddress.latitude = data.coords.latitude;
                        tempAddress.longitude = data.coords.longitude;
                        this.lastGeoLocAccuracy = Number(data.coords.accuracy.toFixed(2));

                        tempAddress.state = reverseResult[0].administrativeArea;
                        tempAddress.city = reverseResult[0].subAdministrativeArea;
                        tempAddress.bairro = reverseResult[0].subLocality;

                        this.actualState = reverseResult[0].administrativeArea;
                        this.actualCity = reverseResult[0].subAdministrativeArea;
                        this.actualBairro = reverseResult[0].subLocality;

                        tempAddress.cep = reverseResult[0].postalCode;
                        tempAddress.street = reverseResult[0].thoroughfare;
                        tempAddress.numero = reverseResult[0].subThoroughfare;

                        this.lastAddress = tempAddress;
                        this.initialized = true;
                        resolve(tempAddress);
                      }, error => {
                        reject({ log: 'Error on reverseGeocode location', error: JSON.stringify(error) });
                      });
                  })
                  .catch(error => {
                    reject({ log: 'Error on getCurrentPosition', error: JSON.stringify(error) });
                  });
              },
                error => {
                  reject({ log: 'Error requesting location permissions', error: JSON.stringify(error) });
                });
          } else {
            reject({ log: 'Error localization Provider cannot request location permissions' });
          }
        });
    });
  }

  /**
   * @return Retorna uma `Promise` que é resolvida com o ultimo Address registrado
   */
  getLastAddress(): Promise<Address> {
    return new Promise((resolve, reject) => {
      if (this.initialized) {
        resolve(this.lastAddress);
      } else {
        this.init()
          .then(() => {
            resolve(this.lastAddress);
          })
          .catch(e => { reject(e) })
      }
    });
  }


  private deg2rad(deg): number {
    return deg * (Math.PI / 180)
  }

  /**
   * Calcula a distancia entre as duas coordenadas em Km 
   * @param {number} lat1 latitude da 1ª coordenada
   * @param {number} lon1 longitude da 1ª coordenada
   * @param {number} lat2 latitude da 2ª coordenada
   * @param {number} lon2 longitude da 2ª coordenada
   * @return {number} `number` distancia entre as duas coordenadas em Km com 2 casas decimais
   */
  getDistanceFromCoordinatesInKm(lat1: number, lon1: number, lat2: number, lon2: number): number {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return Number(d.toFixed(2));
  }

  /**
   * Converte o UF para o Nome do Estado Brasileiro 
   * @param {state} uf Unidadte Federativa
   * @return {string} `string` Nome do estado Brasileiro
   */
  public ufToState(uf: string): string {
    uf = uf.toUpperCase();
    switch (uf) {
      case 'AC':
        return 'Acre';
      case 'AL':
        return 'Alagoas';
      case 'AP':
        return 'Amapá';
      case 'AM':
        return 'Amazonas';
      case 'BA':
        return 'Bahia';
      case 'CE':
        return 'Ceará';
      case 'DF':
        return 'Distrito Federal';
      case 'ES':
        return 'Espírito Santo';
      case 'GO':
        return 'Goiás';
      case 'MA':
        return 'Maranhão';
      case 'MT':
        return 'Mato Grosso';
      case 'MS':
        return 'Mato Grosso do Sul';
      case 'MG':
        return 'Minas Gerais';
      case 'PA':
        return 'Pará';
      case 'PB':
        return 'Paraíba';
      case 'PR':
        return 'Paraná';
      case 'PE':
        return 'Pernambuco';
      case 'PI':
        return 'Piauí';
      case 'RJ':
        return 'Rio de Janeiro';
      case 'RN':
        return 'Rio Grande do Norte';
      case 'RS':
        return 'Rio Grande do Sul';
      case 'RO':
        return 'Rondônia';
      case 'RR':
        return 'Roraima';
      case 'SC':
        return 'Santa Catarina';
      case 'SP':
        return 'São Paulo';
      case 'SE':
        return 'Sergipe';
      case 'TO':
        return 'Tocantins';
      default:
        return '';
    }
  }

  /**
   * Converte o Nome do estado para a uf 
   * @param {string} state Nome do estado Brasileiro
   * @return {string} `string` Unidadte Federativa
   */
  public stateuToUf(state: string): string {
    switch (state) {
      case 'Acre':
        return 'AC';
      case 'Alagoas':
        return 'AL';
      case 'Amapá':
        return 'AP';
      case 'Amazonas':
        return 'AM';
      case 'Bahia':
        return 'BA';
      case 'Ceará':
        return 'CE';
      case 'Distrito Federal':
        return 'DF';
      case 'Espírito Santo':
        return 'ES';
      case 'Goiás':
        return 'GO';
      case 'Maranhão':
        return 'MA';
      case 'Mato Grosso':
        return 'MT';
      case 'Mato Grosso do Sul':
        return 'MS';
      case 'Minas Gerais':
        return 'MG';
      case 'Pará':
        return 'PA';
      case 'Paraíba':
        return 'PB';
      case 'Paraná':
        return 'PR';
      case 'Pernambuco':
        return 'PE';
      case 'Piauí':
        return 'PI';
      case 'Rio de Janeiro':
        return 'RJ';
      case 'Rio Grande do Norte':
        return 'RN';
      case 'Rio Grande do Sul':
        return 'RS';
      case 'Rondônia':
        return 'RO';
      case 'Roraima':
        return 'RR';
      case 'Santa Catarina':
        return 'SC';
      case 'São Paulo':
        return 'SP';
      case 'Sergipe':
        return 'SE';
      case 'Tocantins':
        return 'TO';
      default:
        return '';
    }
  }

  /**
   * Retorna um vetor com todas as Cidades do estado Brasileiro 
   * @param {string} state Nome do estado Brasileiro
   * @return {string} `string` Unidadte Federativa
   */
  public getCities(state: string): string[] {
    switch (this.stateuToUf(state)) {
      case 'AC':
        return this.brStates.AC;
      case 'AL':
        return this.brStates.AL;
      case 'AP':
        return this.brStates.AP;
      case 'AM':
        return this.brStates.AM;
      case 'BA':
        return this.brStates.BA;
      case 'CE':
        return this.brStates.CE;
      case 'DF':
        return this.brStates.DF;
      case 'ES':
        return this.brStates.ES;
      case 'GO':
        return this.brStates.GO;
      case 'MA':
        return this.brStates.MA;
      case 'MT':
        return this.brStates.MT;
      case 'MS':
        return this.brStates.MS;
      case 'MG':
        return this.brStates.MG;
      case 'PA':
        return this.brStates.PA;
      case 'PB':
        return this.brStates.PB;
      case 'PR':
        return this.brStates.PR;
      case 'PE':
        return this.brStates.PE;
      case 'PI':
        return this.brStates.PI;
      case 'RJ':
        return this.brStates.RJ;
      case 'RN':
        return this.brStates.RN;
      case 'RS':
        return this.brStates.RS;
      case 'RO':
        return this.brStates.RO;
      case 'RR':
        return this.brStates.RR;
      case 'SC':
        return this.brStates.SC;
      case 'SP':
        return this.brStates.SP;
      case 'SE':
        return this.brStates.SE;
      case 'TO':
        return this.brStates.TO;
      default:
        return [];
    }
  }

}