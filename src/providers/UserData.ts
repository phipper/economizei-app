import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

import { User } from '../models/User';
import { Message } from '../models/Message';
import { Subscription } from 'rxjs/Subscription';
import { UiFeedBackProvider } from './UifeedBack/UifeedBack';
import { CoreProvider } from './CoreProvider';

@Injectable()
export class UserDataProvider {

  public localUser: User = new User();
  public hasUser: boolean = false;
  public localUserIsProvider: boolean = false;
  public numOfNotifications: number = 0;
  public hasUnreadMsg: boolean = false;

  private oldChats: string[] = [];
  private localUnreadMsgPerChat = new Map<string, boolean>();
  private userSub: Subscription = null;
  private notifSub: any = null;
  private chatSubs: any[] = [];
  private initialized = false;
  private providerVerificationPrompted = false;
  private providerWorkAdressPrompted = false;

  constructor(
    private afs: AngularFirestore,
    private fire: AngularFireAuth,
    private UiFeedBackCtrl: UiFeedBackProvider,
    private ecoCore: CoreProvider
  ) {
  }

  /**
   * Inicia o Provider 
   * @return Retorna uma `Promise<void>`
   */
  public init(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!this.initialized) {
        this.fire.authState.subscribe(dataAuth => {

          this.initialized = true;
          if (dataAuth != null) {

            this.userSub = this.afs.collection('Users').doc(this.fire.auth.currentUser.uid).valueChanges()
              .subscribe(userData => {
                if (userData != null) {
                  this.localUser = userData as User;
                  this.hasUser = true;
                  this.localUserIsProvider = this.localUser.type == 1 ? false : true;
                  this.providerVerificationCheck();
                  this.providerWorkAdressCheck();
                  resolve();
                  if (this.oldChats != this.localUser.chats) {
                    this.oldChats = this.localUser.chats;
                    this.initializeChatSubs();
                  }
                } else {
                  reject(JSON.stringify({ log: 'Error at User subscriber', message: 'the doc for the currentUser does not exists' }));
                }
              },
                error => {
                  console.log(`UserDataProvider says: Error at User subscriber, message: ${error.message} error: ${JSON.stringify(error)}`);
                  reject(JSON.stringify({ log: 'Error at User subscriber', message: error.message, error: error }));
                });

            this.notifSub = this.afs.collection('Users').doc(this.fire.auth.currentUser.uid).collection('Notifications').ref
              .where('readed', '==', false)
              .orderBy('createdOn', 'desc')
              .onSnapshot(querySnapshot => {
                this.numOfNotifications = querySnapshot.size;
              },
                error => {
                  console.log(`UserDataProvider says: Error at notification subscriber, message: ${error.message} error: ${JSON.stringify(error)}`);
                  reject(JSON.stringify({ log: 'Error at notification subscriber', message: error.message, error: error }));
                });

          } else {

            if (this.userSub != null) {
              this.userSub.unsubscribe();
            }
            if (this.notifSub != null) {
              this.notifSub();
            }
            this.unsubscribeChatSubs();
            this.localUser = new User();
            this.hasUser = false;
            this.providerWorkAdressPrompted = false;
            this.providerVerificationPrompted = false;
          }
        },
          error => {
            this.initialized = false;
            console.log(`UserDataProvider says: Error at authState subscriber, message: ${error.message} error: ${JSON.stringify(error)}`);
            reject(JSON.stringify({ log: 'Error at authState subscriber', message: error.message, error: error }));
          });

      } else {
        if (this.localUser.id == '') {
          console.log(`UserDataProvider says: Already initialized, waiting for subscribers to update`);
          setTimeout(() => {
            resolve(this.init());
          }, 500);
        } else {
          resolve();
          console.log(`UserDataProvider says: Already initialized`);
        }
      }
    });
  }

  private initializeChatSubs() {
    if (this.localUser.chats.length > 0) {
      for (let chatSub of this.chatSubs) {
        if (chatSub != null) {
          chatSub();
        }
      }
      for (let chatId of this.localUser.chats) {
        const tempChatSub = this.afs.collection('Chats').doc(chatId).collection('Messages').ref.orderBy('time', 'desc').limit(1)
          .onSnapshot(querySnapshot => {
            this.hasUnreadMsg = false;
            this.localUnreadMsgPerChat.set(chatId, false);
            if (!querySnapshot.empty) {
              let tempMsg = querySnapshot.docs[0].data() as Message;
              if (tempMsg.senderId != this.localUser.id && tempMsg.readedOn == null) {
                this.localUnreadMsgPerChat.set(chatId, true);
                this.hasUnreadMsg = true;
              }
            }
            if (!this.hasUnreadMsg) {
              for (let chatId of this.localUser.chats) {
                if (this.localUnreadMsgPerChat.get(chatId)) {
                  this.hasUnreadMsg = true;
                  break;
                }
              }
            }
          },
            error => {
              console.log(`UserDataProvider says: Error at chat subscriber, message: ${error.message} error: ${JSON.stringify(error)}`);
            });
        this.chatSubs.push(tempChatSub);
      }
    }
  }

  private unsubscribeChatSubs() {
    for (let chatSub of this.chatSubs) {
      if (chatSub != null) {
        chatSub();
      }
    }
  }


  private failSafe(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.initialized) {
        if (this.hasUser) {
          if (this.localUser.id != '') {
            resolve();
          } else {
            console.log(`UserDataProvider says: waiting for subscribers to update`);
            setTimeout(() => {
              resolve(this.failSafe());
            }, 500);
          }
        } else {
          console.log(`UserDataProvider says: Error app does not have a logged user`);
          reject(JSON.stringify({ log: 'Error app does not have a logged user', message: 'Error app does not have a logged user' }));
        }
      } else {
        this.init()
          .then(() => {
            resolve(this.failSafe());
          })
          .catch(e => {
            reject(e)
          });
      }
    })
  }


  /**
   * Retorna o usuário logado e inicia o provider caso necessario
   * @return Retorna uma `Promise<User>`
   */
  public getUser(): Promise<User> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          resolve(Object.assign({}, this.localUser));
        })
        .catch(e => {
          reject(e)
        });
    });
  }

  /**
   * Retorna o Id do usuário logado e inicia o provider caso necessario
   * @return Retorna uma `Promise<User>`
   */
  public getUid(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          resolve(this.localUser.id.toString());
        })
        .catch(e => {
          reject(e)
        });
    });
  }

  /**
   * Atualiza o usuário logado
   * @param {Partial<User>} user
   * @return Retorna uma `Promise<void>`
   */
  public update(user: Partial<User>): Promise<void> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          resolve(this.afs.collection('Users').doc<User>(this.fire.auth.currentUser.uid).update(user));
        })
        .catch(e => {
          reject(e)
        });
    });
  }

  /**
   * Adiciona o bannerId passado como parametro do favoritos
   * @param {string} bannerId
   * @return Retorna uma `Promise<void>`
   */
  public saveBanner(bannerId: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          if (this.localUser.savedBanners.indexOf(bannerId, 0) == -1) {
            this.localUser.savedBanners.push(bannerId);
            resolve(this.afs.collection('Users').doc<User>(this.fire.auth.currentUser.uid).update({ savedBanners: this.localUser.savedBanners }));
          } else {
            console.log(`UserDataProvider says: Banner already saved`);
            resolve()
          }
        })
        .catch(e => {
          reject(e)
        });
    });
  }

  /**
   * Remove o bannerId passado como parametro do favoritos
   * @param {string} bannerId
   * @return Retorna uma `Promise<void>`
   */
  public unsaveBanner(bannerId: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.failSafe()
        .then(() => {
          if (this.localUser.savedBanners.indexOf(bannerId, 0) != -1) {
            let tempSavedBanners: string[] = [];
            this.localUser.savedBanners.forEach(id => {
              if (id != bannerId) {
                tempSavedBanners.push(id);
              }
            });
            this.localUser.savedBanners = tempSavedBanners;
            resolve(this.afs.collection('Users').doc<User>(this.fire.auth.currentUser.uid).update({ savedBanners: this.localUser.savedBanners }))
          } else {
            console.log(`UserDataProvider says: Banner already unsaved`);
            resolve()
          }
        })
        .catch(e => {
          reject(e)
        });
    });
  }

  /**
   * Verifica se o bannerId passado como parametro foi favoritado
   * @param {string} bannerId
   * @return Retorna uma `boolean`
   */
  public isBannerSaved(bannerId: string): boolean {
    if (this.initialized) {
      if (this.hasUser) {
        for (let tempBannerId of this.localUser.savedBanners) {
          if (tempBannerId == bannerId) {
            return true;
          }
        }
        return false;
      } else {
        return null;
      }
    } else {
      this.init()
        .then(() => {
          return this.isBannerSaved(bannerId)
        })
        .catch(e => {
          return null;
        });
    }
  }

  /**
   * Faz logoff
   * @return Retorna uma `Promise<void>`
   */
  public logout() {
    return new Promise((resolve, reject) => {
      let oldUserId = this.localUser.id;
      this.fire.auth.signOut()
        .then(() => {
          this.afs.collection('Users').doc<User>(oldUserId).update({ token: '' })
            .then(() => {
              resolve();
            });
        })
        .catch(e => {
          reject(JSON.stringify({ log: 'Error at signOut', message: e.message, error: e }));
        });
    });
  }

  private providerVerificationCheck() {
    if (this.providerVerificationPrompted == false &&
      this.localUser.status == 'active' &&
      this.localUser.type == 10 &&
      this.localUser.verifiedProvider == false &&
      this.localUser.providerVerificationSacId == '') {

      this.providerVerificationPrompted = true;
      const alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Você ainda não foi verificado !',
        subTitle: 'Você precisa nos enviar algums dados para que possamos verificar seu usuário de prestador. Esse processo é rápido e você só poderá fazer anuncios e receber pedidos depois de faze-lo. Deseja enviar agora?',
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.ecoCore.navPush('ProviderVerificationPage');
            }
          },
          'Não'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('error');
      alert.present();

    }
  }

  private providerWorkAdressCheck() {
    if (this.providerWorkAdressPrompted == false &&
      this.localUser.status == 'active' &&
      this.localUser.type == 10 &&
      this.localUser.verifiedProvider == true &&
      this.localUser.workAdressId == '') {

      this.providerWorkAdressPrompted = true;
      const alert = this.UiFeedBackCtrl.alertCtrl.create({
        title: 'Você ainda não possui endereço de trabalho selecionado !',
        subTitle: 'Atenção seu perfil só será exibido na pagina de buscas se você possuir um endereço de trabalho cadastrado! Deseja cadastrar agora?',
        buttons: [
          {
            text: 'Sim',
            handler: () => {
              this.ecoCore.navPush('MyAdressesPage');
            }
          },
          'Não'
        ]
      });
      this.UiFeedBackCtrl.hapticCtrl.notification('error');
      alert.present();

    }
  }
}