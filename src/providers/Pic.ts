import { Injectable } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';
import { LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Crop, CropOptions } from '@ionic-native/crop';

import { Pic } from '../models/Pic';
export type PicSrc = 'galery' | 'camera';

@Injectable()
export class PicProvider {


  private TAKE_PICTURE_CAMERA_OPTIONS: CameraOptions = {
    quality: 100,
    targetWidth: 1000,
    targetHeight: 1000,
    encodingType: this.camera.EncodingType.JPEG,
    destinationType: this.camera.DestinationType.FILE_URI,
    sourceType: this.camera.PictureSourceType.CAMERA,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,
    saveToPhotoAlbum: true
  };

  private LOAD_PICTURE_CAMERA_OPTIONS: CameraOptions = {
    quality: 100,
    targetWidth: 1000,
    targetHeight: 1000,
    encodingType: this.camera.EncodingType.JPEG,
    destinationType: this.camera.DestinationType.FILE_URI,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,
    allowEdit: false
  };

  private CROP_OPTIONS: CropOptions = {
    quality: 100,
    targetHeight: 1000,
    targetWidth: 1000
  };

  public userStorageRef = this.afStorage.storage.ref("User Profile Pics/");
  public proVerifStorageRef = this.afStorage.storage.ref("Provider Verification Pics");
  public requestStorageRef = this.afStorage.storage.ref("Requests Pics/");
  public bannerStorageRef = this.afStorage.storage.ref('Banners Pics/');


  constructor(
    private afStorage: AngularFireStorage,
    private loadingCtrl: LoadingController,
    private camera: Camera,
    private crop: Crop
  ) {
  }

  /**
   * Retorna o tamanho da imagem em MB
   * @param {string} data_url original
   * @return {string}
   */
  public getImageSize(data_url: string): string {
    var head = 'data:image/jpeg;base64,';
    return ((data_url.length - head.length) * 3 / 4 / (1024 * 1024)).toFixed(4);
  }

  /**
   * Redimensiona imagem 
   * @param {string} imgUri original imgUri
   * @param {number} MAX_WIDTH the final img width
   * @param {number} MAX_HEIGHT the final img height
   * @param {quality} quality the final img quality
   * @return {Promise<string>} base64JpegFile as `Promise<string>`
   */
  public imgUriToDataUrl(imgUri: string, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1): Promise<string> {
    return new Promise((resolve) => {

      console.log(`Original imgUri: ${imgUri}`);

      const win: any = window;
      const fixedURL = win.Ionic.WebView.convertFileSrc(imgUri);

      var normalizeImgUri = fixedURL;

      console.log(`Fixed imgUri: ${normalizeImgUri}`);

      var canvas: any = document.createElement("canvas");
      var image = new Image();

      image.crossOrigin = 'anonymous';
      // image.setAttribute('crossOrigin', 'anonymous');

      image.onload = () => {
        var width = image.width;
        var height = image.height;

        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
          }
        }
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext("2d");

        ctx.drawImage(image, 0, 0, width, height);

        // IMPORTANT: 'jpeg' NOT 'jpg'
        var dataUrl = canvas.toDataURL('image/jpeg', quality);

        resolve(dataUrl)
      }

      image.src = fixedURL;
    });
  }

  /**
   * Redimensiona imagem 
   * @param {string} base64Img original base64JpegFile
   * @param {number} MAX_WIDTH the final img width
   * @param {number} MAX_HEIGHT the final img height
   * @param {quality} quality the final img quality
   * @return {Promise<string>} base64JpegFile as `Promise<string>`
   */
  public reziseImage(base64Img: string, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1): Promise<string> {
    return new Promise((resolve) => {

      var canvas: any = document.createElement("canvas");
      var image = new Image();

      image.onload = () => {
        var width = image.width;
        var height = image.height;

        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
          }
        }
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext("2d");

        ctx.drawImage(image, 0, 0, width, height);

        // IMPORTANT: 'jpeg' NOT 'jpg'
        var dataUrl = canvas.toDataURL('image/jpeg', quality);

        resolve(dataUrl)
      }

      image.src = base64Img;
    });
  }


  /**
   * Usa a camera para tirar uma nova foto
  * @param imgSrc Fonte da imagem (`galery` ou `camera`) (padrão galeria)
   * @return {Promise<string>} base64JpegFile as `Promise<Pic>`
   */
  public newPicture(imgSrc: PicSrc = 'galery'): Promise<Pic> {
    return new Promise((resolve, reject) => {

      let tempPic = new Pic();

      let cameraOptions: CameraOptions = null;

      if (imgSrc == 'galery') {
        cameraOptions = this.LOAD_PICTURE_CAMERA_OPTIONS;
      } else {
        cameraOptions = this.TAKE_PICTURE_CAMERA_OPTIONS;
      }

      this.camera.getPicture(cameraOptions)
        .then(imageUri => {

          this.crop.crop(imageUri, this.CROP_OPTIONS)
            .then(croppedImgeUri => {

              this.imgUriToDataUrl(croppedImgeUri, 1000, 1000, 1)
                .then(base64Image => {

                  tempPic.url = base64Image;

                  console.log(`New pic size: ${this.getImageSize(tempPic.url)} MB`);

                  this.reziseImage(tempPic.url, 220, 220, 1)
                    .then(thumbBase64 => {

                      tempPic.thumbnailUrl = thumbBase64;

                      console.log(`New pic thumbnail size: ${this.getImageSize(tempPic.thumbnailUrl)} MB`);

                      resolve(tempPic)
                    })
                })

            })
            .catch(e => {
              if (e.code == 'userCancelled') {
                reject('userCancelled')
              } else {
                reject(e);
              }
            });

        }, err => {
          reject(err)
        });
    })
  }



  /**
   * Faz o upload da imagem do usuário 
   * @param {string} base64JpegFile base64 da img para fazer upload
   * @param {string} userId 
   * @return {Promise<string>} `downloadURL` in a Promise
   */
  uploadUserImg(base64JpegFile: string, userId: string): Promise<string> {
    let loader = this.loadingCtrl.create({
      content: 'Fazendo Upload...',
    });

    return new Promise((resolve, reject) => {
      loader.present()
        .then(() => {
          const tempPicRef = this.userStorageRef.child(userId + ".jpeg");
          var task = tempPicRef.putString(base64JpegFile, 'data_url');

          task.on('state_changed',
            function progress(snapshot) {
              var percentage = (task.snapshot.bytesTransferred / task.snapshot.totalBytes) * 100;
              loader.setContent("Fazendo Upload... " + Number(percentage).toFixed(2) + " %");
            },
          );

          task.then(() => {
            tempPicRef.getDownloadURL()
              .then(url => {
                loader.dismiss();
                resolve(url);
              }).catch(e => {
                loader.dismiss();
                reject({ log: 'erro ao recuperar Url', error: JSON.stringify(e) });
              });
          });

          task.catch(e => {
            loader.dismiss();
            reject({ log: 'erro ao fazer upload', error: JSON.stringify(e) });
          });

        });
    });
  }

  /**
   * Faz o upload da imagem da solicitação
   * @param {string} base64JpegFile base64 da img para fazer upload
   * @param {info} info - A string com o final.jpeg que sera utilizada como nome da foto no storage 
   * @return {Promise<string>} `downloadURL` in a Promise
   */
  uploadRequestImg(base64JpegFile: string, info: string): Promise<string> {
    let loader = this.loadingCtrl.create({
      content: 'Fazendo Upload...',
    });

    return new Promise((resolve, reject) => {
      loader.present()
        .then(() => {
          const tempPicRef = this.requestStorageRef.child(info);
          var task = tempPicRef.putString(base64JpegFile, 'data_url');

          task.on('state_changed',
            function progress(snapshot) {
              var percentage = (task.snapshot.bytesTransferred / task.snapshot.totalBytes) * 100;
              loader.setContent("Fazendo Upload... " + Number(percentage).toFixed(2) + " %");
            },
          );

          task.then(() => {
            tempPicRef.getDownloadURL()
              .then(url => {
                loader.dismiss();
                resolve(url);
              }).catch(e => {
                loader.dismiss();
                reject({ log: 'erro ao recuperar Url', error: JSON.stringify(e) });
              });
          });

          task.catch(e => {
            loader.dismiss();
            reject({ log: 'erro ao fazer upload', error: JSON.stringify(e) });
          });

        });
    });
  }

  /**
   * Faz o upload da imagem da Provider verification
   * @param {string} base64JpegFile base64 da img para fazer upload
   * @param {info} info - A string com o final.jpeg que sera utilizada como nome da foto no storage 
   * @return {Promise<string>} `downloadURL` in a Promise
   */
  uploadProVeriftImg(base64JpegFile: string, info: string): Promise<string> {
    let loader = this.loadingCtrl.create({
      content: 'Fazendo Upload...',
    });

    return new Promise((resolve, reject) => {
      loader.present()
        .then(() => {
          const tempPicRef = this.proVerifStorageRef.child(info);
          var task = tempPicRef.putString(base64JpegFile, 'data_url');

          task.on('state_changed',
            function progress(snapshot) {
              var percentage = (task.snapshot.bytesTransferred / task.snapshot.totalBytes) * 100;
              loader.setContent("Fazendo Upload... " + Number(percentage).toFixed(2) + " %");
            },
          );

          task.then(() => {
            tempPicRef.getDownloadURL()
              .then(url => {
                loader.dismiss();
                resolve(url);
              }).catch(e => {
                loader.dismiss();
                reject({ log: 'erro ao recuperar Url', error: JSON.stringify(e) });
              });
          });

          task.catch(e => {
            loader.dismiss();
            reject({ log: 'erro ao fazer upload', error: JSON.stringify(e) });
          });

        });
    });
  }

  /**
  * Faz o upload de uma imagem para o banner
  * @param {string} base64JpegFile base64 da img para fazer upload
  * @param {info} info - A string com o final.jpeg que sera utilizada como nome da foto no storage 
  * @return {Promise<string>} `downloadURL` in a Promise
  */
  uploadBannertImg(base64JpegFile: string, info: string): Promise<string> {
    let loader = this.loadingCtrl.create({
      content: 'Fazendo Upload...',
    });

    return new Promise((resolve, reject) => {
      loader.present()
        .then(() => {
          const tempPicRef = this.bannerStorageRef.child(info);
          var task = tempPicRef.putString(base64JpegFile, 'data_url');

          task.on('state_changed',
            function progress(snapshot) {
              var percentage = (task.snapshot.bytesTransferred / task.snapshot.totalBytes) * 100;
              loader.setContent("Fazendo Upload... " + Number(percentage).toFixed(2) + " %");
            },
          );

          task.then(() => {
            tempPicRef.getDownloadURL()
              .then(url => {
                loader.dismiss();
                resolve(url);
              }).catch(e => {
                loader.dismiss();
                reject({ log: 'erro ao recuperar Url', error: JSON.stringify(e) });
              });
          });

          task.catch(e => {
            loader.dismiss();
            reject({ log: 'erro ao fazer upload', error: JSON.stringify(e) });
          });

        });
    });
  }


  /**
  * Faz a deleção de uma imagem para o banner
  * @param {info} info - A string com o final.jpeg que sera utilizada como nome da foto no storage 
  * @return {Promise<string>} `downloadURL` in a Promise
  */
  deleteBannertImg(info: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const tempPicRef = this.bannerStorageRef.child(info);
      tempPicRef.delete()
        .then(() => {
          resolve();
        })
        .catch(e => {
          reject({ log: 'erro ao deletar pic', error: JSON.stringify(e) });
        });
    });
  }

}