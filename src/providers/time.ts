import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


@Injectable()
export class TimeProvider {

  private apiKey = "1f5fe452-fc35-46b5-a574-930de7bfd595";

  constructor(
    private http: Http
  ) {
  }

  /**
   * Consulta a Api do Economizei e retorna um Date com base na referencia de horario 
   * @return Retorna uma `Promise` que é resolvida com um Date com base na referencia de horario 
   * }
   */
  getDate(): Promise<Date> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://us-central1-economizeiapp.cloudfunctions.net/TimeService?key=${this.apiKey}`).toPromise()
        .then(response => {
          resolve(new Date(JSON.parse(response.text()).currentTime));
        })
        .catch(errorResponse => {
          reject(JSON.parse(errorResponse.text()).error);
        });
    });
  }


  /**
   * Consulta a Api do Economizei e retorna o horario referencia em milisegundos 
   * @return Retorna uma `Promise` que é resolvida com o horario referencia em milisegundos 
   * }
   */
  getCurrentTime(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://us-central1-economizeiapp.cloudfunctions.net/TimeService?key=${this.apiKey}`).toPromise()
        .then(response => {
          resolve(JSON.parse(response.text()).currentTime);
        })
        .catch(errorResponse => {
          reject(JSON.parse(errorResponse.text()).error);
        });
    });
  }


  /**
   * Consulta a Api do Economizei e retorna a diferença entre o horario do dispositivo e o horario referencia em milisegundos
   * @return Retorna uma `Promise` 
   * }
   */
  getRefTimeDif(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://us-central1-economizeiapp.cloudfunctions.net/TimeService?key=${this.apiKey}`).toPromise()
        .then(response => {
          resolve(JSON.parse(response.text()).currentTime - new Date().getTime());
        })
        .catch(errorResponse => {
          reject(JSON.parse(errorResponse.text()).error);
        });
    });
  }


}