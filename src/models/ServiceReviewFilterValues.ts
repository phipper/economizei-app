export class ServiceReviewFilterValues {

      minClientOpinion: number;
      maxClientOpinion: number;
      onlyWithText: number;
      onlyModerated: number

      public constructor(init?: Partial<ServiceReviewFilterValues>) {
            
            this.minClientOpinion = 1;
            this.maxClientOpinion = 5;
            this.onlyWithText = 0;
            this.onlyModerated = 0;

            if (init) {
            Object.assign(this, init);
            }
      }
}