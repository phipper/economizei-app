import { Budget } from './Budget';
import { Schedule } from './Schedule';
import { Address } from './Address';

export class Proposal {

      id: string;
      requestId: string;
      requestScheduleType: string;
      requestAddress: Address;

      clientId: string;
      clientName: string;
      clientThumbnailUrl: string;
      serviceTypeId: string;
      serviceTypeTitle: string;
      serviceTypeSymbol: string;

      providerId: string;
      providerName: string;
      providerJob: string;
      providerThumbnailUrl: string;
      providerReputation: number;
      providerPoints: number;

      budgetValue: number;

      acceptedSchedule: boolean;

      budget: Budget;
      schedule: Schedule;
      providerWorkAddress: Address;

      status: number;
      createdOn: Date;

      clientReview: number;

      public constructor(init?: Partial<Proposal>) {

            this.id = '';
            this.requestId = '';
            this.requestScheduleType = '';

            this.clientId = '';
            this.clientName = '';
            this.clientThumbnailUrl = '';
            this.serviceTypeId = '';
            this.serviceTypeTitle = '';
            this.serviceTypeSymbol = '';

            this.providerId = '';
            this.providerName = '';
            this.providerJob = '';
            this.providerThumbnailUrl = '';
            this.providerReputation = 0;
            this.providerPoints = 0;

            this.budgetValue = 0;
            this.acceptedSchedule = null;

            this.budget = null;
            this.schedule = null;
            this.providerWorkAddress = null;

            this.status = 1;
            this.createdOn = new Date();

            this.clientReview = 0;


            if (init) {
                  Object.assign(this, init);
            }
      }
}