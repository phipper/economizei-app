export class Pic {
      
      url: string;
      thumbnailUrl: string;
      info: string;
      index: number;
      createdOn: Date;

      public constructor(init?: Partial<Pic>) {
            
            this.url = "";
            this.thumbnailUrl = "";
            this.info = "";
            this.index = -1;
            this.createdOn = new Date();

            if (init) {
            Object.assign(this, init);
            }
      }
}