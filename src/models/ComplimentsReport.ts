export class ComplimentsReport {


    text: string;
    code: string;
    quantity: number;

    public constructor(init?: Partial<ComplimentsReport>) {

        this.text = '';
        this.code = '';
        this.quantity = 0;

        if (init) {
            Object.assign(this, init);
        }
    }
}