export class AsClientReviewFilterValues {
      punctuality: number;
      minClientOpinion: number;
      maxClientOpinion: number;
      onlyWithText: number;
      onlyModerated: number

      public constructor(init?: Partial<AsClientReviewFilterValues>) {
            
            this.punctuality = 0;
            this.minClientOpinion = 1;
            this.maxClientOpinion = 5;
            this.onlyWithText = 0;
            this.onlyModerated = 0;

            if (init) {
            Object.assign(this, init);
            }
      }
}