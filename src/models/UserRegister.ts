export class UserRegister {

    id: string;
    userId: string;
    userName: string;
    userCpf: string;
    userEmail: string;
    createdOn: Date;

    public constructor(init?: Partial<UserRegister>) {

        this.id = '';
        this.userId = '';
        this.userName = '';
        this.userCpf = '';
        this.userEmail = '';
        this.createdOn = new Date();

        if (init) {
            Object.assign(this, init);
        }
    }
}