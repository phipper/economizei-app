export class LocalDBInviteData {

    hasProviderInvite: boolean;
    inviteId: string;

    public constructor(init?: Partial<LocalDBInviteData>) {

        this.hasProviderInvite = false;
        this.inviteId = '';

        if (init) {
            Object.assign(this, init);
        }
    }
}