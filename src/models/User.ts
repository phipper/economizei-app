import { Pic } from '../models/Pic';

export class User {

    id: string;
    cpf: string;
    firstName: string;
    lastName: string;
    fullName: string;
    status: string;

    homeAdressId: string;

    workAdressId: string;
    workBairro: string;
    workCity: string;
    workState: string;

    job: string;
    jobKeyWords: string;
    cellPhone: string;
    workPhone: string;
    email: string;
    gender: string;
    type: number;
    birthDate: Date;
    registerDate: Date;
    loginMethod: string;
    termsOfUseAcceptedOn: Date;
    termsOfUseAcceptedVersion: string;
    token: string;
    pic: Pic;
    emailVerified: boolean;
    cellPhoneVerified: boolean;
    emailNotifications: boolean;
    generalAlertsNotifications: boolean;
    newMsgNotifications: boolean;
    newOrderNotifications: boolean;
    newBudgetNotifications: boolean;
    newReviewNotifications: boolean;
    newsNotification: boolean;

    bannerCriationTutorial: boolean;
    clientTutorial: boolean;
    providerTutorial: boolean;

    chats: string[];

    savedBanners: string[];

    providerVerificationSacId: string;
    verifiedProvider: boolean;

    clientReputationSum: number;
    clientReputationCount: number;
    clientReputation: number;

    clientPoints: number;
    asClientOnTimeOrders: number;
    asClientLateOrders: number;

    providerReputationSum: number;
    providerReputationCount: number;
    providerReputation: number;

    providerPoints: number;
    performedServices: number;
    asProviderOnTimeOrders: number;
    asProviderLateOrders: number;
    asProviderOnTimeDeliveredOrders: number;
    asProviderLateDeliveredOrders: number;
    metBudgetOrders: number;
    notMetBudgetOrders: number;


    public constructor(init?: Partial<User>) {

        this.id = '';
        this.cpf = '';
        this.firstName = '';
        this.lastName = '';
        this.fullName = '';
        this.status = 'active';
        this.homeAdressId = '';
        this.workAdressId = '';
        this.workBairro = '';
        this.workCity = '';
        this.workState = '';
        this.job = '';
        this.jobKeyWords = ';'
        this.cellPhone = '';
        this.workPhone = '';
        this.email = '';
        this.gender = '';
        this.type = 0;
        this.birthDate = null;
        this.registerDate = new Date();
        this.loginMethod = '';
        this.termsOfUseAcceptedOn = null;
        this.termsOfUseAcceptedVersion = '';
        this.token = '';
        this.pic = ({
            url: 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582',
            thumbnailUrl: 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582',
            info: 'standartPic', createdOn: this.registerDate, index: null
        });
        this.emailVerified = false;
        this.cellPhoneVerified = false;
        this.emailNotifications = true;
        this.generalAlertsNotifications = true;
        this.newMsgNotifications = true;
        this.newOrderNotifications = true;
        this.newBudgetNotifications = true;
        this.newReviewNotifications = true;
        this.newsNotification = true;

        this.bannerCriationTutorial = false;
        this.clientTutorial = false;
        this.providerTutorial = false;

        this.chats = [];
        this.savedBanners = [];

        this.providerVerificationSacId = '';
        this.verifiedProvider = false;

        this.clientReputationSum = 0;
        this.clientReputationCount = 0;
        this.clientReputation = 0;

        this.clientPoints = 0;
        this.asClientOnTimeOrders = 0;
        this.asClientLateOrders = 0;

        this.providerReputationSum = 0;
        this.providerReputationCount = 0;
        this.providerReputation = 0;

        this.providerPoints = 0;
        this.performedServices = 0;
        this.asProviderOnTimeOrders = 0;
        this.asProviderLateOrders = 0;
        this.asProviderOnTimeDeliveredOrders = 0;
        this.asProviderLateDeliveredOrders = 0;
        this.metBudgetOrders = 0;
        this.notMetBudgetOrders = 0;


        if (init) {
            Object.assign(this, init);
        }
    }
}   