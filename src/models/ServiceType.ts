export class ServiceType {

      id: string;
      title: string;
      symbol: string;
      requiredHomeDelivery: boolean;      
      acceptHomeDelivery: boolean;

      public constructor(init?: Partial<ServiceType>) {
            
      this.id = '';
      this.title = '';
      this.symbol = '';
      this.requiredHomeDelivery = true;
      this.acceptHomeDelivery = false;

            if (init) {
            Object.assign(this, init);
            }
      }
}