export class Range {
 
    lower: number;
    upper: number;

    public constructor(init?: Partial<Range>) {
        
        if (init) {
            Object.assign(this, init);
        }
    }
}