export class WorkDay {
 
    begin: number;
    end: number;
    work: boolean;

    public constructor(init?: Partial<WorkDay>) {

        this.begin = 0;
        this.end = 24;
        this.work = true;
        
        if (init) {
            Object.assign(this, init);
        }
    }
}