import { GeoLocation } from "./Geolocation";

export class BannerView {
      
      time: Date;
      userId: string;
      geoLocation: GeoLocation;

      public constructor(init?: Partial<BannerView>) {
            
            this.time = new Date();
            this.userId = "";
            this.geoLocation = new GeoLocation();

            if (init) {
            Object.assign(this, init);
            }
      }
}