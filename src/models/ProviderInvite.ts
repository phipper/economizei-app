export class ProviderInvite {

      id: string;
      providerId: string;
      providerName: string;
      providerThumbnailUrl: string;
      rewardUsed: boolean;
      dynamicLink: string;
      createdOn: Date;

      public constructor(init?: Partial<ProviderInvite>) {

            this.id = '';
            this.providerId = ``;
            this.providerName = ``;
            this.providerThumbnailUrl = ``;
            this.rewardUsed = false;
            this.dynamicLink = ``;
            this.createdOn = new Date();

            if (init) {
                  Object.assign(this, init);
            }
      }
}