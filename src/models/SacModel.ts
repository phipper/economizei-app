export class SacModel {

      type: string;
      priority: string;

      businessRoleId: string;
      categoryId: string;
      subCategoryId: string;
      assignedTeamId: string;

      public constructor(init?: Partial<SacModel>) {

            this.type = '';
            this.priority = "";

            this.businessRoleId = '';
            this.categoryId = '';
            this.subCategoryId = '';
            this.assignedTeamId = '';

            
            if (init) {
                  Object.assign(this, init);
            }
      }
}