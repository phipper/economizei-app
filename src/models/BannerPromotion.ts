export class BannerPromotion {
      bannerId: string;
      time: Date;
      timeInMs: number;

      public constructor(init?: Partial<BannerPromotion>) {
            
            this.bannerId = "";
            this.time = new Date();
            this.timeInMs = this.time.getTime();

            if (init) {
            Object.assign(this, init);
            }
      }
}