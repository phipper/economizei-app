export class Schedule {
    
    id: string;
    clientId: string;
    providerId: string;
    orderId: string;
    requestId: string;
    dueDate: Date;
    homeDelivery: boolean;
    createdOn: Date;
    status: string;
    createdBy: string;

    geoCheckin: boolean;
    checkinCode: string;
    actualCheckinDate: Date;
    delayInMilSec: number;

    onClientCalendar: boolean;
    onProviderCalendar: boolean;

    rejectReason: string;

    public generateGeoCode(): string {
        var s = '';
        var r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

        for (var i=0; i < 5; i++) { 
            s += r.charAt(Math.floor(Math.random()*r.length)); 
        }
        return s;
    }

    public constructor(init?: Partial<Schedule>) {
            
        this.id = "";
        this.providerId = "";
        this.clientId = "";
        this.orderId = "";
        this.requestId = "";
        this.dueDate = null;
        this.homeDelivery = null;
        this.createdOn = new Date();
        this.status = "new";
        this.createdBy = "";

        this.geoCheckin = true;
        this.checkinCode = this.generateGeoCode();
        this.actualCheckinDate = null;
        this.delayInMilSec = null;

        this.onClientCalendar = false;
        this.onProviderCalendar = false;

        this.rejectReason = "";

        if (init) {
        Object.assign(this, init);
        }
    }

}