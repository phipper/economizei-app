import { Pic } from '../models/Pic';

export class ProviderVerfication {
      
      id: string;
      providerId: string;
      providerName: string;
      status: number;
      pics: Pic[];
      createdOn: Date;

      public constructor(init?: Partial<ProviderVerfication>) {
            
            this.id = '';
            this.providerId = '';
            this.providerName = '';
            this.status = 1;
            this.pics = [];
            this.createdOn = new Date();

            if (init) {
            Object.assign(this, init);
            }
      }
}