export class GeoLocation {

      latitude: number;
      longitude: number;
      accuracy: number;
      state: string;
      city: string;
      bairro: string;
      
      public constructor(init?: Partial<GeoLocation>) {
            
            this.latitude = 0;
            this.longitude = 0;
            this.accuracy = 0;
            this.state = "";
            this.city = "";
            this.bairro = "";

            if (init) {
            Object.assign(this, init);
            }
      }
}