import { Address } from '../models/Address';
import { Schedule } from './Schedule';

export class Request {

      id: string;
      clientId: string;
      clientName: string;
      clientJob: string;
      clientThumbnailUrl: string;
      serviceTypeId: string;
      serviceTypeTitle: string;
      serviceTypeSymbol: string;
      description: string;
      requiredHomeDelivery: boolean;
      acceptHomeDelivery: boolean;
      thumbnail: string;
      status: number;
      createdOn: Date;

      dynamicLink: string;

      views: number;

      addressId: string;
      address: Address;

      scheduleType: string;
      schedule: Schedule;

      canceledReason: string;


      public constructor(init?: Partial<Request>) {
            
            this.id = '';
            this.clientId = '';
            this.clientName = '';
            this.clientJob = '';
            this.clientThumbnailUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582';
            this.serviceTypeId = '';
            this.serviceTypeTitle = '';
            this.serviceTypeSymbol = '';
            this.description = '';
            this.requiredHomeDelivery = false;
            this.acceptHomeDelivery = false;
            this.thumbnail = '';
            this.status = 1;
            this.createdOn = new Date();

            this.dynamicLink = '';

            this.views = 0;

            this.addressId = '';

            this.scheduleType = '';
            this.schedule = null;

            this.canceledReason = '';

            if (init) {
            Object.assign(this, init);
            }
      }
}