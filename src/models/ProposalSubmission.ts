export class ProposalSubmission {

      proposalId: string;
      requestId: string;
      time: Date;
      timeInMs: number;

      public constructor(init?: Partial<ProposalSubmission>) {
            
            this.proposalId = "";
            this.requestId = "";
            this.time = new Date();
            this.timeInMs = this.time.getTime();

            if (init) {
            Object.assign(this, init);
            }
      }
}