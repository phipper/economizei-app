export class SacSolverTeam {

    id: string;
    name: string;
    description: string;
    usersIds: string[];


    public constructor(init?: Partial<SacSolverTeam>) {
          
        this.id = '';
        this.name = '';
        this.description = '';
        this.usersIds = [];
        
          if (init) {
          Object.assign(this, init);
          }
    }
}