export class ProviderInviteUtilization {

      id: string;
      providerId: string;
      providerName: string;
      providerThumbnailUrl: string;

      createdOn: Date;

      public constructor(init?: Partial<ProviderInviteUtilization>) {

            this.id = '';
            this.providerId = ``;
            this.providerName = ``;
            this.providerThumbnailUrl = ``;
            this.createdOn = new Date();

            if (init) {
                  Object.assign(this, init);
            }
      }
}