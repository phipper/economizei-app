import { Address } from '../models/Address';
import { Banner } from '../models/Banner';
import { Request } from '../models/Request';
import { Schedule } from './Schedule';
import { Budget } from './Budget';

export class Order {

      id: string;
      title: string;
      clientId: string;
      clientName: string;
      clientThumbnailUrl: string;
      providerId: string;
      providerName: string;
      providerThumbnailUrl: string;
      banner: Banner;
      request: Request;
      step: number;
      status: number;
      createdOn: Date;

      chatId: string;

      serviceTypeId: string;
      serviceTypeTitle: string;
      serviceTypeSymbol: string;

      requiredHomeDelivery: boolean;
      acceptHomeDelivery: boolean;

      addressId: string;
      address: Address;

      rejectReason: string;

      providerReviewId: string;
      clientReviewId: string;

      acceptedSchedule: Schedule;
      acceptedBudget: Budget;

      public constructor(init?: Partial<Order>) {

            this.id = '';
            this.title = '';
            this.clientId = '';
            this.clientName = '';
            this.clientThumbnailUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582';
            this.providerId = '';
            this.providerName = '';
            this.providerThumbnailUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582';
            this.banner = null;
            this.request = null;
            this.step = 1;
            this.status = 1;
            this.createdOn = new Date();
            this.addressId = '';
            this.address = null;
            this.rejectReason = '';
            this.chatId = '';

            this.serviceTypeId = '';
            this.serviceTypeTitle = '';
            this.serviceTypeSymbol = '';

            this.requiredHomeDelivery = true;
            this.acceptHomeDelivery = false;

            this.providerReviewId = '';
            this.clientReviewId = '';

            this.acceptedSchedule = null;
            this.acceptedBudget = null;


            if (init) {
                  Object.assign(this, init);
            }
      }
}