export class SacSubCategory {

    id: string;
    name: string;
    designedGroupId: string;
    priority: string;

    public constructor(init?: Partial<SacSubCategory>) {
          
        this.id = '';
        this.name = '';
        this.designedGroupId = '';
        this.priority = '';

          if (init) {
          Object.assign(this, init);
          }
    }
}