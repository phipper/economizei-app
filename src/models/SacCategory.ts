export class SacCategory {

    id: string;
    name: string;
    designedGroupId: string;
    priority: string;

    public constructor(init?: Partial<SacCategory>) {
          
        this.id = '';
        this.name = '';
        this.designedGroupId = '';
        this.priority = '';

          if (init) {
          Object.assign(this, init);
          }
    }
}