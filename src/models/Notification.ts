export class Notification {
    id: string;
    title: string;
    text: string;
    type: string;
    action: any;
    readed: boolean;
    senderName: string;
    createdOn: Date;

    public constructor(init?: Partial<Notification>) {

        this.id = "";
        this.title = "";
        this.text = "";
        this.type = "";
        this.readed = false;
        this.senderName = "";
        this.createdOn = new Date();

        if (init) {
            Object.assign(this, init);
        }
    }
}