export class ActivityChange {

    fieldName: string;
    oldValue: string;
    newValue: string;

    public constructor(init?: Partial<ActivityChange>) {
          
        this.fieldName = '';
        this.oldValue = '';
        this.newValue = '';

          if (init) {
          Object.assign(this, init);
          }
    }
}