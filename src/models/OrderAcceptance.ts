export class OrderAcceptance {
      orderId: string;
      time: Date;
      timeInMs: number;

      public constructor(init?: Partial<OrderAcceptance>) {
            
            this.orderId = "";
            this.time = new Date();
            this.timeInMs = this.time.getTime();

            if (init) {
            Object.assign(this, init);
            }
      }
}