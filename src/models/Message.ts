export class Message {
    
    id: string;
    chatId: string;
    senderId: string;
    senderFullName: string;
    recipientId: string;
    onFirebase: boolean;
    message: string;
    originalMessage: string;
    dateString: string;
    timeString: string;
    time: Date;
    type: string;
    verified: boolean;
    moderated: boolean;
    delivered: Date;
    readedOn: Date;

    public constructor(init?: Partial<Message>) {
            
        this.id = "";
        this.chatId = "";
        this.senderId = "";
        this.senderFullName = "";
        this.recipientId = "";
        this.onFirebase = false;
        this.message = "";
        this.originalMessage = "";
        this.dateString = "";
        this.timeString = "";
        this.time = new Date;
        this.type = "";
        this.verified = false;
        this.moderated = false;
        this.delivered = null;
        this.readedOn = null;

        if (init) {
        Object.assign(this, init);
        }
  }
}