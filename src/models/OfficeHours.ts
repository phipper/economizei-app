import { WorkDay } from "./WorkDay";

export class OfficeHours {
 
    id: string;
    providerId: string;
    updatedOn: Date;
    createdOn: Date;

    dom: WorkDay;
    seg: WorkDay;
    ter: WorkDay;
    qua: WorkDay;
    qui: WorkDay;
    sex: WorkDay;
    sab: WorkDay;

    public constructor(init?: Partial<OfficeHours>) {

        this.id = '';
        this.providerId = '';
        this.updatedOn = new Date();
        this.createdOn = new Date();

        this.dom = Object.assign({}, new WorkDay());
        this.seg = Object.assign({}, new WorkDay());
        this.ter = Object.assign({}, new WorkDay());
        this.qua = Object.assign({}, new WorkDay());
        this.qui = Object.assign({}, new WorkDay());
        this.sex = Object.assign({}, new WorkDay());
        this.sab = Object.assign({}, new WorkDay());
        
        if (init) {
            Object.assign(this, init);
        }
    }
}