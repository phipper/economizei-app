import { ChatUserData } from "./ChatUserData";

export class Chat {

    id: string;
    orderId: string;
    title: string;
    status: string;
    usersData: ChatUserData[];
    createdOn: Date;

    public constructor(init?: Partial<Chat>) {
            
        this.id = "";
        this.orderId = "";
        this.title = "";
        this.status = "active";
        this.usersData = [];
        this.createdOn = new Date();

        if (init) {
        Object.assign(this, init);
        }
  }
}