export class AsProviderReviewFilterValues {
      punctuality: number;
      serviceDelivery: number;
      budgetAccuracy: number;
      minClientOpinion: number;
      maxClientOpinion: number;
      onlyWithText: number;
      onlyModerated: number

      public constructor(init?: Partial<AsProviderReviewFilterValues>) {
            
            this.punctuality = 0;
            this.serviceDelivery = 0;
            this.budgetAccuracy = 0;
            this.minClientOpinion = 1;
            this.maxClientOpinion = 5;
            this.onlyWithText = 0;
            this.onlyModerated = 0;

            if (init) {
            Object.assign(this, init);
            }
      }
}