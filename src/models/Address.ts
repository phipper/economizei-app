export class Address {
    
    id: string;
    cep: string;
    street: string;
    complemento: string;
    bairro: string;
    numero: string;
    city: string;
    state: string;
    userId: string;
    ibge: string;
    gia: string;
    latitude: number;
    longitude: number;
    createdOn: Date;

    public constructor(init?: Partial<Address>) {
            
        this.id = "";
        this.cep = "";
        this.street = "";
        this.complemento = "";
        this.bairro = "";
        this.numero = "";
        this.city = "";
        this.state = "";
        this.userId = "";
        this.ibge = "";
        this.gia = "";
        this.latitude = 0;
        this.longitude = 0;
        this.createdOn = new Date();

        if (init) {
        Object.assign(this, init);
        }
  }
}
