export class AppConfig {

    isTutorialDone: boolean;
    tutorialVersion: number;

    isConfigured: boolean;
    hapticFeedback: boolean;

    statusBarMode: string;

    city: string;
    state: string;

    public constructor(init?: Partial<AppConfig>) {

        this.isTutorialDone = false;
        this.tutorialVersion = 0;

        this.isConfigured = false;
        this.hapticFeedback = true;

        this.statusBarMode = 'default';

        this.city = '';
        this.state = '';

        if (init) {
            Object.assign(this, init);
        }
    }
}