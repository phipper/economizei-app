export class Category {

    id: string;
    title: string;
    description: string;
    cssColor: string;
    textColor: string;
    index: number;
    serviceTypesIds: string[];

    public constructor(init?: Partial<Category>) {

        this.id = '';
        this.title = '';
        this.description = '';
        this.cssColor = '';
        this.textColor = '';
        this.index = null;
        this.serviceTypesIds = [];

        if (init) {
            Object.assign(this, init);
        }
    }
}