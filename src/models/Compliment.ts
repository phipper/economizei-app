export class Compliment {


    text: string;
    code: string;

    public constructor(init?: Partial<Compliment>) {

        this.text = '';
        this.code = '';

        if (init) {
            Object.assign(this, init);
        }
    }
}