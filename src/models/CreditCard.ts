export class CreditCard {

      id: string;
      number: string;
      brand: string;
      expirationMonth: string;
      expirationYear: string;

      ownerName: string;
      ownerCPF: string;
      ownerBirthDate: string;

      createdOn: Date;

      public constructor(init?: Partial<CreditCard>) {

            this.id = '';
            this.number = '';
            this.brand = '';
            this.expirationMonth = '';
            this.expirationYear = '';

            this.ownerName = '';
            this.ownerCPF = '';
            this.ownerBirthDate = '';
            this.createdOn = new Date();

            if (init) {
                  Object.assign(this, init);
            }
      }
}