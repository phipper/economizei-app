import { SacModel } from "./SacModel";

export class Core {

    version: number;

    homepageBannersV: number;
    categoriesV: number;
    serviceTypeV: number;

    newBannerSacModel: SacModel;
    editBannerSacModel: SacModel;
    providerVerficationSacModel: SacModel;

    public constructor(init?: Partial<Core>) {

        this.version = 0;
        
        this.homepageBannersV = 0;
        this.categoriesV = 0;
        this.serviceTypeV = 0;

        this.newBannerSacModel = new SacModel();
        this.editBannerSacModel = new SacModel();
        this.providerVerficationSacModel = new SacModel();
        
        if (init) {
            Object.assign(this, init);
        }
    }
}   