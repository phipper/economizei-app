export class HomePageBanner {
      id: string;
      title: string;
      description: string;
      position: number;
      action: string;
      actionData: any;
      picUrl: string;
      status: string;
      createdOn: Date;


      public constructor(init?: Partial<HomePageBanner>) {
            
            this.id = "";
            this.title = "";
            this.description = "";
            this.position = null;
            this.action = "";
            this.actionData = null;
            this.picUrl = "";
            this.status = "new";
            this.createdOn = new Date();
            
            if (init) {
            Object.assign(this, init);
            }
      }

      isActive(): boolean{
            return this.status == 'active' ? true : false;
      }
}