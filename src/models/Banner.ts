import { Address } from '../models/Address';

export class Banner {

      id: string;
      dynamicLink: string;
      providerId: string;
      providerThumbnailUrl: string;
      serviceTypeId: string;
      serviceTypeTitle: string;
      serviceTypeSymbol: string;
      title: string;
      description: string;
      keyWords: string;
      price: number;
      priceType: string;
      payMethods: string[];
      thumbnail: string;
      status: number;
      createdOn: Date;

      bannerReputationSum: number;
      bannerReputationCount: number;
      bannerReputation: number;
      points: number;
      performedServices: number;
      promoted: boolean;
      promotionValidTo: Date;

      addressId: string;
      address: Address;

      requiredHomeDelivery: boolean;
      acceptHomeDelivery: boolean;

      warranty: boolean;
      warrantyTime: string;


      public constructor(init?: Partial<Banner>) {

            this.id = '';
            this.dynamicLink = '';
            this.providerId = '';
            this.providerThumbnailUrl = 'https://firebasestorage.googleapis.com/v0/b/economizeiapp.appspot.com/o/Util%2Fstandart-profile-pic.png?alt=media&token=6d2de797-935f-4e6c-b601-e75d897d8582';
            this.serviceTypeId = '';
            this.serviceTypeTitle = '';
            this.serviceTypeSymbol = '';
            this.title = '';
            this.description = '';
            this.keyWords = '';
            this.requiredHomeDelivery = true;
            this.acceptHomeDelivery = false;
            this.price = null;
            this.priceType = '';
            this.payMethods = [];
            this.thumbnail = '';
            this.status = -2;
            this.createdOn = new Date();

            this.bannerReputationSum = 0;
            this.bannerReputationCount = 0;
            this.bannerReputation = 0;
            this.performedServices = 0;
            this.promoted = false;
            this.promotionValidTo = null;
            this.points = 0;
            this.addressId = '';

            this.warranty = false;
            this.warrantyTime = '';

            if (init) {
                  Object.assign(this, init);
            }
      }
}