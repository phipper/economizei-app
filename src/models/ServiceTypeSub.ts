import { Address } from './Address';

export class ServiceTypeSub {

      id: string;
      serviceTypeId: string;
      serviceTypeTitle: string;
      serviceTypeSymbol: string;

      providerId: string;
      notif: boolean;      
      notifRad: number;

      providerWorkAddress: Address;

      createdOn: Date;

      public constructor(init?: Partial<ServiceTypeSub>) {
            
      this.id = '';
      this.serviceTypeId = '';
      this.serviceTypeTitle = '';
      this.serviceTypeSymbol == '';

      this.providerId = '';
      this.notif = true;
      this.notifRad = 10;

      this.providerWorkAddress = null;

      this.createdOn = new Date();

            if (init) {
            Object.assign(this, init);
            }
      }
}