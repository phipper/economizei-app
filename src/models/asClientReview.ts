export class AsClientReview {

    id: string;
    title: string;
    punctuality: number;
    providerOpinion: number;
    points: number;
    text: string;
    complimentCode: string;
    originalText: string;
    processed: boolean;
    verified: boolean;
    moderated: boolean;
    clientId: string;
    providerId: string;
    providerName: string;
    orderId: string;
    createdOn: Date;


    public constructor(init?: Partial<AsClientReview>) {

        this.id = '';
        this.title = '';
        this.punctuality = 0;
        this.providerOpinion = 0;
        this.points = 0;
        this.text = '';
        this.complimentCode = '';
        this.originalText = '';
        this.processed = false;
        this.verified = false;
        this.moderated = false;
        this.clientId = '';
        this.providerId = '';
        this.providerName = '';
        this.orderId = '';
        this.createdOn = new Date();

        if (init) {
            Object.assign(this, init);
        }
    }
}