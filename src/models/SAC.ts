import { Pic } from '../models/Pic';
import { SacBusinessRole } from './SacBusinessRole';
import { SacCategory } from './SacCategory';
import { SacSubCategory } from './SacSubCategory';
import { SacSolverTeam } from './SacSolverTeam';

export class SAC {

      id: string;
      title: string;
      description: string;
      type: string;
      priority: string;
      createdOn: any;
      dueDate: any;

      chatId: string;

      userId: string;
      userName: string;
      status: any;
      providerVerificationId: string;  
      requestId: string;
      orderId: string;
      bannerId: string;
      parentSacId: string;
      
      pics: Pic[];
      
      assignedUserId: string;
      assignedUserName: string;

      businessRole: SacBusinessRole;
      category: SacCategory;
      subCategory: SacSubCategory;
      assignedTeam: SacSolverTeam;

      public constructor(init?: Partial<SAC>) {

            this.id = "";
            this.title = "";
            this.description = "";
            this.type = "";

            this.priority = "";
            this.createdOn = new Date;
            this.dueDate = null;

            this.chatId = "";

            this.userId = "";
            this.userName = '';
            this.status = 1;
            this.providerVerificationId = "";      
            this.requestId = "";
            this.orderId = "";
            this.bannerId = "";
            this.parentSacId = "";
            
            this.pics =  null;
            
            this.assignedUserId = "";
            this.assignedUserName = "";

            this.businessRole = new SacBusinessRole();
            this.category = new SacCategory();
            this.subCategory = new SacSubCategory();
            this.assignedTeam = new SacSolverTeam();

            if (init) {
            Object.assign(this, init);
            }
      }
}