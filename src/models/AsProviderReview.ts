export class AsProviderReview {

    id: string;
    title: string;

    punctuality: number;
    serviceDelivery: number;
    budgetAccuracy: number;
    clientOpinion: number;
    points: number;
    text: string;
    complimentCode: string;
    originalText: string;

    processed: boolean;
    verified: boolean;
    moderated: boolean;
    providerId: string;

    clientId: string;
    clientName: string;

    orderId: string;
    bannerId: string;
    createdOn: Date;


    public constructor(init?: Partial<AsProviderReview>) {

        this.id = '';
        this.title = '';
        this.punctuality = 0;
        this.serviceDelivery = 0;
        this.budgetAccuracy = 0;
        this.clientOpinion = 0;
        this.points = 0;
        this.text = '';
        this.complimentCode = '';
        this.originalText = '';
        this.processed = false;
        this.verified = false;
        this.moderated = false;
        this.providerId = '';
        this.clientId = '';
        this.clientName = '';
        this.orderId = '';
        this.bannerId = '';
        this.createdOn = new Date();

        if (init) {
            Object.assign(this, init);
        }
    }
}