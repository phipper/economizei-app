export class ChatUserData {

    
    userId: string;
    userName: string;
    userThumbnailUrl: string;

    public constructor(init?: Partial<ChatUserData>) {
            
        this.userId = "";
        this.userName = "";
        this.userThumbnailUrl = "";

        if (init) {
        Object.assign(this, init);
        }
  }
}