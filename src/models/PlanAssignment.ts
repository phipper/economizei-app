import { Plan } from '../models/Plan';

export class PlanAssignment {

      id: string;
      providerId: string;
      signatureId: string;
      plan: Plan;
      createdOn: Date;

      public constructor(init?: Partial<PlanAssignment>) {

            this.id = '';
            this.providerId = ``;
            this.signatureId = '';
            this.plan = new Plan;
            this.createdOn = new Date();

            if (init) {
                  Object.assign(this, init);
            }
      }
}