export class OrderCompletion {
      orderId: string;
      clientId: string;
      providerId: string;
      bannerId: string;
      time: Date;
      timeInMs: number;

      public constructor(init?: Partial<OrderCompletion>) {
            
            this.orderId = "";
            this.clientId = "";
            this.providerId = "";
            this.bannerId = "";
            this.time = new Date();
            this.timeInMs = this.time.getTime();

            if (init) {
            Object.assign(this, init);
            }
      }
}