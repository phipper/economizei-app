import { SacBusinessRole } from "./SacBusinessRole";
import { SacCategory } from "./SacCategory";
import { SacSubCategory } from "./SacSubCategory";
import { SacSolverTeam } from "./SacSolverTeam";

export class SacModelData {

      type: string;
      priority: string;
      businessRole: SacBusinessRole;
      category: SacCategory;
      subCategory: SacSubCategory;
      assignedTeam: SacSolverTeam;

      public constructor(init?: Partial<SacModelData>) {

            this.type = '';
            this.priority = "";

            this.businessRole = new SacBusinessRole();
            this.category = new SacCategory();
            this.subCategory = new SacSubCategory();
            this.assignedTeam = new SacSolverTeam();

            
            if (init) {
                  Object.assign(this, init);
            }
      }
}