export class Characteristic {

      id: string;
      orderNumber: number;
      IsDependent: boolean;
      dependencyCON: number;
      dependencyD: string;
      bcTitle: string;
      title: string;
      rcDataSelection: string;
      data: string[];

      public constructor(init?: Partial<Characteristic>) {

            this.id = '';
            this.orderNumber = null;
            this.IsDependent = null;
            this.dependencyCON = null;
            this.dependencyD = null;
            this.bcTitle = null;
            this.title = null;
            this.rcDataSelection = null;
            this.data = [];

            if (init) {
                  Object.assign(this, init);
            }
      }
}