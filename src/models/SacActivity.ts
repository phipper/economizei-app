import { ActivityChange } from "./ActivityChange";

export class SacActivity {

    id: string;
    description: string;
    userId: string;
    userThumbUrl: string;
    userName: string;
    createdOn: any;
    type: string;
    changes: ActivityChange[];

    public constructor(init?: Partial<SacActivity>) {
          
        this.id = '';
        this.description = '';
        this.userId = '';
        this.userThumbUrl = '';
        this.userName = '';
        this.createdOn = new Date();
        this.type = 'Comentário';
        this.changes = [];

          if (init) {
          Object.assign(this, init);
          }
    }
}