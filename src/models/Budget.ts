export class Budget {
      id: string;
      clientId: string;
      providerId: string;
      orderId: string;
      requestId: string;

      value: number;
      payMethods: string[];
      warranty: boolean;
      warrantyTime: string;
      description: string;
      status: string;
      createdOn: Date;
      
      public constructor(init?: Partial<Budget>) {
            
            this.id = "";
            this.clientId = "";
            this.providerId = "";
            this.orderId = "";
            this.requestId = "";

            this.value = null;
            this.payMethods = [];
            this.warranty = false;
            this.warrantyTime = "";
            this.description = "";
            this.status = "client_analysis";
            this.createdOn = new Date();

            if (init) {
            Object.assign(this, init);
            }
      }
}