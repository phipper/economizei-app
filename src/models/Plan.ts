export class Plan {

      id: string;
      title: string;
      description: string;

      pagSegCode: string;

      price: number;
      renovationPeriod: number;
      validTo: Date;

      freePromotions: number;
      maxAcceptedOrdersPerDay: number;
      maxSentProposalsPerDay: number;
      maxPicsPerBanner: number;
      maxNumOfBanners: number;
      maxServiceTypesSub: number;
      botEcoAmigo: boolean;
      analytics: boolean;

      public constructor(init?: Partial<Plan>) {

            this.id = "";
            this.title = "";
            this.description = "";
            this.pagSegCode = '';

            this.price = 0;
            this.renovationPeriod = 0;
            this.freePromotions = 0;
            this.maxAcceptedOrdersPerDay = 0;
            this.maxSentProposalsPerDay = 0;
            this.maxPicsPerBanner = 0;
            this.maxNumOfBanners = 0;
            this.maxServiceTypesSub = 0;
            this.botEcoAmigo = false;
            this.analytics = false;
            this.validTo = null;

            if (init) {
                  Object.assign(this, init);
            }
      }
}