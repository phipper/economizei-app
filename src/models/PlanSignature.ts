import { Plan } from "./Plan";
import { CreditCard } from "./CreditCard";

export class PlanSignature {

      id: string;
      reference: string;
      userId: string;
      status: string;
      createdOn: number;

      ceditCard: CreditCard;
      plan: Plan;

      public constructor(init?: Partial<PlanSignature>) {

            this.id = '';
            this.reference = '';
            this.userId = '';
            this.status = 'PENDING_PROCESSING';
            this.createdOn = new Date().getTime();
            
            this.ceditCard = new CreditCard();
            this.plan = new Plan();

            if (init) {
                  Object.assign(this, init);
            }
      }
}