export class BannerAnalytics {
      id: string;
      createdOn: Date;
      timeOfFirstView: Date;
      timeToLastView: Date;
      last24hViews: number;

      public constructor(init?: Partial<BannerAnalytics>) {
            
            this.id = "";
            this.createdOn = new Date();
            this.timeOfFirstView = null;
            this.timeToLastView = null;
            this.last24hViews = 0;

            if (init) {
            Object.assign(this, init);
            }
      }
}