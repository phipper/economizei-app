# economizei

O jeito mais prático de contratar serviços!

## Getting Started

Para instalar o economizei na sua maquina atentise a as alterações realizadas por nossa equipe nos plugins e na plataforma android para possibilitar a compatibilidade dos plugins com a plataforma android >= 7.0.


### Installing

* Pegar chaves de criptografia SHA-1 e SHA-256 para a autenticação do Goggle plus

    Simply enter these into Windows command prompt.

    cd C:\Program Files\Java\jdk1.7.0_09\bin
    
    keytool -exportcert -alias androiddebugkey -keystore "C:\Users\userName\.android\debug.keystore" -list -v

    The base password is android

    You will be presented with the MD5, SHA1, and SHA256 keys; Choose the one you need.

        Last run on yure`s laptop:

            C:\Users\yureo>keytool -exportcert -alias androiddebugkey -keystore "C:\Users\yureo\.android\debug.keystore" -list -v
            Informe a senha da área de armazenamento de chaves:
            Nome do alias: androiddebugkey
            Data de criação: 27/03/2019
            Tipo de entrada: PrivateKeyEntry
            Comprimento da cadeia de certificados: 1
            Certificado[1]:
            Proprietário: C=US, O=Android, CN=Android Debug
            Emissor: C=US, O=Android, CN=Android Debug
            Número de série: 1
            Válido de Wed Mar 27 01:37:55 GFT 2019 até Fri Mar 19 01:37:55 GFT 2049
            Fingerprints do certificado:
                    MD5:  A6:1B:40:C8:8E:AB:D6:2A:39:C2:1C:B0:73:75:9C:D3
                    SHA1: 3A:4E:B0:F4:0E:A7:B7:24:A5:0E:B9:9A:F9:4E:FE:D2:DE:B8:B0:59
                    SHA256: 54:EC:BE:64:FB:83:26:2F:8E:DE:2A:CD:1A:6B:1F:20:41:4E:05:3E:66:63:50:B9:F4:DE:5D:25:6A:D7:43:66
            Nome do algoritmo de assinatura: SHA1withRSA
            Algoritmo de Chave Pública do Assunto: Chave RSA de 2048 bits
            Versão: 1

            Warning:
            O armazenamento de chaves JKS usa um formato proprietário. É recomendada a migração para PKCS12, que é um formato de padrão industrial que usa "keytool -importkeystore -srckeystore C:\Users\yureo\.android\debug.keystore -destkeystore C:\Users\yureo\.android\debug.keystore -deststoretype pkcs12".


    Fonte [how-can-i-find-and-run-the-keytool](https://stackoverflow.com/questions/5488339/how-can-i-find-and-run-the-keytool) 

    
* FATAL ERROR: CALL_AND_RETRY_LAST Allocation failed - JavaScript heap out of memory:

    Erro 0:

        npm install -g increase-memory-limit

        increase-memory-limit

    Fonte [increase-memory-limit](https://github.com/endel/increase-memory-limit)

* Ao instalar o plugin universal links alterar o seguinte arquivo no repositorio do plugin:

    Erro 1:

    file: plugins/cordova-universal-links-plugin/hooks/lib/android/manifestWriter.js

        //Alterações para compatibilidade com a plataforma android >=7.0
        //var pathToManifest = path.join(cordovaContext.opts.projectRoot, 'platforms', 'android', 'AndroidManifest.xml');
        var pathToManifest = path.join(
            cordovaContext.opts.projectRoot, 
            'platforms', 
            'android', 
            'app', 
            'src', 
            'main', 
            'AndroidManifest.xml');

        Fonte [cordova-universal-links-plugin/pull](https://github.com/nordnet/cordova-universal-links-plugin/pull/135),
            [cordova-universal-links-plugin/issues](https://github.com/nordnet/cordova-universal-links-plugin/issues/146)


  Erro 2:

  file: plugins/cordova-universal-links-plugin/hooks/lib/ios/xcodePreferences.js

    function loadProjectFile() {
    var platform_ios;
    var projectFile;

    try {
        // try pre-5.0 cordova structure
        platform_ios = context.requireCordovaModule('cordova-lib/src/plugman/platforms')['ios'];
        projectFile = platform_ios.parseProjectFile(iosPlatformPath());
    } catch (e) {
        try {
            // let's try cordova 5.0 structure
            platform_ios = context.requireCordovaModule('cordova-lib/src/plugman/platforms/ios');
            projectFile = platform_ios.parseProjectFile(iosPlatformPath());
        } catch (e) {
            // Then cordova 7.0
            var project_files = context.requireCordovaModule('glob').sync(path.join(iosPlatformPath(), '*.xcodeproj', 'project.pbxproj'));
            
            if (project_files.length === 0) {
                throw new Error('does not appear to be an xcode project (no xcode project file)');
            }
            
            var pbxPath = project_files[0];
            
            var xcodeproj = context.requireCordovaModule('xcode').project(pbxPath);
            xcodeproj.parseSync();
            
            projectFile = {
                'xcode': xcodeproj,
                write: function () {
                    var fs = context.requireCordovaModule('fs');
                    
                var frameworks_file = path.join(iosPlatformPath(), 'frameworks.json');
                var frameworks = {};
                try {
                    frameworks = context.requireCordovaModule(frameworks_file);
                } catch (e) { }
                
                fs.writeFileSync(pbxPath, xcodeproj.writeSync());
                    if (Object.keys(frameworks).length === 0){
                        // If there is no framework references remain in the project, just remove this file
                        context.requireCordovaModule('shelljs').rm('-rf', frameworks_file);
                        return;
                    }
                    fs.writeFileSync(frameworks_file, JSON.stringify(this.frameworks, null, 4));
                }
            };
        }
    }

    return projectFile;
    } 

    Fonte [cordova-universal-links-plugin/issues](https://github.com/nordnet/cordova-universal-links-plugin/issues/110)


* Ao instalar o cordova plugin fcm alterar o seguinte arquivo no repositorio do plugin:

  file: plugins/cordova-plugin-fcm/scripts/fcm_config_files_process.js

    // change
    var strings = fs.readFileSync("platforms/android/res/values/strings.xml").toString();
    // to
    var strings = fs.readFileSync("platforms/android/app/src/main/res/values/strings.xml").toString();

    // AND

    //change
    fs.writeFileSync("platforms/android/res/values/strings.xml", strings);

    //to
    fs.writeFileSync("platforms/android/app/src/main/res/values/strings.xml", strings);

    Fonte [cordova-plugin-fcm/issues/481](https://github.com/fechanique/cordova-plugin-fcm/issues/481)

    file: platforms/android/cordova-plugin-fcm/economizei-FCMPlugin.gradle
    
      From:
      apply plugin: com.google.gms.googleservices.GoogleServicesPlugin
      
      To:
      ext.postBuildExtras = {
          apply plugin: com.google.gms.googleservices.GoogleServicesPlugin
      }

    Fonte [cordova-plugin-googleplus/issues/349](https://github.com/EddyVerbruggen/cordova-plugin-googleplus/issues/349#issuecomment-285248483)


* Ao alterar um arquivo em um diretorio ignorado:

  git add -f filepath

## Testando

ionic cordova run android --aot

ionic cordova run android --aot --minifyjs --minifycss --optimizejs

### Deploy at Google play

Siga os passos em /googlePlay_build/steps.txt


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

Nós utilizamos o [gitkraken](https://www.gitkraken.com/) para versionamento.

## Authors

* **Leonardo Cuenca Mendes** - *Initial work and developer* - [linkedin](https://www.linkedin.com/in/leonardo-cuenca-ba8306aa/)

* **Yure Rodrigues de Sousa** - *developer* - [linkedin](https://www.linkedin.com/in/yureorge/)

* **Mateus Lopes** - *design* - [linkedin](https://www.linkedin.com/in/mateus-lopes-44b482aa/)


## License

Este projeto é protegido pela lei de direitos autorais e é propriedade intelectual da [Phipper LLC](https://www.phipper.com.br/)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

